(* $Id$ *)

(* IMPROVEMENTS:
     - input file has two json strings, separated by TAB. The first 
       is for attributes, the second for features. This would allow it
       to process the attributes only, and e.g. to have one pass for every
       attribute when memory is tight on an indexer node
 *)

(* New first pass:

   - Sort data by 2nd column (stable sort). The 2nd column contains the
     user_docid (except for A lines).


   New second pass:

   - We read this file then in. We keep track of the record number
     (not line number). The record number is only increased when a new
     user_docid is seen.

   - Read in blocks of records. It only counts the last U or D line,
     optionally updated by a B line.

   - Build these attribute-like data structures
     @docboost_of_docid: p -> docboost
     @record_of_docid:   p -> record number

     Initially, p is just the record number. But the two
     arrays with p as index will be sorted.


   - Sort @docboost_of_docid and @line_of_docid in parallel, in the 
     order of decreasing docboosts. We call p now the (internal) docid.

   - Drop @docboost_of_docid

   - Invert @record_of_docid and get
     @docid_of_record:    record number -> p

     This is just a linear pass over the array.

   - Drop @record_of_docid

   - While reading the input, also collect information about attributes
     (A lines)

  RESULT:

     @docid_of_record:    record number -> docid

     Also, we know the maximum docid.
  
     We know the attributes and their types.

 *)

(* First pass:

     - Read only the user_docid and the docboost from the input file.
       Also keep track of the line number.

     - Build these attribute-like data structures
       @docboost_of_docid: p -> docboost
       @line_of_docid:     p -> linenumber

       Initially, p is just the linenumber. But the two
       arrays with p as index will be sorted.

       We only process update input lines here. Deletes are ignored
       (so deleted docs will get a docid). If there are two updates
       for the same document, we assign two docid's (one will be picked
       later).

     - Sort @docboost_of_docid and @line_of_docid in parallel, in the 
       order of decreasing docboosts. We call p now the (internal) docid.

     - Drop @docboost_of_docid

     - Invert @line_of_docid and get
       @docid_of_line:    linenumber -> p

       This is just a linear pass over the array.

     - Drop @line_of_docid

    RESULT:

       @docid_of_line:    linenumber -> docid

       Also, we know the maximum docid.

    IMPROVE: Avoid that A and D lines are counted for docid
 *)

(* Second pass:

     - Read only the user_docid and the docboost from the input file.
       Also keep track of the line number.

     - Build these attribute-like data structures
       @udocid_of_q:       q -> user_docid
       @line_of_q:         q -> linenumber

       q is just the linenumber initially, but again these arrays
       will be sorted.

     - Sort @udocid_of_q and @line_of_q in parallel, in the order of
       user_docid's.

     - Make a pass over both arrays. If there are several lines with the
       same user_docid's, only keep the entry in @line_of_q with the
       highest line number. The other entries are set to 0.

     - Drop @udocid_of_q

     - Create @valid_line as boolean attribute vector. Make a pass
       over @line_of_q, and set every entry in @valid_line to '1'
       that has a non-zero line number
       
     - Drop @line_of_q

    RESULT:

       @valid_line:     linenumber -> is_valid

 *)

(* Third pass:

     - Read all user_docid, docboost, and the JSON-encoded data from
       the input file. Also keep track of the line number.

     - For all "A" lines, the attribute arrays are created.

     - Create the automatic attributes (with the right array size):
       @udocid:      docid -> user_docid
       @boost:       docid -> docboost

     - In the index directory, create for every feature an empty
       file "<name>.idx"
     
     - Processing of a U (update) line:

       * Skip the line if @valid_line is '0' for this line

       * look up <docid> by consulting @docid_of_line for the line number

       * Update @udocid, and @boost.

       * Attributes are put into the in-memory attribute arrays

       * Every <term> for a feature <feat> is added to the end of the file
         <feat>.idx: A new line is added with the contents:

         <term> TAB <docid> TAB <docw> TAB <positions>

         where <term> is the term as string, <docid> is the (internal)
         docid as decimal number, <docw> is the w(d,t) number as
         FP number with one digit after the decimal, and <positions>
         is the list of (space-separated) positions.

     - Processing of a D (delete) line: These lines can be ignored.
       If a D line deletes a preceding U line, it is already ensured
       that the U line is not valid according to @valid_line.

     - After the file is processed, write out all attribute vectors to
       disk, and free them in memory (GC cycle)

       This generates <name>.att files.

 *)

(* Fourth pass:

     - Sort every <feat>.idx file, first by <term> then by <docid>.
       This is done by an external /usr/bin/sort process, because
       these files can be huge.

       http://webtools.live2support.com/linux/sort.php

     - The sorted <feat>.idx files can be read in by the searcher.
 *)

open Printf
open Amblib_attvector

let debug = ref false 


module S64_S64_vector = struct
  open Bigarray
  type t =
      { v1 : (int64,int64_elt,c_layout) Array1.t;
	v2 : (int64,int64_elt,c_layout) Array1.t
      }
  type elt = int64

  let create v1 v2 =
    { v1 = v1; v2 = v2 }

  let left v = v.v1
  let right v = v.v2

  let length v =
    let l1 = Array1.dim v.v1 in
    let l2 = Array1.dim v.v1 in
    assert(l1 = l2);
    l1

  let get v p = v.v1.{ p }

  let compare = Int64.compare

  let swap v pa pb =
    let x1 = v.v1.{ pa } in
    let x2 = v.v2.{ pa } in
    v.v1.{ pa } <- v.v1.{ pb };
    v.v2.{ pa } <- v.v2.{ pb };
    v.v1.{ pb } <- x1;
    v.v2.{ pb } <- x2
end


module F32_S64_vector = struct
  open Bigarray
  type t =
      { v1 : (float,float32_elt,c_layout) Array1.t;
	v2 : (int64,int64_elt,c_layout) Array1.t
      }
  type elt = float

  let create v1 v2 =
    { v1 = v1; v2 = v2 }

  let left v = v.v1
  let right v = v.v2

  let length v =
    let l1 = Array1.dim v.v1 in
    let l2 = Array1.dim v.v1 in
    assert(l1 = l2);
    l1

  let get v p = v.v1.{ p }

  let compare = (Pervasives.compare : float -> float -> int)

  let swap v pa pb =
    let x1 = v.v1.{ pa } in
    let x2 = v.v2.{ pa } in
    v.v1.{ pa } <- v.v1.{ pb };
    v.v2.{ pa } <- v.v2.{ pb };
    v.v1.{ pb } <- x1;
    v.v2.{ pb } <- x2
end

module F32_S64_vector_decreasing = struct
  include F32_S64_vector
  let compare (x:float) (y:float) =
    - (Pervasives.compare x y)
end


module S64_S64_Quicksort = Quicksort(S64_S64_vector)
module F32_S64_Quicksort = Quicksort(F32_S64_vector)
module F32_S64_Quicksort_decreasing = Quicksort(F32_S64_vector_decreasing)



module Read_input_file = struct

  type inrec =
    [ `A of string * att_type
    | `F of string
    | `U of int64 * float * Yojson.Basic.json
    | `D of int64
    ]

  let read_channel (process_record : int -> int -> inrec -> unit) 
                   (json_flag : bool)
		   (filename : string) 
		   (f : in_channel) =
    let line_nr = ref 1 in
    let rec_nr = ref 1 in
    let current = ref None in
    let parse_flag = ref true in

    let process_current() =
      match !current with
	| None -> ()
	| Some r ->
	    parse_flag := false;
	    process_record !line_nr !rec_nr r;
	    parse_flag := true;
	    match r with
	      | `U _ -> incr rec_nr   (* only U records increase rec_nr! *)
	      | _ -> ()
    in

    let current_udocid() =
      match !current with
	| None -> None
	| Some(`A _) -> None
        | Some(`F _) -> None
	| Some(`U(udocid,_,_)) -> Some udocid
	| Some(`D udocid) -> Some udocid
    in

    try
      while true do
	let line = input_line f in
	let llen = String.length line in
	let tab1 = String.index_from line 0 '\t' in
	if tab1 <> 1 then raise Not_found;
	let line_type = line.[0] in
	let r =
	  match line_type with
	    | 'A' ->
		let tab2 = String.index_from line 2 '\t' in
		let att_name = String.sub line 2 (tab2 - 2) in
		let att_ty_s = String.sub line (tab2 + 1) (llen - tab2 - 1) in
		let att_ty = typ_of_string att_ty_s in
		(`A(att_name, att_ty))
            | 'F' ->
                let ft_name = String.sub line 2 (llen - 2) in
                (`F ft_name)
	    | 'U' ->
		let tab2 = String.index_from line 2 '\t' in
		let tab3 = String.index_from line (tab2+1) '\t' in
		let udocid_s = String.sub line 2 (tab2 - 2) in
		let udocid = Int64.of_string udocid_s in
		let docbs_s = String.sub line (tab2 + 1) (tab3 - tab2 - 1) in
		let docbs = float_of_string docbs_s in
		if docbs < 0.0 then raise Not_found;
		if json_flag then (
		  let json_s = String.sub line (tab3 + 1) (llen - tab3 - 1) in
		  let json = 
                    Yojson.Safe.to_basic
		      (Yojson.Safe.from_string json_s) in
		  (`U(udocid,docbs,json))
		)
		else
		  (`U(udocid,docbs,`Null))
	    | 'D' ->
		let udocid_s = String.sub line 2 (llen - 2) in
		let udocid = Int64.of_string udocid_s in
		(`D udocid)
	    | 'B' ->
		let tab2 = String.index_from line 2 '\t' in
		let udocid_s = String.sub line 2 (tab2 - 2) in
		let udocid = Int64.of_string udocid_s in
		let docbs_s = String.sub line (tab2 + 1) (llen - tab2 - 1) in
		let docbs = float_of_string docbs_s in
		if docbs < 0.0 then raise Not_found;
		(`B(udocid,docbs))
	    | _ -> raise Not_found in
	incr line_nr;
	if !line_nr < 0 then
	  failwith ("Input file too long: " ^ filename);
	( match r with
	    | `A _ as r ->
		process_current();
		current := Some r
	    | `F _ as r ->
		process_current();
		current := Some r
	    | `U(udocid,_,_) as r ->
		if current_udocid() <> Some udocid then
		  process_current();
		current := Some r
	    | `D udocid as r ->
		if current_udocid() <> Some udocid then
		  process_current();
		current := Some r
	    | `B(udocid,docbs) ->
		if current_udocid() = Some udocid then (
		  match !current with
		    | Some(`U(_,_,json)) -> 
			current := Some(`U(udocid,docbs,json))
		    | _ -> ()
		)
	)
      done;
      assert false
    with
      | End_of_file when !parse_flag ->
	  ()
      | _ when !parse_flag ->
	  failwith ("Syntax error in file " ^ filename ^ ", line " ^ 
		      string_of_int !line_nr)

  let read process_record json_flag filename =
    let f = open_in filename in
    ( try
	read_channel process_record json_flag filename f
      with
	| error ->
	    close_in f;
	    raise error
    );
    close_in f

end


let compute_Wd json =
  (* Compute W(d) for the json representation of one document *)
  let freq_of_term = Hashtbl.create 10 in
  ( match json with
      | `List [ `Assoc features; _ ] ->
	  List.iter
	    (fun (_, feature_value) ->
	       match feature_value with
		 | `List terms ->
		     List.iter
		       (fun term ->
			  match term with
			    | `List (`String term :: `Int freq :: _) ->
				let n =
				  try Hashtbl.find freq_of_term term
				  with Not_found -> 0 in
				Hashtbl.replace freq_of_term term (n+freq)
			    | _ ->
				assert false
		       )
		       terms
		 | _ ->
		     assert false
	    )
	    features
      | _ ->
	  assert false
  );
  let acc = ref 0.0 in
  Hashtbl.iter
    (fun _ freq ->
       let wdt = Amblib_score.wdt freq in
       acc := !acc +. (wdt *. wdt)
    )
    freq_of_term;
  sqrt !acc


module Create_index = struct

  let very_small = (* the smallest single-precision number *)
    Int32.float_of_bits 1l


(* --- OLD PASS 1 *)
(*
  let pass1 input_filename =
    printf "Pass1\n%!";
    let line_of_docid = Attribute.create `S64 in
    let max_docid = ref (-1) in
    ( let docboost_of_docid = Attribute.create `F32 in
      Attribute.expand docboost_of_docid 1;
      Attribute.expand line_of_docid 1;
      Attribute.set_int64 line_of_docid 0 0L;
      Attribute.set_float docboost_of_docid 0 (-1.0);
      Read_input_file.read 
	(fun line_nr r ->   (* note: first line_nr is 1 *)
	   let line_nr_64 = Int64.of_int line_nr in
	   Attribute.expand docboost_of_docid (line_nr+1);
	   Attribute.expand line_of_docid (line_nr+1);
	   Attribute.set_int64 line_of_docid line_nr line_nr_64;
	   match r with
	     | `A _ | `D _ ->
		 Attribute.set_float docboost_of_docid line_nr (-1.0)
		 
	     | `U(udocid,userboost,j) ->
		 let _B = 
                   max (abs_float userboost /. compute_Wd j) very_small in
		 incr max_docid;
		 Attribute.set_float docboost_of_docid line_nr _B
	)
	true
	input_filename;
      Attribute.dbg_print_int line_of_docid "line_of_docid (pre-sort)";
      Attribute.dbg_print_float docboost_of_docid "docboost_of_docid (pre-sort)";
      let ba_docboost = Attribute.rep_float32 docboost_of_docid in
      let ba_line = Attribute.rep_int64 line_of_docid in
      let v = F32_S64_vector_decreasing.create ba_docboost ba_line in
      F32_S64_Quicksort_decreasing.sort v;
      Attribute.dbg_print_int line_of_docid "line_of_docid";
      Attribute.dbg_print_float docboost_of_docid "docboost_of_docid";
    );
    let max_linenr = Attribute.size line_of_docid - 1 in
    (* unlcear whether we need the following, since line_of_docid is a 
       permutation, and by definition symmetric
     *)
    let docid_of_line = Attribute.create_for_size `S64 (max_linenr+1) in
    Attribute.expand docid_of_line (max_linenr+1);
    for docid = 1 to max_linenr do
      let line_64 = Attribute.get_int64 line_of_docid docid in
      let line = Int64.to_int line_64 in
      Attribute.set_int64 docid_of_line line (Int64.of_int docid)
    done;
    Attribute.dbg_print_int docid_of_line "docid_of_line";
    (docid_of_line, !max_docid, max_linenr)
 *)

(* -- OLD PASS 2 -- *)

(*
  let pass2 input_filename max_linenr =
    (* Note: max_docid = max_line_nr *)
    printf "Pass2\n%!";
    let valid_line = Attribute.create_for_size `U1 (max_linenr+1) in
    Attribute.expand valid_line (max_linenr+1);
    let line_of_q = Attribute.create_for_size `S64 (max_linenr+1) in
    Attribute.expand line_of_q (max_linenr+1);
    ( let udocid_of_q = Attribute.create_for_size `S64 (max_linenr+1) in
      Attribute.expand udocid_of_q (max_linenr+1);
      Read_input_file.read 
	(fun line_nr r ->   (* note: first line_nr is 1 *)
	   match r with
	     | `A _ -> ()
	     | `D udocid | `U(udocid,_,_) ->
		 let line_nr_64 = Int64.of_int line_nr in
		 Attribute.set_int64 udocid_of_q line_nr udocid;
		 Attribute.set_int64 line_of_q line_nr line_nr_64;
	)
	false
	input_filename;
      let ba_udocid = Attribute.rep_int64 udocid_of_q in
      let ba_line = Attribute.rep_int64 line_of_q in
      let v = S64_S64_vector.create ba_udocid ba_line in
      S64_S64_Quicksort.sort v;

      let last = ref(ba_udocid.{1}, ba_line.{1}, 1) in
      for k = 2 to max_linenr do
	let last_udocid, last_line, last_cell = !last in
	if ba_udocid.{k} = last_udocid then (
	  if ba_line.{k} > last_line then (
	    ba_line.{ last_cell } <- 0L;
	    last := (last_udocid, ba_line.{k}, k)
	  )
	)
	else
	  last := (ba_udocid.{k}, ba_line.{k}, k)
      done;

      Attribute.dbg_print_int line_of_q "line_of_q";
      Attribute.dbg_print_int udocid_of_q "udocid_of_q";
    );

    for q = 1 to max_linenr do
      let line_64 = Attribute.get_int64 line_of_q q in
      let line = Int64.to_int line_64 in
      if line > 0 then
	Attribute.set_int64 valid_line line 1L
    done;

    Attribute.dbg_print_int valid_line "valid_line";

    valid_line
 *)

  let pass1 input_filename output_dir =
    printf "Pass1\n%!";
    let cmd =
      sprintf "LC_ALL=C sort -s -t '\t' -k 2n,2n %s > %s"
	     (Filename.quote input_filename)
	     (Filename.quote (output_dir ^ "/input.sorted")) in
    printf "+ %s\n%!" cmd;
    let code =
      Sys.command cmd in
    if code <> 0 then
      failwith ("Command exited with error: " ^ cmd)


  let pass2 output_dir =
    printf "Pass2\n%!";
    let input_filename = output_dir ^ "/input.sorted" in
    let record_of_docid = Attribute.create `S64 in
    let att_decls = Hashtbl.create 10 in
    let ft_decls = Hashtbl.create 10 in
    let docboost_of_docid = Attribute.create `F32 in
    Attribute.expand docboost_of_docid 1;
    Attribute.expand record_of_docid 1;
    Attribute.set_int64 record_of_docid 0 0L;
    Attribute.set_float docboost_of_docid 0 (-1.0);
    Read_input_file.read 
      (fun line_nr rec_nr r ->   (* note: first rec_nr is 1 *)
	 let rec_nr_64 = Int64.of_int rec_nr in
	 Attribute.expand docboost_of_docid (rec_nr+1);
	 Attribute.expand record_of_docid (rec_nr+1);
	 Attribute.set_int64 record_of_docid rec_nr rec_nr_64;
	 match r with
	   | `A(attname,typ) ->
	       if Hashtbl.mem att_decls attname then
		 failwith("Attribute defined twice: " ^ attname);
	       Hashtbl.add att_decls attname typ

	   | `F ftname ->
	       if Hashtbl.mem ft_decls ftname then
		 failwith("Feature defined twice: " ^ ftname);
	       Hashtbl.add ft_decls ftname ()
		 
	   | `D _ ->
	       ()
		 
	   | `U(udocid,userboost,j) ->
	       let _B = 
                 max (abs_float userboost /. compute_Wd j) very_small in
	       Attribute.set_float docboost_of_docid rec_nr _B
      )
      true
      input_filename;
    if !debug then (
      Attribute.dbg_print_int record_of_docid "record_of_docid (pre-sort)";
      Attribute.dbg_print_float docboost_of_docid "docboost_of_docid (pre-sort)";
    );
    
    let max_recnr = Attribute.size record_of_docid - 1 in
    (* The bigarrays are only filled for the index 1 onwards. So we get a
       sub bigarray starting there, and sort this.
     *)
    let ba_docboost0 = Attribute.rep_float32 docboost_of_docid in
    let ba_docboost =  Bigarray.Array1.sub ba_docboost0 1 max_recnr in
    let ba_line0 = Attribute.rep_int64 record_of_docid in
    let ba_line = Bigarray.Array1.sub ba_line0 1 max_recnr in
    let v = F32_S64_vector_decreasing.create ba_docboost ba_line in
    F32_S64_Quicksort_decreasing.sort v;
    if !debug then (
      Attribute.dbg_print_int record_of_docid "record_of_docid";
      Attribute.dbg_print_float docboost_of_docid "docboost_of_docid";
    );
    (* unlcear whether we need the following, since record_of_docid is a 
       permutation, and by definition symmetric
     *)
    let docid_of_record = Attribute.create_for_size `S64 (max_recnr+1) in
    Attribute.expand docid_of_record (max_recnr+1);
    for docid = 1 to max_recnr do
      let rec_64 = Attribute.get_int64 record_of_docid docid in
      let rec_i = Int64.to_int rec_64 in
      Attribute.set_int64 docid_of_record rec_i (Int64.of_int docid)
    done;
    if !debug then
      Attribute.dbg_print_int docid_of_record "docid_of_record";
    (docid_of_record, max_recnr, att_decls, ft_decls)

    


  let enter_attribute att name docid att_val_j =
    let att_type = Attribute.typ att in
    if att_type = `F32 || att_type = `F64 then (
      (* FP mode *)
      let x =
	match att_val_j with
	  | `Int i -> float i
	  | `Float f -> f
	  | `String s -> float_of_string s
	  | _ -> raise Not_found in
      Attribute.set_float att docid x
    )
    else (
      (* integer mode *)
      let x =
	match att_val_j with
	  | `Int i -> Int64.of_int i
	  | `Float f -> Int64.of_float f
	  | `String s -> Int64.of_string s
	  | _ -> raise Not_found in
      let x_min, x_max =   (* TODO: move to Attribute *)
	match att_type with
	  | `U1 -> 0L, 1L
	  | `U2 -> 0L, 3L
	  | `U4 -> 0L, 15L
	  | `U8 -> 0L, 255L
	  | `U16 -> 0L, 65535L
	  | `S8 -> (-128L), 127L
	  | `S16 -> (-32768L), 32767L
	  | `S32 -> (-0x8000_0000L), 0x7FFFF_FFFFL
	  | `S64 -> Int64.min_int, Int64.max_int 
	  | _ -> assert false in
      if x < x_min || x > x_max then 
	failwith("Attribute out of range: " ^ name);
      Attribute.set_int64 att docid x
    )


  let output_feature_line feat_file docid feat_j =
    match feat_j with
      | `List feat_a ->
	  List.iter
	    (fun term_j ->
	       match term_j with
		 | `List ( `String term :: `Int freq :: pos_list ) ->
		     output_string feat_file term;
		     output_string feat_file "\t";
		     output_string feat_file (string_of_int docid);
		     output_string feat_file "\t";
		     (* TODO: <docw>? *)
		     output_string feat_file (string_of_int freq);
		     output_string feat_file "\t";
		     let first = ref true in
		     List.iter
		       (fun pj ->
			  match pj with
			    | `Int p ->
				if not !first then
				  output_string feat_file " ";
				output_string feat_file (string_of_int p);
				first := false
			    | _ -> raise Not_found
		       )
		       pos_list;
		     output_string feat_file "\n"
		 | _ ->
		     raise Not_found
	    )
	    feat_a
      | _ ->
	  raise Not_found


  let pass3 output_dir max_docid docid_of_record att_decls ft_decls =
    printf "Pass3\n%!";
    let input_filename = output_dir ^ "/input.sorted" in
    let atts = Hashtbl.create 17 in
    Hashtbl.iter
      (fun name typ ->
	 let att = Attribute.create_for_size typ (max_docid+1) in
	 Attribute.expand att (max_docid+1);
	 Hashtbl.add atts name att
      )
      att_decls;
    let udocid_att = Attribute.create_for_size `S64 (max_docid+1) in
    Attribute.expand udocid_att (max_docid+1);
    Hashtbl.add atts "@udocid" udocid_att;
    let boost_att = Attribute.create_for_size `F32 (max_docid+1) in
    Attribute.expand boost_att (max_docid+1);
    Hashtbl.add atts "@boost" boost_att;
    
    let features = Hashtbl.create 10 in
    let feature_docs = Hashtbl.create 10 in
    
    Hashtbl.iter
      (fun feat_name _ ->
         let f = 
	   open_out (output_dir ^ "/" ^ feat_name ^ ".unsorted") in
         Hashtbl.add features feat_name f;
         Hashtbl.add feature_docs feat_name 0L
      )
      ft_decls;

    Read_input_file.read 
      (fun line_nr rec_nr r ->   (* note: first rec_nr is 1 *)
	 try
	   match r with
	     | `A(name,typ) ->
		 ()
             | `F name ->
                 ()
	     | `D _ -> 
		 ()
	     | `U(udocid,userboost,j) ->
		 let _B = userboost /. compute_Wd j in
		 let docid64 = Attribute.get_int64 docid_of_record rec_nr in
		 let docid = Int64.to_int docid64 in
		 Attribute.set_int64 udocid_att docid udocid;
		 Attribute.set_float boost_att docid _B;
		 match j with
		   | `List [ `Assoc features_o; 
			     `Assoc attributes_o
			   ] ->
		       List.iter
			 (fun (att_name, att_val_j) ->
			    let att = Hashtbl.find atts att_name in
			    enter_attribute att att_name docid att_val_j
			 )
			 attributes_o;
		       List.iter
			 (fun (feat_name, feat_j) ->
                            if not (Hashtbl.mem ft_decls feat_name) then
                              failwith("Undeclared feature: " ^ feat_name);
			    let feat_file =
			      try Hashtbl.find features feat_name
			      with Not_found -> assert false in
			    output_feature_line feat_file docid feat_j;
                            let feat_docs =
                              try Hashtbl.find feature_docs feat_name
                              with Not_found -> 0L in
                            Hashtbl.replace 
                              feature_docs feat_name (Int64.succ feat_docs)
			 )
			 features_o;

		   | _ -> raise Not_found
	 with
	   | Not_found ->
	       failwith ("Invalid data in file " ^ input_filename ^ 
			   ", line " ^ string_of_int line_nr)
	   | Failure msg | Invalid_argument msg ->
	       failwith ("Invalid data in file " ^ input_filename ^ 
			   ", line " ^ string_of_int line_nr ^ ": " ^ msg)
      )
      true
      input_filename;

    Hashtbl.iter
      (fun _ file -> close_out file)
      features;

    let n = ref 0 in
    Hashtbl.iter
      (fun name att ->
	 let fname = output_dir ^ "/" ^ string_of_int !n ^ ".att" in
	 printf "+ writing %s\n%!" fname;
	 let f = open_out fname in
	 output_string f (name ^ "\n"); 
	 output_string f (string_of_int (Attribute.size att) ^ "\n");
	 output_string f (string_of_typ (Attribute.typ att) ^ "\n");
	 for k = 0 to Attribute.size att - 1 do
	   output_string f (Int64.to_string (Attribute.get_int64 att k) ^ "\n")
	 done;
	 close_out f;
	 incr n
      )
      atts;

    let fname = output_dir ^ "/feature_counts.txt" in
    printf "+ writing %s\n%!" fname;
    let f = open_out fname in
    Hashtbl.iter
      (fun ft_name docs ->
         fprintf f "all_base %S %Ld\n" ft_name docs;
         fprintf f "eff_base %S %Ld\n" ft_name docs;
         fprintf f "all %S %Ld\n" ft_name docs;
         fprintf f "eff %S %Ld\n" ft_name docs;
      )
      feature_docs;
    close_out f;

    (* return names of features *)
    Hashtbl.fold
      (fun name _ acc -> name :: acc)
      features
      []

  let pass4 output_dir featnames =
    printf "Pass4\n%!";
    List.iter
      (fun name ->
	 let cmd =
	   sprintf "LC_ALL=C sort -t '\t' -k 1,1 -k 2n,2n %s > %s"
	     (Filename.quote (output_dir ^ "/" ^ name ^ ".unsorted"))
	     (Filename.quote (output_dir ^ "/" ^ name ^ ".idx")) in
	 printf "+ %s\n%!" cmd;
	 let code =
	   Sys.command cmd in
	 if code <> 0 then
	   failwith ("Command exited with error: " ^ cmd)
      )
      featnames


end
