(* $Id$ *)

open Printf

let split_line line =
  (* Split into 4 tab-separated fields *)
  try
    let len = String.length line in
    let tab0 = String.index_from line 0 '\t' in
    let tab1 = String.index_from line (tab0+1) '\t' in
    let tab2 = String.index_from line (tab1+1) '\t' in
    (String.sub line 0 tab0,
     String.sub line (tab0+1) (tab1-tab0-1),
     String.sub line (tab1+1) (tab2-tab1-1),
     String.sub line (tab2+1) (len-tab2-1)
    )
  with
    | Not_found ->
	failwith "File format error"


let split_positions s =
  (* Split the positions into space-separated numbers *)
  let l = ref [] in
  let j = ref 0 in
  if s <> "" then (
    try
      while true do
	let k = String.index_from s !j ' ' in
	let field = String.sub s !j (k - !j) in
	l := (int_of_string field) :: !l;
	j := k + 1
      done
    with Not_found -> ()
  );
  if s <> "" then (
    let field = String.sub s !j (String.length s - !j) in
    l := (int_of_string field) :: !l;
  );
  Array.of_list(List.rev !l)


(* read_toc: just read the toc object from path=feature_counts.txt.
   Note that for "base" dumps, all the four numbers all_base, eff_base,
   all, and eff must be identical. For "update" dump, the can be distinct.
 *)

let read_toc path att_names =
  let all_base = Hashtbl.create 10 in
  let eff_base = Hashtbl.create 10 in
  let all = Hashtbl.create 10 in
  let eff = Hashtbl.create 10 in

  let incr ht name n_add =
    let n_old = try Hashtbl.find ht name with Not_found -> 0L in
    Hashtbl.replace ht name (Int64.add n_old n_add) in
  let total ht =
    Hashtbl.fold (fun _ v acc -> Int64.add v acc) ht 0L in
  
  let f = open_in path in
  ( try
      while true do
        Scanf.fscanf f "%s %S %Ld\n"
                     (fun kind ftname n ->
                      match kind with
                        | "all_base" -> incr all_base ftname n
                        | "eff_base" -> incr eff_base ftname n
                        | "all" -> incr all ftname n
                        | "eff" -> incr eff ftname n
                        | _ ->
                            failwith ("Bad tag found in: " ^ path)
                     )
      done
    with
      | End_of_file -> ()
  );
  close_in f;

  let ft_names =
    Array.of_list (Hashtbl.fold (fun n _ acc -> n :: acc) all []) in
  let ft_all_docs =
    Array.map 
      (fun n -> try Hashtbl.find all n with Not_found -> 0L) 
      ft_names in
  let ft_eff_docs =
    Array.map 
      (fun n -> try Hashtbl.find eff n with Not_found -> 0L) 
      ft_names in
  let byte_order =
    (Netnumber.HO.int4_as_string (Netnumber.int4_of_int 1)).[0] = '\000' in

  { Amblib_dump_fmt_aux.format = 4711;
    byte_order;
    set = Amblib_dump_fmt_aux.base;  (* FIXME: just assume "base" for now *)
    all_docs_base = total all_base;
    eff_docs_base = total eff_base;
    all_docs = total all;
    eff_docs = total eff;
    att_names = Array.of_list att_names;
    ft_names;
    ft_all_docs;
    ft_eff_docs
  }
   
let load_line f =
  let line = input_line f in
  (* printf "Line: %s\n%!" line; *)
  let (term_name, docid_s, freq_s, pos_field) = split_line line in
  let pos = split_positions pos_field in
  let freq = int_of_string freq_s in
  let docid = int_of_string docid_s in
  let wdt = Amblib_score.wdt freq in
  
  (term_name, docid, wdt, pos)


let safe_map f l =
  List.rev (List.rev_map f l)


let num_terms = ref 0
let num_slices = ref 0
let num_short_vectors = ref 0
let compr_size = ref 0
let termstrings = ref 0


(* scan_feature scans the feature file once, and returns the feature
   object to append to the dump file. As side effect, it also fills
   the hashtbl wdt_slices, so that the table contains for every term
   the list of maximum wdt values in every slice. This list is in
   reverse order (last slice first), and the maxima are only per slice.
   Later we will compute for every slice the maximum for the whole rest
 *)

let scan_feature dump path feature_id feature_name slice_len wdt_slices =
  printf "Feature (scan): %s\n%!" feature_name;

  let counts = Hashtbl.create 1000 in
  (* Doc counts per term *)

  let terms = ref [] in

  let rec read_counts() =
    let f = open_in path in
    ( try
        let (term_name, _, wdt, _) = load_line f in
        incr num_terms;
        read_term_count f term_name 1L 1 wdt;
      with End_of_file -> ()
    );
    close_in f

  and read_term_count f term_name count n slice_max_wdt =
    (* count: number of docs for term_name so far.
       n: number of docs in the current slice
       slice_max_wdt: the maximum wdt value of the current slice
     *)
    let n' =
      if n = slice_len then (
        store_wdt term_name slice_max_wdt;
        0
      )
      else
        n in
    let slice_max_wdt' =
      if n = slice_len then 0.0 else slice_max_wdt in
    let next, wdt =
      try
        let (term_next, _, wdt, _) = load_line f in
        if term_name = term_next then
          None, wdt
        else (
          assert(term_next > term_name);
          Some (Some term_next), wdt
        )
      with End_of_file -> Some None, 0.0 in
    match next with
      | None ->
          read_term_count
            f term_name (Int64.succ count) (n'+1) (max slice_max_wdt' wdt)
      | Some t ->
          if count <= Int64.of_int slice_len then
            (* don't need this for shortvectors *)
            Hashtbl.remove wdt_slices term_name  
          else if n' > 0 then
            store_wdt term_name slice_max_wdt;
          Hashtbl.replace counts term_name count;
          terms := term_name :: !terms;
          incr num_terms;
          match t with
            | None -> ()
            | Some n -> 
                read_term_count f n 1L 1 wdt

  and store_wdt term_name slice_max_wdt =
    let wdt_list =
      try Hashtbl.find wdt_slices term_name with Not_found -> [] in
    Hashtbl.replace wdt_slices term_name (slice_max_wdt :: wdt_list);
  in

  let vector name =
    let count = try Hashtbl.find counts name with Not_found -> assert false in
    let slices = 
      Int64.succ(Int64.div (Int64.pred count) (Int64.of_int slice_len)) in
    if slices > Int64.of_int max_int then
      failwith ("Too many slices for: " ^ name);
    termstrings := !termstrings + String.length name;
    { Amblib_dump_fmt_aux.vec_key = name;
      vec_slices = Int64.to_int slices;
      vec_docs = count
    } in

  read_counts();

  let vecs =
    Array.of_list
      (List.rev_map
         vector
         !terms
      ) in
  Array.sort
    (fun x y -> Amblib_dump_fmt_aux.(String.compare x.vec_key y.vec_key))
    vecs;

  { Amblib_dump_fmt_aux.ft_name = feature_name;
    ft_ftid = feature_id;
    ft_datalen = 0L;   (* updated later *)
    ft_vectors = vecs;
  }
;;


(* dump_feature: does another scan of the feature file, and now outputs
   the term vector slices.

   wdt_slices must be the hashtable from scan_feature.
 *)

let dump_feature dump path feature_id feature_name slice_len wdt_slices =
  printf "Feature (dump): %s\n%!" feature_name;

  let datalen = ref 0L in

  (* wdt_list fixup: On input, the list l contains for every slice the
     maximum wdt value of that slice. Also, l is in reverse order.

     On output, the list contains for every slice the maximum wdt value
     of the slice and all following slices. The list is now in real
     order
   *)

  let rec fixup_wdt_list acc l m =
    match l with
      | wdt :: l' ->
          let wdt' = max wdt m in
          fixup_wdt_list (wdt' :: acc) l' wdt'
      | [] ->
          acc in

  let f = open_in path in

  let rec dump_term term_name term_idx acc n n_total min_docid max_docid
                    wdt_list =
    let next =
      try
        let (term_next, docid, wdt, pos_array) = load_line f in
        if term_name = term_next then
          `Same(docid, wdt, pos_array)
        else (
          assert(term_next > term_name);
          `Next(term_next, docid, wdt, pos_array)
        )
      with End_of_file -> `EOF in
    match next with
      | `Same(docid, wdt, pos) ->
          let (acc1, n1, wdt_list1) =
            if n = slice_len then (
              let wdt_rest = List.hd wdt_list in
              let n_prev = Int64.sub n_total (Int64.of_int n) in
              dump_slice 
                term_idx n_prev n min_docid max_docid (List.rev acc) wdt_rest;
              ( [], 0, List.tl wdt_list )
            )
            else
              (acc, n, wdt_list) in
          let acc' = (docid, wdt, pos) :: acc1 in
          dump_term 
            term_name term_idx acc' (n1 + 1) (Int64.succ n_total)
            (min min_docid docid) (max max_docid docid) wdt_list1
      | `Next(term, docid, wdt, pos) ->
          if n_total <= Int64.of_int slice_len then
            dump_shortvector term_idx (List.rev acc)
          else (
            let n_prev = Int64.sub n_total (Int64.of_int n) in
            let wdt_rest = List.hd wdt_list in
            assert(List.tl wdt_list = []);
            dump_slice 
              term_idx n_prev n min_docid max_docid (List.rev acc) wdt_rest;
          );
          let acc' = [docid, wdt, pos] in
          let next_wdt_list = 
            try
              fixup_wdt_list [] (Hashtbl.find wdt_slices term) 0.0
            with Not_found -> [] in
          dump_term
            term (term_idx+1) acc' 1 1L docid docid next_wdt_list
      | `EOF ->
          if n_total <= Int64.of_int slice_len then
            dump_shortvector term_idx (List.rev acc)
          else (
            let n_prev = Int64.sub n_total (Int64.of_int n) in
            let wdt_rest = List.hd wdt_list in
            assert(List.tl wdt_list = []);
            dump_slice 
              term_idx n_prev n min_docid max_docid (List.rev acc) wdt_rest;
          )


  and dump_shortvector term_index data =
    let (doc_a, wdt_a, pos_a) = get_triple data in
    let sl_data = Amblib_zslice.encode_slice doc_a wdt_a pos_a in
    let slice =
      let open Amblib_dump_fmt_aux in
      { ssl_index = term_index;
        ssl_ftid = feature_id;
        ssl_data = Bytes.to_string sl_data
      } in
    let sl_data_len = Bytes.length sl_data in
    compr_size := !compr_size + sl_data_len;
    datalen := 
      Int64.add 
        !datalen
        (Int64.of_int (sl_data_len + Amblib_zslice.bytes_of_vint sl_data_len));
    incr num_short_vectors;
    Amblib_dump.write_object dump (`shortvector slice)

  and dump_slice term_index sl_start sl_count sl_min_docid sl_max_docid 
                 data wdt_rest =
    let (doc_a, wdt_a, pos_a) = get_triple data in
    let sl_data = Amblib_zslice.encode_slice doc_a wdt_a pos_a in
    let sl_ord =
      Int64.to_int (Int64.div sl_start (Int64.of_int slice_len)) in
    let slice =
      let open Amblib_dump_fmt_aux in
      { sl_index = term_index;
        sl_ftid = feature_id;
        sl_ord;
        sl_start = sl_start;
        sl_count = sl_count;
        sl_min_docid = Int64.of_int sl_min_docid;
        sl_max_docid = Int64.of_int sl_max_docid;
        sl_rest_max_docw = wdt_rest;
        sl_data = Bytes.to_string sl_data;
      } in
    let sl_data_len = Bytes.length sl_data in
    compr_size := !compr_size + sl_data_len;
    datalen := 
      Int64.add 
        !datalen
        (Int64.of_int (sl_data_len + Amblib_zslice.bytes_of_vint sl_data_len));
    incr num_slices;
    Amblib_dump.write_object dump (`vectorslice slice)

  and get_triple data =
    let doc_a = 
      Array.of_list
        (safe_map (fun (docid,_,_) -> docid) data) in
    let wdt_a = 
      Array.of_list
        (safe_map
           (fun (_,wdt,_) -> Amblib_zslice.half_of_float wdt)
           data
        ) in
    let pos_a = 
      Array.of_list
        (safe_map 
           (fun (_,_,posarr) -> 
              Array.sort (fun x y -> x-y) posarr;
              posarr
           )
           data
        ) in
    (doc_a, wdt_a, pos_a)
  in
    
  ( try
      let (term_name, docid, wdt, pos) = load_line f in
      let acc = [docid, wdt, pos] in
      let wdt_list = 
        try
          fixup_wdt_list [] (Hashtbl.find wdt_slices term_name) 0.0
        with Not_found -> [] in
      dump_term term_name 0 acc 1 1L docid docid wdt_list
    with End_of_file -> ()
  );
  close_in f;

  !datalen
;;


let dump_att dump path =
  let f = open_in path in

  let att_name = input_line f in
  let att_size = int_of_string(input_line f) in
  let att_type = Amblib_attvector.typ_of_string (input_line f) in
  
  let att = 
    Amblib_attvector.Attribute.create_for_size att_type att_size in

  Amblib_attvector.Attribute.expand att att_size;

  for k = 0 to att_size - 1 do
    let v = Int64.of_string(input_line f) in
    Amblib_attvector.Attribute.set_int64 att k v
  done;

  close_in f;

  let mem = Amblib_attvector.Attribute.memory att in
  let ms = Netxdr_mstring.memory_to_mstring mem in
  
  let obj =
    let open Amblib_dump_fmt_aux in
    `attribute
      { att_name;
	att_size = Int64.of_int att_size;
	att_offset = 0L;  (* always 0 for a base set *)
        att_type = Amblib_attvector.string_of_typ att_type;
        att_set = base;
        att_value = ms;
      } in
  Amblib_dump.write_object dump obj


let dump_index dumpfile dirname slice_len =
  let files = Array.to_list(Sys.readdir dirname) in
  let dump = Amblib_dump.create_dump_file dumpfile in

  let att_names =
    List.map
      (fun name -> Filename.chop_suffix name ".att" )
      (List.filter
         (fun name -> Filename.check_suffix name ".att")
         files
      ) in
  
  (* Create TOC *)
  let toc = read_toc (dirname ^ "/feature_counts.txt") att_names in
  Amblib_dump.write_object dump (`toc toc);

  (* First we must load the attributes, so that docboosts are available *)

  List.iter
    (fun name ->
       let path = Filename.concat dirname name in
       if Filename.check_suffix name ".att" then (
	 dump_att dump path;
       )
    )
    files;

  (* Go on and dump the features: *)

  let feature_id = ref 0 in
  List.iter
    (fun name ->
       let path = Filename.concat dirname name in
       if Filename.check_suffix name ".idx" then (
         let feature_name =
           Filename.chop_suffix name ".idx" in
         let wdt_slices =
           Hashtbl.create 10 in
         let ft =
           scan_feature 
             dump path !feature_id feature_name slice_len wdt_slices in
         let ft_pos = Amblib_dump.pos_writer dump in
         Amblib_dump.write_object dump (`feature ft);
         let datalen =
           dump_feature
             dump path !feature_id feature_name slice_len wdt_slices in
         (* Update the feature object already written, and set datalen to
            the right value:
          *)
         let ft' =
           { ft with Amblib_dump_fmt_aux.ft_datalen = datalen } in
         Amblib_dump.seek_writer dump ft_pos;
         Amblib_dump.write_object dump (`feature ft');
         Amblib_dump.seek_end_writer dump
       );
    )
    files;

  Amblib_dump.close_dump_writer dump;

  eprintf "Total number of terms: %d\n" !num_terms;
  eprintf "Total number of long vector slices: %d\n" !num_slices;
  eprintf "Total number of short vectors: %d\n" !num_short_vectors;
  eprintf "Total size of term strings: %d\n" !termstrings;
  eprintf "Total size of compressed slices: %d\n%!" !compr_size


  
