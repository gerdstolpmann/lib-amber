(* $Id$ *)

open Printf

let main() =
  let input_file = ref None in
  let dir = ref "." in
  let dump = ref None in
  let slice_len = ref 1000 in
  Arg.parse
    [ "-in", Arg.String (fun s -> input_file := Some s),
      "<file>   read this file";

      "-dir", Arg.Set_string dir,
      "<dir>   output to this directory instead of cwd";

      "-dump", Arg.String (fun s -> dump := Some s),
      "<file>   create a dump file";

      "-slice-len", Arg.Set_int slice_len,
      ("<n>    Set the slice length (default " ^ string_of_int !slice_len ^ ")")
    ]
    (fun s -> raise(Arg.Bad("unexpected arg: "  ^ s)))
    (sprintf "%s: [options]" Sys.argv.(0));

  ( match !input_file with
      | Some fname ->

          let () =
            Amb_create_index.Create_index.pass1 fname !dir in

          let docid_of_record, max_docid, att_decls, ft_decls =
            Amb_create_index.Create_index.pass2 !dir in

          let feature_names =
            Amb_create_index.Create_index.pass3
              !dir max_docid docid_of_record att_decls ft_decls in

          let () =
            Amb_create_index.Create_index.pass4 !dir feature_names in
          printf "Multi-file index created.\n%!"

      | None ->
          ()
  );
  ( match !dump with
      | Some fname ->
          Amb_index_dump.dump_index fname !dir !slice_len;
          printf "Dump file index created.\n%!"

      | None ->
          ()
  )
;;


let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Error: %s\n" (Netexn.to_string error);
        eprintf "Backtrace: %s\n" bt;
        exit 1
