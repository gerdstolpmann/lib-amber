(* $Id$ *)

open Pxp_types
open Printf

module J = Json_type

let analyze_doc_feature s =
  let words =
    List.map
      (function
	 | `Word(w,pos) ->
	     (Amblib_tokenize.normalize w, pos)
      )
      (Amblib_tokenize.tokenize s) in
  let num_words = List.length words in
  let word_hash = Hashtbl.create num_words in
  List.iter
    (fun (w,pos) ->
       let l =
	 try Hashtbl.find word_hash w with Not_found -> [] in
       Hashtbl.replace word_hash w (pos :: l)
    )
    words;
  let fvalue =
    J.Array
      (Hashtbl.fold
	 (fun w pos_list acc ->
	    let freq = 10 * List.length pos_list in
	    let item =
	      J.Array
		( (J.String w) :: 
		    (J.Int freq) ::
		    (List.map (fun p -> J.Int p) pos_list) ) in
	    item :: acc
	 )
	 word_hash
	 []
      ) in
  (num_words, fvalue)


let ts_re = Str.regexp "\\([0-9][0-9][0-9][0-9]\\)-\\([0-9][0-9]\\)-\\([0-9][0-9]\\)T\\([0-9][0-9]\\):\\([0-9][0-9]\\):\\([0-9][0-9]\\)Z"


let timezone =
  fst(Unix.mktime
	{ Unix.tm_sec = 0;
	  tm_min = 0;
	  tm_hour = 0;
	  tm_mday = 1;
	  tm_mon = 0;
	  tm_year = 70;
	  tm_wday = 0;
	  tm_yday = 0;
	tm_isdst = false;
	})

let get_ts s =
  if Str.string_match ts_re s 0 then
    let year = Str.matched_group 1 s in
    let month = Str.matched_group 2 s in
    let day = Str.matched_group 3 s in
    let hour = Str.matched_group 4 s in
    let minute = Str.matched_group 4 s in
    let second = Str.matched_group 6 s in
    let tm =
      { Unix.tm_sec = int_of_string second;
	tm_min = int_of_string  minute;
	tm_hour = int_of_string  hour;
	tm_mday =  int_of_string day;
	tm_mon =  int_of_string month - 1;
	tm_year =  int_of_string year - 1900;
	tm_wday = 0;
	tm_yday = 0;
	tm_isdst = false;
      } in
    Some(fst(Unix.mktime tm)-.timezone)
  else
    None


let tag1_re =
  Str.regexp "\\[\\[[a-zA-Z-]+:\\([^]]*\\)\\]\\]"
    (* [[macro:arguments]] is rewritten to: arguments *) 


let tag2_re =
  Str.regexp "{{[^}]*}}"
    (* {{ any }} is removed *)

let tag3_re =
  Str.regexp "</?[a-zA-Z][^>]*>"
    (* <tag any> and </tag> are removed *)



let ignorable_re = Str.regexp "Template:\\|MediaWiki:"

let is_ignorable s =
  Str.string_match ignorable_re s 0



let throw_out_tags s =
  Str.global_replace tag3_re
    ""
    (Str.global_replace tag2_re
       ""
       (Str.global_replace tag1_re "\\1" s))

exception Skip_doc



let analyze_doc data_tab =
  let id_str =
    try Hashtbl.find data_tab (List.rev ["mediawiki";"page";"id"])
    with Not_found -> failwith "Page without id" in
  let id =
    try Int64.of_string id_str
    with _ -> failwith "Invalid page id" in
  if id <= 0L then
    failwith "Non-positive page id not allowed";
  let title =
    try Hashtbl.find data_tab (List.rev ["mediawiki";"page";"title"])
    with Not_found -> failwith "Page without title" in
  if is_ignorable title then
    raise Skip_doc;
  let doc_boost =
    1.0 (* 0.01 +. Random.float 5.0 *) in
  let feature_names_0 =
    [ "username", ["mediawiki";"page";"revision";"contributor";"username"];
      "comment",  ["mediawiki";"page";"revision";"comment"];
      "text",     ["mediawiki";"page";"revision";"text"]
    ] in
  let feature_names =
    List.map (fun (fname,fpath) -> (fname,List.rev fpath)) feature_names_0 in
  let features_plus =
    List.map
      (fun (fname, fpath) ->
	 let (num_words, fvalue) =
	   try
	     let data = Hashtbl.find data_tab fpath in
	     analyze_doc_feature (throw_out_tags data)
	   with Not_found -> (0, J.Array []) in
	 (fname, fvalue, num_words)
      )
      feature_names in
  let features =
    J.Object 
      (List.map (fun (fname,fvalue,_) -> (fname,fvalue)) features_plus) in
(* DEBUG *)
(*
  let textname = List.rev ["mediawiki";"page";"revision";"text"] in
  let text = try Hashtbl.find data_tab textname with Not_found -> "" in
  prerr_endline ("\n\nORIGINAL: " ^ text);
  prerr_endline ("\n\nTEXT: " ^ throw_out_tags text);
 *)
(* /DEBUG *)
  let text_word_count =
    try 
      let (_,_,c) =
	List.find (fun (fname,_,_) -> fname = "text") features_plus in
      c
    with Not_found -> 0 in
  let timestamp_path =
    List.rev [ "mediawiki"; "page"; "revision"; "timestamp" ] in
  let timestamp_s =
    try
      Hashtbl.find data_tab timestamp_path
    with Not_found -> "" in
  let attributes =
    J.Object
      ( [ "text_word_count", J.Int text_word_count ] @
	  ( match get_ts timestamp_s with
	      | None -> []
	      | Some ts ->
		  [ "timestamp", J.Float ts ]
	  ) )

 in
  let doc_details = 
    J.Array [ features; attributes ] in
  (id, doc_boost, doc_details)


let print_wordlist doc_details =
  match doc_details with
    | J.Array [ J.Object features; _ ] ->
	let text = 
	  try List.assoc "text" features with Not_found -> J.Array [] in
	( match text with
	    | J.Array terms ->
		List.iter
		  (fun term ->
		     match term with
		       | J.Array (J.String tname :: J.Int tfreq :: _) ->
			   print_endline (tname ^ "\t" ^ 
					    string_of_int (tfreq/10))
		       | _ ->
			   assert false
		  )
		  terms
	    | _ ->
		assert false
	)
    | _ ->
	assert false


let parse filename limit wordlist_mode =
  let pxp_config =
    { Pxp_types.default_config with encoding = `Enc_utf8 } in
  let pxp_source =
    if filename = "-" then
      Pxp_types.from_channel stdin 
    else
      Pxp_types.from_file filename in
  let pxp_entry =
    `Entry_document [] in
  let pxp_manager =
    Pxp_ev_parser.create_entity_manager pxp_config pxp_source in
  let next =
    Pxp_ev_parser.create_pull_parser pxp_config pxp_entry pxp_manager in
  
  let path = ref [] in   (* reverse element path *)
  let cont = ref true in
  let end_seen = ref false in

  let data_tab = Hashtbl.create 10 in
    (* Maps path to data *)

  let position = ref (0, 0) in
  let count = ref 0 in

  while !cont do
    match next() with
      | None -> 
	  cont := false

      | Some (E_start_tag(element, _, _, _)) ->
	  path := element :: !path;
	  if element = "page" then 
	    Hashtbl.clear data_tab

      | Some (E_end_tag(element, _)) ->
	  ( match !path with
	      | el :: path' ->
		  assert(el = element);
		  path := path'
	      | [] ->
		  assert false
	  );
	  if element = "page" then (
	    try
	      let (id, doc_boost, doc_details) = analyze_doc data_tab in
	      if wordlist_mode then (
		print_wordlist doc_details
	      )
	      else (
		printf "U\t%Ld\t%f\t%s\n"
		  id doc_boost
		  (Json_io.string_of_json ~compact:true doc_details);
	      );
	      if !count = limit then
		cont := false
	      else
		incr count
	    with
	      | Skip_doc -> ()
	      | error ->
		  eprintf "Exception at/after line %d pos %d: %s\n%!"
		    (fst !position)
		    (snd !position)
		    (Printexc.to_string error)
	  )

      | Some (E_char_data data) ->
	  let s =
	    try Hashtbl.find data_tab !path with Not_found -> "" in
	  let s' =
	    if s = "" then data else s ^ data in
	  Hashtbl.replace data_tab !path s'

      | Some (E_position(_,line,pos)) ->
	  position := (line, pos)

      | Some E_end_of_stream ->
	  end_seen := true

      | Some _ ->
	  ()
  done

(*
  if not !end_seen then
    failwith "End of XML stream is missing"
 *)
;;


let main () =
  let filename = ref "" in
  let limit = ref max_int in
  let wordlist = ref false in
  Arg.parse
    [ "-limit", Arg.Set_int limit, "<n>  Only parse the first n docs";
      "-wordlist", Arg.Set wordlist, "  Only output wordlist";
      "-", Arg.Unit (fun () -> filename := "-"), "  Parse stdin"
    ]
    (fun s -> filename := s)
    "wikiparse <filename>";

  if !filename = "" then failwith "Missing filename";

  if not !wordlist then (
    print_endline "A\ttext_word_count\tS32";
    print_endline "A\ttimestamp\tF64";
  );

  parse !filename !limit !wordlist
;;


let () = main()

