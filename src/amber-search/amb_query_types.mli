(* $Id$ *)

(* This kind of query implies a certain algorithm *)

type feature = string
    (* feature given by name *)

type attribute = string
    (* attribute given by name *)

type att_or_feat = string
    (* attribute or feature name, whatever works better *)


module type QUERY = sig
  type qword
    (** This needs to be specialized by a qword representation *)

  type qword_or_phrase =
      [ `Word of qword * float * bool    (** Word, boost, strict *)
      | `Phrase of qword array * float * bool (** Phrase, boost, strict. Phrase nonempty *)
      ]
    (** Strict flag: If set, the word or phrase must occur in the document.
        If not set, the rank of the document may be improved by an 
        occurrence, which, however, is not required. A document needs
        only to have one of the query words in order to be included
        into the result set.
     *)



  (* A condition is an additional clause. It is evaluated for every doc in
     the potential result set, and only docs remain where it is true.
     The words occurring in conditions are not part of [query_words],
     and do not influence ranking.
   *)

  type condition =
      [ `Doc_mem of att_or_feat * string
          (** The doc must contain this word *)
      | `Att_cmp_int64 of att_modified * comparison * int64
	  (** This att fulfills the comparison with the int64 number *)
      | `Att_cmp_float of att_modified * comparison * float
	  (** This att fulfills the comparison with the float number *)
      | `Att_bitset_all of att_modified * int64
	  (** All 1 bits in the number are set in the attribute *)
      | `Att_bitset_some of att_modified * int64
	  (** Some 1 bits in the number are set in the attribute *)
      | `Not of condition      (** negation *)
      | `And of condition * condition
      | `Or of condition * condition
      ]
	
  and comparison =
      [ `Gt | `Ge | `Lt | `Le | `Eq | `Ne ]

  and att_modified =
    [ `Att of attribute
        (* The attribute as such *)
    | `Bits of att_modified * int * int
        (* Extract bits *)
    | `Signed of att_modified
        (* Handle as signed number *)
    | `Unsigned of att_modified
        (* Handle as unsigned number *)
    ]

  type filter =
      [ `PFilter of pfilter ]

   and pfilter =
     [ `Pos_cmp_int64 of (* bits: *) int * int * comparison * int64
     | `Not of pfilter
     | `And of pfilter * pfilter
     | `Or of pfilter * pfilter
     ]
	
  type query =
      [ `Occurrence of qword_or_phrase list
          (** The words/phrases may occur in the doc. List non-empty *)
      | `Condition of query * condition * filter list
          (** This condition must also hold *)
      | `Union of query * query
	  (** Form the union of the result sets of two queries *)
      | `Sum of query * query
	  (** Form the union of the result sets of two queries.
	      In this variant documents are boosted that occur in both
	      sets.
	   *)
      ]
end


module LQUERY :
  QUERY with type qword = feature * string
  (** Literal query: The qword is simply a string *)



type query_words =
    { q_words : (feature * string) array;
         (** Words occurring in a query. The left string is the feature
             name, and the right string is the term itself
	  *)
      q_freq : int array;
         (** How often the word occurs in the query. Frequencies can be
             counted for the whole query or for parts only (e.g. for 
             the words of a certain feature).
	  *)
    }

module WQUERY :
  QUERY with type qword = int
  (** Weighted query: The qword is the index of the [q_words] array *)
