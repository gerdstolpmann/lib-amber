(* $Id$ *)

{ 
  open Amb_query_parser
}

let identifier =
  [ 'a'-'z' 'A'-'Z' '_' ]  [ 'a'-'z' 'A'-'Z' '_' '0'-'9']*

let sign =
  [ '+' '-' ]

let dec_number =
  [ '0'-'9' ]+

let hex_number =
  "0x" [ '0'-'9' 'A'-'F' 'a'-'f' ]+

let oct_number =
  "0o" [ '0'-'7' ]+

let bin_number =
  "0b" [ '0'-'1' ]+

let number =
  dec_number | hex_number | oct_number | bin_number

let string =
  [^ '"' ]*
  (* TODO: escape sequences *)

let whitespace =
  [ ' ' '\t' '\r' '\n' ]+


rule parse_token = parse
  | identifier
      { let s = Lexing.lexeme lexbuf in
	match s with
	  | "WHERE" -> KW_WHERE
	  | "UNION" -> KW_UNION
	  | "SUM" -> KW_SUM
	  | "OR" -> KW_OR
	  | "AND" -> KW_AND
	  | "NOT" -> KW_NOT
	  | "ALL_BITS" -> KW_ALL_BITS
	  | "SOME_BITS" -> KW_SOME_BITS
          | "PFILTER" -> KW_PFILTER
	  | "SKIP" -> KW_SKIP
	  | _ -> IDENT s
      }
  | sign? number 
      { let s = Lexing.lexeme lexbuf in
	INT64 (Int64.of_string s)
      }
  | sign? dec_number "." dec_number
      { let s = Lexing.lexeme lexbuf in
	FLOAT (float_of_string s)
      }
  | sign? dec_number ("." dec_number)? "E" sign? dec_number
      { let s = Lexing.lexeme lexbuf in
	FLOAT (float_of_string s)
      }
  | '"' string '"' 
      { let s = Lexing.lexeme lexbuf in
	WORD (String.sub s 1 (String.length s - 2))
      }

  | ":"   { SYM_COLON }
  | "*"   { SYM_STAR }
  | "?"   { SYM_QMARK }
  | "["   { SYM_LBRACKET }
  | "]"   { SYM_RBRACKET }
  | "("   { SYM_LPAREN }
  | ")"   { SYM_RPAREN }
  | ">"   { SYM_GT }
  | ">="  { SYM_GE }
  | "<"   { SYM_LT }
  | "<="  { SYM_LE }
  | "="   { SYM_EQ }
  | "<>"  { SYM_NE }
  | ","   { SYM_COMMA }
  | "."   { SYM_DOT }
  | ".."  { SYM_DOTDOT }

  | whitespace 
      { let n = Lexing.lexeme_end lexbuf - Lexing.lexeme_start lexbuf in
	IGNORE n
      }

  | eof { EOF }
