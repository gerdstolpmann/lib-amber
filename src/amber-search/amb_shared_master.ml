(* $Id$ *)

open Printf

type feature_counts = Amb_types.feature_counts
type feature_handle = Amb_shared_feature.feature_handle
type att_handle = Amb_shared_att.att_handle

type ft_handle =
    feature_counts * feature_handle * feature_handle option

module A = Amblib_dump_fmt_aux

module FH = struct 
  type t = feature_handle
  let compare = compare
end

module AH = struct 
  type t = att_handle
  let compare = compare
end

module StrMap = Map.Make(String)
module FHMap = Map.Make(FH)
module AHMap = Map.Make(AH)

type master =
    { mutable generation : int;
      mutable feature_map : ft_handle StrMap.t;
      mutable att_map : att_handle StrMap.t;
      mutable feature_count : int ref FHMap.t;
      mutable att_count : int ref AHMap.t;
      mutable toc : Amblib_dump_fmt_aux.toc option;
    }

type master_ref =
    { master : master Netmcore_ref.sref;
      mutable feature_added : ft_handle StrMap.t;
      mutable att_added : att_handle StrMap.t;
      mutable toc_added : Amblib_dump_fmt_aux.toc option;
    }

type read_handle =
    { master_gen : int;
      feature_snap : ft_handle StrMap.t;
      att_snap : att_handle StrMap.t;
      toc_snap : Amblib_dump_fmt_aux.toc option;
    }

(* Ideas:

   - for reading or writing master_sref, a write lock is required
   - creating a read handle: just copy
       feature_snap := copy feature_map
       att_snap := copy att_map
   - mem management: when a read handle is created, feature_count and
     att_count are increased. When the read handle is closed, the counts
     are decreased. If a count hits 0, the objects can be destroyed.
   - add_feature, add_attribute: check whether the pool_id is ok
   - add_feature, add_attribute: just add the new handles to
     feature_added and att_added
   - release: first decrease the ref count of old features/atts. If the
     counts reach 0, destroy the old objects. Then copy feature_added/att_added
     to feature_map/att_map, resp., and increase the ref count.
 *)

module Master_svar =
  Netplex_sharedvar.Make_var_type (
    struct type t = master Netmcore_ref.sref_descr end
  )


let pool = ref None

let get_pool_id() =
  match !pool with
    | None -> failwith "Pool not initialized"
    | Some p -> p

let init_pool ctrl size =
  let p = Netmcore_mempool.create_mempool size in
  pool := Some p;
  (* Check whether there is an old pool to deallocate *)
  let sockdir = ctrl#controller_config#socket_directory in
  let filename = sockdir ^ "/amber_pool.txt" in
  if Sys.file_exists filename then (
    let f = open_in filename in
    let name = input_line f in
    close_in f;
    (* The file in /dev/shm may be gone because of a system reboot *)
    try Netsys_posix.shm_unlink name with _ -> ()
  );
  let res = Netmcore.get_resource p in
  let name = 
    match res#repr with
      | `Posix_shm_preallocated(name,_) -> name
      | `Posix_shm_preallocated_sc(name,_,_) -> name
      | _ -> assert false in
  let f = open_out filename in
  output_string f (name ^  "\n");
  close_out f


let master_ref = ref None
  (* The handle of the current process *)


let create_master () =
  { generation = 0;
    feature_map = StrMap.empty;
    att_map = StrMap.empty;
    feature_count = FHMap.empty;
    att_count = AHMap.empty;
    toc = None
  }


let get_master_ref () =
  (* Get the resource ID of the master sref *)
  let p = get_pool_id() in
  match !master_ref with
    | Some h ->
	h
    | None ->
	(* This is only necessary once per worker process *)
        let mu = Netplex_mutex.access "Amber.master_mutex" in
        Netplex_mutex.lock mu;
        let create = 
          Netplex_sharedvar.create_var ~enc:true "Amber.master_descr" in
        let master_descr =
          if create then (
	    let m = create_master() in
	    let m_sref = Netmcore_ref.sref p m in
	    let m_descr = Netmcore_ref.descr_of_sref m_sref in
	    Master_svar.set "Amber.master_descr" m_descr;
	    m_descr
	  )
	  else
	    Master_svar.get "Amber.master_descr" in
	let h =
	  { master = Netmcore_ref.sref_of_descr p master_descr;
	    feature_added = StrMap.empty;
	    att_added = StrMap.empty;
            toc_added = None;
	  } in
	master_ref := Some h;
        Netplex_mutex.unlock mu;
	h


let with_lock mref f =
    Netmcore_heap.modify
    (Netmcore_ref.heap mref.master)
    (fun mut ->
       let m = Netmcore_ref.deref_ro mref.master in
       f mut m
    )



let add_feature mref name set ft_counts ft =
  let p = get_pool_id() in
  let fth = Amb_shared_feature.handle_of_feature ft in
  if Amb_shared_feature.pool fth <> p then
    failwith "Amb_shared_master.add_feature: pool mismatch";
  match set with
    | `Base ->
	mref.feature_added <- 
	  StrMap.add name (ft_counts, fth, None) mref.feature_added
    | `Update ->
	let fth_base =
	  with_lock 
	    mref
	    (fun mut m ->
	       let (_,base,_) = 
		 try StrMap.find name m.feature_map
		 with Not_found ->
		   failwith "Amb_shared_master.add_feature: update without base"
	       in
	       Netmcore_heap.copy base
	    ) in
	mref.feature_added <- 
	  StrMap.add name (ft_counts, fth_base, Some fth) mref.feature_added


let add_attribute mref name set atth =
  let p = get_pool_id() in
  if Amb_shared_att.pool atth <> p then
    failwith "Amb_shared_master.add_attribute: pool mismatch";
  
  if set = `Update then
    failwith "Amb_shared_master.add_attribute: `Update not supported";

  mref.att_added <- StrMap.add name atth mref.att_added


let add_toc mref new_toc =
  mref.toc_added <- Some new_toc


let debug = false

let dbg_version x =
  let hex = Digest.to_hex (Digest.string (Marshal.to_string x [])) in
  String.sub hex 0 8


let ft_increase mut m fth name =
  let ucount = 
    try FHMap.find fth m.feature_count
    with Not_found -> 
      let ucount = ref 0 in
      m.feature_count <-
	Netmcore_heap.add mut (FHMap.add fth ucount m.feature_count);
      FHMap.find fth m.feature_count in
  incr ucount;
  if debug then
    eprintf "ft_increase name=%s version=%s count=%d\n%!" 
            name (dbg_version fth) !ucount


let ft_decrease mut m fth name =
  let ucount = 
    try FHMap.find fth m.feature_count
    with Not_found -> assert false in
  decr ucount;
  if !ucount = 0 then (
    Amb_shared_feature.destroy fth;
    m.feature_count <-
      Netmcore_heap.add mut (FHMap.remove fth m.feature_count);
  );
  if debug then
    eprintf "ft_decrease name=%s version=%s count=%d\n%!"
            name (dbg_version fth) !ucount


let att_increase mut m atth name =
  let ucount = 
    try AHMap.find atth m.att_count
    with Not_found -> 
      let ucount = ref 0 in
      m.att_count <-
	Netmcore_heap.add mut (AHMap.add atth ucount m.att_count);
      AHMap.find atth m.att_count in
  incr ucount;
  if debug then
    eprintf "att_increase name=%s version=%s count=%d\n%!" 
            name (dbg_version atth) !ucount



let att_decrease mut m atth name =
  let ucount = 
    try AHMap.find atth m.att_count
    with Not_found -> assert false in
  decr ucount;
  if !ucount = 0 then (
    Amb_shared_att.destroy atth;
    m.att_count <-
      Netmcore_heap.add mut (AHMap.remove atth m.att_count);
  );
  if debug then
    eprintf "att_decrease name=%s version=%s count=%d\n%!"
            name (dbg_version atth) !ucount


let release mref =
  with_lock 
    mref
    (fun mut m ->
       StrMap.iter
	 (fun name (ft_counts, fth_base, fth_upd_opt) ->
	    (* Increase ref count on new handles: *)
	    ft_increase mut m fth_base name;
	    ( match fth_upd_opt with
		| None -> ()
		| Some fth_upd -> ft_increase mut m fth_upd name
	    );
	    (* Decrease ref count on handles being replaced: *)
	    ( try
		let (_, old_base, old_upd_opt) = 
		  StrMap.find name m.feature_map in
		ft_decrease mut m old_base name;
		( match old_upd_opt with
		    | None -> ()
		    | Some old_upd -> ft_decrease mut m old_upd name
		);
	      with Not_found -> ()
	    );
	    (* Add to feature_map *)
	    m.feature_map <-
	      Netmcore_heap.add mut 
	        (StrMap.add 
		   name (ft_counts, fth_base, fth_upd_opt) m.feature_map);
	 )
	 mref.feature_added;

       StrMap.iter
	 (fun name atth ->
	    (* Increase ref count on new handles: *)
	    att_increase mut m atth name;
	    (* Decrease ref count on handles being replaced: *)
	    ( try
		let old_att =
		  StrMap.find name m.att_map in
		att_decrease mut m old_att name;
	      with Not_found -> ()
	    );
	    (* Add to att_map *)
	    m.att_map <-
	      Netmcore_heap.add mut 
	        (StrMap.add name atth m.att_map);
	 )
	 mref.att_added;

       (* toc *)
       ( match mref.toc_added with
           | None -> ()
           | Some new_toc ->
                m.toc <- Netmcore_heap.add mut (Some new_toc)
       );

       (* Increase generation *)
       m.generation <- m.generation + 1
    )


let read mref f =
  let rd =
    with_lock 
      mref
      (fun mut m ->
	 (* Increase use counts for all features and atts: *)
	 StrMap.iter
	   (fun name (ft_counts, fth_base, fth_upd_opt) ->
	      (* Increase ref count on new handles: *)
	      ft_increase mut m fth_base name;
	      ( match fth_upd_opt with
		  | None -> ()
		  | Some fth_upd -> ft_increase mut m fth_upd name
	      );
	   )
	   m.feature_map;
	 StrMap.iter
	   (fun name atth ->
	      (* Increase ref count on new handles: *)
	      att_increase mut m atth name;
	   )
	   m.att_map;
	 (* Return the snapshot: *)
	 { master_gen = m.generation;
	   feature_snap = Netmcore_heap.copy m.feature_map;
	   att_snap = Netmcore_heap.copy m.att_map;
           toc_snap = Netmcore_heap.copy m.toc;
	 }
      ) in
  let post_action() =
    with_lock 
      mref
      (fun mut m ->
	 (* Decrease use counts for all features and atts: *)
	 StrMap.iter
	   (fun name (ft_counts, fth_base, fth_upd_opt) ->
	      ft_decrease mut m fth_base name;
	      ( match fth_upd_opt with
		  | None -> ()
		  | Some fth_upd -> ft_decrease mut m fth_upd name
	      );
	   )
	   rd.feature_snap;
	 StrMap.iter
	   (fun name atth ->
	      att_decrease mut m atth name;
	   )
	   rd.att_snap;
      ) in
  try
    let res = f rd in
    post_action();
    res
  with
    | error ->
	post_action();
	raise error


let num_all_docs rh =
  match rh.toc_snap with
    | None -> 0
    | Some toc -> Amblib_dump.hyper toc.A.all_docs

let num_eff_docs rh =
  match rh.toc_snap with
    | None -> 0
    | Some toc -> Amblib_dump.hyper toc.A.eff_docs

let num_all_docs_base rh =
  match rh.toc_snap with
    | None -> 0
    | Some toc -> Amblib_dump.hyper toc.A.all_docs_base

let num_eff_docs_base rh =
  match rh.toc_snap with
    | None -> 0
    | Some toc -> Amblib_dump.hyper toc.A.eff_docs_base


let get_feature rh name =
  let (ft_counts, fth_base, fth_upd_opt) =
    StrMap.find name rh.feature_snap in
  (ft_counts,
   Amb_shared_feature.feature_of_handle fth_base,
   ( match fth_upd_opt with
       | None -> None
       | Some fth_upd -> Some(Amb_shared_feature.feature_of_handle fth_upd)
   )
  )

let get_attribute rh name =
  let atth = StrMap.find name rh.att_snap in
  Amb_shared_att.get_att atth

let user_docid rh =
  let att = get_attribute rh "@udocid" in
  (fun docid -> Amblib_attvector.Attribute.get_int64 att docid)

let user_docid_array rh =
  Array.map (user_docid rh)

let boost rh =
  let att = get_attribute rh "@boost" in
  (fun docid -> Amblib_attvector.Attribute.get_float att docid)

let boost_array rh =
  Array.map (boost rh)

let exists rh =
  let b = boost rh in
  (fun docid -> b docid > 0.0)

let exists_array rh =
  Array.map (exists rh)
