(* $Id$ *)

open Amb_types
open Printf

type shared_tvector_rec =
    { fth : Amb_shared_feature.feature_handle;
      ft : Amb_shared_feature.feature;
      term_index : int
    }

type shared_tvector =
  | Empty
  | From_feature of shared_tvector_rec


type tvector_iter =
    { slices : (Amb_types.tvector_slice_head * bytes * int * int) array;
      mutable cur_slice : int;
      mutable cur_head : Amb_types.tvector_slice_head;
      mutable cur_docids : int array;
      mutable cur_weights : Amblib_zslice.half_float array;
      mutable cur_pos_idx : int;
      mutable cur_pos_decoder : Amblib_zslice.pos_decoder;
    }

exception Empty_slice



let get_tvector ft name =
  let fth = Amb_shared_feature.handle_of_feature ft in
  let term_index = Amb_shared_feature.find_term ft name in
  From_feature { fth; ft; term_index }

let empty_tvector () =
  Empty


let decode_short_repr data p len =
    let (cur_docids, cur_weights, cur_pos_idx) = 
      Amblib_zslice.decode_slice data p len in
    if cur_docids = [| |] then raise End_of_file;
    let cur_head =
      { tvec_slice_start = 0;
        tvec_slice_count = Array.length cur_docids;
        tvec_slice_min_docid = cur_docids.(0);
        tvec_slice_max_docid = cur_docids.(Array.length cur_docids - 1);
        tvec_rest_max_wdt = 
          Amblib_zslice.float_of_half 
            (Array.fold_left
               (fun acc w -> max acc w)
               (Amblib_zslice.half_of_float 0.0)
               cur_weights)
      } in
    let cur_pos_decoder =
      Amblib_zslice.create_pos_decoder
        data cur_pos_idx (p + len - cur_pos_idx) (Array.length cur_docids) in
    { slices = [| cur_head, data, p, len |];
      cur_slice = 0;
      cur_head;
      cur_docids;
      cur_weights;
      cur_pos_idx;
      cur_pos_decoder;
    }


let decode_sliced_repr slices cur_slice =
  let (cur_head, data, p, len) = slices.(cur_slice) in
  let (cur_docids, cur_weights, cur_pos_idx) = 
    Amblib_zslice.decode_slice data p len in
  if cur_docids = [| |] then raise Empty_slice;
  let cur_pos_decoder =
    Amblib_zslice.create_pos_decoder
      data cur_pos_idx (p + len - cur_pos_idx) (Array.length cur_docids) in
(* eprintf "create_pos_decoder k_pos=%d k_end=%d\n%!" cur_pos_idx (p+len); *)
  { slices;
    cur_slice;
    cur_head;
    cur_docids;
    cur_weights;
    cur_pos_idx;
    cur_pos_decoder;
  }
    

let rec decode_slice ttvec cur_slice =
  match ttvec with
    | Empty ->
         raise End_of_file
    | From_feature tvec ->
         try
           if cur_slice > 0 then raise Not_found;
           let (data,p,len) =
             Amb_shared_feature.get_shortvector tvec.ft tvec.term_index in
           decode_short_repr data p len
         with Not_found ->
           try
             let slices =
               Amb_shared_feature.get_sliced_vector tvec.ft tvec.term_index in
             if cur_slice >= Array.length slices then raise End_of_file;
             decode_sliced_repr slices cur_slice
           with 
             | Not_found ->
                  failwith "Amb_tvector.decode_tvector: could neither find a \
                            shortvector nor a sliced vector"
             | Empty_slice ->
                  decode_slice ttvec (cur_slice+1)


let decode_tvector tvec =
  decode_slice tvec 0
    


let min_docid slices k =
  let (head,_,_,_) = slices.(k) in
  head.tvec_slice_min_docid


let max_docid slices k =
  let (head,_,_,_) = slices.(k) in
  head.tvec_slice_max_docid


let mem_tvector ttvec docid =
  (* test whether docid is member of the vector *)
  match ttvec with
    | Empty ->
         false
    | From_feature tvec ->
         try
           let (data,p,len) =
             Amb_shared_feature.get_shortvector tvec.ft tvec.term_index in
           Amblib_zslice.mem_slice docid data p len
         with Not_found ->
           ( try
               let slices =
                 Amb_shared_feature.get_sliced_vector tvec.ft tvec.term_index in
               (* FIXME: use a binary search algorithm instead *)
               let cur_slice = ref 0 in
               while !cur_slice < Array.length slices - 1 && 
                       max_docid slices !cur_slice < docid 
               do
                 incr cur_slice
               done;
               let (cur_head, data, p, len) = slices.( !cur_slice ) in
               docid >= min_docid slices !cur_slice &&
                 Amblib_zslice.mem_slice docid data p len
             with 
               | Not_found ->
                    failwith "Amb_tvector.decode_tvector_at: could neither \
                              find a shortvector nor a sliced vector"
               | Empty_slice ->
                    false
           )
           

let rec next_tvector_slice iter =
  let cur_slice = iter.cur_slice + 1 in
  if cur_slice >= Array.length iter.slices then raise End_of_file;
  try
    decode_sliced_repr iter.slices cur_slice
  with
    | Empty_slice ->
         next_tvector_slice iter


let slice_docids iter = iter.cur_docids
let slice_weights iter = iter.cur_weights
let slice_head iter = iter.cur_head


let decode_position_array iter =
  let (_, data, k, len) = iter.slices.( iter.cur_slice ) in
  let k_end = k+len in
  let k_pos = iter.cur_pos_idx in
(* eprintf "decode_position_array k_pos=%d k_end=%d\n%!" k_pos k_end; *)
  let pa =
    Amblib_zslice.decode_slice_positions
      data k_pos (k_end - k_pos) (Array.length iter.cur_docids) in
  (* DEBUG *)
(*
  eprintf "docid/pos\n";
  Array.iteri
    (fun i docid ->
       let plist = Array.to_list pa.(i) in
       eprintf "%d/%s\n" docid (String.concat "," (List.map string_of_int plist))
    )
    iter.cur_docids;
 *)
  pa


let position_decoder iter =
  iter.cur_pos_decoder


let decode_positions iter i =
  let pos_decoder = position_decoder iter in
  Amblib_zslice.get_positions pos_decoder i


let rec count_size_tvector f_exists iter acc =
  let acc' =
    Array.fold_left
      (fun acc docid ->
        if f_exists docid then acc+1 else acc
      )
      acc
      iter.cur_docids in
  let iter' =
    try Some(next_tvector_slice iter)
    with End_of_file -> None in
  match iter' with
    | Some iter' -> count_size_tvector f_exists iter' acc'
    | None -> acc'
   


let eff_size_tvector rh ttvec =
  match ttvec with
    | Empty ->
         0
    | From_feature tvec ->
         try
           Amblib_dump.hyper
             (Amb_shared_feature.term_eff_docs tvec.ft tvec.term_index)
         with
           | Not_found ->
                try
                  count_size_tvector
                    (Amb_shared_master.exists rh)
                    (decode_tvector ttvec)
                    0
                with End_of_file -> 0
                                      
