(* $Id$ *)

(** Representation of an attribute in shared memory *)

type att_handle
  (** A value in normal memory containing the descriptors to shared mem *)

val create_att : Netmcore.res_id -> Amblib_dump_fmt_aux.attribute -> att_handle
  (** Creates the attribute structure in shared mem for the given pool ID *)

val get_att : att_handle -> Amblib_attvector.Attribute.att_vector
  (** Returns the attribute object living in shared memory *)

val pool : att_handle -> Netmcore.res_id
val destroy : att_handle -> unit
