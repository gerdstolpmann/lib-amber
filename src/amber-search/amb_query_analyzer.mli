(* $Id$ *)

exception Syntax_error

val l_analyze : string -> Amb_query_types.LQUERY.query

val w_analyze : string -> 
                   Amb_query_types.query_words * Amb_query_types.WQUERY.query
