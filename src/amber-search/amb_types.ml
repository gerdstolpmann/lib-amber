(* $Id$ *)

type tvector_slice_head =
    { tvec_slice_start : int;  (** where this slice starts *)
      tvec_slice_count : int;  (** elements in this slice *)
      tvec_slice_min_docid : int;   (** smallest docid *)
      tvec_slice_max_docid : int;   (** biggest docid *)
      tvec_rest_max_wdt : float;    (** biggest doc weight w(d,t) *)
    }
 (** Head of a decoded slice of a term vector. The whole vector has
     length [tvec_size], and this slice starts at index
     [tvec_slice_start] and has a lenght of [tvec_slice_count]. The
     first doc in the slice has the smallest docid.
     The last doc has the biggest docid. The docs should also roughly
     by ordered by weights so that docs with bigger weight come first.
     It is, however, not required to stick to this ordering strictly.
     In [tvec_rest_max_docw] the biggest weight of the whole following
     vector is stored (i.e. of this slice and all following slices).
  *)

type feature_counts =
    { ft_all_docs : int64;  (** number of docs, including deleted *)
      ft_eff_docs : int64;  (** number of non-deleted docs *)
    }

type set_kind = [ `Base | `Update ]
