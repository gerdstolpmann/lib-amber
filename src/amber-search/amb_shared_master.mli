(* $Id$ *)

(** Access to shared memory *)

type master_ref
  (** A value in normal memory containing the "pointers" to shared mem *)

type read_handle
  (** This type of handle allows consistent read accesses of a snapshot *)

val init_pool : Netplex_types.controller -> int -> unit
  (** Sets the size of the pool. This must happen before forking! *)

val get_pool_id : unit -> Netmcore.res_id
  (** return the pool ID *)

val get_master_ref : unit -> master_ref
  (** Get the master handle. This function must be used from Netplex-managed
      child processes.
   *)

val add_feature : master_ref -> string -> Amb_types.set_kind ->
                  Amb_types.feature_counts ->
                  Amb_shared_feature.feature -> unit
  (** [add_feature mh name kind counts fh]: Adds a `Base or `Update feature.
      Adding an `Update is only possible after adding a `Base feature,
      which can only be added once.

      The new feature is first visible when [release] is called.
   *)

val add_attribute : master_ref -> string -> Amb_types.set_kind ->
                    Amb_shared_att.att_handle -> unit
  (** Adds/replaces an attribute

      The new attribute is first visible when [release] is called.

      Restriction: currently, 'Update attributes are unsupported here.
   *)

val add_toc : master_ref -> Amblib_dump_fmt_aux.toc -> unit
  (** Load the table of contents *)

val release : master_ref -> unit
  (** Makes changes visible (all at once in an atomic operation) *)

val read : master_ref -> (read_handle -> 'a) -> 'a
  (** [read mh f]: Sets up a read handle and calls [f] with it. While
      [f] is running, it is guaranteed that the objects accessed with the
      read handle exist, and are consistent.
   *)

val num_all_docs : read_handle -> int
  (** Number of all documents (base+update sets), including deleted docs.
      The doc IDs have numbers 1 .. num_all_docs
   *)

val num_eff_docs : read_handle -> int
  (** Number of non-deleted documents in base+update. *)

val num_all_docs_base : read_handle -> int
  (** Number of documents in the base set, including deleted docs.
      The doc IDs in the base set have numbers 1 .. num_all_docs_base,
      and the doc IDs in the update set have numbers
      num_all_docs_base+1 .. num_all_docs.
   *)

val num_eff_docs_base : read_handle -> int
  (** Number of non-deleted documents in base only. *)

val get_feature : read_handle -> string ->
                  (Amb_types.feature_counts *
                   Amb_shared_feature.feature *
                   Amb_shared_feature.feature option)
  (** [get_feature rh name]: Looks up the feature, and returns
      [(counts, base_feature, update_feature)]. 
      The update feature is only optional.

      Raises [Not_found] if the feature does not exist.
   *)


val get_attribute : read_handle -> string -> 
                      Amblib_attvector.Attribute.att_vector
  (** [get_attribute rh name]: Looks up the attribute, and returns it.

      Raises [Not_found] if the attribute does not exist.
   *)

val user_docid : read_handle -> int -> int64
  (** [user_docid rh docid]: returns the user docID for the docID *)

val user_docid_array : read_handle -> int array -> int64 array
  (** An array version of [user_docid] *)

val boost : read_handle -> int -> float
  (** [boost rh docid]: returns the boost value for the docID. A boost
      of zero means that the doc does not exist
   *)

val boost_array : read_handle -> int array -> float array
  (** An array version of [boost] *)

val exists : read_handle -> int -> bool
  (** [exists rh docid]: whether the doc exists (boost is positive) *)

val exists_array : read_handle -> int array -> bool array
  (** An array version of [exists] *)



