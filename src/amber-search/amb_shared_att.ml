(* $Id$ *)

open Printf

module A = Amblib_dump_fmt_aux

type att_handle =
    { descr : Amblib_attvector.Attribute.att_vector Netmcore_ref.sref_descr;
      pool : Netmcore.res_id
    }

let create_att pool_id att_obj =
  try
    let att_type = Amblib_attvector.typ_of_string A.(att_obj.att_type) in
    let att_size = Amblib_dump.hyper A.(att_obj.att_size) in
    let att = Amblib_attvector.Attribute.create_for_size att_type att_size in
    Amblib_attvector.Attribute.expand att att_size;
    Amblib_attvector.Attribute.set_immutable_size att;
    let mem = Amblib_attvector.Attribute.memory att in
    let att_value = A.(att_obj.att_value) in
    if Bigarray.Array1.dim mem <> att_value#length then
      failwith 
        (sprintf "length mismatch, \
                  bigarray_dim=%d byte_length=%d att_size=%d att_type=%s"
                 (Bigarray.Array1.dim mem)
                 (att_value#length)
                 att_size
                 A.(att_obj.att_type)
        );
    att_value # blit_to_memory 0 mem 0 att_value#length;
    let r = Netmcore_ref.sref pool_id att in
    let d = Netmcore_ref.descr_of_sref r in
    { descr = d;
      pool = pool_id
    }
  with
    | Failure msg ->
	failwith ("Cannot load attribute '" ^ A.(att_obj.att_name) ^ "': " ^ 
		    msg)

let get_att handle =
  let r = Netmcore_ref.sref_of_descr handle.pool handle.descr in
  Netmcore_ref.deref_ro r


let pool handle = handle.pool

let destroy handle =
  let r = Netmcore_ref.sref_of_descr handle.pool handle.descr in
  Netmcore_heap.destroy (Netmcore_ref.heap r)
