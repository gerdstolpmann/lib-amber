(* $Id$ *)

open Amb_types
open Printf

module A = Amblib_dump_fmt_aux

let dec_set_kind k =
  if k = A.base then
    `Base
  else 
    if k = A.update then
      `Update
    else
      failwith "dec_set_kind"


let load ?(verbose=false) file =
  let mref = Amb_shared_master.get_master_ref() in
  let pool = Amb_shared_master.get_pool_id() in
  let rd = Amblib_dump.open_dump_file file in
  let set = ref `Base in
  let features = Hashtbl.create 17 in
  let feature_counts = Hashtbl.create 17 in

  let stats = ref ("", 0, 0, 0) in
  let display_stats() =
    let (s_name, s_vs, s_sv, s_total) = !stats in
    if s_name <> "" then
      eprintf
        "Loading feature %s: loaded %d shortvectors and %d vectorslices\n%!" 
        s_name s_sv s_vs in
  let maybe_display_stats() =
    let (s_name, s_vs, s_sv, s_total) = !stats in
    if s_total mod 100000 = 0 then
      display_stats() in
  let add_vs name =
    let (s_name, s_vs, s_sv, s_total) = !stats in
    if s_name = name then (
      stats := (s_name, s_vs+1, s_sv, s_total+1);
      maybe_display_stats()
    )
    else (
      display_stats();
      stats := (name, 1, 0, 1)
    ) in
  let add_sv name =
    let (s_name, s_vs, s_sv, s_total) = !stats in
    if s_name = name then (
      stats := (s_name, s_vs, s_sv+1, s_total+1);
      maybe_display_stats()
    )
    else (
      display_stats();
      stats := (name, 0, 1, 1)
    ) in

  ( try
      while true do
        let obj = Amblib_dump.read_object rd in
        match obj with
          | `toc (toc : Amblib_dump_fmt_aux.toc) ->
              if verbose then
                eprintf "Loading TOC\n%!";
               set := dec_set_kind toc.A.set;
               Array.iteri
                 (fun i name ->
                    let counts =
                      { ft_all_docs = toc.A.ft_all_docs.(i);
                        ft_eff_docs = toc.A.ft_eff_docs.(i);
                      } in
                    Hashtbl.add feature_counts name counts
                 )
                 toc.A.ft_names;
               Amb_shared_master.add_toc mref toc
          | `attribute att ->
              if verbose then
                eprintf "Loading attribute %s\n%!" att.A.att_name;
               let atth = Amb_shared_att.create_att pool att in
               let set = dec_set_kind A.(att.att_set) in
               Amb_shared_master.add_attribute mref A.(att.att_name) set atth
          | `feature ftdump ->
              let name =  A.(ftdump.ft_name) in
              let ft = Amb_shared_feature.create_feature pool ftdump in
              let counts =
                try Hashtbl.find feature_counts name
                with Not_found ->
                  failwith ("Amb_loader: missing toc entry for feature: " ^ 
                              name) in
              Amb_shared_master.add_feature mref name !set counts ft;
              Hashtbl.replace features ftdump.A.ft_ftid (ft,name);
          | `vectorslice vs ->
              let (ft,ft_name) =
                 try Hashtbl.find features vs.A.sl_ftid
                 with Not_found ->
                   failwith
                     ("Vectorslice: reference to feature not resolvable") in
              if verbose then add_vs ft_name;
               Amb_shared_feature.load_vectorslice ft vs
          | `shortvector svec ->
               let (ft, ft_name) =
                 try Hashtbl.find features svec.A.ssl_ftid
                 with Not_found ->
                   failwith
                     ("Shortvector: reference to feature not resolvable") in
              if verbose then add_sv ft_name;
              Amb_shared_feature.load_shortvector ft svec
          | `endmark -> 
              if verbose then (
                display_stats();
                eprintf "Endmark\n%!";
              );
              raise End_of_file
      done
    with End_of_file ->
      Amblib_dump.close_dump_reader rd;
  );
  Amb_shared_master.release mref
