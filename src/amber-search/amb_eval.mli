(* $Id$ *)

(** Evaluator *)

type rvector_head =
    { rvec_size : int;           (** returned size *)
      rvec_min_size : int;       (** a lower bound of the total size *)
      rvec_est_size : int;       (** estimated size of the total vector *)
      rvec_time_exceeded : bool; (** whether the time limit was exceeded *)
    }

type rvector =
    { rvec_head : rvector_head;
      rvec_docid : int array;      (** internal document IDs *)
      rvec_udocid : int64 array;   (** user-supplied document IDs *)
      rvec_score : float array;    (** Scores (higher=better) *)
      rvec_pos : int array array;  (** term positions per document *)
    }


val eval : Amb_shared_master.read_handle ->
           Amb_query_types.query_words ->
           Amb_query_types.WQUERY.query ->
           len:int ->
           time:float ->
           include_positions:bool ->
           exact:bool ->
           phrase_shift:int ->
           unit ->
             rvector
  (** Evaluates the query for this index. Only a prefix of the total result
      vector is computed. This prefix has length [len]. Furthermore, the
      time for collecting the result set (before sorting) is limited by
      [time]

      [exact]: if true, [rvec_min_size] will contain the exact number
      of elements in the total vector. Otherwise, it is just a lower
      bound.

      [include_positions]: if true, [rvec_pos] includes results. Otherwise
      it is an empty array.

      [phrase_shift]: the number of bits to shift the position integers to
      the right before using the positions for phrase matching. (Set to 0
      if you don't use this feature.)
   *)


val count : Amb_shared_master.read_handle ->
            Amb_query_types.query_words ->
            Amb_query_types.WQUERY.query ->
            (*time:float ->
            exact:bool ->*)
            phrase_shift:int ->
            unit ->
             rvector_head
  (** Count the result set size. If [exact] the number [rvec_min_size] is exact
   *)
