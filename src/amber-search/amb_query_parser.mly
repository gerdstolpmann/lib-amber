%{  (* $Id$ *)

  (* Result types: Similar to Amb_query_types, but the qword is a string,
     not an int
   *)
  open Amb_query_types.LQUERY

  let att_cmp (name, cmp, number) =
    match number with
      | `Int64 n -> `Att_cmp_int64(name,cmp,n)
      | `Float n -> `Att_cmp_float(name,cmp,n)

%}

%token KW_WHERE
%token KW_UNION
%token KW_SUM
%token KW_OR
%token KW_AND
%token KW_NOT
%token KW_ALL_BITS
%token KW_SOME_BITS
%token KW_PFILTER
%token KW_SKIP

%token SYM_COLON
%token SYM_STAR
%token SYM_QMARK
%token SYM_LBRACKET
%token SYM_RBRACKET
%token SYM_LPAREN
%token SYM_RPAREN
%token SYM_COMMA
%token SYM_GT
%token SYM_LT
%token SYM_GE
%token SYM_LE
%token SYM_EQ
%token SYM_NE
%token SYM_DOT
%token SYM_DOTDOT

%token<string> WORD
%token<int64> INT64
%token<float> FLOAT
%token<string> IDENT

%token<int> IGNORE

%token EOF

%left KW_OR
%left KW_AND
%nonassoc KW_NOT
%left KW_WHERE
%left KW_UNION
%left KW_SUM
%left SYM_COMMA

%start query
%type<Amb_query_types.LQUERY.query> query

%%

query: 
  expr EOF
    { $1 }

expr:
| expr SYM_COMMA expr
    { let left = 
        match $1 with
          | `Occurrence l -> l
          | _ -> failwith "not allowed" in
      let right = 
        match $3 with
          | `Occurrence l -> l
          | _ -> failwith "not allowed" in
      `Occurrence (left @ right)
    }
| expr KW_UNION expr
    { `Union($1, $3) }
| expr KW_SUM expr
    { `Sum($1, $3) }
| expr KW_WHERE att_query filters
    { `Condition($1, $3, $4) }
| SYM_LPAREN expr SYM_RPAREN
    { $2 }
| term 
    { `Occurrence [ $1 ] }

term:
  IDENT SYM_COLON word_or_phrase boost_opt nonstrict_opt
    { let wop =
        Array.map (fun w -> ($1, w)) $3 in
      match wop with
        | [| |] -> assert false
	| [| word |] ->
	    `Word(word, $4, $5)
        | phrase ->
	    `Phrase(phrase, $4, $5)
    }

word_or_phrase:
  WORD
    { [| $1 |] }
| SYM_LBRACKET word_list SYM_RBRACKET
    { Array.of_list $2 }

word_list:
  WORD
    { [$1] }
| WORD word_list
    { $1 :: $2 }
| KW_SKIP
    { [ "" ] }
| KW_SKIP word_list
    { "" :: $2 }


boost_opt:
  SYM_STAR FLOAT
    { $2 }
|
    { 1.0 }

nonstrict_opt:
  SYM_QMARK
    { false }
|
    { true }

att_query:
  att_query KW_OR att_query
    { `Or($1, $3) }
| att_query KW_AND att_query
    { `And($1, $3) }
| KW_NOT att_query
    { `Not $2 }
| condition
    { $1 }
| SYM_LPAREN att_query SYM_RPAREN
    { $2 }

condition:
  IDENT SYM_COLON WORD
    { `Doc_mem($1, $3) }
| att_mod SYM_GT number
    { att_cmp ($1, `Gt, $3) }
| att_mod SYM_GE number
    { att_cmp ($1, `Ge, $3) }
| att_mod SYM_LT number
    { att_cmp ($1, `Lt, $3) }
| att_mod SYM_LE number
    { att_cmp ($1, `Le, $3) }
| att_mod SYM_EQ number
    { att_cmp ($1, `Eq, $3) }
| att_mod SYM_NE number
    { att_cmp ($1, `Ne, $3) }
| att_mod KW_ALL_BITS INT64
    { `Att_bitset_all($1, $3) }
| att_mod KW_SOME_BITS INT64
    { `Att_bitset_some($1, $3) }

att_mod:
  IDENT
    { `Att $1 }
| att_mod SYM_LBRACKET INT64 SYM_DOTDOT INT64 SYM_RBRACKET
    { `Bits($1, Int64.to_int $3, Int64.to_int $5) }
| att_mod SYM_DOT IDENT
    { match $3 with
        | "s" | "signed" | "S" | "SIGNED" ->
            `Signed $1
        | "u" | "unsigned" | "U" | "UNSIGNED" ->
            `Unsigned $1
        | _ ->
           failwith ("Bad attribute conversion: " ^ $3)
    }

filters:
   pfilter filters
     { `PFilter $1 :: $2 }
|
     { [] }

pfilter:
   KW_PFILTER pfilter_expr
     { $2 }

pfilter_expr:
  position SYM_GT INT64
    { let bit_s, bit_e = $1 in
      `Pos_cmp_int64(bit_s, bit_e, `Gt, $3)
    }
| position SYM_GE INT64
    { let bit_s, bit_e = $1 in
      `Pos_cmp_int64(bit_s, bit_e, `Ge, $3)
    }
| position SYM_LT INT64
    { let bit_s, bit_e = $1 in
      `Pos_cmp_int64(bit_s, bit_e, `Lt, $3)
    }
| position SYM_LE INT64
    { let bit_s, bit_e = $1 in
      `Pos_cmp_int64(bit_s, bit_e, `Le, $3)
    }
| position SYM_EQ INT64
    { let bit_s, bit_e = $1 in
      `Pos_cmp_int64(bit_s, bit_e, `Eq, $3)
    }
| position SYM_NE INT64
    { let bit_s, bit_e = $1 in
      `Pos_cmp_int64(bit_s, bit_e, `Ne, $3)
    }
| pfilter_expr KW_OR pfilter_expr
    { `Or($1,$3) }
| pfilter_expr KW_AND pfilter_expr
    { `And($1,$3) }
| KW_NOT pfilter_expr
    { `Not $2 }
| SYM_LPAREN pfilter_expr SYM_RPAREN
    { $2 }

position:
  IDENT
    { if $1 <> "p" then failwith "Only 'p' is allowed here";
      (0, 62)
    }
| IDENT SYM_LBRACKET INT64 SYM_DOTDOT INT64 SYM_RBRACKET
    { if $1 <> "p" then failwith "Only 'p' is allowed here";
      (Int64.to_int $3, Int64.to_int $5)
    }



number:
  INT64
    { `Int64 $1 }
| FLOAT
    { `Float $1 }


%%

