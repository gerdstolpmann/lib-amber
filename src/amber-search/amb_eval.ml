(* $Id$ *)


open Amb_query_types
open Amb_types
open Printf

(* Interface types: *)

type rvector_head =
  { rvec_size : int;
    rvec_min_size : int;
    rvec_est_size : int;
    rvec_time_exceeded : bool;
  }

type rvector =
  { rvec_head : rvector_head;
    rvec_docid : int array;
    rvec_udocid : int64 array;
    rvec_score : float array;
    rvec_pos : int array array;
  }


(* Types during computation: *)

class type walker =
object
  method end_reached : bool
  (* This (empty) slice marks the end of the vector *)

  method min_docid : int
  (* The docid's in this slice and the following slices are >= this number *)

  method max_score : float
    (* The scores in this slice and the following slices are <= this value *)

  method est_size : float
    (* estimated size of the vector *)

  method slice : (int * float) list
    (* The current slice of docid's and scores. Note that the slice can
       be empty
     *)

  method positions : int array array
    (* The position array, in the same order as [slice]
     *)

  method positions_for : int list -> int array list
    (* Request the position array only for certain elements *)

  method next : unit -> unit
    (* Switch to the next slice *)

end

(* Improved extraction of positions:

   method positions_for : int list -> int array list
     (* The input list contains the index positions of the documents to
        decode. The index position is the position in the [slice] list.
      *)

   This makes only sense when we have an iterative position decoder,
   and it is only faster if [positions_for] contains only a few entries
   when it reaches qword_walker.
 *)

(* Remember that vectors are sorted by docid. This also means that [walker]
   returns the documents in docid order.
 *)


class delegate (w:walker) : walker =
object(self)
  method end_reached = w#end_reached
  method min_docid = w#min_docid
  method max_score = w#max_score
  method est_size = w#est_size
  method slice = w#slice
  method positions = w#positions
  method positions_for = w#positions_for
  method next = w#next
end


class qword_walker rd query_words qw qb : walker =
  (* qw: index of the word in the query to look at
     qb: query boost
   *)
  (* The score of the doc is S'(q,d) = w(d,t) * B(d) * w(q,t).
     w(d,t) is stored in the term vector for every occurrence.
     B(d) can be retrieved from the attribute "@boost".
     w(q,t) can be computed for query_words 
   *)
  let (feature,term) = query_words.q_words.(qw) in
  let (counts, base_ft, _) = Amb_shared_master.get_feature rd feature in
  (* FIXME: allow to select upd_ft *)

  let tvector = 
    try Amb_tvector.get_tvector base_ft term 
    with Not_found -> Amb_tvector.empty_tvector() in
  let tvec_size =
    Amb_tvector.eff_size_tvector rd tvector in
  let get_boost =
    Amb_shared_master.boost rd in
  let n_docs =
    Amb_shared_master.num_eff_docs rd in
  let wqt = 
    qb *. log(1.0 +. float n_docs /. float tvec_size) in

object(self)  
  val mutable state = `Beginning

  val mutable eof = false
  val mutable slice = []
  val mutable positions = [||]    (* only filled on demand *)
  val mutable min_docid = 0
  val mutable max_score = infinity

  method private manage_iteration() =
    (* Turn "directives" like `Beginning or `Next into "states".
       This method is idempotent.
     *)
    match state with
      | `Beginning ->
	  ( try 
	      let i = Amb_tvector.decode_tvector tvector in
	      state <- `Iter i;
	      self # copy_slice i;
	    with End_of_file ->
	      state <- `End;
	      self # set_eof()
	  )
      | `Next i ->
	  ( try
	      let i' = Amb_tvector.next_tvector_slice i in
	      state <- `Iter i';
	      self # copy_slice i';
	    with End_of_file ->
	      state <- `End;
	      self # set_eof()
	  )
      | `Iter _ ->
	  ()
      | `End ->
	  ()

  method private copy_slice i =
    let docids = Amb_tvector.slice_docids i in
    let weights = Amb_tvector.slice_weights i in
    let head = Amb_tvector.slice_head i in
    let pairs =
      List.combine (Array.to_list docids) (Array.to_list weights) in
    let tuples =
      Array.to_list
      (Array.mapi
        (fun k (docid,weight) ->
           let _Bd = get_boost docid in
           (k, docid, weight, _Bd)
        )
        (Array.of_list pairs)) in
    let ex_tuples =  (* non-positive boosts mean that the doc is deleted *)
      List.filter (fun (_,_,_,_Bd) -> _Bd > 0.0) tuples in
    let scores =
      List.map
        (fun (k,docid,weight,_Bd) ->
           let score = Amblib_zslice.float_of_half weight *. _Bd *. wqt in
           (k,docid,score)
        )
        ex_tuples in
   
    slice <- scores;
    min_docid <- fst(List.hd pairs);

    (* Remember that the docs in the term vectors are sorted by B(d), in 
       descending order. So the B(d) value of the first doc is the maximum
       value we can find in the rest of the vector
     *)
    ( match ex_tuples with
        | (_,_,_,_Bd) :: _ ->
             max_score <- head.tvec_rest_max_wdt *. _Bd *. wqt
        | [] ->
            (* FIXME: get _BD from next slice 

               As a simple workaround, we leave the max_score unchanged,
               i.e. take it from the previous slice.
             *)
            ()
    );

    positions <- [||]

  method private set_eof() =
    eof <- true;
    slice <- [];
    min_docid <- max_int;
    max_score <- 0.0


  method private copy_positions i =
    positions <- Amb_tvector.decode_position_array i


  method positions =
    self # manage_iteration();
    match state with
      | `Iter i when positions=[||] ->
	  self # copy_positions i;
	  positions
      | `End ->
	  [||]
      | _ ->
	  positions

  method positions_for l =
    self # manage_iteration();
    (* ignore(self#positions); *)  (* DEBUG *)
    (* List.map (fun idx -> p.(idx)) l*)
    match state with
      | `Iter i ->
          let dec = Amb_tvector.position_decoder i in
          List.map (fun idx -> Amblib_zslice.get_positions dec idx) l
      | `End ->
          List.map (fun _ -> [| |]) l
      | _ ->
          assert false
   	  
  method end_reached = 
    self # manage_iteration(); eof

  method min_docid =
    self # manage_iteration(); min_docid

  method max_score =
    self # manage_iteration(); max_score

  method est_size =
    float tvec_size

  method slice =
    self # manage_iteration();
    List.map (fun (_,docid,score) -> (docid,score)) slice

  method next() =
    self # manage_iteration();
    match state with
      | `Beginning -> 
	  assert false
      | `Next _ ->
	  assert false
      | `Iter i ->
	  state <- `Next i
      | `End ->
	  ()
end


let phrase_match p1 p2 phrase_offset phrase_shift =
  (* First check whether there is a match before allocating any memory: *)
  (* Note: assuming the position arrays p1,p2 are SORTED! *)
  let l1 = Array.length p1 in
  let l2 = Array.length p2 in
  let k1 = ref 0 in
  let k2 = ref 0 in
  let matches = ref 0 in
  while !k1 < l1 && !k2 < l2 do
    let q1 = p1.( !k1 ) in
    let r1 = q1 asr phrase_shift in
    let q2 = p2.( !k2 ) in
    let r2 = (q2 asr phrase_shift) - phrase_offset in
    if r1 = r2 then (
      incr matches;
      incr k1;
      incr k2
    )
    else (
      if r1 < r2 then
	incr k1
      else
	incr k2
    )
  done;
  (* If there is a match, return the positions: *)
  if !matches > 0 then (
    let presult = Array.make !matches 0 in
    k1 := 0;
    k2 := 0;
    matches := 0;
    while !k1 < l1 && !k2 < l2 do
      let q1 = p1.( !k1 ) in
      let r1 = q1 asr phrase_shift in
      let q2 = p2.( !k2 ) in
      let r2 = (q2 asr phrase_shift) - phrase_offset in
      if r1 = r2 then (
	presult.(!matches) <- q1;   (* the UNSHIFTED version *)
	incr matches;
	incr k1;
	incr k2
      )
      else (
	if r1 < r2 then
	  incr k1
	else
	  incr k2
      )
    done;
    presult
  )
  else
    [| |]


let merge_positions p1 p2 =
  let i1 = ref 0 in
  let i2 = ref 0 in
  let l1 = Array.length p1 in
  let l2 = Array.length p2 in
  let out = ref [] in

  while !i1 < l1 && !i2 < l2 do
    let q1 = p1.( !i1 ) in
    let q2 = p2.( !i2 ) in
    if q1=q2 then (
      out := q1 :: !out;
      incr i1;
      incr i2
    )
    else if q1 < q2 then (
      out := q1 :: !out;
      incr i1;
    ) else (
      out := q2 :: !out;
      incr i2;
    )
  done;
  
  if !i1 < l1 then (
    out := List.rev_append (Array.to_list (Array.sub p1 !i1 (l1 - !i1))) !out;
  );

  if !i2 < l2 then (
    out := List.rev_append (Array.to_list (Array.sub p2 !i2 (l2 - !i2))) !out;
  );

  Array.of_list (List.rev !out)


let repeat n f =
  for k = 1 to n do
    f()
  done


let str_pos p =
  String.concat "," (List.map string_of_int (Array.to_list p))


class strict_and2_query (w1:walker) (w2:walker) 
                        phrase_flag phrase_offset phrase_shift =
  (* Implements a strict AND of two sub-walkers

     phrase_flag: this is for a phrase query. A doc matches only
     if some positions in w1 are equal to positions in w2 - phrase_offset.
   *)
  let in1 = ref [] in
  let in2 = ref [] in
  let idx1 = ref 0 in
  let idx2 = ref 0 in
  let count = ref 0 in
  let total1 = ref 0 in
  let total2 = ref 0 in
object(self)
  val mutable beginning = true
  val mutable at_end = false
  val mutable out_slice = []
  val mutable out_positions = None
  val mutable out_descr = [||]
  val mutable min_docid = 0
  val mutable max_score = infinity

  method private load_next_slice() =
    let out_pos = ref [] in
    let out_d = ref [] in
    out_slice <- [];
    out_positions <- None;
    (* For all empty input slices, switch to the next slice. We keep
       old input slices that were not yet fully consumed!
     *)
    if beginning then in1 := w1#slice;
    while !in1 = [] && not w1#end_reached do
      w1#next();
      in1 := w1#slice;
      idx1 := 0;
    done;
    if beginning then in2 := w2#slice;
    while !in2 = [] && not w2#end_reached do
      w2#next();
      in2 := w2#slice;
      idx2 := 0;
    done;
    (* TODO optimization: If the max docid of one slice is < the min docid
       of the other slice, skip the slice and the following slices until
       a document with this min docid is found
     *)
    beginning <- false;
    max_score <- w1#max_score +. w2#max_score;
    min_docid <- min w1#min_docid w2#min_docid;
    (* Are all input slices non-empty? If there is an empty slice, we
       cannot proceed, and this slice also remains empty.
     *)
    if !in1 <> [] && !in2 <> [] then (
      (* Now loop until one of the input slices becomes empty. *)
      let cont = ref true in
      while !cont do
	(* If both slices have the same docid next, output it. 
           Otherwise, switch the lower input slice to the next docid
	 *)
	let (d1,b1) = List.hd !in1 in
	let (d2,b2) = List.hd !in2 in
	if d1 = d2 then (
	  if phrase_flag then (
	    (* phrase query: get position arrays and compare them *)
	    let p1 = List.hd (w1#positions_for [ !idx1 ]) in
	    let p2 = List.hd (w2#positions_for [ !idx2 ]) in
	    let presult = phrase_match p1 p2 phrase_offset phrase_shift in
(*
eprintf "PHRASE: d1=%d d2=%d offset=%d shift=%d p1=%s p2=%s presult=%s\n%!"
  d1 d2 phrase_offset phrase_shift
  (str_pos p1) (str_pos p2) (str_pos presult);
 *)
	    if presult <> [| |] then (
	      out_slice <- (d1, b1 +. b2) :: out_slice;
	      out_pos := presult :: !out_pos;
	    );
	  )
	  else (
	    (* no phrase query. Don't look at positions *)
	    out_slice <- (d1, b1 +. b2) :: out_slice;
	  );
          out_d := (!idx1, !idx2) :: !out_d;
	  in1 := List.tl !in1;
	  in2 := List.tl !in2;
          incr idx1;
          incr idx2;
          incr count;
          incr total1;
          incr total2;
	  if !in1 = [] || !in2 = [] then cont := false;
	)
	else (
	  if d1 < d2 then (
	    in1 := List.tl !in1;
            incr idx1;
            incr total1;
	    let k = ref 1 in
	    while !in1 <> [] && fst(List.hd !in1) < d2 do
	      in1 := List.tl !in1;
	      incr k;
              incr idx1;
              incr total1;
	    done;
	    if !in1 = [] then cont := false
	  )
	  else (
	    in2 := List.tl !in2;
            incr idx2;
            incr total2;
	    let k = ref 1 in
	    while !in2 <> [] && fst(List.hd !in2) < d1 do
	      in2 := List.tl !in2;
	      incr k;
              incr idx2;
              incr total2;
	    done;
	    if !in2 = [] then cont := false
	  )
	)
      done
    );
    out_slice <- List.rev out_slice;
    out_descr <- Array.of_list (List.rev !out_d);
    if phrase_flag then
      out_positions <- Some(Array.of_list(List.rev !out_pos));
    (* maybe at end? *)
    if !in1 = [] || !in2 = [] then (
      if w1#end_reached || w2#end_reached then
	at_end <- true
    )

  method private load_positions() =
    match out_positions with
      | None ->
          (* as it is likely that the output is much shorter than the
             input, we use here positions_for
           *)
          let pos1_for = w1#positions_for in 
          let pos2_for = w2#positions_for in 
          let out_pos =
            Array.map
              (fun (idx1,idx2) ->
               let p1 = List.hd (pos1_for [idx1]) in
               let p2 = List.hd (pos2_for [idx2]) in
               merge_positions p1 p2
              )
              out_descr in
(*
          let pos1 = w1#positions in 
          let pos2 = w2#positions in 
          let out_pos =
            Array.map
              (fun (idx1,idx2) ->
                 merge_positions pos1.(idx1) pos2.(idx2)
              )
              out_descr in
 *)
          out_positions <- Some out_pos;
          out_pos

      | Some pos ->
          pos


  method end_reached = 
    if beginning then self#load_next_slice();
    at_end

  method min_docid =
    if beginning then self#load_next_slice();
    min_docid

  method max_score =
    if beginning then self#load_next_slice();
    max_score

  method slice =
    if beginning then self#load_next_slice();
    out_slice

  method positions =
    if beginning then self#load_next_slice();
    self # load_positions()
    

  method positions_for l =
    if beginning then self#load_next_slice();
    match out_positions with
      | Some pos ->
          List.map (fun idx -> pos.(idx)) l
      | None ->
          let idx1_l, idx2_l =
            List.split
              (List.map
                 (fun idx -> out_descr.(idx))
                 l
              ) in
          let p1_l = w1#positions_for idx1_l in
          let p2_l = w2#positions_for idx2_l in
          List.map2
            (fun p1 p2 ->
               merge_positions p1 p2
            )
            p1_l
            p2_l
          

  method next() =
    if beginning then self#load_next_slice();
    self#load_next_slice()

  method est_size =
    if beginning then self#load_next_slice();
    let p1 = if !total1=0 then 0.0 else float !count /. float !total1 in
    let s1 = p1 *. w1#est_size in
    let p2 = if !total2=0 then 0.0 else float !count /. float !total2 in
    let s2 = p2 *. w2#est_size in
    (s1 +. s2) *. 0.5
 
end

(* TODO optimization: separate strict_and2_query for phrase queries *)



class nonstrict_and2_query (w1:walker) (w2:walker) : walker =
  (* Implements an "AND" of two sub-walkers where the docs of the left
     walker are strictly required and the docs of the right walker are
     optional.
   *)
  let in1 = ref [] in
  let in2 = ref [] in
  let idx1 = ref 0 in
  let idx2 = ref 0 in
object(self)
  val mutable beginning = true
  val mutable at_end = false
  val mutable out_slice = []
  val mutable out_descr = [||]
  val mutable out_positions = None
  val mutable min_docid = 0
  val mutable max_score = infinity

  method private load_next_slice() =
    let out_d = ref [] in                   
    out_slice <- [];
    out_positions <- None;
    (* For all empty input slices, switch to the next slice: *)
    if beginning then in1 := w1#slice;
    while !in1 = [] && not w1#end_reached do
      w1#next();
      in1 := w1#slice;
      idx1 := 0;
    done;
    if beginning then in2 := w2#slice;
    while !in2 = [] && not w2#end_reached do
      w2#next();
      in2 := w2#slice;
      idx2 := 0;
    done;
    (* TODO: as an optimization we can skip w2 slices when the next in1
       docid is bigger than the max docid of the next w2 slice
     *)
    beginning <- false;
    max_score <- w1#max_score +. w2#max_score;
    min_docid <- min w1#min_docid w2#min_docid;
    (* Are all input slices non-empty? If there is an empty slice, we
       cannot proceed, and this slice also remains empty.
     *)
    if !in1 <> [] && !in2 <> [] then (
      (* Now loop until one of the input slices becomes empty. *)
      let cont = ref true in
      while !cont do
	(* Simply output all docs of w1. The w2 docs may modify the boost *)
	let (d1,b1) = List.hd !in1 in
	let (d2,b2) = List.hd !in2 in
	if d2 < d1 then (
	  in2 := List.tl !in2;
          incr idx2;
	  while !in2 <> [] && fst(List.hd !in2) < d1 do
	    in2 := List.tl !in2;
            incr idx2
	  done;
	  if !in2 = [] then cont := false
	);
	if !cont then (
	  let (d2,b2) = List.hd !in2 in
	  let w = b1 +. (if d1 = d2 then b2 else 0.0) in
	  out_slice <- (d1, w) :: out_slice;
          out_d := (!idx1, if d1=d2 then !idx2 else (-1)) :: !out_d;
	  in1 := List.tl !in1;
	  in2 := List.tl !in2;
          incr idx1;
          incr idx2;
	  if !in1 = [] || !in2 = [] then cont := false
	)
      done
    );
    (* Special case: w2 at end *)
    if !in1 <> [] && !in2 = [] && w2#end_reached then (
      while !in1 <> [] do
	let (d1,b1) = List.hd !in1 in
	out_slice <- (d1,b1) :: out_slice;
        out_d := (!idx1, (-1)) :: !out_d;
	in1 := List.tl !in1;
        incr idx1
      done
    );
    out_slice <- List.rev out_slice;
    out_descr <- Array.of_list (List.rev !out_d);
    (* maybe at end? Only w1 counts *)
    if !in1 = [] then (
      if w1#end_reached then
	at_end <- true
    )

  method end_reached = 
    if beginning then self#load_next_slice();
    at_end

  method min_docid =
    if beginning then self#load_next_slice();
    min_docid

  method max_score =
    if beginning then self#load_next_slice();
    max_score

  method slice =
    if beginning then self#load_next_slice();
    out_slice

  method positions =
    if beginning then self#load_next_slice();
    match out_positions with
      | None ->
          let pos1 = w1#positions in 
          let pos2 = w2#positions in 

          let out_pos =
            Array.map
              (fun (idx1,idx2) ->
                 if idx2 >= 0 then
                   merge_positions pos1.(idx1) pos2.(idx2)
                 else
                   pos1.(idx1)
              )
              out_descr in

          out_positions <- Some out_pos;
          out_pos

      | Some pos ->
          pos

  method positions_for l =
    if beginning then self#load_next_slice();
    match out_positions with
      | Some pos ->
          List.map (fun idx -> pos.(idx)) l
      | None ->
          let idx1_l, idx2_l =
            List.split
              (List.map
                 (fun idx -> out_descr.(idx))
                 l
              ) in
          let idx2a_l =
            List.map (fun idx -> max idx 0) idx2_l in
          let p1_l = w1#positions_for idx1_l in
          let p2_l = 
            if !idx2 > 0 then
              w2#positions_for idx2a_l
            else
              List.map (fun _ -> [||]) idx2a_l in
          let p12_l = List.combine p1_l p2_l in
          List.map2
            (fun (p1,p2) idx2 ->
               if idx2 >= 0 then
                 merge_positions p1 p2
               else
                 p1
            )
            p12_l
            idx2_l


  method next() =
    if beginning then self#load_next_slice();
    self#load_next_slice()
 
  method est_size = w1#est_size

end


class or2_query (w1:walker) (w2:walker) sum_flag eq_flag : walker =
  (* An OR of w1 and w2. If sum_flag, the scores of the docs are added.
     If eq_flag the scores are equalized based on the first slice.
   *)
  let in1 = ref [] in
  let in2 = ref [] in
  let idx1 = ref 0 in
  let idx2 = ref 0 in
  let count = ref 0 in  (* for stats: count the intersection *)
  let total1 = ref 0 in
  let total2 = ref 0 in
  let factor2 = ref 0.0 in
  let max1 = ref 0.0 in  (* total max *)
  let max2 = ref 0.0 in
object(self)
  val mutable beginning = true
  val mutable at_end = false
  val mutable out_slice = []
  val mutable out_descr = [||]
  val mutable out_positions = None
  val mutable min_docid = 0
  val mutable max_score = infinity

  method private load_next_slice() =
    let out_d = ref [] in
    out_slice <- [];
    out_positions <- None;
    (* For all empty input slices, switch to the next slice: *)
    if beginning then in1 := w1#slice;
    while !in1 = [] && not w1#end_reached do
      w1#next();
      in1 := w1#slice;
      idx1 := 0;
    done;
    if beginning then in2 := w2#slice;
    while !in2 = [] && not w2#end_reached do
      w2#next();
      in2 := w2#slice;
      idx2 := 0;
    done;
    if !factor2 = 0.0 then (
      if eq_flag then (
	let sum1 = List.fold_left (fun acc (_,b) -> acc+.b) 0.0 !in1 in
	let n1 = List.length !in1 in
	let avg1 = sum1 /. float n1 in
	let sum2 = List.fold_left (fun acc (_,b) -> acc+.b) 0.0 !in2 in
	let n2 = List.length !in2 in
	let avg2 = sum2 /. float n2 in
	factor2 := if n1=0 || n2=0 then 1.0 else avg1 /. avg2;
      )
      else factor2 := 1.0;
      max1 := w1#max_score;
      max2 := w2#max_score *. !factor2
    );
    beginning <- false;
    max_score <- (if sum_flag then 
		    2.0
		  else
		    max w1#max_score (w2#max_score *. !factor2));
    min_docid <- min w1#min_docid w2#min_docid;
    (* Are all input slices non-empty? If there is an empty slice, we
       cannot proceed, and this slice also remains empty.
     *)
    if !in1 <> [] && !in2 <> [] then (
      (* Now loop until one of the input slices becomes empty. *)
      let cont = ref true in
      while !cont do
	(* Simply output all docs of w1 and w2. Check only for dups *)
	let (d1,b1) = List.hd !in1 in
	let (d2,b2_raw) = List.hd !in2 in
	if d1 = d2 then (
	  let b2 = b2_raw *. !factor2 in
	  let b = 
            if sum_flag then
              (b1 +. b2) /. (!max1 +. !max2) +. 1.0   (* range 1.0 ... 2.0 *)
            else
              max b1 b2 in
	  out_slice <- (d1, b) :: out_slice;
          out_d := (!idx1, !idx2) :: !out_d;
	  in1 := List.tl !in1;
	  in2 := List.tl !in2;
          incr idx1;
          incr idx2;
          incr count;
          incr total1;
          incr total2;
	  cont := !in1 <> [] && !in2 <> []
	)
	else (
	  if d1 < d2 then (
            let b = if sum_flag then b1 /. !max1 else b1 in
	    out_slice <- (d1, b) :: out_slice;
            out_d := (!idx1, (-1)) :: !out_d;
	    in1 := List.tl !in1;
            incr idx1;
            incr total1;
	    cont := !in1 <> []
	  )
	  else (
	    let b2 = b2_raw *. !factor2 in
            let b = if sum_flag then b2 /. !max2 else b2 in
	    out_slice <- (d2, b) :: out_slice;
            out_d := ((-1), !idx2) :: !out_d;
	    in2 := List.tl !in2;
            incr idx2;
            incr total2;
	    cont := !in2 <> []
	  )
	)
      done
    );
    (* Special case: w1 at end *)
    if !in2 <> [] && !in1 = [] && w1#end_reached then (
      while !in2 <> [] do
	let (d2,b2_raw) = List.hd !in2 in
	let b2 = b2_raw *. !factor2 in
        let b = if sum_flag then b2 /. !max2 else b2 in
	out_slice <- (d2,b) :: out_slice;
        out_d := ((-1), !idx2) :: !out_d;
	in2 := List.tl !in2;
        incr idx2;
        incr total2;
      done
    );
    (* Special case: w2 at end *)
    if !in1 <> [] && !in2 = [] && w2#end_reached then (
      while !in1 <> [] do
	let (d1,b1) = List.hd !in1 in
        let b = if sum_flag then b1 /. !max1 else b1 in
	out_slice <- (d1,b) :: out_slice;
        out_d := (!idx1, (-1)) :: !out_d;
	in1 := List.tl !in1;
        incr idx1;
        incr total1;
      done
    );
    out_slice <- List.rev out_slice;
    out_descr <- Array.of_list (List.rev !out_d);
    (* maybe at end? *)
    if !in1 = [] && !in2 = [] then (
      if w1#end_reached && w2#end_reached then
	at_end <- true
    )

  method end_reached = 
    if beginning then self#load_next_slice();
    at_end

  method min_docid =
    if beginning then self#load_next_slice();
    min_docid

  method max_score =
    if beginning then self#load_next_slice();
    max_score

  method slice =
    if beginning then self#load_next_slice();
    out_slice

  method positions =
    if beginning then self#load_next_slice();
    match out_positions with
      | None ->
          let pos1 = w1#positions in 
          let pos2 = w2#positions in 

          let out_pos =
            Array.map
              (fun (idx1,idx2) ->
                 if idx1 >= 0 then
                   if idx2 >= 0 then
                     merge_positions pos1.(idx1) pos2.(idx2)
                   else
                     pos1.(idx1)
                 else
                   pos2.(idx2)
              )
              out_descr in

          out_positions <- Some out_pos;
          out_pos

      | Some pos ->
          pos

  method positions_for l =
    if beginning then self#load_next_slice();
    match out_positions with
      | Some pos ->
          List.map (fun idx -> pos.(idx)) l
      | None ->
          let idx12_l = 
            List.map
              (fun idx -> out_descr.(idx))
              l in
          let idx1_l, idx2_l = List.split idx12_l in
          let idx1a_l =
            List.map (fun idx -> max idx 0) idx1_l in
          let idx2a_l =
            List.map (fun idx -> max idx 0) idx2_l in
          let p1_l =
            if !idx1 > 0 then
              w1#positions_for idx1a_l
            else
              List.map (fun _ -> [||]) idx1a_l in
          let p2_l = 
            if !idx2 > 0 then
              w2#positions_for idx2a_l
            else
              List.map (fun _ -> [||]) idx2a_l in
          let p12_l = List.combine p1_l p2_l in
          List.map2
            (fun (p1,p2) (idx1,idx2) ->
               if idx1 >= 0 then (
                 if idx2 >= 0 then
                   merge_positions p1 p2
                 else
                   p1
               )
               else p2
            )
            p12_l
            idx12_l

  method next() =
    if beginning then self#load_next_slice();
    self#load_next_slice()

  method est_size =
    if beginning then self#load_next_slice();
    let e1 = w1#est_size in
    let e2 = w2#est_size in
    (* p1: fraction of docs from w1 that are also in w2 *)
    let p1 = if !total1=0 then 0.0 else float !count /. float !total1 in
    (* p2: fraction of docs from w2 that are also in w1 *)
    let p2 = if !total2=0 then 0.0 else float !count /. float !total2 in
    let s = (1.0 -. 0.5 *. p1) *. e1 +. (1.0 -. 0.5 *. p2) *. e2 in
    (* eprintf "UNION estimate: e1=%f e2=%f s=%f\n%!" e1 e2 s; *)
    s
 
end


class phrase_query ~phrase_shift rh query_words qwa qb : walker =
  let qwa = Array.to_list qwa in
  let w, _ =
    match qwa with
      | qwa_hd :: qwa_tl ->
	  let w_hd =
	    new qword_walker rh query_words qwa_hd qb in
	  List.fold_left
	    (fun (acc,offset) qw -> 
	       let (feature,term) = query_words.q_words.(qw) in
	       if term = "" then
		 (acc,offset+1)
	       else
		 let wword = new qword_walker rh query_words qw qb in
		 let w' = 
                   new strict_and2_query acc wword true offset phrase_shift in
		 (w', offset+1)
	    )
	    (w_hd, 1)
	    qwa_tl
      | [] ->
	  failwith "empty phrase" in
  delegate w


let is_strict =
  function
    | `Word(_,_,strict) -> strict
    | `Phrase(_,_,strict) -> strict


let qword_or_phrase_walker ~phrase_shift index query_words e =
  match e with
    | `Word(qw,qb,_) ->
	new qword_walker index query_words qw qb
    | `Phrase(qwa,qb,_) ->
	new phrase_query ~phrase_shift index query_words qwa qb 


class and_query ~phrase_shift rh query_words (q:WQUERY.qword_or_phrase list)
                 : walker =
  (* If all subqueries are strict, join them with strict_and2_query.
     If all subqueries are non-strict, join them with or2_query.
     Otherwise, group the strict and the non-strict subqueries, and
     form

     ((strict1 SAND strict2) ... SAND strictN)
        NSAND nonstrict1
          NSAND nonstrict2 ...
            NSAND nonstrictM

     where SAND = strict_and2_query, and NSAND = nonstrict_and2_query.
   *)
  let q_strict, q_nonstrict = 
    List.partition is_strict q in

  (* TODO: think about whether the order of the subqueries has an impact
     on the performance.

     q_strict: it could be advantageous to do the shorter term vectors first.
     Although this does not reduce the number of vector slices that need to
     be scanned, a positive effect could be that the intermediate result
     slices are shorter, and less data need to be copied in total.

     q_nonstrict: it is very unclear what is best. Nothing seems to be 
     obvious.
   *)

  let w_strict = 
    List.map (qword_or_phrase_walker ~phrase_shift rh query_words) q_strict in
  let w_nonstrict = 
    List.map (qword_or_phrase_walker ~phrase_shift rh query_words) q_nonstrict 
  in
  let w =
    match w_strict with
      | w_strict_hd :: w_strict_tl ->
	  let w_sand =
	    List.fold_left
	      (fun acc e -> new strict_and2_query acc e false 0 0)
	      w_strict_hd
	      w_strict_tl in
	  List.fold_left
	    (fun acc e -> new nonstrict_and2_query acc e)
	    w_sand
	    w_nonstrict
      | [] ->
	  ( match w_nonstrict with
	      | w_nonstrict_hd :: w_nonstrict_tl ->
		  List.fold_left
		    (fun acc e -> new or2_query acc e true false)
		    w_nonstrict_hd
		    w_nonstrict_tl
	      | [] ->
		  failwith "Amb_eval: no query words"
	  )
  in
  delegate w


class union_query sum_flag (l:walker list) : walker =
  let w =
    match l with
      | l_hd :: l_tl ->
	  List.fold_left
	    (fun acc e -> new or2_query acc e sum_flag sum_flag)
	    l_hd
	    l_tl
      | [] ->
	  assert false
  in
  delegate w


let eval_cmp x cmp y =
  match cmp with
    | `Gt -> x > y
    | `Ge -> x >= y
    | `Lt -> x < y
    | `Le -> x <= y
    | `Eq -> x = y
    | `Ne -> x <> y


type val_kind = [ `Signed | `Unsigned | `FP ]

let bit_length =
  function
    | `U1 -> 1
    | `U2 -> 2
    | `U4 -> 4
    | `U8 -> 8
    | `U16 -> 16
    | `S8 -> 8
    | `S16 -> 16
    | `S32 -> 32
    | `S64 -> 64
    | `F32 -> 32
    | `F64 -> 64

let val_kind =
  function
    | `U1 | `U2 | `U4 | `U8 | `U16 -> `Unsigned
    | `S8 | `S16 | `S32 | `S64 -> `Signed
    | `F32 | `F64 -> `FP

let to_float x bit_length =
  match bit_length with
    | 64 ->
         Int64.float_of_bits x
    | 32 ->
         Int32.float_of_bits (Int64.to_int32 x)
    | 16 ->
         let x16 = (Int64.to_int x) land 0xffff in
         let x15 = x16 land 0x7fff in
         let negative = x16 > x15 in
         (* Amblib_zslice only implements _non-negative_ half floats *)
         let abs_fp = 
           Amblib_zslice.float_of_half(Amblib_zslice.half_float_of_bits x15) in
         if negative then -. abs_fp else abs_fp
    | _ ->
         failwith ("Cannot convert to float (restricted to 64/32/16 bits)")
   


(* FIXME: In the following we just raise Not_found if a feature or
   attribute is not found. Better exception.
 *)


let rec attribute_with_modifier rh att_mod =
  match att_mod with
    | `Att aname ->
	 let att =
           try Amb_shared_master.get_attribute rh aname 
           with Not_found ->
	     failwith ("Attribute not found: " ^ aname) in
	 let att_size =
	   Amblib_attvector.Attribute.size att in
	 let att_typ =
	   Amblib_attvector.Attribute.typ att in
         let is_float =
           att_typ = `F32 || att_typ = `F64 in
         let get_int64 docid =
	   if docid >= 0 && docid < att_size then
             Amblib_attvector.Attribute.get_int64 att docid
           else
             raise Not_found in
         let get_float docid =
	   if docid >= 0 && docid < att_size && is_float then
             Amblib_attvector.Attribute.get_float att docid
           else
             raise Not_found in
         (val_kind att_typ, bit_length att_typ, get_int64, get_float)

    | `Bits(am_sub, startbit, endbit) ->
         let (val_kind_sub, bit_length_sub, get_int64_sub, get_float_sub) =
           attribute_with_modifier rh am_sub in
         if startbit < 0 || startbit > endbit || endbit >= bit_length_sub then
           failwith (sprintf 
                       "Bit extractor out of range: [%d..%d]"
                       startbit
                       endbit);
         if val_kind_sub = `FP then
           failwith "Cannot extract bits of a floating-point number";
         let bit_length_out =
           endbit - startbit + 1 in
         let get_int64_signed docid =
           let x = get_int64_sub docid in
           let n_left = 63 - endbit in
           let n_right = n_left + startbit in
           Int64.shift_right (Int64.shift_left x n_left) n_right in
         let m = Int64.pred(Int64.shift_left 1L (endbit - startbit + 1)) in
         let get_int64_unsigned docid =
           let x = get_int64_sub docid in
           Int64.logand (Int64.shift_right x startbit) m in
         let get_int64 =
           if val_kind_sub = `Signed then 
             get_int64_signed 
           else
             get_int64_unsigned in
         let get_float docid =
           let x = get_int64_signed docid in
           to_float x bit_length_out in
         (val_kind_sub, bit_length_out, get_int64, get_float)

    | `Signed am_sub ->
         let (val_kind_sub, bit_length_sub, get_int64_sub, get_float_sub) =
           attribute_with_modifier rh am_sub in
         if val_kind_sub = `FP then
           failwith "Cannot convert floating-point number to signed number";
         let get_int64_signed docid =
           let x = get_int64_sub docid in
           let n = 64 - bit_length_sub in
           Int64.shift_right (Int64.shift_left x n) n in
         let get_int64 =
           if val_kind_sub = `Signed then get_int64_sub else get_int64_signed in
         let get_float docid =
           let x = get_int64 docid in
           to_float x bit_length_sub in
         (`Signed, bit_length_sub, get_int64, get_float)

    | `Unsigned am_sub ->
         let (val_kind_sub, bit_length_sub, get_int64_sub, get_float_sub) =
           attribute_with_modifier rh am_sub in
         if val_kind_sub = `FP then
           failwith "Cannot convert floating-point number to unsigned number";
         if bit_length_sub = 64 then
           failwith
             "Implementation restriction: no 64 bit unsigned type available";
         let m = Int64.pred(Int64.shift_left 1L bit_length_sub) in
         let get_int64_unsigned docid =
           let x = get_int64_sub docid in
           Int64.logand x m in
         let get_int64 =
           if val_kind_sub = `Unsigned then
             get_int64_sub
           else
             get_int64_unsigned in
         let get_float docid =
           let x = get_int64 docid in
           to_float x bit_length_sub in
         (`Unsigned, bit_length_sub, get_int64, get_float)



type filter = int (* docid *) -> bool

let rec create_filter rh (c:WQUERY.condition) : filter =
  match c with
    | `Doc_mem(fname, word) ->
	(* TODO: check whether there is an attribute *)
         let (_, base_ft, _) = Amb_shared_master.get_feature rh fname in
         (* FIXME: allow to select upd_ft *)

         let tvector = 
           try Amb_tvector.get_tvector base_ft word
           with Not_found -> Amb_tvector.empty_tvector() in
	 (fun docid ->
	    Amb_tvector.mem_tvector tvector docid
	 )
    | `Att_cmp_int64(am, cmp, n) ->
         let (val_kind, bit_length, get_int64, get_float) =
           attribute_with_modifier rh am in
         (fun docid ->
            match val_kind with
              | `FP ->
                   failwith "Comparison of float att with integer"
              | `Signed | `Unsigned ->
                   ( try
                       let x = get_int64 docid in
                       eval_cmp x cmp n
                     with Not_found -> false
                   )
         )
    | `Att_cmp_float(am, cmp, y) ->
         let (val_kind, bit_length, get_int64, get_float) =
           attribute_with_modifier rh am in
         (fun docid ->
            try
              let x = get_float docid in
              eval_cmp x cmp y
            with Not_found -> false
         )
    | `Att_bitset_all(am, n) ->
         let (val_kind, bit_length, get_int64, get_float) =
           attribute_with_modifier rh am in
         if val_kind = `FP then
           failwith "No bitset operations for floating-point numbers";
	 (fun docid ->
            try
              let x = get_int64 docid in
	      Int64.logand x n = n
            with Not_found -> false
	 )
    | `Att_bitset_some(am, n) ->
         let (val_kind, bit_length, get_int64, get_float) =
           attribute_with_modifier rh am in
         if val_kind = `FP then
           failwith "No bitset operations for floating-point numbers";
	 (fun docid ->
            try
              let x = get_int64 docid in
	      Int64.logand x n <> 0L
            with Not_found -> false
	 )
    | `Not c1 ->
	let f1 = create_filter rh c1 in
	(fun docid -> not(f1 docid))
    | `And(c1,c2) ->
	let f1 = create_filter rh c1 in
	let f2 = create_filter rh c2 in
	(fun docid -> (f1 docid) && (f2 docid))
    | `Or(c1,c2) ->
	let f1 = create_filter rh c1 in
	let f2 = create_filter rh c2 in
	(fun docid -> (f1 docid) || (f2 docid))


let array_filter f a =
  (* TODO: speed up *)
  Array.of_list
    (List.filter f (Array.to_list a))


let rec pfilter_positions pf pos_array =
  match pf with
    | `Pos_cmp_int64(bit_s, bit_e, cmp, n) ->
         if bit_e < 0 || bit_s > bit_e || bit_e > 62 then
           failwith "PFILTER: bad bit extractor";
         let m = Int64.pred(Int64.shift_left 1L (bit_e - bit_s + 1)) in
         array_filter
           (fun p ->
              let p1 = 
                Int64.logand
                  (Int64.shift_right (Int64.of_int p) bit_s) m in
              eval_cmp p1 cmp n
           )
           pos_array
    | `And(pf1, pf2) ->
         let pa1 = pfilter_positions pf1 pos_array in
         pfilter_positions pf2 pa1
    | `Or(pf1, pf2) ->
         let pa1 = pfilter_positions pf1 pos_array in
         let pa2 = pfilter_positions pf2 pos_array in
         merge_positions pa1 pa2
    | `Not pf1 ->
         assert false (* TODO *)


let filter_positions filter_expr pos_array =
  match filter_expr with
    | `PFilter pf ->
         pfilter_positions pf pos_array


class where_query rh (w1:walker) (c:WQUERY.condition) filters : walker =
  (* Implements a filter. All docs of w1 must satisfy c. *)
  let in1 = ref [] in
  let idx1 = ref 0 in
  let filter = create_filter rh c in
  let count1 = ref 0 in
  let total1 = ref 0 in
  let apply_filters pos_array =
    List.fold_left
      (fun acc filter ->
         filter_positions filter acc
      )
      pos_array
      filters in
object(self)
  val mutable beginning = true
  val mutable at_end = false
  val mutable out_slice = []
  val mutable out_descr = [||]
  val mutable out_positions = None
  val mutable min_docid = 0
  val mutable max_score = infinity

  method private load_next_slice() =
    let out_d = ref [] in
    out_slice <- [];
    out_positions <- None;
    (* For all empty input slices, switch to the next slice: *)
    if beginning then in1 := w1#slice;
    while !in1 = [] && not w1#end_reached do
      w1#next();
      in1 := w1#slice;
      idx1 := 0;
    done;
    beginning <- false;
    max_score <- w1#max_score;
    min_docid <- w1#min_docid;
    (* Are all input slices non-empty? If there is an empty slice, we
       cannot proceed, and this slice also remains empty.
     *)
    if !in1 <> [] then (
      (* Now loop until one of the input slices becomes empty. *)
      while !in1 <> [] do
	(* Simply output all docs of w1 satisying c/filter *)
	let (d1,b1) = List.hd !in1 in
	if filter d1 then (
	  out_slice <- (d1, b1) :: out_slice;
          out_d := !idx1 :: !out_d;
          incr count1;
        );
	in1 := List.tl !in1;
        incr idx1;
        incr total1;
      done
    );
    out_slice <- List.rev out_slice;
    out_descr <- Array.of_list (List.rev !out_d);
    (* maybe at end? *)
    if !in1 = [] then (
      if w1#end_reached then
	at_end <- true
    )

  method end_reached = 
    if beginning then self#load_next_slice();
    at_end

  method min_docid =
    if beginning then self#load_next_slice();
    min_docid

  method max_score =
    if beginning then self#load_next_slice();
    max_score

  method slice =
    if beginning then self#load_next_slice();
    out_slice

  method positions =
    (* CHECK: maybe also map this to positions_for, as for 
       strict ands
     *)
    if beginning then self#load_next_slice();
    match out_positions with
      | None ->
          let pos1 = w1#positions in 

          let out_pos =
            Array.map
              (fun idx1 -> pos1.(idx1))
              out_descr in

          out_positions <- Some (Array.map apply_filters out_pos);
          out_pos

      | Some pos ->
          pos

  method positions_for l =
    if beginning then self#load_next_slice();
    match out_positions with
      | Some pos ->
          List.map (fun idx -> pos.(idx)) l
      | None ->
           let a =
             w1#positions_for
                  (List.map
                     (fun idx -> out_descr.(idx))
                     l
                  ) in
           List.map apply_filters a

  method next() =
    if beginning then self#load_next_slice();
    self#load_next_slice()

  method est_size =
    if beginning then self#load_next_slice();
    let p1 = if !total1=0 then 0.0 else float !count1 /. float !total1 in
    let s1 = p1 *. w1#est_size in
    s1
 
end


type pos =
  | PA of int array
  | PI of int   (* index in slice only *)


module Scoredoc = struct
  type t = int * float   (* (docid, score, pos) *)

  let compare ( (d1,s1) : t ) ( (d2,s2) : t ) =
    match -(Pervasives.compare s1 s2) with
      | 0 ->
	  Pervasives.compare d1 d2
      | n ->
	  n
end


module Rmap = Map.Make(Scoredoc)



let pull ~include_positions rh walker size =
  (* Pull docs from [walker] until the first [size] docs of the result
     vector are known. That means: When we have at least [size] docs,
     and the doc with the lowest score is bigger than [walker#max_score]
     we can stop pulling docs from [walker].

     Use a result set of type Set over a pair (score,docid) where 
     the ordering criterion is:
      - first order by [score]
      - then order by [docid]
   *)
  assert(size > 0);
  let rset = ref Rmap.empty in
  let rset_size = ref 0 in
  let total_size = ref 0 in
  
  let stop = ref walker#end_reached in

  while not !stop do
    if !rset_size >= size then (
      let ((_, min_score), _) = Rmap.max_binding !rset in
      (* Note: because of the definition of Rmap, max_elt returns the element
         with the lowest score
       *)
      if min_score > walker#max_score then
	stop := true
    );
    if not !stop then (
      let slice = walker#slice in
      let idx = ref 0 in
      let added_rset = ref Rmap.empty in
      List.iter
	(fun (docid,score) ->
	   (* The walker already ensures that every doc is only returned
              once.
	    *)
         (* let pa = List.hd (walker#positions_for [!idx]) in *)
	   rset := Rmap.add (docid,score) (PI !idx) (* (PA pa) *) !rset;
           if include_positions then
	     added_rset := Rmap.add (docid,score) !idx !added_rset;
	   incr rset_size;
           incr total_size;
           incr idx
	)
	slice;
      (* probably rset now got too big *)
      if !rset_size > size then (
	(* Rset.iter iterates in decreasing order of score *)
	let new_rset = ref Rmap.empty in
	let new_rset_size = ref 0 in
	( try
	    Rmap.iter
	      (fun scoredoc pos ->
		 new_rset := Rmap.add scoredoc pos !new_rset;
		 incr new_rset_size;
		 if !new_rset_size >= size then raise Exit
	      )
	      !rset
	  with Exit -> ()
	);
	rset := !new_rset;
	rset_size := !new_rset_size
      );
      (* fix up positions before going to next slice *)
      if include_positions then (
        let new_pos = Array.make !idx [||] in
        let extracted_indices =
          List.map
            (fun(_,idx) -> idx)
            (Rmap.bindings !added_rset) in
        let corresponding_positions =
          walker # positions_for extracted_indices in
        List.iter2
          (fun idx pa ->
             new_pos.(idx) <- pa
          )
          extracted_indices
          corresponding_positions;
        rset := 
          Rmap.map
            (function
              | PI idx -> PA new_pos.(idx)
              | other -> other
            )
            !rset
      );
      walker # next();
      stop := walker#end_reached
    )
  done;

  let get_pa =
    function
    | PI _ -> assert false
    | PA pa -> pa in

  let elements = Rmap.bindings !rset in
  let docid_a = Array.of_list(List.map (fun ((docid,_),_) -> docid) elements) in
  let score_a = Array.of_list(List.map (fun ((_,score),_) -> score) elements) in
  let pos_a = Array.of_list(List.map (fun (_,pos) -> get_pa pos) elements) in
  let udocid_a = Amb_shared_master.user_docid_array rh docid_a in

  { rvec_head = { 
      rvec_size = Array.length docid_a;
      rvec_min_size = !total_size;
      rvec_est_size =
	if walker#end_reached then !total_size else truncate walker#est_size;
      rvec_time_exceeded = false; (* TODO *)
    };
    rvec_docid = docid_a;
    rvec_udocid = udocid_a;
    rvec_score = score_a;
    rvec_pos = pos_a
  }


let rec construct_walker ~phrase_shift rh query_words query =
  match query with
    | `Occurrence [] ->
	failwith "Amb_eval: empty list of words after `Occurrence"
    | `Occurrence [ `Word(qw, qb, strict) ] ->
	(* strict does not have any influence here *)
	new qword_walker rh query_words qw qb
    | `Occurrence [ `Phrase(qwa, qb, strict) ] ->
	new phrase_query ~phrase_shift rh query_words qwa qb
    | `Occurrence l ->
	new and_query ~phrase_shift rh query_words l
    | `Condition(subquery, cond, filters) ->
	let w = construct_walker ~phrase_shift rh query_words subquery in
	new where_query rh w cond filters
    | `Union(subquery1,subquery2) ->
	let w1 = construct_walker ~phrase_shift rh query_words subquery1 in
	let w2 = construct_walker ~phrase_shift rh query_words subquery2 in
	new union_query false [w1;w2]
    | `Sum(subquery1,subquery2) ->
	let w1 = construct_walker ~phrase_shift rh query_words subquery1 in
	let w2 = construct_walker ~phrase_shift rh query_words subquery2 in
	new union_query true [w1;w2]


let eval rh query_words query ~len ~time ~include_positions ~exact 
         ~phrase_shift  () =
  (* TODO: Look at ~time *)
  let walker = construct_walker ~phrase_shift rh query_words query in
  let rvec = pull ~include_positions rh walker (max len 1) in
  
  (* TODO: guess this result (no idea yet) *)
  let stop = ref walker#end_reached in
  let n = ref rvec.rvec_head.rvec_min_size in
  if exact then (
    while not !stop do
      let slice = walker#slice in
      n := !n + List.length slice;
      walker # next();
      stop := walker#end_reached
    done;
  );

  let rvec_head =
    { rvec.rvec_head with rvec_min_size = !n } in
  { rvec with rvec_head }



let count rh query_words query ~phrase_shift () =
  let walker = construct_walker ~phrase_shift rh query_words query in
  let stop = ref walker#end_reached in

  let n = ref 0 in

  while not !stop do
    let slice = walker#slice in
    n := !n + List.length slice;
    walker # next();
    stop := walker#end_reached
  done;

  { rvec_size = !n;
    rvec_min_size = !n;
    rvec_est_size = truncate walker # est_size;
    rvec_time_exceeded = false; (* TODO *)
  }
