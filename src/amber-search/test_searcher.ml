open Printf

module Query_encap = Netplex_encap.Make_encap(struct type t=string*string end)
module Unit_encap = Netplex_encap.Make_encap(struct type t=unit end)

open Amb_eval


let load dumpfile =
  printf "Start loading\n%!";
  Amb_loader.load ~verbose:true dumpfile;
  printf "Finished loading\n%!"


let load_process_fork, load_process_join =
  Netmcore.def_process
    (fun query_encap ->
       let (dumpfile,query) = Query_encap.unwrap query_encap in
       let qwords, parsed = Amb_query_analyzer.w_analyze query in
       load dumpfile;
       Amb_shared_master.read
         (Amb_shared_master.get_master_ref())
         (fun rh ->
            let result =
              Amb_eval.eval rh qwords parsed ~len:100 ~time:100.0 () in
            let n = Array.length result.rvec_docid in
            printf "Result size: %d\n" n;
            for k = 0 to n - 1 do
              printf "%d: %f\n" result.rvec_docid.(k) result.rvec_score.(k)
            done;
         );
       Unit_encap.wrap ()
    )


let main() =
  let pool_size = ref 500_000_000 in
  let file = ref None in
  let query = ref None in
  Arg.parse
    [ "-pool-size", Arg.Set_int pool_size,
      "<bytes>   Set the pool size in bytes";

      "-q", Arg.String (fun s -> query := Some s),
      "<query>   The query to run";
    ]
    (fun arg -> file := Some arg)
    "usage: test_loader <file.dump>";

  let query =
    match !query with
      | None ->
          failwith "Missing -q"
      | Some q -> q in

  match !file with
    | None ->
        failwith "Missing dump file argument"
    | Some dumpfile ->
        Netmcore.startup
        ~socket_directory:"/tmp/test_loader_sockets" 
        ~init_ctrl:(fun ctrl -> 
                      ctrl#add_plugin Netplex_mutex.plugin;
                      ctrl#add_plugin Netplex_sharedvar.plugin;
                      Amb_shared_master.init_pool ctrl !pool_size
                   )
        ~first_process:(fun () -> 
                          let query_encap =
                            Query_encap.wrap (dumpfile,query) in
                          Netmcore.start load_process_fork query_encap
                       )
        ()


let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Error: %s\n%!" (Netexn.to_string error);
        if bt <> "" then eprintf "Backtrace: %s\n%!" bt;
        exit 1
