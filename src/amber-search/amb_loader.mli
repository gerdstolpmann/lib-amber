(* $Id$ *)

(** Load an index into shared memory *)

val load : ?verbose:bool -> string -> unit
  (** [load dumpfile]: Load from this dumpfile

      This function MUST be called in a child process. Also,
      {!Amb_shared_master} must already be initialized.
   *)
