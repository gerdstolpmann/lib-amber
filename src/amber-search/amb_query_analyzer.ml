(* $Id$ *)

open Amb_query_types
open Amb_query_parser

exception Syntax_error

let rec parse_token_no_ws lexbuf =
  match Amb_query_lexer.parse_token lexbuf with
    | IGNORE _ ->
	parse_token_no_ws lexbuf
    | tok ->
	tok


let l_analyze q =
  let lexbuf = Lexing.from_string q in
  try
    Amb_query_parser.query parse_token_no_ws lexbuf
  with
    | Parsing.Parse_error | Failure _ ->
	raise Syntax_error

let w_analyze q =
  let lq = l_analyze q in
  let freqtab = Hashtbl.create 10 in
  
  let count qw =
    let n = try Hashtbl.find freqtab qw with Not_found -> 0 in
    Hashtbl.replace freqtab qw (n+1)
  in

  let rec count_qwords =
    function
      | `Occurrence l ->
	  List.iter
	    (function
	       | `Word(qw,_,_) -> 
		   count qw
	       | `Phrase(qwa,_,_) ->
		   Array.iter count qwa
	    )
	    l
      | `Condition(l,_,_) -> 
	  count_qwords l
      | `Union(x1,x2) -> 
	  count_qwords x1; count_qwords x2
      | `Sum(x1,x2) -> 
	  count_qwords x1; count_qwords x2
  in
    
  count_qwords lq;

  let freqlist =
    Hashtbl.fold
      (fun qw freq acc -> (qw,freq) :: acc)
      freqtab
      [] in

  let idxtab = Hashtbl.create 10 in
  let idx = ref 0 in
  List.iter
    (fun (qw, _) ->
       Hashtbl.replace idxtab qw !idx;
       incr idx
    )
    freqlist;

  let q_words = Array.of_list (List.map fst freqlist) in
  let q_freq = Array.of_list (List.map snd freqlist) in

  let rec rewrite =
    function
      | `Occurrence l ->
	  `Occurrence
	    (List.map
	       (function
		  | `Word(qw,boost,strict) -> 
		      let i = Hashtbl.find idxtab qw  in
		      `Word(i,boost,strict)
		  | `Phrase(qwa,boost,strict) ->
		      `Phrase(Array.map (Hashtbl.find idxtab) qwa,
			      boost,
			      strict)
	       )
	       l
	    )
      | `Condition(l,c,fl) -> 
	  `Condition(rewrite l,c,fl)
      | `Union(x1,x2) -> 
	  `Union(rewrite x1, rewrite x2)
      | `Sum(x1,x2) -> 
	  `Sum(rewrite x1, rewrite x2)
  in
    
  let wq =
    try
      rewrite lq
    with
      | Not_found -> assert false in
  
  ( { q_words=q_words; q_freq=q_freq }, wq )

