open Printf

module String_encap = Netplex_encap.Make_encap(struct type t=string end)
module Unit_encap = Netplex_encap.Make_encap(struct type t=unit end)


let load dumpfile =
  printf "Start loading\n%!";
  Amb_loader.load ~verbose:true dumpfile;
  printf "Finished loading\n%!"


let load_process_fork, load_process_join =
  Netmcore.def_process
    (fun dumpfile_encap ->
       let dumpfile = String_encap.unwrap dumpfile_encap in
       load dumpfile;
       Unit_encap.wrap ()
    )


let main() =
  let pool_size = ref 500_000_000 in
  let file = ref None in
  Arg.parse
    [ "-pool-size", Arg.Set_int pool_size,
      "<bytes>   Set the pool size in bytes";
    ]
    (fun arg -> file := Some arg)
    "usage: test_loader <file.dump>";

  match !file with
    | None ->
        failwith "Missing dump file argument"
    | Some dumpfile ->
        Netmcore.startup
        ~socket_directory:"/tmp/test_loader_sockets" 
        ~init_ctrl:(fun ctrl -> 
                      ctrl#add_plugin Netplex_mutex.plugin;
                      ctrl#add_plugin Netplex_sharedvar.plugin;
                      Amb_shared_master.init_pool ctrl !pool_size
                   )
        ~first_process:(fun () -> 
                          let dumpfile_encap =
                            String_encap.wrap dumpfile in
                          Netmcore.start load_process_fork dumpfile_encap
                       )
        ()


let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
        let bt = Printexc.get_backtrace() in
        eprintf "Error: %s\n%!" (Netexn.to_string error);
        if bt <> "" then eprintf "Backtrace: %s\n%!" bt;
        exit 1
