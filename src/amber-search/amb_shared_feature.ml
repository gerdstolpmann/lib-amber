(* $Id$ *)

module A = Amblib_dump_fmt_aux

open Amb_types
open Printf

type feature_handle =
    { pool_id : Netmcore.res_id;
      head_descr : head Netmcore_ref.sref_descr;
      lvec_descr : (int, lvec * int64, unit) Netmcore_hashtbl.t_descr;
    }

and feature =
    { head : head Netmcore_ref.sref;
      lvec_table : (int, lvec * int64, unit) Netmcore_hashtbl.t;
        (* maps the k-th term string to an array of slices *)
    }

and head =
    { termstrings : bytes;
      termstring_index : int array;
      datastrings : bytes;
      mutable datastring_end : int;
      num_terms : int;
      svec : svec;
        (* maps the k-th term string to a byte pos in datastrings *)
    }

and svec =
    [ `S16 of svec16
    | `S32 of svec32
    | `S64 of svec64
    ]

and svec16 =
    (int, Bigarray.int16_signed_elt, Bigarray.c_layout) Bigarray.Array1.t

and svec32 =
    (int32, Bigarray.int32_elt, Bigarray.c_layout) Bigarray.Array1.t

and svec64 =
    (int64, Bigarray.int64_elt, Bigarray.c_layout) Bigarray.Array1.t

and lvec =
    tvec_slice array

and tvec_slice =
    { tvec_head : Amb_types.tvector_slice_head;
      tvec_data_pos : int;
         (* byte offset in datastrings, > 0 *)
      tvec_data_len : int;
         (* length of the string in datastrings *)
    }

let null_slice =
  { tvec_head = {
      tvec_slice_start = 0;
      tvec_slice_count = 0;
      tvec_slice_min_docid = 0;
      tvec_slice_max_docid = 0;
      tvec_rest_max_wdt = 0.0
    };
    tvec_data_pos = 0;
    tvec_data_len = 0;
  }

let feature_of_handle fh =
  { head = Netmcore_ref.sref_of_descr fh.pool_id fh.head_descr;
    lvec_table = Netmcore_hashtbl.hashtbl_of_descr fh.pool_id fh.lvec_descr
  }

let handle_of_feature ft =
  { pool_id = Netmcore_heap.pool (Netmcore_ref.heap ft.head);
    head_descr = Netmcore_ref.descr_of_sref ft.head;
    lvec_descr = Netmcore_hashtbl.descr_of_hashtbl ft.lvec_table;
  }

let pool fh = fh.pool_id

let destroy fh =
  let ft = feature_of_handle fh in
  Netmcore_heap.destroy (Netmcore_ref.heap ft.head);
  Netmcore_heap.destroy (Netmcore_hashtbl.heap ft.lvec_table)

let index_mult = 20

(* FIXME:

   It would be better if termstrings and datastrings were directly allocated
   in the heap rather than copied to the heap. However, the required
   function in Netmcore_heap is missing (only present for arrays).

   Same for svec!
 *)

let rec create pool_id head lvec_table =
  { head = Netmcore_ref.sref pool_id head;
    lvec_table;
  }

let zero svec =
  match svec with
    | `S16 ba -> Bigarray.Array1.fill ba 0
    | `S32 ba -> Bigarray.Array1.fill ba 0l
    | `S64 ba -> Bigarray.Array1.fill ba 0L

let set_svec svec k x =
  match svec with
    | `S16 ba -> assert(x < 65536); 
                 ba.{k} <- if x < 32768 then x else x - 65536
    | `S32 ba -> assert(x <= 1073741823);
                 ba.{k} <- Int32.of_int x
    | `S64 ba -> ba.{k} <- Int64.of_int x

let get_svec svec k =
match svec with
    | `S16 ba -> let x = ba.{k} in
                 if x < 0 then x+65536 else x
    | `S32 ba -> Int32.to_int ba.{k}
    | `S64 ba -> Int64.to_int ba.{k}
 


let create_feature pool_id dump_ft =
  try
    let vector_num = Array.length A.(dump_ft.ft_vectors) in
    let tstring_len =
      Array.fold_left
	(fun acc vec ->
           let l = String.length A.(vec.vec_key) in
           acc + l + Amblib_zslice.bytes_of_vint l
	)
	0
	A.(dump_ft.ft_vectors) in
    let datalen =  Amblib_dump.hyper A.(dump_ft.ft_datalen) in
    if datalen < 0 then failwith "invalid ft_datalen";
    let svec =
      if datalen <= 65535 then
      `S16 (Bigarray.Array1.create
              Bigarray.int16_signed Bigarray.c_layout vector_num)
      else
	if datalen <= 1073741823 then
          `S32 (Bigarray.Array1.create
                  Bigarray.int32 Bigarray.c_layout vector_num)
	else
          `S64 (Bigarray.Array1.create
                  Bigarray.int64 Bigarray.c_layout vector_num) in
    let head =
      { termstrings = Bytes.create tstring_len;
	termstring_index = Array.make ((vector_num-1) / index_mult) 0;
	datastrings = Bytes.create (datalen + 1);
	datastring_end = 1;
	num_terms = vector_num;
	svec;
      } in
    let lvec_table =
      Netmcore_hashtbl.create pool_id () in
    
    (* Initialize head: *)
    zero svec;
    
    let p_term = ref 0 in
    let i = ref 0 in
    let j = ref 0 in
    Array.iteri
      (fun vecidx vec ->
	 if A.(vec.vec_slices) <= 0 then
           failwith ("invalid vec_slices: " ^ 
                    string_of_int A.(vec.vec_slices));
	 
	 if !i = index_mult then (
           i := 0;
           head.termstring_index.( !j ) <- !p_term;
           incr j;
	 );
	 let term = A.(vec.vec_key) in
	 let l = String.length term in
	 let b = Amblib_zslice.bytes_of_vint l in
	 assert(!p_term + l + b <= tstring_len);
	 Amblib_zslice.set_vint head.termstrings !p_term l;
	 p_term := !p_term + b;
	 String.blit term 0 head.termstrings !p_term l;
	 p_term := !p_term + l;
	 incr i;
	 if A.(vec.vec_slices) >= 2 then (
           let lvec = Array.make A.(vec.vec_slices) null_slice in
           Netmcore_hashtbl.add lvec_table vecidx A.(lvec, vec.vec_docs)
	 )
      )
      A.(dump_ft.ft_vectors);
    
    create pool_id head lvec_table
      (* do the copying of [head] into shm in a separate function to ensure
	 that dump_ft is not reachable anymore
       *)
  with
    | Failure msg ->
	failwith ("Cannot load feature '" ^ A.(dump_ft.ft_name) ^ "': " ^ 
		    msg)


let load_vectorslice ft dump_slice =
  try
    let head = Netmcore_ref.deref_ro ft.head in
    let lvec_table = ft.lvec_table in
    
    (* Checks *)
    if A.(dump_slice.sl_index < 0 || dump_slice.sl_index >= head.num_terms) then
      failwith "bad sl_index";
    
    (* TODO: also check that there is no shortvector for sl_index *)
    
    (* Add sl_data to datastrings: *)
    let data_pos = head.datastring_end in
    assert(data_pos > 0);
    let ds_len = Bytes.length head.datastrings in
    let l = String.length A.(dump_slice.sl_data) in
    let b = Amblib_zslice.bytes_of_vint l in
    if data_pos + l + b > ds_len then
      failwith "datastring buffer overflow";
    
    Amblib_zslice.set_vint head.datastrings data_pos l;
    String.blit A.(dump_slice.sl_data) 0 head.datastrings (data_pos+b) l;
    head.datastring_end <- data_pos + l + b;
    
    (* create tvec_head *)
    let tvec_head =
      { tvec_slice_start = A.(Amblib_dump.hyper dump_slice.sl_start);
	tvec_slice_count = A.(dump_slice.sl_count);
	tvec_slice_min_docid = Amblib_dump.hyper A.(dump_slice.sl_min_docid);
	tvec_slice_max_docid = Amblib_dump.hyper A.(dump_slice.sl_max_docid);
	tvec_rest_max_wdt = A.(dump_slice.sl_rest_max_docw);
      } in
    
    let lvec_slice =
      { tvec_head;
	tvec_data_pos = data_pos + b;
	tvec_data_len = l;
      } in
    
    try
      Netmcore_heap.modify
	(Netmcore_hashtbl.heap lvec_table)
	(fun mut ->
           let (cur_lvec, _) =
             Netmcore_hashtbl.find_ro
               lvec_table
               A.(dump_slice.sl_index) in
           if A.(dump_slice.sl_ord) >= Array.length cur_lvec then
             failwith("Bad ordinal slice number sl_ord=" ^ 
                        string_of_int A.(dump_slice.sl_ord) ^ 
                          " when expected number of slices=" ^ 
                            string_of_int (Array.length cur_lvec));
           cur_lvec.( A.(dump_slice.sl_ord) ) <- 
             Netmcore_heap.add mut lvec_slice;
	)
    with
      | Not_found ->
          (* Well, this should not be possible... *)
          Netmcore_hashtbl.add
            lvec_table
            A.(dump_slice.sl_index)
            ( [| lvec_slice |], 0L )
  with
    | Failure msg ->
	failwith ("Cannot load vectorslice: " ^ msg)

	    

let term_eff_docs ft k =
  let (_, n) =
    Netmcore_hashtbl.find_ro ft.lvec_table k in
  if n = 0L then raise Not_found;
  Int64.add n 0L   (* make a copy *)


let load_shortvector ft dump_svec =
  try
    let head = Netmcore_ref.deref_ro ft.head in

    (* Checks *)
    if A.(dump_svec.ssl_index < 0 || dump_svec.ssl_index >= head.num_terms) then
      failwith ("bad ssl_index: " ^ string_of_int A.(dump_svec.ssl_index));
    
    (* TODO: also check that there is no shortvector for sl_index *)
    
    (* Add ssl_data to datastrings: *)
    let data_pos = head.datastring_end in
    assert(data_pos > 0);
    let l = String.length A.(dump_svec.ssl_data) in
    let b = Amblib_zslice.bytes_of_vint l in
    Amblib_zslice.set_vint head.datastrings data_pos l;
    String.blit A.(dump_svec.ssl_data) 0 head.datastrings (data_pos+b) l;
    head.datastring_end <- data_pos + l + b;
    
    (* Remember data_pos in the svec array: *)
    set_svec head.svec A.(dump_svec.ssl_index) data_pos
  with
    | Failure msg ->
	failwith ("Cannot load shortvector: " ^ msg)
	
let find_term ft term =
  let term = Bytes.of_string term in
  let head = Netmcore_ref.deref_ro ft.head in
  let cache = Hashtbl.create 25 in

  let rec term_at k =
    try
      Hashtbl.find cache k
    with
      | Not_found ->
          let m = k / index_mult in
          let p = 
            if k < index_mult then
              0
            else 
              head.termstring_index.( m-1 ) in
          term_fill_cache p (m*index_mult) 0;
          term_at k

  and term_fill_cache p k j =
    if j < index_mult && k < head.num_terms then (
      let l = Amblib_zslice.get_vint head.termstrings p in
      let b = Amblib_zslice.bytes_of_vint l in
      let t = Bytes.sub head.termstrings (p+b) l in
      Hashtbl.add cache k t;
      term_fill_cache (p+b+l) (k+1) (j+1)
    )
    else
      ()  in
    
  let rec bsearch lbound ubound =
    (* term_at lbound <= term <= term_at ubound *)
    if lbound > ubound then
      raise Not_found
    else (
      let middle = lbound + (ubound - lbound) / 2 in
      let t = term_at middle in
      let c = Bytes.compare term t in
      if c = 0 then
        middle
      else
        if c > 0 then
          bsearch (middle+1) ubound
        else
          bsearch lbound (middle-1)
    ) in

  bsearch 0 (head.num_terms-1)


let get_shortvector ft k =
  let head = Netmcore_ref.deref_ro ft.head in
  if k < 0 || k >= head.num_terms then
    invalid_arg "get_shortvector";

  let p = get_svec head.svec k in
  if p=0 then
    raise Not_found
  else (
    let l = Amblib_zslice.get_vint head.datastrings p in
    let b = Amblib_zslice.bytes_of_vint l in
(* eprintf "get_shortvector k=%d p=%d l=%d b=%d\n%!" k p l b; *)
    (head.datastrings, (p+b), l)
  )


let get_sliced_vector ft k =
  let head = Netmcore_ref.deref_ro ft.head in
  if k < 0 || k >= head.num_terms then
    invalid_arg "get_sliced_vector";

  let (slices,_) = Netmcore_hashtbl.find_ro ft.lvec_table k in
  Array.map
    (fun sl ->
       (sl.tvec_head, head.datastrings, sl.tvec_data_pos, sl.tvec_data_len)
    )
    slices

