(* $Id$ *)

(** Term vectors *)

type shared_tvector
  (** A handle into shared memory for a term vector.

      This version of the handle is an extended 
      {!Amb_shared_feature.feature_handle}.
   *)

type tvector_iter
  (** An iterator. It also contains pointers into shared memory *)


val get_tvector : Amb_shared_feature.feature -> string -> shared_tvector
  (** Get the term vector for a given term. May raise [Not_found] *)

val eff_size_tvector : Amb_shared_master.read_handle -> shared_tvector -> int
  (** Get the number of documents, excluding deleted ones. Note that the
      decoder will also return deleted documents!
   *)

val empty_tvector : unit -> shared_tvector
  (** A dummy tvector. Trying to iterate over it results immediately in
      an [End_of_file] exception.
   *)

val decode_tvector : shared_tvector -> tvector_iter
  (** Decode a term vector from the beginning. Raises End_of_file if the
      vector is empty.
   *)

(*
val decode_tvector_at : shared_tvector -> int -> tvector_iter
  (** Decode a term vector starting at the slice that is expected to
      contain the passed docid. If there is no such slice, the function raises
      [End_of_file].
   *)
*)

val slice_docids : tvector_iter -> int array
val slice_weights : tvector_iter -> Amblib_zslice.half_float array
val slice_head : tvector_iter -> Amb_types.tvector_slice_head
  (** Return the current slice or the slice head, resp. It is ensured
      that [slice_docids] and [slice_weights] never return empty arrays.

      These values reside in normal memory, not shared memory.
   *)

val decode_positions : tvector_iter -> int -> int array
  (** Return the document positions for a document in the current slice.
      The document is given by its index in the slice (nth doc in the 
      slice, n>=0).
   *)

val decode_position_array : tvector_iter -> int array array
  (** Returns the position arrays for all document in the current slice *)

val position_decoder : tvector_iter -> Amblib_zslice.pos_decoder
  (** Returns the position decoder (which accumulates knowledge about
      positions)
   *)

val next_tvector_slice : tvector_iter -> tvector_iter
  (** Switch to the next non-empty slice, or raise [End_of_file]. *)

val mem_tvector : shared_tvector -> int -> bool
  (** Returns whether this docid is a member of the vector *)
