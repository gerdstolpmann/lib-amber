(* $Id$ *)

(** Representation of a feature in shared memory *)

(** The caller creates the feature with [create_feature], followed by
    a number of calls of [load_vectorslice] and [load_shortvector].
    The feature must have been fully created before lookups are possible.

    This modules does not lock anything. The caller must ensure that:
    - The feature is fully created by the loader process before any other
      process does lookups.
    - The feature continues to exist during the lookups.

    This is normally achieved by interactions with {!Amb_shared_master}:
    - The feature is first made visible to any processes after it is
      loaded. 
    - While doing lookups, the [read_handle] provided by
      {!Amb_shared_master} guarantees the existence of the feature.
 *)


type feature_handle
  (** A value in normal memory containing the descriptors to shared mem. *)

type feature
  (** This value contains real pointers. In particular, this value must
      not be copied to shared memory, nor be marshalled.
   *)

val handle_of_feature : feature -> feature_handle
val feature_of_handle : feature_handle -> feature

val create_feature : Netmcore.res_id -> Amblib_dump_fmt_aux.feature -> feature
  (** Creates the feature structure in shared mem for the given pool ID *)

val load_vectorslice : feature -> Amblib_dump_fmt_aux.vectorslice -> unit
  (** Load the [vectorslice] record from a dump file *)

val load_shortvector : feature -> Amblib_dump_fmt_aux.shortvector -> unit
  (** Load the [shortvector] record from a dump file *)

val pool : feature_handle -> Netmcore.res_id
  (** Get the pool ID *)

val destroy : feature_handle -> unit
  (** Destroy the feature *)

val find_term : feature -> string -> int
  (** Determine the position of the term in the array of terms. Raise
      Not_found if not found.
   *)

val get_shortvector : feature -> int -> (bytes * int * int)
  (** [let (s,p,len) = get_shortvector h k]: For the term position [k]
      (as returned by [find_term]) return the string [s] containing the
      data string, and the position [p] of the data string, and the 
      length [len] (i.e. [data_string = String.sub s p len]).
      The data string can be decoded with the module {!Amblib_zslice}.

      The string [s] points into shared memory!

      Raises [Not_found] if there is no shortvector for this term.
   *)

val get_sliced_vector : 
        feature -> int -> 
          (Amb_types.tvector_slice_head * bytes * int * int) array
  (** [let slices = get_sliced_vector h k]: For the term position [k]
      (as returned by [find_term]) return the vector slices.
      Every slice is a tuple [(head,s,p,len)]. The data string is
      given as for [get_shortvector], i.e.
      [data_string = String.sub s p len].

      The [head] record and the string [s] point into shared memory!

      Raises [Not_found] if there is no sliced vector for this term.
   *)

val term_eff_docs : feature -> int -> int64
  (** [term_eff_docs h k]: returns the number of docs for sliced
      vectors. Raise Not_found otherwise
   *)

