(* $Id$ *)

open Amb_eval
open Netcgi
open Printf

let url_esc = Netencoding.Url.encode
let html_esc = Netencoding.Html.encode ~in_enc:`Enc_utf8 ()

let search url_prefix index (cgi:cgi) =
  let out = cgi # out_channel # output_string in
  let q = cgi # argument_value "q" in
  out (sprintf "<html><head><title>Search</title></head>\n");
  out "<body>\n";
  out "<form action=''>\n";
  out (sprintf "Search for: <input type=text name=q size=60 value=\"%s\">"
         (html_esc q));
  out "<a href=\"docs/QUERY_SYNTAX.txt\">Query syntax</a><br>\n";
  out "<input name=count type=checkbox>Count only<br>\n";
  out "</form>\n";
  out "<hr>\n";

  if q <> "" then (
    try
      let qwords, parsed = Amb_query_analyzer.w_analyze q in
      if cgi # argument_value "count" <> "" then (
	let t0 = Unix.gettimeofday() in
	let rhead = Amb_eval.count index qwords parsed () in
	let t1 = Unix.gettimeofday() in
	out (sprintf "eval time: %f<br>\n" (t1-.t0));
	out (sprintf "size: %d<br>\n" rhead.rvec_size);
      )
      else (
	let t0 = Unix.gettimeofday() in
	let result = Amb_eval.eval index qwords parsed ~len:50 ~time:100.0 () in
	let n = Array.length result.rvec_docid in
	let udocid_att = Amb_index.get_attribute index "@udocid" in
	let t1 = Unix.gettimeofday() in
	out (sprintf "eval time: %f<br>\n" (t1-.t0));
	out "<ul>\n";
	for k = 0 to n - 1 do
	  let docid = result.rvec_docid.(k) in
	  let udocid = Amblib_attvector.Attribute.get_int64 udocid_att docid in
	  out "<li>";
	  out
	    (sprintf "<a href=\"%s/w/index.php?curid=%Ld\">ID %d - XID %Ld - score %f\n"
	       url_prefix
	       udocid
	       docid
	       udocid
	       result.rvec_score.(k))
	done;
	out "</ul>";
      )
    with
      | Amb_query_analyzer.Syntax_error ->
	  out "<b>Syntax error</b>"

  );

  out "</body>\n";
  out "</html>\n";
  cgi # out_channel # commit_work()


let search_handler url_prefix index =
  { Nethttpd_services.dyn_handler = 
      (fun eenv cgi -> 
	 search url_prefix index cgi);
    dyn_activation = 
      Nethttpd_services.std_activation `Std_activation_buffered;
    dyn_uri = 
      None;
    dyn_translator = 
      (fun _ -> "");
    dyn_accept_all_conditionals = 
      false;
  }


let web_factory url_prefix index =
  Nethttpd_plex.nethttpd_factory
    ~handlers:["search", (search_handler url_prefix index) ]
    ()



let start() =
  let (opt_list, cmdline_cfg) = Netplex_main.args() in

  let mem_file = ref "" in
  let index_dir = ref "" in
  let url_prefix = ref "http://en.wikipedia.org" in

  let opt_list' =
    [ "-mem", Arg.Set_string mem_file, "<file>  Set the memory file";

      "-index-dir", Arg.Set_string index_dir,
      "<dir>  set the inverted index dir";
      
      "-url-prefix", Arg.Set_string url_prefix, 
      "<url>  Set the URL prefix";

    ] @ opt_list in

  Arg.parse
    opt_list'
    (fun s -> raise (Arg.Bad ("Don't know what to do with: " ^ s)))
    "usage: webui [options]";

  let parallelizer = Netplex_mp.mp() in

  (* connect_file does not yet work because of missing support for bigarrays
     in ancient
   *)

  (* let mem = Amb_ancient.connect_file !mem_file in*)
  let mem = 
    Amb_ancient.create
      (if !mem_file = "" then `Shared_memory else `File !mem_file) in
  Amb_index.init mem;
  Amb_tvector.init mem;
  (* let index = Amb_index.restore() in *)
  let index = Amb_loader.load !index_dir in

  Netplex_main.startup
    parallelizer
    Netplex_log.logger_factories
    Netplex_workload.workload_manager_factories
    [ web_factory !url_prefix index ]
    cmdline_cfg
;;

Sys.set_signal Sys.sigpipe Sys.Signal_ignore;
start();;
