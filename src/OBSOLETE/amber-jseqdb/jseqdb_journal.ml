(* $Id$ *)

open Jseqdb_types
open Printf

let ( +@ ) = Int64.add
let ( -@ ) = Int64.sub
let ( *@ ) = Int64.mul
let ( /@ ) = Int64.div
let ( ~> ) = Int64.of_int  (* ">" = "convert to the bigger type" *)
let ( ~< ) = Int64.to_int  (* ">" = "convert to the smaller type" *)


let fake_journal() : journal =
object(self)
  val mutable state = `Incomplete
  val files = Hashtbl.create 10
  val file_names = Hashtbl.create 10
  val mutable next_file_index = 0

  method state =
    state

  method file_descr =
    failwith "Jseqdb_journal.file_descr: this is a fake journal"

  method reset() = 
    state <- `Incomplete;
    Hashtbl.clear files;
    Hashtbl.clear file_names;
    next_file_index <- 0

  method add_file jf rd_wr =
    let idx =
      try Hashtbl.find file_names jf.jf_basename
      with Not_found ->
	let idx = next_file_index in
	next_file_index <- idx + 1;
	Hashtbl.replace file_names jf.jf_basename idx;
	idx in
    Hashtbl.replace files idx (jf,rd_wr);
    idx
    
  method add_entry idx e =
    let (_, rd_wr) =
      try Hashtbl.find files idx
      with Not_found ->
	failwith "Jseqdb_journal.fake_journal: bad file index" in
    match e with
      | `Old_data _ ->
	  ()
      | `New_data(s,spos,slen,fpos) ->
	  rd_wr # seek fpos;
	  rd_wr # really_output s spos slen
      | `New_truncate eof ->
	  rd_wr # set_eof eof
      | `New_sync ->
	  rd_wr # sync()
      | `New_lock_op(op,k) ->
	  rd_wr # lock_op op k

  method commit() =
    ()

  method file_table =
    Hashtbl.fold
      (fun k (jf,_) acc ->
	 (k, jf) :: acc
      )
      files
      []

  method rollback _ =
    failwith "Jseqdb_journal.fake_journal: rollback not supported"

end


let encode_jf jf : string=
  let l_b = String.length jf.jf_basename in
  let l_v = String.length jf.jf_version in
  if l_b > 255 || l_v > 255 then
    failwith "Jseqdb_journal.encode_jf";
  let l = l_b + l_v + 18 in
  let s = String.create l in
  s.[0] <- Char.chr l_b;
  String.blit jf.jf_basename 0 s 1 l_b;
  s.[l_b+1] <- Char.chr l_v;
  String.blit jf.jf_version 0 s (l_b+2) l_v;
  Rtypes.write_int8 s (l_b+l_v+2) (Rtypes.int8_of_int64 jf.jf_log_eof);
  Rtypes.write_int8 s (l_b+l_v+10) (Rtypes.int8_of_int64 jf.jf_real_eof);
  s
  

let decode_jf (s:string) : journal_file =
  (* rely on index checks! don't compile with -unsafe! *)
  try
    let l_b = Char.code s.[0] in
    let b = String.sub s 1 l_b in
    let l_v = Char.code s.[l_b+1] in
    let v = String.sub s (l_b+2) l_v in
    let le = Rtypes.int64_of_int8 (Rtypes.read_int8 s (l_b+l_v+2)) in
    let re = Rtypes.int64_of_int8 (Rtypes.read_int8 s (l_b+l_v+10)) in
    { jf_basename = b;
      jf_version = v;
      jf_log_eof = le;
      jf_real_eof = re;
    }
  with
    | _ ->
	failwith "Jseqdb_journal.decode_jf"


let with_lock_op lock_op op f =
  try
    lock_op op;
    let r = f() in
    lock_op `Unlock;
    r
  with
    | error ->
	lock_op `Unlock;
	raise error
    

let access_journal 
      ?(jsuper_lock_op = (fun _ -> ()))
      md : journal =
  (* Check what we have: if the journal kvseq file can be opened, this is
     fine and we look in the superblock for the state. Valid journals
     can be rolled back! If incomplete, or the file cannot be opened, we
     remember that, and will create a good journal the first time something
     is added
   *)
  let fj = fake_journal() in
  let jstate_name = Jseqdb_superblock.Constants.jstate_name in
  let (init_kvseq_opt, init_state) =
    with_lock_op jsuper_lock_op `Shared
      (fun () ->
	 try
	   let m = Jseqdb_kvseq.manager() in
	   let kvseq = m # write md fj in
	   let purpose = kvseq#config#purpose in
	   if purpose <> "JOURNAL" then failwith "Jseqdb_journal: Not a journal";
	   let state =
	     try
	       match kvseq#superblock#variable jstate_name with
		 | 0L -> `Incomplete
		 | 1L -> `Valid
		 | 2L -> `Valid_v1
		 | 3L -> `Valid_v2
		 | _ -> raise Not_found
	     with
	       | Not_found ->
		   failwith "Jseqdb_journal: Not a journal" in
	   (Some kvseq, state)
	 with
	   | _ ->
	       (None, `Incomplete)
      )
  in
object(self)
  val mutable state = init_state
  val mutable create_flag = (init_state = `Incomplete)
    (* We set this to [true] if the journal needs to be created (again).
       If the initial journal is incomplete, this flag is initially true.
     *)
  val mutable kvseq_opt = init_kvseq_opt


  val files = Hashtbl.create 10
  val file_names = Hashtbl.create 10
  val mutable next_file_index = 0

  method state =
    state

  method file_descr =
    md

  method reset() = 
    with_lock_op jsuper_lock_op `Exclusive
      (fun () ->
	 state <- `Incomplete;
	 Hashtbl.clear files;
	 Hashtbl.clear file_names;
	 self # create_journal()
	   (* Intentionally do not reset next_file_index *)
      )

  method private create_journal() =
    let m = Jseqdb_kvseq.manager() in
    let jstate_name = Jseqdb_superblock.Constants.jstate_name in
    (* [create] just writes a new superblock, and truncates the file. 
       There is a sync at the end of create, so we can be sure the 
       data is really written to disk.
     *)
    m # create
      ~sbvars:[ jstate_name, 0L ]
      ( object
	  method fileincr = 65536L
	  method purpose = "JOURNAL"
	end
      )
      { gciter = 0L;
	gctime = (Int64.of_float (Unix.time()));
	gcvers = 0L
      }
      md;
    let kvseq = m # write md fj in
    kvseq_opt <- Some kvseq;
    create_flag <- false

  method add_file jf (rd_wr : reader_writer) =
    if state <> `Incomplete then
      failwith "Jseqdb_journal: can only add files to incomplete journals";
    if create_flag then
      with_lock_op jsuper_lock_op `Exclusive
	(fun () -> self # create_journal());
    match kvseq_opt with
      | None ->
	  failwith "Jseqdb_journal: no journal"
      | Some kvseq ->
	  let idx =
	    try Hashtbl.find file_names jf.jf_basename
	    with Not_found ->
	      let idx = next_file_index in
	      next_file_index <- idx + 1;
	      Hashtbl.replace file_names jf.jf_basename idx;
	      let jf_s = encode_jf jf in
	      ignore(
		kvseq # add
		  { k_rowid = 0L;   (* not needed here *)
		    k_key = "F" ^ string_of_int idx;
		  } 
		  jf_s
		  0
		  (String.length jf_s)
	      );
	      idx in
	  let iq = Queue.create() in
	  Hashtbl.replace files idx (jf,rd_wr,iq);
	  idx

  method add_entry idx e =
    if state <> `Incomplete then
      failwith "Jseqdb_journal: can only add entries to incomplete journals";
   let (_, rd_wr,iq) =
      try Hashtbl.find files idx
      with Not_found ->
	failwith "Jseqdb_journal: bad file index" in
    if create_flag then
      with_lock_op jsuper_lock_op `Exclusive
	(fun () -> self # create_journal());
   match kvseq_opt with
      | None ->
	  failwith "Jseqdb_journal: no journal"
      | Some kvseq ->
	  ( match (e : journal_entry) with
	      | `Old_data(s,spos,slen,fpos) ->
		  ignore(
		    kvseq # add
		      { k_rowid = 0L;   (* not needed here *)
			k_key =
			  "D" ^ string_of_int idx ^ "," ^ Int64.to_string fpos;
		      } 
		      s
		      spos
		      slen)
	      | `New_data(_,_,_,_)
	      | `New_truncate _
	      | `New_sync
	      | `New_lock_op(_,_) ->
		  Queue.add e iq
	  )

  method private make_valid() =
    match kvseq_opt with
      | None ->
	  assert false
      | Some kvseq ->
	  let jstate_name = Jseqdb_superblock.Constants.jstate_name in
	  kvseq#superblock#set_variable jstate_name 1L;  (* `Valid *)
	  kvseq#flush();   (* implies a sync *)
	  state <- `Valid
    

  method commit() =
    if state <> `Incomplete then
      failwith "Jseqdb_journal: can only commit incomplete journals";
    match kvseq_opt with
      | None ->
	  failwith "Jseqdb_journal: no journal"
      | Some kvseq ->
	  (* First we need to mark the external journal as valid: *)
	  with_lock_op jsuper_lock_op `Exclusive
	    (fun () ->
	       if create_flag then
		 self # create_journal();
	       self # make_valid();
	    );

          (* TODO: allow better data transfer between data object and
             journal. Right now `New_data is stored twice in some cases,
             once in the data object and once in the journal. This can
             be avoided. E.g. `New_stream: get a stream of data updates.
	   *)

	  (* Now iterate over the files and the internal queues. *)
	  Hashtbl.iter
	    (fun idx (jf,rd_wr,iq) ->
	       Queue.iter
		 (function
		    | `New_data(s,spos,slen,fpos) ->
			rd_wr # seek fpos;
			rd_wr # really_output s spos slen
		    | `New_truncate eof ->
			rd_wr # set_eof eof
		    | `New_sync ->
			rd_wr # sync()
		    | `New_lock_op(op,k) ->
			rd_wr # lock_op op k
		    | `Old_data _ ->
			assert false
		 )
		 iq
	    )
	    files;

	  (* Finally, put into the journal our success: *)
	  with_lock_op jsuper_lock_op `Exclusive
	    (fun () ->
	       (* We have to sync twice, to ensure the superblock is
                  written atomically
		*)
	       kvseq#flush();   (* implies a sync *)
	       let jstate_name = Jseqdb_superblock.Constants.jstate_name in
	       kvseq#superblock#set_variable jstate_name 3L;  (* `Valid_v2 *)
	       kvseq#flush();   (* implies a sync *)
	       state <- `Valid_v2
	    )

  method file_table =
    Hashtbl.fold
      (fun k (jf,_,_) acc ->
	 (k, jf) :: acc
      )
      files
      []

  method rollback get_buf =
    if state = `Incomplete then (
      if create_flag then 
	failwith "Jseqdb_journal: cannot roll back an incomplete journal";
      with_lock_op jsuper_lock_op `Exclusive
	(fun () -> self # make_valid());
    );
    assert(not create_flag);

    match kvseq_opt with
      | None ->
	  failwith "Jseqdb_journal: no journal"
      | Some kvseq ->
	  Hashtbl.clear files;
	  Hashtbl.clear file_names;

          ( try
	      let ptr = ref (kvseq#first()) in
	      while true do
		let k = kvseq#lookup !ptr in
		if k.k_key = "" then 
		  failwith
		    (sprintf "Jseqdb_journal: invalid journal [1] @ 0x%Lx" !ptr);
		let key = k.k_key in
		( match key.[0] with
		    | 'F' ->
			let (jf,rd_wr) =
			  try
			    let idx =
			      int_of_string
				(String.sub key 1 (String.length key - 1)) in
			    let jf = decode_jf (kvseq#value !ptr) in
			    let rd_wr = get_buf jf in
			    let iq = Queue.create() in
			    Hashtbl.replace files idx (jf,rd_wr,iq);
			    Hashtbl.replace file_names jf.jf_basename idx;
			    next_file_index <- max next_file_index (idx+1);
			    (jf,rd_wr)
			  with
			    | error ->
				failwith
				  (sprintf
				     "Jseqdb_journal: invalid journal [2] @ 0x%Lx nexted exn: %s"
				     !ptr (Printexc.to_string error)) in
			rd_wr # set_eof jf.jf_log_eof;
			rd_wr # set_eof jf.jf_real_eof
		    | 'D' ->
			let (idx, jf, rd_wr, fpos, v) =
			  try
			    let comma =
			      String.index_from key 1 ',' in
			    let idx =
			      int_of_string
				(String.sub key 1 (comma - 1)) in
			    let fpos =
			      Int64.of_string
				(String.sub key 
				   (comma+1) (String.length key-comma-1)) in
			    let (jf,rd_wr,_) = Hashtbl.find files idx in
			    let len = kvseq # length !ptr in
			    if fpos < 0L || 
			       fpos +@ len > jf.jf_log_eof
			    then
			      raise Not_found;
			    let v = kvseq # value !ptr in
			    (idx, jf, rd_wr, fpos, v)
			  with
			    | error  ->
				failwith 
				  (sprintf
				     "Jseqdb_journal: invalid journal [3] @ 0x%Lx nexted exn: %s"
				   !ptr (Printexc.to_string error)) in
			rd_wr # seek fpos;
			rd_wr # really_output v 0 (String.length v)
		    | _ ->
			failwith 
			  (sprintf 
			     "Jseqdb_journal: invalid journal [4] @ 0x%Lx"
			     !ptr)
		);
		ptr := kvseq # next !ptr
	      done;
	      assert false
	    with
	      | End_of_file -> ()
	  );

	  (* Sync all files: *)
	  Hashtbl.iter
	    (fun _ (_,rd_wr,_) ->
	       rd_wr # sync()
	    )
	    files;

	  (* Finally set the state of the journal itself: *)
	  with_lock_op jsuper_lock_op `Exclusive
	    (fun () ->
	       (* We have to sync twice, to ensure the superblock is
                  written atomically
		*)
	       kvseq#flush();   (* implies a sync *)
	       let jstate_name = Jseqdb_superblock.Constants.jstate_name in
	       kvseq#superblock#set_variable jstate_name 2L;  (* `Valid_v1 *)
	       kvseq#flush();   (* implies a sync *)
	       state <- `Valid_v1
	    )
end
