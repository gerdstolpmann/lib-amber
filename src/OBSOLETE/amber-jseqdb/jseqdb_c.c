/* $Id$ */

#define _GNU_SOURCE

#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>

#include "caml/alloc.h"
#include "caml/bigarray.h"
#include "caml/callback.h"
#include "caml/custom.h"
#include "caml/fail.h"
#include "caml/memory.h"
#include "caml/misc.h"
#include "caml/mlvalues.h"
#include "caml/unixsupport.h"
#include "caml/signals.h"

/* Double-check with:

   http://lkml.indiana.edu/hypermail/linux/kernel/0004.0/0728.html
   http://www.cs.ucla.edu/honors/UPLOADS/kousha/thesis.pdf

   Maybe we want to switch to pread/pwrite instead, at least when
   sequential reads are done.
*/


#define block_val(v) ((void **) Data_custom_val(v))
/* block_val(v): returns the "void *" pointing to the mmap'ed block
*/


#define OUR_PAGE_SIZE \
  (sysconf(_SC_PAGE_SIZE) < 16384 ? 16384 : sysconf(_SC_PAGE_SIZE))


value jseqdb_get_block_size_ml(value dummy)
{
    return Val_long(OUR_PAGE_SIZE);
}


static void block_finalize(value val)
{
    void **b;
    int code;

    b = block_val(val);
    if (*b != NULL) {
	code = munmap(*b, OUR_PAGE_SIZE);
	*b = NULL;
    };
}


static struct custom_operations block_ops = {
  "http://gerd-stolpmann.de/amber-jseqdb/block",
  block_finalize,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};



value jseqdb_map_block_ml(value fd_val, value rw_val, value pos_val)
{
    int fd, rw;
    int64 pos;
    void *m;
    value b_val;

    fd = Int_val(fd_val);
    rw = Bool_val(rw_val);
    pos = Int64_val(pos_val);

    m = mmap64(NULL, 
	       OUR_PAGE_SIZE, 
	       (rw ? PROT_READ|PROT_WRITE : PROT_READ),
	       MAP_SHARED,
	       fd,
	       pos);
    if (m == MAP_FAILED) {
	uerror("mmap64", Nothing);
    };

    b_val = caml_alloc_custom(&block_ops, sizeof(void *), 0, 1);
    *(block_val(b_val)) = m;

    return b_val;
}


value jseqdb_unmap_block_ml(value b_val) 
{
    void **b;
    int code;

    b = block_val(b_val);
    if (*b != NULL) {
	code = munmap(*b, OUR_PAGE_SIZE);
	*b = NULL;
	if (code == -1) uerror("munmap", Nothing);
    };
    return Val_unit;
}


value jseqdb_block_seek_ml(value b_val,
			   value fd_val,
			   value rw_val,
			   value pos_val)
{
    void **b;
    int fd, rw;
    int64 pos;
    void *m;

    fd = Int_val(fd_val);
    rw = Bool_val(rw_val);
    pos = Int64_val(pos_val);

    b = block_val(b_val);
    if (*b == NULL) {
	m = mmap64(NULL, 
		   OUR_PAGE_SIZE, 
		   (rw ? PROT_READ|PROT_WRITE : PROT_READ),
		   MAP_SHARED,
		   fd,
		   pos);
	if (m == MAP_FAILED) {
	    uerror("mmap64", Nothing);
	};
	*b = m;
    }
    else {
	m = mmap64(*b, 
		   OUR_PAGE_SIZE, 
		   (rw ? PROT_READ|PROT_WRITE : PROT_READ),
		   MAP_SHARED|MAP_FIXED,
		   fd,
		   pos);
	if (m == MAP_FAILED) {
	    uerror("mmap64", Nothing);
	};
    }

    return Val_unit;
}


value jseqdb_block_input_ml(value b_val, value bpos_val,
			    value s_val, value spos_val,
			    value len_val)
{
    void **b;
    char *s;
    int bpos;
    int spos;
    int len;

    b = block_val(b_val);
    s = String_val(s_val);
    spos = Int_val(spos_val);
    bpos = Int_val(bpos_val);
    len = Int_val(len_val);

    /* Unsafe! */
    memcpy(s+spos, 
	   ((char *) (*b)) + bpos,
	   len);

    return Val_unit;
}


value jseqdb_block_output_ml(value b_val, value bpos_val,
			     value s_val, value spos_val,
			     value len_val)
{
    void **b;
    char *s;
    int bpos;
    int spos;
    int len;

    b = block_val(b_val);
    s = String_val(s_val);
    spos = Int_val(spos_val);
    bpos = Int_val(bpos_val);
    len = Int_val(len_val);

    /* Unsafe! */
    memcpy(((char *) (*b)) + bpos,
	   s+spos, 
	   len);

    return Val_unit;
}


value jseqdb_block_input_vint_ml(value b_val,
				 value state_val,
				 value pos_val,
				 value len_val,
				 value cell_val)
{
    unsigned char *b;
    unsigned char c;
    int state;
    int pos;
    int len;
    int64 n;
    int m;
    CAMLparam1(cell_val);   /* The other values do not need registration */

    state = Int_val(state_val) & 0xf;
    pos = Int_val(pos_val);
    len = Int_val(len_val);
    b = * ((unsigned char **) (block_val(b_val)));
    n = Int64_val(Field(cell_val, 0));
    m = 0;

    if (state == 0) {
	c = b[pos];
	switch (c >> 4) {
	case 0: case 1: case 2:	case 3:	case 4:	case 5:	case 6:	case 7:
	    state = 0x10;
	    n = c;
	    goto out;
	case 8:	case 9:	case 10: case 11:
	    state = 1;
	    n = c & 0x3f;
	    break;
	case 12: case 13:
	    state = 2;
	    n = c & 0x1f;
	    break;
	case 14:
	    state = 3;
	    n = c & 0x0f;
	    break;
	case 15:
	    {
		switch (c & 15) {
		case 0: case 1: case 2:	case 3:	case 4:	case 5:	case 6:	case 7:
		    state = 4;
		    n = c & 7;
		    break;
		case 8:	case 9:	case 10: case 11:
		    state = 5;
		    n = c & 3;
		case 12: case 13:
		    state = 6;
		    n = c & 0x1;
		    break;
		case 14:
		    state = 7;
		    n = 0;
		    break;
		case 15:
		    state = 8;
		    n = 0;
		    break;
		}
	    }
	};
	pos++;
	m=1;
    };
    while (state > 0 && m < len) {
	n = (n << 8) | b[pos];
	state--;
	m++;
	pos++;
    };
    state = state | (m << 4);
 out:
    Store_field(cell_val, 0, caml_copy_int64(n));
    CAMLreturn(Val_int(state));
}


value jseqdb_block_output_vint_ml(value b_val,
				  value state_val,
				  value pos_val,
				  value len_val,
				  value number_val)
{
    unsigned char *b;
    int state;
    int pos;
    int len;
    int m;
    uint64 n;

    state = Int_val(state_val) & 0xf;
    pos = Int_val(pos_val);
    len = Int_val(len_val);
    n = Int64_val(number_val);
    b = * ((unsigned char **) (block_val(b_val)));
    m = 0;

    if (state == 0) {
	if (n <= 0x7f) {
	    b[pos] = n;
	    state = 0x10;
	    goto out;
	}
	else if (n <= 0x3fffull) {
	    b[pos] = (n >> 8) | 0x80;
	    state = 1;
	}
	else if (n <= 0x1fffffull) {
	    b[pos] = (n >> 16) | 0xc0;
	    state = 2;
	}
	else if (n <= 0x0fffffffull) {
	    b[pos] = (n >> 24) | 0xe0;
	    state = 3;
	}
	else if (n <= 0x07ffffffffull) {
	    b[pos] = (n >> 32) | 0xf0;
	    state = 4;
	}
	else if (n <= 0x03ffffffffffull) {
	    b[pos] = (n >> 40) | 0xf8;
	    state = 5;
	}
	else if (n <= 0x01ffffffffffffull) {
	    b[pos] = (n >> 48) | 0xfc;
	    state = 6;
	}
	else if (n <= 0xffffffffffffffull) {
	    b[pos] = (n >> 56) | 0xfe;
	    state = 7;
	}
	else {
	    b[pos] = 0xff;
	    state = 8;
	}
	pos++;
	m=1;
    };
    while (state > 0 && m < len) {
	b[pos] = (n >> ((state - 1) << 3)) & 0xff;
	state--;
	pos++;
	m++;
    };
    state = state | (m << 4);
 out:
    return Val_int(state);
}


value jseqdb_fallocate_ml(value fd_val, value off_val, value len_val)
{
    int fd;
    int64 off, len;
    int code;

    fd = Int_val(fd_val);
    off = Int64_val(off_val);
    len = Int64_val(len_val);

    code = posix_fallocate64(fd, off, len);
    if (code != 0) {
	unix_error(code, "posix_fallocate", Nothing);
    }

    return Val_unit;
}


value jseqdb_fdatasync_ml(value fd_val)
{
    int code;

    code = fdatasync(Int_val(fd_val));
    if (code == -1)
	uerror("fdatasync", Nothing);

    return Val_unit;
}


value jseqdb_lock_op_ml(value fd_val, value op_val, value k_val)
{
    int code;
    int fd;
    struct flock lck;

    fd = Int_val(fd_val);
    lck.l_type = 0;
    
    switch (Int_val(op_val)) {
    case 0: 
	lck.l_type = F_RDLCK; break;
    case 1: 
	lck.l_type = F_WRLCK; break;
    case 2: 
	lck.l_type = F_UNLCK; break;
    };

    lck.l_whence = SEEK_SET;
    lck.l_start = Long_val(k_val);
    lck.l_len = 1;
    lck.l_pid = 0;

    enter_blocking_section();
    code = fcntl(fd, F_SETLKW, &lck);
    leave_blocking_section();
    if (code == -1)
	uerror("fcntl[F_SETLK]", Nothing);

    return Val_unit;
}
