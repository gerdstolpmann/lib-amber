(* $Id$ *)

open Jseqdb_types

class managed_descr : 
        filename:string -> 
        flags:Unix.open_flag list ->
        reopen_flags:Unix.open_flag list ->
        perm:int ->
        reopen_perm:int ->
            file_descr
  (** A sample implementation of [file_descr]. The file [filename]
      is opened with open [flags] and open [perm]issions. After the first
      successful disposal the file is automatically reopened with
      the [reopen_flags] and the [reopen_perm]issions.
   *)

val rw_descr : string -> file_descr
  (** Opens this file rw. It must already exist *)

val ro_descr : string -> file_descr
  (** Opens this file ro. *)

class buf_rd_wr : file_descr -> reader_writer
  (** Buffered [reader_writer] accessing the passed file descriptor.
   *)


class sub_rd_wr : ?incr:int64 -> int64 -> reader_writer -> reader_writer
  (** View on the passed [reader_writer]: The size of the file is assumed to
     only be the passed number, or if it is written beyond the old
     assumed size, the written size.

     If [incr] is passed, every time the physical EOF is exceeded,
     [incr] bytes with value 0 are appended.
   *)


(** {2 Utility functions} *)

val input_string : reader_writer -> int -> string
  (** [input_string rw n]: read exactly [n] bytes from [rw] *)

val output_string : reader_writer -> string -> unit
  (** [output_string rw s]: write [s] to [rw] *)

val with_shared_lock : file_descr -> int -> (unit -> 'a) -> 'a
val with_exclusive_lock : file_descr -> int -> (unit -> 'a) -> 'a


val copy_file : string -> string -> unit
