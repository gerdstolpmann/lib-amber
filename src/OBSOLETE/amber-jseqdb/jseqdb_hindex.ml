(* $Id$ *)

open Printf
open Jseqdb_types
open Jseqdb_buffer

let ( +@ ) = Int64.add
let ( -@ ) = Int64.sub
let ( *@ ) = Int64.mul
let ( /@ ) = Int64.div
let ( ~> ) = Int64.of_int  (* ">" = "convert to the bigger type" *)
let ( ~< ) = Int64.to_int  (* ">" = "convert to the smaller type" *)

let sb_get sb name =
  try
    sb#variable name
  with
    | Not_found -> failwith ("Jseqdb_hindex: " ^ name ^ " not found")


let hindex_sbvals (superblock : superblock) =
  let format =
    sb_get superblock Jseqdb_superblock.Constants.format_name in
  let syncsize =
    sb_get superblock Jseqdb_superblock.Constants.syncsize_name in
  let synctime =
    sb_get superblock  Jseqdb_superblock.Constants.synctime_name in
  let purpose =
    Jseqdb_superblock.to_string
      (sb_get superblock Jseqdb_superblock.Constants.purpose_name) in
  let gciter =
    sb_get superblock  Jseqdb_superblock.Constants.gciter_name in
  let gcvers =
    sb_get superblock  Jseqdb_superblock.Constants.gcvers_name in
  let gctime =
    sb_get superblock  Jseqdb_superblock.Constants.gctime_name in
  let entries =
    sb_get superblock Jseqdb_superblock.Constants.entries_name in
  let aentries =
    sb_get superblock Jseqdb_superblock.Constants.aentries_name in
  let htsize =
    sb_get superblock Jseqdb_superblock.Constants.htsize_name in
object
  method format = format
  method syncsize = syncsize
  method synctime = synctime
  method purpose = purpose
  method gciter = gciter
  method gcvers = gcvers
  method gctime = gctime
  method entries = entries
  method aentries = aentries
  method htsize = htsize
end

let jf_version sbvals =
  sprintf "%Lx.%Lx.%Lx.%Lx.%Lx.%Lx"
    sbvals#gciter sbvals#gcvers sbvals#syncsize sbvals#synctime
    sbvals#aentries sbvals#entries


let empty_cell = 0L
let deleted_cell = (-1L)


let hash_pair s =
  (* Returns one hash as 8 byte string, and
     another hash as non-negative int64
   *)
  let md5 = Digest.string s in
  let h1 = String.sub md5 0 8 in
  let h2 = String.sub md5 8 8 in
  let i2 = 
    Int64.logand
      (Rtypes.int64_of_int8 (Rtypes.read_int8 h2 0)) 
      0x7fff_ffff_ffff_ffff_L in
  (h1, i2)


class hindex_impl (md : file_descr) journal_opt : hindex =
  let rdwr0 = new buf_rd_wr md in
  let superblock = (
    let sb = Jseqdb_superblock.create() in
    sb # read rdwr0;
    sb # clear_modified();
    sb
  ) in
  let sbdata = superblock # data in   (* remember that for [flush] *)
  let sbvals = hindex_sbvals superblock in  (* initial values in sb *)
  let () = (
    if sbvals#format <> Jseqdb_superblock.Constants.hindex_format then
      failwith "Jseqdb_hindex: bad FORMAT"
  ) in
  let config = (
    ( object
	method htsize = sbvals#htsize
	method purpose = sbvals#purpose
      end
    )
  ) in
  let num_entries =
    ref (sbvals#entries) in
  let num_aentries =
    ref (sbvals#aentries) in
  let rdwr1 =
    new sub_rd_wr sbvals#syncsize rdwr0 in
  let jidx =
    match journal_opt with
      | None -> 0
      | Some j ->
	  j # add_file
	    { jf_basename = Filename.basename md#filename;
	      jf_version = jf_version sbvals;
	      jf_log_eof = sbvals#syncsize;
	      jf_real_eof = rdwr0#eof
	    }
	    rdwr1 in
  let sbsize = superblock#size in
object(self)
  val overlay = Hashtbl.create 97
    (* Maps cell index to cell contents *)

  val mutable modified = false

  method filename = 
    md # filename

  method superblock =
    superblock

  method config =
    config

  method num_entries =
    !num_entries

  method num_active_entries =
    !num_aentries

  method private get_cell k =
    (* where 0 <= k < config#htsize *)
    try
      Hashtbl.find overlay k
    with
      | Not_found ->
	  let fpos = k *@ 16L +@ sbsize in
	  rdwr1 # seek fpos;
	  let cksum_s = input_string rdwr1 8 in
	  let dpos_s = input_string rdwr1 8 in
	  (cksum_s, Rtypes.int64_of_int8 (Rtypes.read_int8 dpos_s 0))

  method private update_cell k old_cksum old_dpos new_cksum new_dpos =
    match journal_opt with
      | None ->
	  assert false
      | Some j ->
	  Hashtbl.replace overlay k (new_cksum, new_dpos);
	  let d = String.create 16 in
	  String.blit old_cksum 0 d 0 8;
	  Rtypes.write_int8 d 8 (Rtypes.int8_of_int64 old_dpos);
	  j # add_entry jidx (`Old_data(d, 0, 16, k *@ 16L +@ sbsize));
	  modified <- true
	    
  method private probe f k0 =
    (* probes at k0 and following cells. Calls f for each cell. Probing
       does only stop automatically after one whole round. f must return
       whether to continue
     *)
    let cont = ref true in
    let k = ref k0 in
    while !cont do
      let (cksum, dpos) = self # get_cell !k in
      cont := (f !k cksum dpos);
      k := Int64.succ !k;
      if !k = config#htsize then k := 0L;
      if !k = k0 then cont := false
    done

  method lookup key =
    let (key_cksum, i) = hash_pair key in
    let k0 = Int64.rem i config#htsize in
    let r = ref None in
    self # probe
      (fun k cksum dpos ->
	 dpos <> empty_cell && (
	   if dpos <> deleted_cell && cksum = key_cksum then
	     r := Some dpos;
	   true
	 )
      )
      k0;
    !r
      
  method update key new_dpos =
    if journal_opt = None then
      failwith "Jseqdb_hindex: read-only";
    let (key_cksum, i) = hash_pair key in
    let k0 = Int64.rem i config#htsize in
    let r = ref None in
    self # probe
      (fun k cksum dpos ->
	 if dpos = empty_cell then (
	   r := Some(k, cksum, dpos);
	   num_entries := Int64.succ !num_entries;
	   num_aentries := Int64.succ !num_aentries;
	   false
	 )
	 else (
	   if cksum = key_cksum then (
	     if dpos = deleted_cell then
	       num_aentries := Int64.succ !num_aentries;
	     r := Some(k, cksum, dpos);
	     false
	   )
	   else
	     true
	 )
      )
      k0;
    match !r with
      | None ->
	  failwith "Jseqdb_hindex: table full"
      | Some(k, old_cksum, old_dpos) ->
	  self # update_cell k old_cksum old_dpos key_cksum new_dpos

  method delete key =
    if journal_opt = None then
      failwith "Jseqdb_hindex: read-only";
    let (key_cksum, i) = hash_pair key in
    let k0 = Int64.rem i config#htsize in
    self # probe
      (fun k cksum dpos ->
	 dpos <> empty_cell && (
	   if dpos <> deleted_cell && cksum = key_cksum then (
	     let k' = Int64.succ k in
	     let k' = if k' = config#htsize then 0L else k' in
	     let (_, dpos') = self # get_cell k' in
	     if dpos' = empty_cell then (
	       num_entries := Int64.pred !num_entries;
	       num_aentries := Int64.pred !num_aentries;
	       self # update_cell k cksum dpos key_cksum empty_cell
	     )
	     else (
	       num_aentries := Int64.pred !num_aentries;
	       self # update_cell k cksum dpos key_cksum deleted_cell
	     );
	     modified <- true;
	     false
	   )
	   else
	     true
	 )
      )
      k0

  method flush() =
    match journal_opt with
      | None ->
	  failwith "Jseqdb_hindex: read-only"
      | Some j ->
	  if modified || superblock#is_modified then (
	    (* Remember the old superblock: *)
	    j # add_entry jidx (`Old_data sbdata);
	    (* Update the superblock: *)
	    let now64 = Int64.of_float (Unix.time()) in
	    superblock # set_variable
	      Jseqdb_superblock.Constants.syncsize_name rdwr1#eof;
	    superblock # set_variable
	      Jseqdb_superblock.Constants.synctime_name now64;
	    superblock # set_variable
	      Jseqdb_superblock.Constants.entries_name !num_entries;
	    superblock # set_variable
	      Jseqdb_superblock.Constants.aentries_name !num_aentries;
	    (* Write the data, then sync: *)
	    let fpos_array =
	      Array.make (Hashtbl.length overlay) 0L in
	    let p = ref 0 in
	    Hashtbl.iter
	      (fun fpos _ ->
		 fpos_array.( !p ) <- fpos;
		 incr p
	      )
	      overlay;
	    Array.sort Int64.compare fpos_array;
	    (* Note: sorting by fpos could also be done by the journal *)
	    for p = 0 to Array.length fpos_array - 1 do
	      let d = String.create 16 in
	      let k = fpos_array.( p ) in
	      let fpos = k *@ 16L +@ sbsize in
	      let (cksum, dpos) = Hashtbl.find overlay k in
	      String.blit cksum 0 d 0 8;
	      Rtypes.write_int8 d 8 (Rtypes.int8_of_int64 dpos);
	      j # add_entry jidx (`New_data(d, 0, 16, fpos))
	    done;
	    (* Write the new superblock, and sync: *)
	    j # add_entry jidx (`New_data superblock#data);
	    j # add_entry jidx `New_sync;
	    
	    modified <- false;
	    superblock # clear_modified();

	    Hashtbl.clear overlay
	      (* We assume that the [flush] is followed by a journal commit,
                 so the disk is again in sync with the "logical" view, and
                 the overlay can be emptied.

                 The "user" of this feature is the compaction run. During
                 a run, the data is flushed and committed several times.
                 This is ok because the destination files are new anyway
	       *)
	  )

end


class manager() : hindex_manager =
object
  method create config gcp md =
    let now64 = Int64.of_float (Unix.time()) in
    let rdwr0 = new buf_rd_wr md in
    let sb = Jseqdb_superblock.create() in
    let eof = sb#size +@ (16L *@ config#htsize) in
    sb # set_variable 
      Jseqdb_superblock.Constants.format_name 
      Jseqdb_superblock.Constants.hindex_format;
    sb # set_variable
      Jseqdb_superblock.Constants.syncsize_name
      eof;
    sb # set_variable
      Jseqdb_superblock.Constants.synctime_name
      now64;
    sb # set_variable
      Jseqdb_superblock.Constants.purpose_name
      (Jseqdb_superblock.of_string config#purpose);
    sb # set_variable
      Jseqdb_superblock.Constants.gciter_name
      gcp.gciter;
    sb # set_variable
      Jseqdb_superblock.Constants.gcvers_name 
      gcp.gcvers;
    sb # set_variable
      Jseqdb_superblock.Constants.gctime_name
      now64;
    sb # set_variable
      Jseqdb_superblock.Constants.entries_name
      0L;
    sb # set_variable
      Jseqdb_superblock.Constants.aentries_name
      0L;
    sb # set_variable
      Jseqdb_superblock.Constants.htsize_name
      config#htsize;
    sb # write rdwr0;
    rdwr0 # sync();
    rdwr0 # set_eof sb#size;
    rdwr0 # set_eof eof

  method read md =
    new hindex_impl md None

  method write md j =
    new hindex_impl md (Some j)
end


let manager() = new manager()
