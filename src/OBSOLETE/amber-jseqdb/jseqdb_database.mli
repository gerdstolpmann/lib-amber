(* $Id$ *)

open Jseqdb_types

val manager : unit -> database_manager

(** Known bugs:

    - If a table is created or dropped by a process, another process 
      will not see this change until it opens the database again.
 *)
