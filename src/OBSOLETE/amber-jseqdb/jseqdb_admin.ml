(* $Id$ *)

open Jseqdb_types
open Jseqdb_buffer
open Printf

let ( +@ ) = Int64.add
let ( -@ ) = Int64.sub
let ( *@ ) = Int64.mul
let ( /@ ) = Int64.div
let ( ~> ) = Int64.of_int  (* ">" = "convert to the bigger type" *)
let ( ~< ) = Int64.to_int  (* "<" = "convert to the smaller type" *)

let getopt name =
  function
    | None ->
	failwith ("Required arg missing: " ^ name)
    | Some s ->
	s


let show_format v =
  if v = Jseqdb_superblock.Constants.kvseq_format then
    "kvseq"
  else if v = Jseqdb_superblock.Constants.hindex_format then
    "hindex"
  else 
    "unknown"

let show_purpose v =
  Rtypes.int8_as_string (Rtypes.int8_of_int64 v)


let cmd_superblock pgm_name =
  let mods = ref [] in
  let file = ref None in
  Arg.parse
    [ "-set", Arg.Tuple(let name = ref "" in
                        [ Arg.Set_string name;
                          Arg.String
                            (fun s ->
                               let n =
                                 try Int64.of_string s
                                 with _ ->
                                   raise(Arg.Bad("Not an int64: " ^ s)) in
                               mods := !mods @ [ !name, n ]
                            )
                        ]),
      "<name> <value>  Set the variable <name> to a numeric <value>";
    ]
    (fun s ->
       match !file with
         | None -> file := Some s
         | Some _ ->
             raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s superblock [options] file" pgm_name);

  let file = getopt "file" !file in

  if !mods = [] then (
    let md = Jseqdb_buffer.ro_descr file in
    let ro = new Jseqdb_buffer.buf_rd_wr md in
    let sb = Jseqdb_superblock.create() in
    sb # read ro;
    let add_info =
      [ Jseqdb_superblock.Constants.format_name,  show_format;
        Jseqdb_superblock.Constants.purpose_name, show_purpose;
      ] in
    let vars = sb # variables in
    ro # dispose_descr();
    printf "Superblock contents:\n";
    List.iter
      (fun (name, value) ->
         let info =
           try 
             let f = List.assoc name add_info in
             "(" ^ f value ^ ")"
           with Not_found -> "" in
         printf "  %-8s: %19Ld %s\n" name value info
      )
      vars
  )
  else (
    let md = Jseqdb_buffer.rw_descr file in
    let rw = new Jseqdb_buffer.buf_rd_wr md in
    let sb = Jseqdb_superblock.create() in
    sb # read rw;
    List.iter
      (fun (name, value) ->
	 sb # set_variable name value
      )
      !mods;
    sb # write rw;
    rw # dispose_descr();
    print_endline "Modified superblock."
  )


let cmd_create_db pgm_name =
  let dbdir = ref None in
  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Create a DB in this directory";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s create_db -db <dbdir>" pgm_name);
  
  let dbdir = getopt "-db" !dbdir in
  
  let dbm = Jseqdb_database.manager() in
  dbm # createdb dbdir;
  print_endline "Database created."


let cmd_drop_db pgm_name =
  let dbdir = ref None in
  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Drop the DB in this directory";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s drop_db -db <dbdir>" pgm_name);
  
  let dbdir = getopt "-db" !dbdir in
  
  let dbm = Jseqdb_database.manager() in
  dbm # dropdb dbdir;
  print_endline "Database dropped."


let cmd_clean_db pgm_name =
  let dbdir = ref None in
  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Clean the DB in this directory";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s clean_db -db <dbdir>" pgm_name);
  
  let dbdir = getopt "-db" !dbdir in
  
  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  if db # journal # state = `Valid then (
    db # start_transaction ~rw:true ();
    db # commit_transaction();
    prerr_endline "Transaction cleanup done."
  )
  else
    prerr_endline "No clean action required.";

  db # close()


let check_journal db =
  if db # journal # state = `Valid then
    prerr_endline "Warning: Transaction state is not clean"


let cmd_create_table pgm_name =
  let dbdir = ref None in
  let tabname = ref None in
  let purpose = ref "*" in
  let fileincr = ref 65536 in
  let idxsize = ref 4096 in

  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Use the DB in this directory";

      "-table", Arg.String (fun s -> tabname := Some s),
      "<name>   Create this table";

      "-purpose", Arg.Set_string purpose,
      "<string>   Set the purpose string (up to 8 bytes)";

      "-fileincr", Arg.Set_int fileincr,
      "<n>   Set the file increment for the data file (in bytes)";

      "-idxsize", Arg.Set_int idxsize,
      "<n>   Set the initial size of the index file (in #entries)";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s create_table -db <dbdir> -table <tabname>" pgm_name);

  let dbdir = getopt "-db" !dbdir in
  let tabname = getopt "-table" !tabname in

  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  check_journal db;
  
  let config =
    ( object
	method data_config =
	  ( object
	      method fileincr = Int64.of_int !fileincr
	      method purpose = !purpose
	    end
	  )
	method pp_config =
	  ( object
	      method fileincr = 65536L
	      method purpose = !purpose
	    end
	  )
	method idx_config =
	  ( object
	      method htsize = Int64.of_int !idxsize
	      method purpose = !purpose
	    end
	  )
      end
    ) in

  db # table_create config tabname;
  db # close();

  print_endline "Table created."


let cmd_drop_table pgm_name =
  let dbdir = ref None in
  let tabname = ref None in

  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Use the DB in this directory";

      "-table", Arg.String (fun s -> tabname := Some s),
      "<name>   Drop this table";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s drop_table -db <dbdir> -table <tabname>" pgm_name);

  let dbdir = getopt "-db" !dbdir in
  let tabname = getopt "-table" !tabname in

  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  check_journal db;

  db # table_drop tabname;
  db # close();

  print_endline "Table dropped."
  

let cmd_reindex_table pgm_name =
  let dbdir = ref None in
  let tabname = ref None in
  let idxsize = ref None in

  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Use the DB in this directory";

      "-table", Arg.String (fun s -> tabname := Some s),
      "<name>   Reindex this table";

      "-idxsize", Arg.Int (fun i -> idxsize := Some i),
      "<n>   Set the size of the index file (in #entries)";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s reindex_table -db <dbdir> -table <tabname>" pgm_name);

  let dbdir = getopt "-db" !dbdir in
  let tabname = getopt "-table" !tabname in

  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  check_journal db;

  db # start_transaction ~rw:false ();

  let tab = db # table tabname in
  let old_config = tab # config in
  
  let idx_config =
    ( object
	method htsize = 
	  match !idxsize with
	    | None -> old_config # idx_config # htsize
	    | Some n -> Int64.of_int n
	method purpose = 
	  old_config # idx_config # purpose
      end
    ) in

  db # commit_transaction();
  db # reindex idx_config tabname;
  db # close();

  print_endline "Reindexed."



let cmd_compact_table pgm_name =
  let dbdir = ref None in
  let tabname = ref None in
  let idxsize = ref None in

  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Use the DB in this directory";

      "-table", Arg.String (fun s -> tabname := Some s),
      "<name>   Compact this table";

      "-idxsize", Arg.Int (fun i -> idxsize := Some i),
      "<n>   Set the size of the index file (in #entries)";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s compact_table -db <dbdir> -table <tabname>" pgm_name);

  let dbdir = getopt "-db" !dbdir in
  let tabname = getopt "-table" !tabname in

  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  check_journal db;

  db # start_transaction ~rw:false ();

  let tab = db # table tabname in
  let old_config = tab # config in
  
  let config =
    ( object
	method data_config = old_config # data_config
	method pp_config = old_config # pp_config
	method idx_config =
	  ( object
	      method htsize = 
		match !idxsize with
		  | None -> old_config # idx_config # htsize
		  | Some n -> Int64.of_int n
	      method purpose = 
		old_config # idx_config # purpose
	    end
	  )
      end
    ) in

  db # commit_transaction();
  db # compact config tabname;
  db # close();

  print_endline "Compacted."


let cmd_show_tables pgm_name =
  let dbdir = ref None in

  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Use the DB in this directory";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s show_tables -db <dbdir>" pgm_name);

  let dbdir = getopt "-db" !dbdir in

  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  check_journal db;

  let tables = db # table_names in
  db # close();

  List.iter print_endline tables


let print_value table p length =
  let n = ref 0L in
  let s = String.create 4096 in
  while !n < length do
    let m = 
      Int64.to_int
	(min (Int64.of_int (String.length s)) (Int64.sub length !n)) in
    table # blit (p :> any_position) !n s 0 m;
    if m = String.length s then
      print_string s
    else
      print_string (String.sub s 0 m);
    n := Int64.add !n (Int64.of_int m);
  done


let cmd_dump_table pgm_name =
  let dbdir = ref None in
  let tabname = ref None in
  let format = ref `List in

  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Use the DB in this directory";

      "-table", Arg.String (fun s -> tabname := Some s),
      "<name>   Dump this table";

      "-export", Arg.Unit (fun () -> format := `Export),
      "   Dump in export format (can be imported with 'update')";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s dump_table -db <dbdir> -table <tabname>" pgm_name);

  let dbdir = getopt "-db" !dbdir in
  let tabname = getopt "-table" !tabname in

  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  check_journal db;

  ( match !format with
      | `List ->
	  printf "KEY\tROW ID\tSIZE\n"
      | `Export ->
	  printf "T\t%s\n" tabname;
  );

  db # start_transaction ~rw:false ();
  let table = db # table tabname in

  let print_entry p =
    match !format with
      | `List ->
	  let key = table # key (p :> any_position) in
	  let `Row_id rowid = table # row_id (p:> any_position) in
	  let length = table # length (p :> any_position) in
	  printf "%s\t%Ld\t%Ld\n" key rowid length
      | `Export ->
	  let key = table # key (p :> any_position) in
	  let `Row_id rowid = table # row_id (p:> any_position) in
	  let length = table # length (p :> any_position) in
	  printf "B\t%s\t%Ld\t%Ld\n" key rowid length;
	  print_value table p length;
	  printf "\n"
  in
  
  ( try
      let p = ref (table # first()) in
      print_entry !p;
      while true do
	p := table # next (!p :> any_position);
	print_entry !p
      done;
      assert false
    with
      | End_of_file -> ()
  );

  db # commit_transaction();
  db # close()


let cmd_find pgm_name =
  let dbdir = ref None in
  let tabname = ref None in
  let spec = ref None in

  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Use the DB in this directory";

      "-table", Arg.String (fun s -> tabname := Some s),
      "<name>   Look into this table";

      "-key", Arg.String (fun s -> spec := Some(`Key s)),
      "<key>   Find this key";

      "-rowid", Arg.Int (fun n -> spec := Some(`Rowid (Int64.of_int n))),
      "<rowid>   Find this row ID";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s find -db <dbdir> -table <tabname>" pgm_name);

  let dbdir = getopt "-db" !dbdir in
  let tabname = getopt "-table" !tabname in
  let spec = getopt "-key or -rowid" !spec in

  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  check_journal db;

  db # start_transaction ~rw:false ();
  let table = db # table tabname in

  let code = ref 0 in
  ( match spec with
      | `Key key ->
	  ( match table # lookup key with
	      | Some(pos,_) ->
		  let length = table # length (pos :> any_position) in
		  print_value table pos length;
	      | None ->
		  prerr_endline "Not found";
		  code := 1
	  )
      | `Rowid rowid ->
	  ( try
	      let (pos, _) = table # seek (`Row_id rowid) in
	      let length = table # length (pos :> any_position) in
	      print_value table pos length;
	    with
	      | Row_id_not_found _ ->
		  prerr_endline "Not found";
		  code := 1
	  )
  );
  db # commit_transaction();
  db # close();
  exit !code


let describe_update_format () =
  print_endline "
Input lines are TAB-separated. The first field contains the directive
(one letter). The other fields are the arguments.

The following input lines are recognized:

  - T <tablename>
 
    Switch to this table

  - A <key> <rowid> <value>

    Add/update the entry with this key and rowid. <value> is restricted
    to ASCII. If rowid is negative, a new rowid is allocated.

  - F <key> <rowid> <filename>

    Add/update the entry with this key and rowid. The value is given
    by the contents of <filename>. If rowid is negative, a new rowid 
    is allocated.

  - B <key> <rowid> <length>

    Add/update the entry with this key and rowid. The value follows this
    line, and has the given length. After the value, there is a single
    LF character. (And then the next line follows.)  If rowid is negative, 
    a new rowid is allocated.

  - D <key> <rowid>

    Delete an entry. If <rowid> is non-negative, the entry is identified
    by the rowid. Otherwise the entry is identified by the key.
";
  exit 0


let copy table key rowid ch length =
  let j = ref 0L in
  let s = String.create 4096 in
  let value_stream =
    (fun () ->
       let n =
         ~< (min (~> (String.length s)) (length -@ !j)) in
       if n > 0 then (
	 really_input ch s 0 n;
	 j := !j +@ (~> n);
         Some (s, 0, n)
       )
       else
         None
    ) in
  let rid_opt =
    if rowid >= 0L then
      Some (`Row_id rowid)
    else
      None in
  let _new_ptr = table # add_stream key rid_opt value_stream length in
  ()


let tab_re = Str.regexp "\t"


let cmd_update pgm_name =
  let dbdir = ref None in
  let tabname = ref None in
  let file = ref None in
  let stdin_flag = ref true in

  Arg.parse
    [ "-db", Arg.String (fun s -> dbdir := Some s),
      "<dbdir>   Use the DB in this directory";

      "-table", Arg.String (fun s -> tabname := Some s),
      "<name>   Update this table (unless T directives switch to different table)";

      "-file", Arg.String (fun s -> file := Some s; stdin_flag := false),
      "<name>   Read updates from this file instead of stdin";

      "-describe-format", Arg.Unit describe_update_format,
      "   Describe format, then exit";
    ]
    (fun s -> raise(Arg.Bad("Unexpected arg: " ^ s)))
    (sprintf "usage: %s update -db <dbdir> -table <tabname>" pgm_name);

  let dbdir = getopt "-db" !dbdir in
  let tabname = getopt "-table" !tabname in
  let ch, close_flag =
    if !stdin_flag then
      stdin, false
    else
      let file = getopt "-file" !file in
      let ch = open_in file in
      ch, true
  in

  let dbm = Jseqdb_database.manager() in
  let db = dbm # opendb dbdir in

  check_journal db;

  db # start_transaction ~rw:true ();

  let table = ref(db # table tabname) in
  
  let parse_int64 name x =
    try
      Int64.of_string x
    with
      | _ ->
	  failwith ("Format error in field: " ^ name)
  in

  ( try
      while true do
	let line = input_line ch in
	let fields = Str.split tab_re line in
	( match fields with
	    | [ "T"; tabname ] ->
		table := db # table tabname
	    | [ "A"; key; rowid; value ] ->
		let rowid = parse_int64 "rowid" rowid in
		let eof = ref false in
		let stream() =
		  if !eof then None else
		    ( eof := true; Some(value,0,String.length value) ) in
		ignore(!table # add_stream
			 key (Some (`Row_id rowid)) stream
			 (Int64.of_int (String.length value)));
	    | [ "F"; key; rowid; filename ] ->
		let rowid = parse_int64 "rowid" rowid in
		let ch' = open_in filename in
		let length = LargeFile.in_channel_length ch' in
		copy !table key rowid ch' length;
		close_in ch'
	    | [ "B"; key; rowid; length ] ->
		let rowid = parse_int64 "rowid" rowid in
		let length = parse_int64 "length" length in
		copy !table key rowid ch length;
		let c = input_char ch in
		if c <> '\n' then 
		  failwith "Format error in input file"
	    | [ "D"; key; rowid ] ->
		let rowid = parse_int64 "rowid" rowid in
		if rowid >= 0L then (
		  try
		    let (fpos, _) = !table # seek (`Row_id rowid) in
		    !table # delete (fpos :> any_position)
		  with
		    | Row_id_not_found _ ->
			()
		)
		else (
		  match !table # lookup key with
		    | None ->
			()
		    | Some (fpos, _) ->
			!table # delete (fpos :> any_position)
		)
	    | _ ->
		failwith "Format error in input file"
	);
      done;
      assert false
    with
      | End_of_file -> ()
  );

  db # commit_transaction();
  db # close()



let commands =
  [ "superblock",    cmd_superblock,      "Show/modify the superblock";
    "create_db",     cmd_create_db,       "Create new databases";
    "drop_db",       cmd_drop_db,         "Drop databases";
    "clean_db",      cmd_clean_db,        "Clean up last transaction";
    "create_table",  cmd_create_table,    "Create new tables";
    "drop_table",    cmd_drop_table,      "Drop tables";
    "show_tables",   cmd_show_tables,     "Show existing tables";
    "dump_table",    cmd_dump_table,      "Dump table";
    "reindex_table", cmd_reindex_table,   "Reindex table";
    "compact_table", cmd_compact_table,   "Compact table";
    "find",          cmd_find,            "Find entry in table";
    "update",        cmd_update,          "Update table (batch mode)";
  ]


let usage pgm_name =
  eprintf "usage: %s <command> <options> <args>\n" pgm_name;
  eprintf "<command> is one of the following:\n";
  List.iter
    (fun (name, _, text) ->
       eprintf "  %s: %s\n" name text
    )
    commands;
  eprintf "<options> and <args> depend on the command you are issuing.\n";
  eprintf "use '%s <command> -help' to get command-specific help.\n" pgm_name;
  flush stderr;
  exit 2
;;


let main() =
  let pgm_name = Filename.basename Sys.argv.(0) in
  if Array.length Sys.argv <= 1 then
    usage pgm_name;
  let cmd_name = Sys.argv.(1) in
  let _, cmd, _ =
    try
      List.find (fun (name, _, _) -> name = cmd_name) commands 
    with
      | Not_found -> usage pgm_name in
  Arg.current := 1;
  try
    cmd pgm_name
  with
    | Arg.Bad msg
    | Failure msg ->
        prerr_endline (pgm_name ^ ": " ^ msg);
        exit 2
    | Unix.Unix_error(err, _, param) ->
        let prefix = 
          if param = "" then 
            pgm_name ^ ": " 
          else
            pgm_name ^ ": " ^ param ^ ": " in
        prerr_endline (prefix ^ Unix.error_message err);
        exit 2
    | Table_not_found name ->
	prerr_endline ("Table not found. " ^ name);
	exit 2
    | Table_exists name ->
	prerr_endline ("Table already exists. " ^ name);
	exit 2
    | Row_id_not_found _ ->
	prerr_endline "Row ID not found";
	exit 2
;;

main()
