(* $Id$ *)

open Jseqdb_types
open Jseqdb_buffer

let ( +@ ) = Int64.add
let ( -@ ) = Int64.sub
let ( *@ ) = Int64.mul
let ( /@ ) = Int64.div
let ( ~> ) = Int64.of_int  (* ">" = "convert to the bigger type" *)
let ( ~< ) = Int64.to_int  (* ">" = "convert to the smaller type" *)


module Constants = struct
  let magic = "JSEQDB0!"
  let sbsize_name = "SBSIZE"
  let format_name = "FORMAT"
  let purpose_name = "PURPOSE"
  let filesize_name = "FILESIZE"
  let fileincr_name = "FILEINCR"
  let syncsize_name = "SYNCSIZE"
  let synctime_name = "SYNCTIME"
  let entries_name = "ENTRIES"
  let aentries_name = "AENTRIES"
  let htsize_name = "HTSIZE"
  let gciter_name = "GCITER"
  let gctime_name = "GCTIME"
  let gcvers_name = "GCVERS"
  let jstate_name = "JSTATE"
  let pplast_name = "PPLAST"
  let ppnext_name = "PPNEXT"

  let kvseq_format   = 0x1000_0010L
  let hindex_format  = 0x1000_0020L
  let journal_format = 0x1000_0030L

end

let strip_spaces s =
  let rec strip n =
    if n > 0 then (
      if s.[n-1] = ' ' then
        strip (n-1)
      else
        String.sub s 0 n
    )
    else ""
  in
  strip (String.length s)
    
let read_int64 s =
  Rtypes.int64_of_int8 (Rtypes.read_int8 s 0)

let write_int64 n =
  Rtypes.int8_as_string (Rtypes.int8_of_int64 n)

let to_string n =
  strip_spaces(write_int64 n)

let of_string s =
  if String.length s > 8 then
    failwith "Jseqdb_superblock.of_string";
  let t = String.make 8 ' ' in
  String.blit s 0 t 0 (String.length s);
  read_int64 t


let null_name = 
  "\000\000\000\000\000\000\000\000"


let create() : superblock =
object(self)
  val vars = Hashtbl.create 10
  val mutable sbsize = 4096
  val mutable modified = false

  method size = 
    ~> sbsize

  method variable n = 
    Hashtbl.find vars n

  method variables =
    Hashtbl.fold (fun k v acc -> (k,v)::acc) vars []

  method set_variable n v =
    Hashtbl.replace vars n v;
    modified <- true

  method is_modified =
    modified

  method clear_modified() =
    modified <- false

  method read rd_wr =
    rd_wr # seek 0L;
    let magic = strip_spaces(input_string rd_wr 8) in
    if magic <> Constants.magic then
      failwith "Jseqdb_superblock.read: No superblock found";
    let sbsize_n = input_string rd_wr 8 in
    if strip_spaces sbsize_n <> Constants.sbsize_name then
      failwith "Jseqdb_superblock.read: No superblock found";
    let sbsize_s = input_string rd_wr 8 in
    let sbsize_v = read_int64 sbsize_s in
    if sbsize_v < 64L || sbsize_v > 65536L then
      failwith "Jseqdb_superblock.read: SBSIZE outside valid range";
    sbsize <- ~< sbsize_v;

    let rd_wr = new sub_rd_wr sbsize_v rd_wr in
    Hashtbl.clear vars;

    rd_wr # seek 8L;
    ( try
        let name = ref (input_string rd_wr 8) in
        while !name <> null_name do
          let value = read_int64 (input_string rd_wr 8) in
          Hashtbl.replace vars (strip_spaces !name) value;
          name := input_string rd_wr 8
        done
      with
        | End_of_file -> ()
    );

    modified <- true

  method write rd_wr =
    let (s, spos, slen, fpos) = self # data in
    rd_wr # seek fpos;
    rd_wr # really_output s spos slen

  method data =
    let b = Buffer.create sbsize in
    Buffer.add_string b (write_int64 (of_string Constants.magic));
    Buffer.add_string b (write_int64 (of_string Constants.sbsize_name));
    Buffer.add_string b (write_int64 (~> sbsize));
    Hashtbl.iter
      (fun k v ->
	 if k <> Constants.sbsize_name then (
	   Buffer.add_string b (write_int64 (of_string k));
	   Buffer.add_string b (write_int64 v)
	 )
      )
      vars;
    if Buffer.length b < sbsize then
      Buffer.add_string b null_name;
    if Buffer.length b > sbsize then
      failwith "Jseqdb_superblock.data: superblock to small for all variables";
    Buffer.add_string b (String.make (sbsize - Buffer.length b) '\000');
    let s = Buffer.contents b in
    (s, 0, String.length s, 0L)

  method gc_props =
    try
      { gciter = self#variable Constants.gciter_name;
	gctime = self#variable Constants.gctime_name;
	gcvers = self#variable Constants.gcvers_name
      }
    with
      | Not_found -> failwith "Jseqdb_superblock: no gc_props found"
end
