(* $Id$ *)

open Jseqdb_types

let ( +@ ) = Int64.add
let ( -@ ) = Int64.sub
let ( *@ ) = Int64.mul
let ( /@ ) = Int64.div
let ( ~> ) = Int64.of_int  (* ">" = "convert to the bigger type" *)
let ( ~< ) = Int64.to_int  (* "<" = "convert to the smaller type" *)


type block
external get_block_size : unit -> int
  = "jseqdb_get_block_size_ml"
external map_block : Unix.file_descr -> bool -> int64 -> block
  = "jseqdb_map_block_ml"
external unmap_block : block -> unit
  = "jseqdb_unmap_block_ml"
external block_seek : block -> Unix.file_descr -> bool -> int64 -> unit
  = "jseqdb_block_seek_ml"
external block_input : block -> int -> string -> int -> int -> unit
  = "jseqdb_block_input_ml"
external block_output : block -> int -> string -> int -> int -> unit
  = "jseqdb_block_output_ml"
external fallocate : Unix.file_descr -> int64 -> int64 -> unit
  = "jseqdb_fallocate_ml"
external fdatasync : Unix.file_descr -> unit
  = "jseqdb_fdatasync_ml"
external lock_op : Unix.file_descr -> int -> int -> unit
  = "jseqdb_lock_op_ml"


(* "variable int" (vint)-encoded int64:

   0XXX XXXX
   10XX XXXX   + 1 byte
   110X XXXX   + 2 bytes
   1110 XXXX   + 3 bytes
   1111 0XXX   + 4 bytes
   1111 10XX   + 5 bytes
   1111 110X   + 6 bytes
   1111 1110   + 7 bytes
   1111 1111   + 8 bytes

   Everything is big-endian, for conceptual simplicity.
 *)

external block_input_vint : block -> int -> int -> int -> int64 ref -> int
  = "jseqdb_block_input_vint_ml"
  (* let state' = block_input_vint b state pos len cell:

     decodes the vint at position pos of block b which is supposed to
     have byte length len. Call with state=0 the first time, and initialize
     cell to 0 before calling. The function puts the part of the number
     it can read into the cell, and returns as state';
      - in the bits 0-3: how many bytes still need to be read
      - in the bits 4-7: how many bytes have been read
   *)

external block_output_vint : block -> int -> int -> int -> int64 -> int
  = "jseqdb_block_output_vint_ml"
  (* let state' = block_output_vint b state pos len number:

     encodes number as vint, and writes it into b at position pos. The
     block is assumed to have length len. Call with state=0 the first time.
     The function returns in state':
      - in the bits 0-3: how many bytes still need to be written
      - in the bits 4-7: how many bytes have been written
   *)



class managed_descr ~filename ~flags ~reopen_flags ~perm ~reopen_perm 
      : file_descr  =
  let writable = List.mem Unix.O_RDWR flags in
object(self)
  val mutable reopen_flag = false
  val mutable fd = None

  method filename = 
    filename

  method file_descr =
    match fd with
      | Some d -> 
          d
      | None ->
          let d = 
            if reopen_flag then
              Unix.openfile filename reopen_flags reopen_perm
            else
              Unix.openfile filename flags perm in
          reopen_flag <- true;
          fd <- Some d;
          d

  method writable =
    writable
          
  method dispose_descr() =
    match fd with
      | Some d ->
          Unix.close d;
          fd <- None
      | None ->
	  ()

  method lock_op op k =
    let iop =
      match op with
	| `Shared -> 0
	| `Exclusive -> 1
	| `Unlock -> 2 in
    lock_op self#file_descr iop k
end


let rw_descr filename =
  new managed_descr
    ~filename
    ~flags:[Unix.O_RDWR]
    ~reopen_flags:[Unix.O_RDWR]
    ~perm:0
    ~reopen_perm:0


let ro_descr filename =
  new managed_descr
    ~filename
    ~flags:[Unix.O_RDONLY]
    ~reopen_flags:[Unix.O_RDONLY]
    ~perm:0
    ~reopen_perm:0


class virtual really_io =
object (self)
  method virtual input : string -> int -> int -> int
  method virtual output : string -> int -> int -> int

  method really_input s s_pos s_len =
    let p = ref s_pos in
    let n = ref s_len in
    while !n > 0 do
      let m = self # input s !p !n in
      if m = 0 then raise End_of_file;
      p := !p + m;
      n := !n - m
    done


  method really_output s s_pos s_len =
    let p = ref s_pos in
    let n = ref s_len in
    while !n > 0 do
      let m = self # output s !p !n in
      p := !p + m;
      n := !n - m
    done
end




let block_size =
  get_block_size()


let block_size_bits = (
  let n = ref 0 in
  let s = ref block_size in
  while !s > 1 do
    s := !s lsr 1;
    incr n
  done;
  if !s <= 0 then
    failwith "Invalid page size";  (* not a power of 2 *)
  !n
)


let block_size_mask = (
  let s = ref 1 in
  for k = 1 to block_size_bits - 1 do
    s := (!s lsl 1) lor 1
  done;
  !s
)


let block_size_mask_64 =
  Int64.of_int block_size_mask


class buf_rd_wr (md : file_descr) : reader_writer =
  let initial_st = Unix.LargeFile.fstat md#file_descr in
  let initial_size = initial_st.Unix.LargeFile.st_size in
object(self)
  val mutable eof = initial_size
  val mutable block = None
  val mutable pos = 0L

  inherit really_io

  method private map_block p =
    let block_pos = Int64.logand p (Int64.lognot (~> block_size_mask)) in
    match block with
      | None ->
	  let b = map_block md#file_descr self#writable block_pos in
	  block <- Some(b,block_pos);
	  (b,block_pos)
      | Some(b,bpos) ->
	  if bpos <> block_pos then (
	    block_seek b md#file_descr self#writable block_pos;
	    block <- Some(b,block_pos)
	  );
	  (b,block_pos)

  method private unmap_block() =
    match block with
      | None -> ()
      | Some (b,_) -> 
	  unmap_block b; 
	  block <- None    

  method pos =
    pos

  method eof =
    eof

  method seek p =
    if pos < 0L || pos > eof then
      invalid_arg "Jseqdb_buffer.reader_writer.seek";
    pos <- p

  method set_eof p =
    let old_eof = eof in
    if p > old_eof then
      fallocate md#file_descr old_eof (p -@ old_eof)
    else (
      Unix.LargeFile.ftruncate md#file_descr p;
      if pos > p then
	pos <- p
    );
    self # unmap_block();
    eof <- p

  method input s spos len =
    let slen = String.length s in
    if spos < 0 || len < 0 || spos > slen-len then
      invalid_arg "Jseqdb_buffer.reader_writer.input";
    let maxlen = ~< (min (eof -@ pos) (~> len)) in
    if maxlen = 0 then
      0
    else (
      let (b,bpos) = self # map_block pos in
      let boffset = ~< (pos -@ bpos) in
      let n = min maxlen (block_size - boffset) in
      block_input b boffset s spos n;
      pos <- pos +@ (~> n);
      n
    )

  method input_vint() =
    if pos >= eof then raise End_of_file;
    let (b,bpos) = self # map_block pos in
    let boffset = ~< (pos -@ bpos) in
    let len = ~< (min (eof -@ pos) (~> (block_size - boffset))) in 
    let cell = ref 0L in
    let state' = block_input_vint b 0 boffset len cell in
    let n1 = state' lsr 4 in
    pos <- pos +@ (~> n1);
    if state' land 0xf = 0 then
      !cell
    else (
      if pos = eof then raise End_of_file;
      let (b,bpos) = self # map_block pos in
      let boffset = ~< (pos -@ bpos) in
      let len = ~< (min (eof -@ pos) (~> (block_size - boffset))) in 
      let state'' = block_input_vint b state' boffset len cell in
      if state'' land 0xf <> 0 then raise End_of_file;
      let n2 = state'' lsr 4 in
      pos <- pos +@ (~> n2);
      !cell
    )

  method output s spos len =
    let slen = String.length s in
    if spos < 0 || len < 0 || spos > slen-len then
      invalid_arg "Jseqdb_buffer.reader_writer.output";
    if len = 0 then
      0
    else (
      (* Maybe we have to enlarge the file: *)
      let pos' = pos +@ (~> len) in
      if pos' > eof then
	self # set_eof pos';
      let dolen = ~< (min (eof -@ pos) (~> len)) in
      let (b,bpos) = self # map_block pos in
      let boffset = ~< (pos -@ bpos) in
      let n = min dolen (block_size - boffset) in
      block_output b boffset s spos n;
      pos <- pos +@ (~> n);
      n
    )

  method output_vint number =
    if pos = eof then
      self # set_eof (eof +@ 1L);
    let (b,bpos) = self # map_block pos in
    let boffset = ~< (pos -@ bpos) in
    let len = ~< (min (eof -@ pos) (~> (block_size - boffset))) in 
    let state' = block_output_vint b 0 boffset len number in
    let n1 = state' lsr 4 in
    pos <- pos +@ (~> n1);
    if state' land 0xf <> 0 then (
      (* If we increase EOF again, it may happen that we call 
         block_output_vint twice now!
       *)
      let r = state' land 0xf in
      if pos +@ (~> r) > eof then
	self # set_eof (pos +@ (~> r));
      (* Now loop until... *)
      let s = ref state' in
      while !s land 0xf <> 0 do
	let (b,bpos) = self # map_block pos in
	let boffset = ~< (pos -@ bpos) in
	let len = ~< (min (eof -@ pos) (~> (block_size - boffset))) in 
	let state'' = block_output_vint b !s boffset len number in
	let n2 = state'' lsr 4 in
	pos <- pos +@ (~> n2);
	s := state''
      done
    )

  method flush() =
    (* We don't have internal buffers to flush. Also, on Linux there is no
       need to call msync() to synchronize the file views provided by
       mmap() and read/write().
     *)
    ()

  method sync() =
    fdatasync md#file_descr


  method filename =
    md#filename

  method file_descr =
    md#file_descr

  method dispose_descr() =
    md#dispose_descr();
    self # unmap_block()

  method writable =
    md#writable

  method lock_op =
    md#lock_op

end


class sub_rd_wr ?incr eof (rd_wr : reader_writer) =
  (* Enforce that eof is at [eof] *)
object(self)
  inherit really_io

  val mutable log_eof = eof

  method pos = rd_wr # pos

  method seek p =
    if p > rd_wr#eof then
      failwith "sub_rd_wr: seeking outside the valid range";
    rd_wr # seek p

  method eof = log_eof

  method set_eof p =
    rd_wr # set_eof p;
    log_eof <- p

  method input s s_pos s_len =
    let p = rd_wr # pos in
    let n = ~< (min (~> s_len) (log_eof -@ p)) in
    rd_wr # input s s_pos n

  method input_vint() =
    let n = rd_wr # input_vint() in
    if rd_wr # pos > log_eof then raise End_of_file;
    n

  method output s s_pos s_len =
    match incr with
      | Some 0L
      | None ->
          let n = rd_wr # output s s_pos s_len in
          if rd_wr # pos > log_eof then
            log_eof <- rd_wr # pos;
          n
      | Some i ->
          if rd_wr#pos = rd_wr#eof && s_len > 0 then
	    rd_wr # set_eof (rd_wr # pos +@ i);
          let s_len' = ~< (min (~> s_len) (rd_wr#eof -@ rd_wr#pos)) in
          let n = rd_wr # output s s_pos s_len' in
          if rd_wr # pos > log_eof then
            log_eof <- rd_wr # pos;
          n

  method output_vint n =
    let old_eof = rd_wr#eof in
    rd_wr # output_vint n;
    let pos = rd_wr # pos in
    if pos > log_eof then (
      log_eof <- pos;
      match incr with
	| Some 0L
	| None ->
	    ()
	| Some i ->
	    if pos > old_eof then (
	      let new_eof = max (old_eof +@ i) pos in
	      rd_wr # set_eof new_eof
	    )
    )

  method lock_op = rd_wr # lock_op
  method flush = rd_wr # flush
  method sync = rd_wr # sync
  method filename = rd_wr # filename
  method file_descr = rd_wr # file_descr
  method writable = rd_wr # writable
  method dispose_descr = rd_wr # dispose_descr

end


let input_string (rw : reader_writer) n =
  let s = String.create n in
  rw # really_input s 0 n;
  s

let output_string (rw : reader_writer) s =
  rw # really_output s 0 (String.length s)

let with_shared_lock (fd : file_descr) k f =
  fd # lock_op `Shared k;
  try
    let r = f() in
    fd # lock_op `Unlock k;
    r
  with
    | error ->
	fd # lock_op `Unlock k;
	raise error

let with_exclusive_lock (fd : file_descr) k f =
  fd # lock_op `Exclusive k;
  try
    let r = f() in
    fd # lock_op `Unlock k;
    r
  with
    | error ->
	fd # lock_op `Unlock k;
	raise error

(* That's just for testing: *)
let copy_file fn1 fn2 =
  let md1 = 
    new managed_descr ~filename:fn1 ~flags:[Unix.O_RDONLY] 
      ~reopen_flags:[Unix.O_RDONLY] ~perm:0 ~reopen_perm:0 in
  let b1 =
    new buf_rd_wr md1 in
  let md2 = 
    new managed_descr ~filename:fn2
      ~flags:[Unix.O_RDWR; Unix.O_CREAT; Unix.O_TRUNC] 
      ~reopen_flags:[Unix.O_RDWR] ~perm:0o666 ~reopen_perm:0 in
  let b2 =
    new buf_rd_wr md2 in
  b2 # set_eof b1#eof;
  let s =
    String.create 4096 in
  while b1#pos < b1#eof do
    let n = ~<( min 4096L (b1#eof -@ b1#pos) ) in
    let n' = b1 # input s 0 n in
    ignore(b2 # output s 0 n');
  done;
  b1 # dispose_descr();
  b2 # sync();
  b2 # dispose_descr()



