(* $Id$ *)

open Jseqdb_types

val fake_journal : unit -> journal
  (** This "journal" writes all changes immediately out to the data files.
      There is no journal file.
   *)

val access_journal : 
       ?jsuper_lock_op:(lock_op -> unit) ->
       file_descr -> journal
  (** Access this file as journal.

      It is ok to create an empty file in order to create an empty journal
   *)
