(* $Id$ *)

(** {2 Descriptors} *)

type lock_op =
    [ `Shared | `Exclusive | `Unlock ]

class type file_descr =
object
  method filename : string
    (** Returns the filename *)

  method file_descr : Unix.file_descr
    (** Get the file descriptor or raise an exception if this is not possible.
        This method is called when the descriptor is needed but unknown.
        [file_descr] is only allowed to return a different descriptor
        than before when [dispose_hint] has been called in the meantime.
      *)

  method writable : bool
    (** Whether writes are allowed *)

  method lock_op : lock_op -> int -> unit
    (** Perform the lock operation. The int distinguishes between locks on the
        same file
     *)

  method dispose_descr : unit -> unit
    (** Close the descriptor 
     *)
end
  (** Abstraction of an object providing a file descriptor. The [file_descr]
      method is invoked to get a new descriptor after the old one has been
      disposed.
   *)


(** {2 Buffers} *)

class type reader_writer =
object
  inherit file_descr

  method pos : int64
    (** Current read/write position *)

  method seek : int64 -> unit
    (** Set the read/write position *)

  method eof : int64
    (** The eof-of-file position *)

  method set_eof : int64 -> unit
    (** Set the eof position. In this implementation, this also allocates
        file system blocks.
     *)

  method input : string -> int -> int -> int
    (** [input s pos len]: Read up to [len] bytes at the current read/write
        position into [s] at string position [pos]. Returns the actual number
        of read bytes (which is at most [chunk_size]). If [len>0] the result
        0 means that EOF is reached.
     *)

  method really_input : string -> int -> int -> unit
    (** [really_input s pos len]: Read exactly [len] bytes at the current
        read/write position into [s] at string position [pos].

        If less than [len] bytes are available, [End_of_file] is raised,
        and it is unspecified how many bytes have been put into [s].
     *)

  method input_vint : unit -> int64
    (** Read an int64 encoded with variable length at the current file
        position
     *)

  method output : string -> int -> int -> int
    (** [output s pos len]: Write up to [len] bytes from [s] at the string
        position [pos] to the file at the current read/write position.
        Returns the actual number of read bytes (which is at most
        [chunk_size]). 
     *)

  method really_output : string -> int -> int -> unit
    (** [really_output s pos len]: Write exactly [len] bytes from [s] at
        the string position [pos] to the file at the current read/write
        position.
     *)

  method output_vint : int64 -> unit
    (** Write a variable-length-encoded int64 at the current file position *)

  method flush : unit -> unit
    (** Flush all dirty buffers to disk *)

  method sync : unit -> unit
    (** as [flush], plus sync to disk *)

end
  (** A [reader_writer] is a file buffer implementation that supports
      seeking. 
   *)

type journal_state =
    [ `Incomplete
    | `Valid
    | `Valid_v1
    | `Valid_v2
    ]
  (** An [`Incomplete] journal is empty, or under construction, or corrupted.
      A [`Valid] journal can be played back. The state [`Valid_v1] means
      that the data file is in a clean state, and reflects the old version.
      The state [`Valid_v2] means that the data file is in a clean state, 
      and reflects the new version.
   *)
 
type journal_file =
    { mutable jf_basename : string;  (** The basename of the file *)
      mutable jf_version : string;   (** The version of the file one can go back to *)
      mutable jf_log_eof : int64;    (** The logical EOF *)
      mutable jf_real_eof : int64;   (** The real EOF *)
    }
  (** The file positions 0 to [jf_log_eof-1] are considered as the data
      of the file. The positions [jf_log_eof] to [jf_real_eof-1] are 
      assumed to be zero bytes.

      Writes at or after [jf_log_eof] are not recorded in the journal,
      but done immediately.
   *)

type journal_entry =
    [ `Old_data of (string * int * int * int64)
	(** [`Old_data(s,pos,len,fpos)]: The old version of the data file
            has the contents [String.sub s pos len] at file position [fpos]
	 *)
    | `New_data of (string * int * int * int64)
	(** [`New_data(s,pos,len,fpos)]: The new version of the data file
            has the contents [String.sub s pos len] at file position [fpos]
	 *)
    | `New_truncate of int64
	(** Truncate the file to this eof. *)
    | `New_sync
	(** When writing the new file, it is now synced *)
    | `New_lock_op of (lock_op * int )
	(** Do this lock operation *)
    ]

(** A journal records changes of a file: The original version [v1] is
    changed into version [v2]. The journal consists of an internal buffer
    recording the [v2] version of file ranges, and an external journal
    file recording the [v1] version of file ranges.

    The file operations are done in this order:
    + At the beginning of a transaction the journal is emptied, and
      the journal file is marked as incomplete. This is synced to disk.
    + Data is now written to this class type by adding entries. Entries
      about the new version [v2] are recorded in an internal queue.
      Entries about the old version [v1] are recorded
      in the external journal file.
    + The journal is committed: First, the external journal file is
      marked as valid, and synced to disk. Then the internal queue is
      played back, and the data file is modified. Finally it is also
      synced.
 *)
class type journal =
object
  method state : journal_state
    (** Returns the state.

        After opening the journal:
        - [`Incomplete]: the journal contains garbage. [reset] recommended.
        - [`Valid]: the journal contains an uncommitted transaction.
          This transaction must be rolled back, so call [rollback] then
          [reset].
        - [`Valid_v1]: the journal has already been rolled back. Just
          [reset].
        - [`Valid_v2]: the journal contains a committed transaction.
          Call [reset] to continue, or [rollback] and then [reset] to
          undo the transaction.
     *)

  method reset : unit -> unit
    (** Removes all entries from the journal. The journal is marked
        as being incomplete. The file table is also emptied.
     *)

  method add_file : journal_file -> reader_writer -> int
    (** Adds a file to the file table. Returns the index of the file
        in the file table.

        It is required that the [state] is [`Incomplete].
     *)

  method add_entry : int -> journal_entry -> unit
    (** [add_entry file_index entry]:
         Adds an entry to the journal.

        It is required that the [state] is [`Incomplete].
     *)

  method commit : unit -> unit
    (** Finish the transaction by marking the external journal as being
        valid, and by running through the internal queue of file modifications.
        After a successful commit, the journal is set to state [`Valid_v2].

        It is required that the [state] is [`Incomplete] when [commit] is 
        called.
     *)

  method file_table : (int * journal_file) list
    (** Returns the file table *)

  method rollback : (journal_file -> reader_writer) -> unit
    (** [rollback get_md]: Rolls the journal back, i.e. goes back to
        version [v1] of the file. After rolling back, the state of the
        journal is set to [`Valid_v1].

        Rolling back is idempotent, i.e. it is safe to invoke this method
        several times. It is only meaningful if the journal state is in
        [`Valid] or [`Valid_v2]. An [`Incomplete] journal cannot be rolled
        back. If in state [`Valid_v1] it does not have any effect.

        The file table is discarded before rolling back, and re-read
        from disk. Also the descriptors are forgotten about,
        and the function calls [get_md] to get fresh descriptors.
     *)

  method file_descr : file_descr
    (** Low-level *)
end

type gc_props =
    { gciter : int64;  (** 0L for a freshly created file *)
      gctime : int64;  (** timestamp of file creation *)
      gcvers : int64;  (** 0L for a freshly created file *)
    }

class type superblock =
object
  method size : int64
  method variable : string -> int64
  method variables : (string * int64) list
  method set_variable : string -> int64 -> unit
  method is_modified : bool
  method clear_modified : unit -> unit
  method read : reader_writer -> unit
  method write : reader_writer -> unit
  method data : (string * int * int * int64)
    (** Returns the contents of the superblock as data tuple that can be
        added to the journal with tag [`Old_data] or [`New_data]
     *)

  method gc_props : gc_props   (* or Failure *)
end

type key =
    { mutable k_rowid : int64;
      mutable k_key : string;
    }

class type kvseq_config =
object
  method fileincr : int64
  method purpose : string
end

class type kvseq =
object
  method filename : string
  method superblock : superblock
  method config : kvseq_config
  method num_entries : int64
    (** The number of entries *)
  method lookup : int64 -> key
    (** [lookup fpos]: Return the key of the entry at [fpos] *)
  method value : int64 -> string
    (** [value fpos]: Return the value of the entry at [fpos] *)
  method length : int64 -> int64
    (** [length fpos]: Return the length of the value of the entry at [fpos] *)
  method blit : int64 -> int64 -> string -> int -> int -> unit
    (** [blit fpos offs s pos len]: Copies the value at [fpos] to the string [s].
        They bytes are copied from the value at offset offs
       (0 <= offs < length), and they are copied to the substring of s
       starting at [pos] with length [len].
     *)
  method add : key -> string -> int -> int -> int64
    (** [add k s pos len]: Adds a new entry to the kvseq and returns the
        file position. The entry will have key [k] and the value is copied
        out of [s], starting at index [pos], with length [len]
     *)
  method add_stream : key -> (unit -> (string * int * int) option) -> int64 -> 
                        int64
    (** [add_stream k len s]: Adds a new entry to the kvseq and returns the
        file position. The entry will have key [k] and the value is assembled
        by concatenating the elements of stream [s]. Each element [u,upos,ulen]
        is meant as [String.sub u upos ulen]. The sum of all [ulen] must be
        equal to the total length [len].
     *)
  method first : unit -> int64
    (** Returns the file position of the first entry *)
  method next : int64 -> int64
    (** Switches to the next entry, or raises [End_of_file] *)
  method flush : unit -> unit
    (** Adds any missing updates to the journal. This is required after
        a sequence of modifications has been done to the kvseq, and before
        the journal is committed.
     *)
end

class type kvseq_manager =
object
  method create : ?sbvars:(string*int64) list -> 
                  kvseq_config -> gc_props -> file_descr -> unit
    (** Writes an empty kvseq structure into this file. Can also be used
        to erase an existing kvseq. In [sbvars] one can set further
        superblock variables. [create] implies a [sync].
     *)
  method read : file_descr -> kvseq
    (** Starts a read transaction. The obtained kvseq never sees newly
        added entries (invoke [read] again to see them). When the file
        descriptor is closed, the kvseq may become unusable.
     *)
  method write : file_descr -> journal -> kvseq
    (** Starts a write transaction. It is possible to [add] new entries.
     *)
  method inspect : file_descr -> string
    (** Checks whether this file has a valid superblock, and returns
        the version string (as used in the journal's jf_version)
     *)
end

class type hindex_config =
object
  method htsize : int64
  method purpose : string
end

class type hindex =
object
  method filename : string
  method superblock : superblock
  method config : hindex_config
  method num_entries : int64
    (** The total number of entries, including the delete marks *)
  method num_active_entries : int64
    (** The number of entries pointing to a real kv entry *)
  method lookup : string -> int64 option
    (** Looks the key up and returns the file position in the kvseq file *)
  method update : string -> int64 -> unit
    (** Adds the key and the file position to the index. If there is already
        a mapping for this key, the file position is updated.
     *)
  method delete : string -> unit
    (** Deletes this key *)
  method flush : unit -> unit
    (** Adds any missing updates to the journal. This is required after
        a sequence of modifications has been done to the kvseq, and before
        the journal is committed.

        After a [flush] the object should no longer be used (inefficient,
        and updates could be done twice)
     *)
end

class type hindex_manager =
object
  method create : hindex_config ->  gc_props -> file_descr -> unit
  method read : file_descr -> hindex
  method write : file_descr -> journal -> hindex
end


class type table_config =
object
  method data_config : kvseq_config
  method pp_config : kvseq_config
  method idx_config : hindex_config
end

type file_position =
    [ `File_pos of int64 ]

type row_id =
    [ `Row_id of int64 ]

type any_position =
    [ file_position | row_id ]

type full_position =
    ( file_position * row_id )


exception Row_id_not_found of int64

exception Table_exists of string

exception Table_not_found of string


class type table =
object
  method name : string
    (** The table name. The character '/' is forbidden, and ':' has a
        special meaning: Names with ':' are used for replacement tables.
        E.g. "<name>:compact" is the name of the table newly written 
        during the compaction run
     *)

  method config: table_config

  method lookup : string -> full_position option
    (** [lookup key]: return the position, or [None] *)
  method key : any_position -> string
    (** [key pos]: Return the key of the entry at [pos] *)
  method row_id: any_position -> row_id
    (** [row_id fpos]: Return the row ID of [pos] *)
  method seek : any_position -> full_position
    (** [seek p]: Returns the [fpos] corresponding to the 
        position [p]
     *)
  method value : any_position -> string
    (** [value fpos]: Return the value of the entry at [pos] *)
  method length : any_position -> int64
    (** [length fpos]: Return the length of the value of the entry at [fpos] *)
  method blit : any_position -> int64 -> string -> int -> int -> unit
    (** [blit pos offs s pos len]: Copies the value at [pos] to the string [s].
        They bytes are copied to the substring region starting at [pos]
        with length [len].
     *)
  method add : string -> string -> int -> int -> full_position
    (** [add k s pos len]: Adds a new entry to the table and returns the
        file position. The entry will have key [k] and the value is copied
        out of [s], starting at index [pos], with length [len]
     *)
  method add_stream : string -> row_id option ->
                      (unit -> (string * int * int) option) -> int64 -> 
                      full_position
    (** [add_stream k rid len s]: Adds a new entry to the kvseq and returns the
        file position. The entry will have key [k] and the value is assembled
        by concatenating the elements of stream [s]. Each element [u,upos,ulen]
        is meant as [String.sub u upos ulen]. The sum of all [ulen] must be
        equal to the total length [len].

        If [rid] is [None], the new row will get the next available row ID.
        If [rid] is [Some id], the new row will get [id] as row ID. The user
        is then responsible for assigning unique row ID's.
     *)
  method delete : any_position -> unit
    (** [delete pos]: Marks the entry in the index as deleted *)
  method first : unit -> file_position
    (** Returns the file position of the first entry *)
  method next : any_position -> file_position
    (** Switches to the next entry, or raises [End_of_file].

        If a row ID is passed as argument, the function switches to the next
        entry with bigger row ID.
     *)
end


(** A database is a group of tables that share the same journal *)
class type database =
object
  method filename : string
    (** The directory with the table files *)

  method journal : journal
    (** The journal. It is not tried to automatically roll back
        corrupt files at database opening time. This is delayed until
        the first rw transaction is started. User code that
        wishes to ensure a sane db state even if only read-only
        transactions are going to be done should proceed as follows after
        opening the database: 
        - Test [journal#state]. If it is [`Valid], the journal must be rolled
          back. Otherwise, one can ignore the journal.
        - Call [start_transaction ~rw:true ()], follwed by
          [commit_transaction()] to enforce the rollback.  
     *)

  method start_transaction : rw:bool -> unit -> unit
    (** All table access must happen in transactions. If [rw], the transaction
        allows writing, and gets ownership of the journal.

        If not [rw], only reads are allowed.

        In both cases, the required locks are obtained.
     *)

  method commit_transaction : unit -> unit
    (** Finish the transaction. If it was [rw], the data are actually written
        out. If not [rw], all locks are released.

        No-op if no transaction was started.
     *)

  method rollback_transaction : unit -> unit
    (** Undo the transaction if it was [rw] 

        No-op if no transaction was started.
     *)

  method table_names : string list
    (** The available tables *)

  method table : string -> table
    (** Get the table interface object (or [Table_not_found]) *)

  method table_create : table_config -> string -> unit
    (** Create a new table. Table names must neither contain "/" nor
        ":" characters. (The latter is reserved for automatically generated
        names.)

        This method must be called outside of a transaction.
     *)

  method table_drop : string -> unit
    (** Drop the table

        This method must be called outside of a transaction.
     *)

  method reindex : hindex_config -> string -> unit
    (** Write a new index for the table (based on the passed config). 
        This is a long write transaction! 

        This method must be called outside of a transaction.
     *)

  method compact : table_config -> string -> unit
    (** Do a compaction run for this table. 
        This is a long write transaction! 

        This method must be called outside of a transaction.
     *)

  method generation : int64
    (** Returns the generation counter. This counter is increased by 
        table creations, table drops, compactions, and reindex runs.

        [generation] is only meaningful within a transaction. If the
        generation counter is unchanged, [`File_pos] positions from
        previous transaction(s) remain valid. (However, it can happen
        that the entry at such a position is now deleted.)
     *)

  method close : unit -> unit
    (** Close all descriptors *)

end


class type database_manager =
object
  method opendb : string -> database
    (** The string is the absolute path to the database directory *)

  method dropdb : string -> unit
    (** The string is the absolute path to the database directory *)

  method createdb :   string -> unit
    (** The string is the absolute path to the database directory *)

end
