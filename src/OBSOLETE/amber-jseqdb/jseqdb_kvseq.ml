(* $Id$ *)

open Printf
open Jseqdb_types
open Jseqdb_buffer


let ( +@ ) = Int64.add
let ( -@ ) = Int64.sub
let ( *@ ) = Int64.mul
let ( /@ ) = Int64.div
let ( ~> ) = Int64.of_int  (* ">" = "convert to the bigger type" *)
let ( ~< ) = Int64.to_int  (* ">" = "convert to the smaller type" *)


type entry =
    { mutable e_ptr : int64;
      mutable e_ppos : int64;
      mutable e_key : string;
      mutable e_length : int64;
      mutable e_val_ptr : int64;
    }


let sb_get sb name =
  try
    sb#variable name
  with
    | Not_found -> failwith ("Jseqdb_kvseq: " ^ name ^ " not found")


let kvseq_sbvals (superblock : superblock) =
  let format =
    sb_get superblock Jseqdb_superblock.Constants.format_name in
  let syncsize =
    sb_get superblock Jseqdb_superblock.Constants.syncsize_name in
  let synctime =
    sb_get superblock  Jseqdb_superblock.Constants.synctime_name in
  let fileincr = 
    sb_get superblock Jseqdb_superblock.Constants.fileincr_name in
  let purpose =
    Jseqdb_superblock.to_string
      (sb_get superblock Jseqdb_superblock.Constants.purpose_name) in
  let gciter =
    sb_get superblock  Jseqdb_superblock.Constants.gciter_name in
  let gcvers =
    sb_get superblock  Jseqdb_superblock.Constants.gcvers_name in
  let gctime =
    sb_get superblock  Jseqdb_superblock.Constants.gctime_name in
  let entries =
    sb_get superblock Jseqdb_superblock.Constants.entries_name in
object
  method format = format
  method syncsize = syncsize
  method synctime = synctime
  method fileincr = fileincr
  method purpose = purpose
  method gciter = gciter
  method gcvers = gcvers
  method gctime = gctime
  method entries = entries
end


let jf_version sbvals =
  sprintf "%Lx.%Lx.%Lx.%Lx"
    sbvals#gciter sbvals#gcvers sbvals#syncsize sbvals#synctime


class kvseq_impl (md : file_descr) journal_opt : kvseq =
  (* When a journal is given, the access mode is r/w else r/o *)
  let rdwr0 = new buf_rd_wr md in
  let superblock = (
    let sb = Jseqdb_superblock.create() in
    sb # read rdwr0;
    sb # clear_modified();
    sb
  ) in
  let sbdata = superblock # data in   (* remember that for [flush] *)
  let sbvals = kvseq_sbvals superblock in  (* initial values in sb *)
  let () = (
    if sbvals#format <> Jseqdb_superblock.Constants.kvseq_format then
      failwith "Jseqdb_kvseq: bad FORMAT"
  ) in
  let config = (
    ( object
	method fileincr = sbvals#fileincr
	method purpose = sbvals#purpose
      end
    )
  ) in
  let num_entries =
    ref (sbvals#entries) in
  let rdwr1 =
    new sub_rd_wr ~incr:config#fileincr sbvals#syncsize rdwr0 in
  let jidx =
    match journal_opt with
      | None -> 0
      | Some j ->
	  j # add_file
	    { jf_basename = Filename.basename md#filename;
	      jf_version = jf_version sbvals;
	      jf_log_eof = sbvals#syncsize;
	      jf_real_eof = rdwr0#eof
	    }
	    rdwr1 in
object(self)
  method filename = rdwr0#filename
  method superblock = superblock
  method config = config
  method num_entries = !num_entries

  val mutable eopt = None
  val mutable modified = false

  method private entry_at ptr =
    let do_read =
      match eopt with
	| None -> true
	| Some e -> e.e_ptr <> ptr in
    if do_read then (
      let step = ref 0 in
      try
	rdwr1 # seek ptr;
	step := 1;
	let hd = input_string rdwr1 2 in
	step := 2;
	let ptr0 = Int64.to_int(Int64.logand ptr 0xff_L) in
	if hd.[0] <> '!' || Char.code hd.[1] <> ptr0 then
	  failwith (sprintf "Jseqdb_kvseq: format error [1] @ 0x%Lx" ptr);
	let ppos = rdwr1 # input_vint() in
	step := 3;
	let klen = rdwr1 # input_vint() in
	step := 4;
	if klen < 0L || klen > (~> (Sys.max_string_length)) then
	  failwith (sprintf "Jseqdb_kvseq: format error [2] @ 0x%Lx" ptr);
	if rdwr1#pos +@ klen > rdwr1#eof then
	  failwith (sprintf "Jseqdb_kvseq: format error [3] @ 0x%Lx" ptr);
	let key = input_string rdwr1 (~< klen) in
	step := 5;
	let vlen = rdwr1 # input_vint() in
	step := 6;
	if vlen < 0L || vlen > (~> (Sys.max_string_length)) then
	  failwith (sprintf "Jseqdb_kvseq: format error [4] @ 0x%Lx" ptr);
	if rdwr1#pos +@ vlen > rdwr1#eof then
	  failwith (sprintf "Jseqdb_kvseq: format error [5] @ 0x%Lx" ptr);
	let e =
	  { e_ptr = ptr;
	    e_ppos = ppos;
	    e_key = key;
	    e_length = vlen;
	    e_val_ptr = rdwr1 # pos
	  } in
(* eprintf "entry_at ptr=%Lx ppos=%Lx vlen=%Lx val_ptr=%Lx\n"
  ptr ppos vlen e.e_val_ptr;
 *)
	eopt <- Some e;
	e
      with
	| End_of_file ->
	    failwith (sprintf "Jseqdb_kvseq: format error [6] step %d @ 0x%Lx"
			!step ptr);
    )
    else (
      match eopt with
	| None -> assert false
	| Some e -> e
    )

  method lookup ptr =
    let e = self # entry_at ptr in
    { k_rowid = e.e_ppos;
      k_key = e.e_key
    }

  method value ptr =
    let e = self # entry_at ptr in
    rdwr1 # seek e.e_val_ptr;
    input_string rdwr1 (~< (e.e_length))

  method length ptr =
    let e = self # entry_at ptr in
    e.e_length

  method blit ptr offs s spos len =
    if offs < 0L || len < 0 || spos < 0 || spos > String.length s - len then
      invalid_arg "Jseqdb_kvseq.blit";
    let e = self # entry_at ptr in
    if offs > e.e_length +@ (~> len) then
      invalid_arg "Jseqdb_kvseq.blit";
    rdwr1 # seek (e.e_val_ptr +@ offs);
    rdwr1 # really_input s spos len

  method add k s spos slen =
    (* We can bypass the journal because we always append to the file! *)
    if spos < 0 || slen < 0 || spos > String.length s - slen then
      invalid_arg "Jseqdb_kvseq.add";
    if journal_opt = None then
      failwith "Jseqdb_kvseq.add: read-only";
    let pos = rdwr1 # eof in
    let pos0 = Int64.to_int(Int64.logand pos 0xff_L) in
    rdwr1 # seek pos;
    let hd = String.make 2 '!' in
    hd.[1] <- Char.chr pos0;
    rdwr1 # really_output hd 0 2;
    rdwr1 # output_vint k.k_rowid;
    rdwr1 # output_vint (~> (String.length k.k_key));
    output_string rdwr1 k.k_key;
    rdwr1 # output_vint (~> slen);
    rdwr1 # really_output s spos slen;
    num_entries := Int64.succ !num_entries;
    modified <- true;
    pos


  method add_stream k stream len =
    if len < 0L then
      invalid_arg "Jseqdb_kvseq.add_stream";
    if journal_opt = None then
      failwith "Jseqdb_kvseq.add_stream: read-only";
    let pos = rdwr1 # eof in
    let pos0 = Int64.to_int(Int64.logand pos 0xff_L) in
    rdwr1 # seek pos;
    let hd = String.make 2 '!' in
    hd.[1] <- Char.chr pos0;
    rdwr1 # really_output hd 0 2;
    rdwr1 # output_vint k.k_rowid;
    rdwr1 # output_vint (~> (String.length k.k_key));
    output_string rdwr1 k.k_key;
    rdwr1 # output_vint len;
    let rlen = ref len in
    let cur = ref (stream()) in
    while !cur <> None do
      match !cur with
	| None -> assert false
	| Some(s,spos,slen) ->
	    if spos < 0 || slen < 0 || spos > String.length s - slen then
	      invalid_arg "Jseqdb_kvseq.add_stream";
	    if (~> slen) > !rlen then
	      failwith "Jseqdb_kvseq.add_stream: length mismatch";
	    rdwr1 # really_output s spos slen;
	    rlen := !rlen -@ (~> slen);
	    cur := stream()
    done;
    if !rlen <> 0L then
       failwith "Jseqdb_kvseq.add_stream: length mismatch";
    num_entries := Int64.succ !num_entries;
    modified <- true;
    pos


  method first() =
    let ptr = superblock#size in
    if ptr >= rdwr1 # eof then
      raise End_of_file;
    ptr

  method next ptr =
    let e = self # entry_at ptr in
    let ptr = e.e_val_ptr +@ e.e_length in
    if ptr >= rdwr1 # eof then (
      raise End_of_file;
    );
    ptr

  method flush() =
    (* Basically, flushing means to output the superblock via the journal *)
    match journal_opt with
      | None ->
	  ()   (* Nothing to flush... We could also fail here *)
      | Some j ->
	  if modified || superblock#is_modified then (
	    (* Remember the old superblock: *)
	    j # add_entry jidx (`Old_data sbdata);
	    (* Update the superblock: *)
	    let now64 = Int64.of_float (Unix.time()) in
	    superblock # set_variable
	      Jseqdb_superblock.Constants.syncsize_name rdwr1#eof;
	    superblock # set_variable
	      Jseqdb_superblock.Constants.synctime_name now64;
	    superblock # set_variable
	      Jseqdb_superblock.Constants.entries_name !num_entries;
	    (* Write the new superblock, and sync: *)
	    j # add_entry jidx (`New_data superblock#data);
	    j # add_entry jidx `New_sync;

	    modified <- false;
	    superblock # clear_modified()
	  )
end


class manager () : kvseq_manager =
object
  method create ?(sbvars = []) config gcp md =
    let now64 = Int64.of_float (Unix.time()) in
    let rdwr0 = new buf_rd_wr md in
    let sb = Jseqdb_superblock.create() in
    sb # set_variable 
      Jseqdb_superblock.Constants.format_name 
      Jseqdb_superblock.Constants.kvseq_format;
    sb # set_variable
      Jseqdb_superblock.Constants.syncsize_name
      sb#size;
    sb # set_variable
      Jseqdb_superblock.Constants.synctime_name
      now64;
    sb # set_variable
      Jseqdb_superblock.Constants.fileincr_name
      config#fileincr;
    sb # set_variable
      Jseqdb_superblock.Constants.purpose_name
      (Jseqdb_superblock.of_string config#purpose);
    sb # set_variable
      Jseqdb_superblock.Constants.gciter_name
      gcp.gciter;
    sb # set_variable
      Jseqdb_superblock.Constants.gcvers_name 
      gcp.gcvers;
    sb # set_variable
      Jseqdb_superblock.Constants.gctime_name
      now64;
    sb # set_variable
      Jseqdb_superblock.Constants.entries_name
      0L;
    List.iter
      (fun (n,v) ->
	 sb # set_variable n v
      )
      sbvars;
    sb # write rdwr0;
    rdwr0 # sync();
    rdwr0 # set_eof sb#size

  method read md =
    new kvseq_impl md None

  method write md j =
    new kvseq_impl md (Some j)
    
  method inspect md =
    let kvseq = new kvseq_impl md None in
    let sbvals = kvseq_sbvals kvseq#superblock in
    jf_version sbvals
end


let manager() = new manager()
