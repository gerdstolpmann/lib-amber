(* $Id$ *)

(* Locking:

   All locking is managed in this module. There is a separate "db.lock"
   file in the database directory. It may hold various locks:

   Byte position    Symbolic name    Meaning
   ----------------------------------------------------------------------
   0                jsuper_lock      Shared: this lock is held as shared
                                       lock while the journal is opened
                                     Excl: it is held as exlusive lock
                                       when the journal superblock is written
   1                journal_lock     Shared: not used
                                     Excl: it is held as exlusive lock
                                       during the whole write transaction
   2                doorman_lock     "doorman" for data_lock (see below)
   3                data_lock        Shared: this lock is held as shared
                                       lock during any transaction
                                     Excl: it is held as exclusive lock
                                       during the commit or rollback
   4                dir_lock         Shared: while accessing the db directory
                                       and opening files
                                     Excl: while creating/renaming/deleting
                                       files


   2-step locking: 

   There are two locks l1 and l2.
     To get a shared lock:
       - lock_shared l1; unlock l1; lock_shared l2
     To get an exclusive lock:
       - lock_exclusive l1; lock_exclusive l2; unlock l1
     l1 acts like a "doorman" for l2. Exclusive locks have a chance even
     when there are many parallel requests for shared locks.

   The locks are acquired and released in this order:

   - Open db (before read or write):

     1. get shared jsuper_lock
     2.     <do reads on journal superblock>
     3. unlock     jsuper_lock

   - Read transaction:

     1. get shared doorman_lock
     2. unlock     doorman_lock
     3. get shared data_lock
     4.     <do reads on data files>
     5. unlock     data_lock

   - Write transaction:

     1. get excl   journal_lock
     2. get excl   jsuper_lock
     3.     <initialize journal or roll back>
     4. unlock     jsuper_lock
     5. get shared doorman_lock
     6. unlock     doorman_lock
     7. get shared data_lock
     8.     <do reads on data files; accumulate writes in journal>

     When commit or rollback is done:

      9. get excl   doorman_lock
     10. get excl   data_lock    (upgrade from shared lock)
     11. unlock     doorman_lock
     12.     <apply journal to data files>
     13. get excl   jsuper_lock
     14.     <perform the state change in the journal>
     15. unlock     jsuper_lock
     16. unlock     journal_lock
     17. unlock     data_lock

   - reindex transaction: comparable to write transaction. Note, however,
     that the new index is a separate file, and updates don't go through
     the journal.

   Properties of this locking scheme:
   - There can be only one writer
   - Only during commit/rollback time the writer must lock out readers
   - The writer can put through that new readers have to wait when a
     commit/rollback is going to be done
   - This scheme is deadlock-free:
       - The read transaction cannot cause deadlocks with anything else
         because only a single lock is acquired at a time
       - A deadlock between two write transactions is also not possible
         because the very first lock the writer acquires is the journal_lock.
         There is only one writer at a time.
   *)


open Jseqdb_types
open Jseqdb_buffer
open Printf

class type xtable =
object
  inherit table

  method start : rw:bool -> 
                 data_md:file_descr -> 
                 idx_md:file_descr ->
                 pp_md:file_descr -> 
                 journal ->
                   unit

  method flush : unit -> unit
    (* pre-commit *)

  method finish : unit -> unit
    (* post commit *)

  method idx_superblock : superblock
  method data_superblock : superblock

end


let journal_lock = 0
let jsuper_lock = 1
let doorman_lock = 2
let data_lock = 3
let dir_lock = 4


let ( +@ ) = Int64.add
let ( -@ ) = Int64.sub
let ( *@ ) = Int64.mul
let ( /@ ) = Int64.div
let ( ~> ) = Int64.of_int  (* ">" = "convert to the bigger type" *)
let ( ~< ) = Int64.to_int  (* "<" = "convert to the smaller type" *)


type transaction =
    { rw : bool;
      data_md : file_descr;
      data : kvseq;
      idx_md : file_descr;
      idx : hindex;
      pp_md : file_descr;
      pp : kvseq;
    }


class table_impl name : xtable =
  let kvseq_mng = Jseqdb_kvseq.manager() in
  let hindex_mng = Jseqdb_hindex.manager() in
object(self)
  val mutable transaction = None

  method name = name

  method private get_trans rw =
    match transaction with
      | None ->
	  failwith "Jseqdb_database: outside of a transaction"
      | Some trans ->
	  if rw && not trans.rw then
	    failwith "Jseqdb_database: the transaction is read-only";
	  trans

  method start ~rw ~data_md ~idx_md ~pp_md journal =
    assert(transaction = None);   (* [start] is an internal method *)
    (* The journal is already locked, and in a sane state. *)
    (* Open the files: *)
    let data =
      if rw then
	kvseq_mng # write data_md journal
      else
	kvseq_mng # read data_md in
    let idx =
      if rw then
	hindex_mng # write idx_md journal
      else
	hindex_mng # read idx_md in
    let pp =
      if rw then
	kvseq_mng # write pp_md journal
      else
	kvseq_mng # read pp_md in
    let trans =
      { rw = rw;
	data_md = data_md;
	data = data;
	idx_md = idx_md;
	idx = idx;
	pp_md = pp_md;
	pp = pp;
      } in
    transaction <- Some trans

  method flush() =
    assert(transaction <> None);
    let trans = self # get_trans false in
    if trans.rw then (
      trans.data # flush();
      trans.idx # flush();
      trans.pp # flush();
    )

  method finish() =
    assert(transaction <> None);
    transaction <- None

  method config =
    let trans = self # get_trans false in
    let data_config = trans.data # config in
    let pp_config = trans.pp # config in
    let idx_config = trans.idx # config in
    ( object
	method data_config = data_config
	method pp_config = pp_config
	method idx_config = idx_config
      end
    )

  method idx_superblock =
    let trans = self # get_trans false in
    trans.idx # superblock

  method data_superblock =
    let trans = self # get_trans false in
    trans.data # superblock

  method lookup key =
    let trans = self # get_trans false in
    match trans.idx # lookup key with
      | None ->
	  None
      | Some ptr ->
	  let k = trans.data # lookup ptr in
	  Some (`File_pos ptr, `Row_id k.k_rowid)

  method key pos =
    let trans = self # get_trans false in
    match pos with
      | `File_pos ptr ->
	  let k = trans.data # lookup ptr in
	  k.k_key
      | `Row_id ppos ->
	  let ptr = self # pp_find ppos in
	  let k = trans.data # lookup ptr in
	  k.k_key

  method private pp_find_maybe_next ppos =
    (* Returns the key of the entry at row ID [ppos], or the key of the
       next entry with bigger row ID if [ppos] cannot be found. The entry
       may happen to be deleted. Raises [End_of_file] if the end of the file
       is hit.
     *)
    let trans = self # get_trans false in
    let last_ptr = ref None in
    ( try
	let ptr = ref(trans.pp # first()) in   (* or End_of_file *)
	let pk = trans.pp # lookup !ptr in
	let cont = ref (pk.k_rowid < ppos) in
	if !cont then last_ptr := Some(pk.k_key);
	while !cont do
	  ptr := trans.pp # next !ptr;
	  let pk = trans.pp # lookup !ptr in
	  cont := pk.k_rowid < ppos;
	  if !cont then last_ptr := Some(pk.k_key);
	done;
      with
	| End_of_file -> ()
    );
    let data_ptr =
      ref(match !last_ptr with
	    | None ->
		trans.data # first()
	    | Some lptr ->
		Int64.of_string lptr) in
    let dk = ref(trans.data # lookup !data_ptr) in
    while !dk.k_rowid < ppos do
      data_ptr := trans.data # next !data_ptr;
      dk := trans.data # lookup !data_ptr
    done;
    (!dk, !data_ptr)

  method private pp_find ppos =
    let trans = self # get_trans false in
    try
      let (dk, data_ptr) = self # pp_find_maybe_next ppos in
      if dk.k_rowid <> ppos then raise End_of_file;
      if trans.idx # lookup dk.k_key <> Some data_ptr then raise End_of_file;
      data_ptr
    with
      | End_of_file ->
	  raise (Row_id_not_found ppos)

  method row_id pos =
    let trans = self # get_trans false in
    match pos with
      | `File_pos ptr ->
	  let k = trans.data # lookup ptr in
	  `Row_id k.k_rowid
      | `Row_id ppos ->
	  let ptr = self # pp_find ppos in
	  let k = trans.data # lookup ptr in
	  `Row_id k.k_rowid
    
  method seek pos =
   let trans = self # get_trans false in
    match pos with
      | `File_pos ptr ->
	  let k = trans.data # lookup ptr in
	  (`File_pos ptr, `Row_id k.k_rowid)
      | `Row_id ppos ->
	  let ptr = self # pp_find ppos in
	  (`File_pos ptr, `Row_id ppos)
 
  method value pos =
    let trans = self # get_trans false in
    let (`File_pos ptr, _) = self # seek pos in
    trans.data # value ptr

  method length pos =
    let trans = self # get_trans false in
    let (`File_pos ptr, _) = self # seek pos in
    trans.data # length ptr

  method blit pos offs s spos slen =
    let trans = self # get_trans false in
    let (`File_pos ptr, _) = self # seek pos in
    trans.data # blit ptr offs s spos slen

  method add key s spos slen =
    let ppnext_name = Jseqdb_superblock.Constants.ppnext_name in
    let trans = self # get_trans true in
    let ppos = trans.pp # superblock # variable ppnext_name in
    trans.pp # superblock # set_variable ppnext_name (Int64.succ ppos);
    let k = { k_key = key; k_rowid = ppos  } in  
    let ptr = trans.data # add k s spos slen in
    trans.idx # update key ptr;
    self # maybe_add_pp ptr ppos;
    (`File_pos ptr, `Row_id ppos)

  method add_stream key rid_opt stream len =
    let ppnext_name = Jseqdb_superblock.Constants.ppnext_name in
    let trans = self # get_trans true in
    let ppos = 
      match rid_opt with
	| None ->
	    let rid = trans.pp # superblock # variable ppnext_name in
	    trans.pp # superblock # set_variable ppnext_name (Int64.succ rid);
	    rid
	| Some (`Row_id rid) ->
	    let sb_rid = trans.pp # superblock # variable ppnext_name in
	    let sb_rid' = max (Int64.succ rid) sb_rid in
	    trans.pp # superblock # set_variable ppnext_name sb_rid';
	    rid  in
    let k = { k_key = key; k_rowid = ppos  } in  
    let ptr = trans.data # add_stream k stream len in
    trans.idx # update key ptr;
    self # maybe_add_pp ptr ppos;
    (`File_pos ptr, `Row_id ppos)

  method private maybe_add_pp ptr ppos =
    let trans = self # get_trans true in
    let pplast_name = Jseqdb_superblock.Constants.pplast_name in
    let pplast = trans.pp # superblock # variable pplast_name in
    if ptr > pplast +@ 0x10_0000L then (
      let k = { k_key = Int64.to_string ptr; k_rowid = ppos  } in
      let _pp_ptr = trans.pp # add k "" 0 0 in
      trans.pp # superblock # set_variable pplast_name ptr
    )

  method delete pos =
    let trans = self # get_trans true in
    let k = self # key pos in
    trans.idx # delete k

  method first () =
    let trans = self # get_trans false in
    let ptr = trans.data # first() in   (* or End_of_file *)
    `File_pos(self # next_existing trans ptr)

  method next pos =
    let trans = self # get_trans false in
    match pos with
      | `File_pos ptr0 ->
	  let ptr1 = trans.data # next ptr0 in      (* or End_of_file *)
	  `File_pos(self # next_existing trans ptr1)
      | `Row_id id ->
	  let (k, ptr0) = self # pp_find_maybe_next id in (* or End_of_file *)
	  let ptr1 =
	    if k.k_rowid = id then
	      trans.data # next ptr0
	    else
	      ptr0 in
	  `File_pos(self # next_existing trans ptr1)


  method private next_existing trans ptr0 =
    let ptr = ref ptr0 in
    let exists() =
      let k = trans.data # lookup !ptr in
      let b = trans.idx # lookup k.k_key = Some !ptr in
      b
    in
    while not(exists()) do
      ptr := trans.data # next !ptr   (* or End_of_file *)
    done;
    !ptr
end


let regexp_glob dirname rex =
  let l = Sys.readdir dirname in
  List.filter
    (fun n -> Pcre.pmatch ~rex n)
    (Array.to_list l)


let create_file dirname name suffix =
  new managed_descr
    ~filename:(Filename.concat dirname (name ^ suffix))
    ~flags:[Unix.O_RDWR; Unix.O_CREAT; Unix.O_TRUNC]
    ~reopen_flags:[Unix.O_RDWR]
    ~perm:0o666
    ~reopen_perm:0o666 


class database_impl dirname : database =
  let kvseq_mng = Jseqdb_kvseq.manager() in
  let hindex_mng = Jseqdb_hindex.manager() in
  let lock_filename =
    Filename.concat dirname "db.lock" in
  let lock_md =
    new managed_descr
      ~filename:lock_filename
      ~flags:[Unix.O_RDWR; Unix.O_CREAT]
      ~reopen_flags:[Unix.O_RDWR]
      ~perm:0o666
      ~reopen_perm:0o666 in
  let journal_filename =
    Filename.concat dirname "db.journal" in
  let journal_md =
    new managed_descr
      ~filename:journal_filename
      ~flags:[Unix.O_RDWR; Unix.O_CREAT]
      ~reopen_flags:[Unix.O_RDWR]
      ~perm:0o666
      ~reopen_perm:0o666 in
  let journal = 
      Jseqdb_journal.access_journal 
	~jsuper_lock_op:(fun op -> lock_md # lock_op op jsuper_lock)
	journal_md in
  let generation_filename =
    Filename.concat dirname "db.generation" in
  let generation_md =
     new managed_descr
      ~filename:generation_filename
      ~flags:[Unix.O_RDWR; Unix.O_CREAT]
      ~reopen_flags:[Unix.O_RDWR]
      ~perm:0o666
      ~reopen_perm:0o666 in
  let generation_rw = new buf_rd_wr generation_md in
  (* Note: all locking of jsuper_lock is done by Jseqdb_journal. This
     is uncritical
   *)
  let table_md = Hashtbl.create 50 in
  let (init_table_names, init_generation) =
    with_exclusive_lock   (* exclusive because of possible fixups *)
      lock_md
      dir_lock
      (fun () ->
	 generation_rw # seek 0L;
	 let gen = generation_rw # input_vint() in
	 let files = regexp_glob dirname (Pcre.regexp "^[^:]*\\.data$") in
	 let init_table_names = 
	   List.map (fun f -> Filename.chop_suffix f ".data") files in
	 List.iter
	   (fun tabname ->
	      (* Check for a crash at the end of the compaction run: *)
	      let have_sav_pp =
		Sys.file_exists
		  (Filename.concat dirname (tabname ^ ":compactsav.pp")) in
	      let have_sav_idx =
		Sys.file_exists
		  (Filename.concat dirname (tabname ^ ":compactsav.idx")) in
	      let have_comp_data =
		Sys.file_exists
		  (Filename.concat dirname (tabname ^ ":compact.data")) in
	      if have_comp_data then (
		if have_sav_idx then
		  Sys.rename
		    (Filename.concat dirname (tabname ^ ":compactsav.idx"))
		    (Filename.concat dirname (tabname ^ ".idx"));
		if have_sav_pp then
		  Sys.rename
		    (Filename.concat dirname (tabname ^ ":compactsav.pp"))
		    (Filename.concat dirname (tabname ^ ".pp"));
	      );
	      (* Open the files *)
	      let data_md = 
		rw_descr (Filename.concat dirname (tabname ^ ".data")) in
	      let idx_md =
		rw_descr (Filename.concat dirname (tabname ^ ".idx")) in
	      let pp_md =
		rw_descr (Filename.concat dirname (tabname ^ ".pp")) in
	      let table =
		new table_impl tabname in
	      ignore(data_md # file_descr); (* really open the files! *)
	      ignore(idx_md # file_descr);
	      ignore(pp_md # file_descr);
	      Hashtbl.add table_md tabname (data_md, idx_md, pp_md, table);
	      (* Check for temporary files that can be removed: *)
	      List.iter
		(fun suffix ->
		   try
		     Sys.remove
		       (Filename.concat dirname (tabname ^ suffix))
		   with _ -> ()
		)
		[ ":compact.data"; ":compact.idx"; ":compact.pp";
		  ":compactsav.idx"; ":compactsav.pp"; ":reindex.idx"
		]
	   )
	   init_table_names;
	( init_table_names, gen)
      ) in
object(self)

  val mutable have_transaction = None
  val mutable table_names = init_table_names
  val mutable files_are_open = true
  val mutable generation = init_generation

  method filename = dirname
  method journal = journal

  method private reopen_files() =
    with_shared_lock
      lock_md
      dir_lock
      self#reopen_files_l

  method private reopen_files_if() =
    with_shared_lock
      lock_md
      dir_lock
      (fun () ->
	 generation_rw # seek 0L;
	 let gen = generation_rw # input_vint() in
	 if gen > generation then
	   self # reopen_files_l()
      )

  method private reopen_files_l() =
    Hashtbl.iter
      (fun _ (data_md, idx_md, pp_md, _) ->
	 data_md # dispose_descr();
	 idx_md # dispose_descr();
	 pp_md # dispose_descr();
      )
      table_md;
    files_are_open <- false;
    Hashtbl.iter
      (fun _ (data_md, idx_md, pp_md, _) ->
	 ignore(data_md # file_descr);
	 ignore(idx_md # file_descr);
	 ignore(pp_md # file_descr);
      )
      table_md;
    files_are_open <- true;
    generation_rw # seek 0L;
    let gen = generation_rw # input_vint() in
    generation <- gen


  method private incr_generation_l() =
    (* Must be called while dir_lock is exclusively held *)
    generation_rw # seek 0L;
    let gen = generation_rw # input_vint() in
    let gen' = gen +@ 1L in
    generation_rw # seek 0L;
    generation_rw # output_vint gen'


  method start_transaction ~rw () =
    if have_transaction <> None then
      failwith "Jseqdb_database: already in a transaction";

    (* Ensure a consistent view on the files: *)
    if files_are_open then
      self # reopen_files_if()
    else
      self # reopen_files();

    (* First acquire an exclusive lock on the journal if this is rw: *)
    if rw then (
      lock_md # lock_op `Exclusive journal_lock;
      (* Also, if the journal is in an inacceptable state, fix this: *)
      if journal # state = `Valid then  (* valid but incomplete commit! *)
	journal # rollback self#get_rw;
      (* Finally, start the transaction in the journal: *)
      journal # reset();
    );

    (* Get a shared lock (2-step scheme): *)
    lock_md # lock_op `Shared doorman_lock;
    lock_md # lock_op `Unlock doorman_lock;
    lock_md # lock_op `Shared data_lock;

    Hashtbl.iter
      (fun tabname (data_md,idx_md,pp_md,table) ->
	 table # start ~rw ~data_md ~idx_md ~pp_md journal
      )
      table_md;

    have_transaction <- Some rw

  method generation =
    if have_transaction = None then
      failwith "Jseqdb_database: not in a transaction";
    generation

  method private get_rw jf =
    let tabname = Filename.chop_extension jf.jf_basename in
    let (data_md,idx_md,pp_md,table) =
      try Hashtbl.find table_md tabname
      with Not_found -> assert false in
    let md =
      if Filename.check_suffix jf.jf_basename ".data" then
	data_md
      else 
	if Filename.check_suffix jf.jf_basename ".pp" then
	  pp_md
	else
	  if Filename.check_suffix jf.jf_basename ".idx" then
	    idx_md
	  else
	    assert false in
    new buf_rd_wr md

  method commit_transaction() =
    self # commit_transaction_int()

  method private commit_transaction_int ?(post_commit = fun () -> ()) () =
    match have_transaction with
      | None ->
	  ()
      | Some rw ->
	  if rw then (
	    (* Flush everything to the journal: *)
	    Hashtbl.iter
	      (fun tabname (data_md,idx_md,pp_md,table) ->
		 table # flush()
	      )
	      table_md;

	    (* Upgrade the data lock (2-step scheme): *)
	    lock_md # lock_op `Exclusive doorman_lock;
	    lock_md # lock_op `Exclusive data_lock;
	    lock_md # lock_op `Unlock doorman_lock;
	    
	    (* Write out data: *)
	    journal # commit();

	    (* Post-commit hook: *)
	    post_commit();

	    (* Unlock journal: *)
	    lock_md # lock_op `Unlock journal_lock
	  );
	  
	  (* Finish: *)
	  Hashtbl.iter
	    (fun tabname (data_md,idx_md,pp_md,table) ->
	       table # finish()
	    )
	    table_md;
	  (* Release all locks: *)
	  lock_md # lock_op `Unlock data_lock;
	  have_transaction <- None

  method private flush() =
    (* That's only to be used by [compact], to flush data from time to
       time, so the journal does not fill up too much. 
     *)
    assert(have_transaction = Some true);
    Hashtbl.iter
      (fun tabname (data_md,idx_md,pp_md,table) ->
	 table # flush()
      )
      table_md;
    journal # commit()


  method rollback_transaction() =
    match have_transaction with
      | None ->
	  ()
      | Some rw ->
	  if rw then (
	    (* We don't flush here...
               TODO: improve the protocol. flush needs an argument that says
               whether we commit or rollback
	     *)
	    
	    (* Upgrade the data lock (2-step scheme): *)
	    lock_md # lock_op `Exclusive doorman_lock;
	    lock_md # lock_op `Exclusive data_lock;
	    lock_md # lock_op `Unlock doorman_lock;

	    (* Write out data: *)
	    journal # rollback self#get_rw;

	    (* Unlock journal: *)
	    lock_md # lock_op `Unlock journal_lock
	  );

	  (* Finish: *)
	  Hashtbl.iter
	    (fun tabname (data_md,idx_md,pp_md,table) ->
	       table # finish()
	    )
	    table_md;
	  (* Release all locks: *)
	  lock_md # lock_op `Unlock data_lock;
	  have_transaction <- None
	    
	    
  method table_names =
    table_names

  method table name =
    try
      let (_, _, _, t) = Hashtbl.find table_md name in
      (t :> table)
    with
      | Not_found ->
	  raise (Table_not_found name)

  method table_create (config:table_config) name =
    if have_transaction <> None then
      failwith "Jseqdb_database: in a transaction";
    if List.mem name table_names then
      raise(Table_exists name);
    with_exclusive_lock
      lock_md
      dir_lock
      (fun () ->
	 let (data_md, idx_md, pp_md, table) =
	   self # table_create_int config name in
	 Hashtbl.add table_md name (data_md, idx_md, pp_md, table);
	 table_names <- name :: table_names
      )

  method private table_create_int config name =
    if Sys.file_exists (Filename.concat dirname (name ^ ".data")) then
      raise (Table_exists name);
    let pp_md = create_file dirname name ".pp" in
    let idx_md = create_file dirname name ".idx" in
    let data_md = create_file dirname name ".data" in
    let gcprops =
      { gciter = 0L;
	gctime = Int64.of_float (Unix.time());
	gcvers = 0L 
      } in
    let pplast_name = Jseqdb_superblock.Constants.pplast_name in
    let ppnext_name = Jseqdb_superblock.Constants.ppnext_name in
    kvseq_mng # create config#pp_config
      ~sbvars:[ pplast_name, 0L; ppnext_name, 0L ]
      gcprops pp_md;
    hindex_mng # create config#idx_config gcprops idx_md;
    kvseq_mng # create config#data_config gcprops data_md;
    let table = new table_impl name in
    self # incr_generation_l ();
    (data_md, idx_md, pp_md, table)


  method table_drop name =
    if have_transaction <> None then
      failwith "Jseqdb_database: in a transaction";
    if not(List.mem name table_names) then
     raise(Table_not_found name);
    with_exclusive_lock
      lock_md
      dir_lock
      (fun () ->
	 self # table_drop_int name
      );
    Hashtbl.remove table_md name;
    table_names <- List.filter (fun n -> n <> name) table_names
 

  method private table_drop_int name =
    List.iter
      (fun suffix ->
	 Sys.remove (Filename.concat dirname (name ^ suffix))
      )
      [ ".pp"; ".idx"; ".data" ];
    self # incr_generation_l ()


  method reindex (idx_config : hindex_config) name =
    if have_transaction <> None then
      failwith "Jseqdb_database: in a transaction";
    let (data_md,idx_md,pp_md, t) = 
      try Hashtbl.find table_md name
      with Not_found -> raise(Table_not_found name) in
    self # start_transaction ~rw:true ();
    let new_idx_md_opt = ref None in
    let counter = ref 0 in
    ( try
	let new_idx_md = create_file dirname (name ^ ":reindex") ".idx" in
	new_idx_md_opt := Some new_idx_md;
	let gcprops = t#idx_superblock#gc_props in
	hindex_mng # create idx_config gcprops new_idx_md;
	let fj = Jseqdb_journal.fake_journal() in
	let new_idx = hindex_mng # write new_idx_md fj in
	( try
	    let ptr = ref (t#first()) in
	    while true do
	      let key = t#key (!ptr :> any_position) in
	      let `File_pos fpos = !ptr in
	      new_idx # update key fpos;
	      ptr := t # next (!ptr :> any_position);
	      incr counter;
	      if !counter = 5000 then (
		new_idx # flush();
		counter := 0;
	      )
	    done
	  with
	    | End_of_file ->
		()
	);
	new_idx # flush();
	self # commit_transaction_int
	  ~post_commit:(fun () ->
			  (* This is done after the journal is committed,
                             but before the journal lock is released.
			   *)
			  with_exclusive_lock
			    lock_md
			    dir_lock
			    (fun () ->
			       Sys.rename 
				 (Filename.concat dirname 
				    (name ^ ":reindex.idx"))
				 (Filename.concat dirname
				    (name ^ ".idx"));
			       self # incr_generation_l()
			    )
		       )
	  ();
	idx_md # dispose_descr();
	Hashtbl.replace table_md name (data_md,new_idx_md,pp_md,t)
      with
	| error ->
	    ( match !new_idx_md_opt with
		| None -> ()
		| Some md -> 
		    md # dispose_descr();
		    Sys.remove
		      (Filename.concat dirname (name ^ ":reindex.idx"));
	    );
	    self # rollback_transaction();
	    raise error
    )

  method compact (new_config : table_config) name =
    if have_transaction <> None then
      failwith "Jseqdb_database: in a transaction";
    let (old_data_md,old_idx_md,old_pp_md, t_old) = 
      try Hashtbl.find table_md name
      with Not_found -> raise(Table_not_found name) in
    let now = Unix.gettimeofday() in
    self # start_transaction ~rw:true ();
    let (new_data_md,new_idx_md,new_pp_md, t_new) =
      self # table_create_int new_config (name ^ ":compact") in
    let fj = Jseqdb_journal.fake_journal() in
    t_new # start 
      ~rw:true ~data_md:new_data_md ~idx_md:new_idx_md
      ~pp_md:new_pp_md fj;
    let gc_props = t_old # data_superblock # gc_props in
    let counter = ref 0 in
    let gcvers_acc = ref (Digest.string "") in
    let have_entry = ref false in
    ( try
	( try
	    let ptr = ref (t_old#first()) in
	    have_entry := true;
	    while true do
	      let key = t_old#key (!ptr :> any_position) in
	      let rid = t_old#row_id (!ptr :> any_position) in
	      let length = t_old#length (!ptr :> any_position) in
	      let `Row_id pp = rid in
	      gcvers_acc := 
		Digest.string
		  (sprintf "%s/%s/%Ld/%Ld" !gcvers_acc key pp length);
	      let j = ref 0L in
	      let s = String.create 4096 in
	      let value_stream =
		(fun () ->
		   let n =
		     ~< (min (~> (String.length s)) (length -@ !j)) in
		   if n > 0 then (
		     t_old#blit 
		       (!ptr :> any_position)
		       !j s 0 n;
		     j := !j +@ (~> n);
		     Some (s, 0, n)
		   )
		   else
		     None
		) in
	      let _new_ptr = 
		t_new # add_stream key (Some rid) value_stream length in
	      ptr := t_old # next (!ptr :> any_position);

	      incr counter;
	      if !counter = 5000 then (
		t_new # flush();
		counter := 0;
	      )
	    done
	  with
	    | End_of_file ->
		()
	);

	let gciter = Int64.succ gc_props.gciter in
	let gctime = Int64.of_float now in
	let gcvers = 
	  if !have_entry then
	    Int64.logand
	      (Rtypes.int64_of_int8 (Rtypes.read_int8 !gcvers_acc 0)) 
	      0x7fff_ffff_ffff_ffff_L
	  else
	    0L in
	List.iter
	  (fun (n,v) ->
	     t_new # data_superblock # set_variable n v)
	  [ Jseqdb_superblock.Constants.gciter_name, gciter;
	    Jseqdb_superblock.Constants.gctime_name, gctime;
	    Jseqdb_superblock.Constants.gcvers_name, gcvers;
	  ];
	t_new # flush();
	fj # commit();
	self # commit_transaction_int
	  ~post_commit:(fun () ->
			  (* This is done after the journal is committed,
                             but before the journal lock is released.

                             If the system crashes during this rename
                             block, there is some logic at database_impl
                             creation time for sorting out the mess.
			   *)
			  with_exclusive_lock
			    lock_md
			    dir_lock
			    (fun () ->
			       List.iter
				 (fun del_suffix ->
				    try
				      Sys.remove
					(Filename.concat 
					   dirname (name ^ del_suffix))
				    with _ -> ()
				 )
				 [ ":compactsav.pp"; ":compactsav.idx" ];
			       List.iter
				 (fun (from_suffix,to_suffix) ->
				    Sys.rename 
				      (Filename.concat 
					 dirname (name ^ from_suffix))
				      (Filename.concat 
					 dirname (name ^ to_suffix))
				 )
				 [ ".pp",           ":compactsav.pp";
				   ".idx",          ":compactsav.idx";
				   ":compact.pp",   ".pp";
				   ":compact.idx",  ".idx";
				   ":compact.data", ".data";
				 ];
			       List.iter
				 (fun del_suffix ->
				    Sys.remove
				      (Filename.concat 
					 dirname (name ^ del_suffix)))
				 [ ":compactsav.pp"; ":compactsav.idx" ];
			       self # incr_generation_l()
			    )
		       )
	  ();
	old_data_md # dispose_descr();
	old_idx_md # dispose_descr();
	old_pp_md # dispose_descr();
	Hashtbl.replace table_md name (new_data_md,new_idx_md,new_pp_md,t_new)
      with
	| error -> 
	    new_data_md # dispose_descr();
	    new_idx_md # dispose_descr();
	    new_pp_md # dispose_descr();
	    self # rollback_transaction();
	    self # table_drop_int (name ^ ":compact");
	    raise error
    )


  method close () =
    if have_transaction <> None then
      failwith "Jseqdb_database: in a transaction";
    Hashtbl.iter
      (fun tabname (data_md,idx_md,pp_md,_) ->
	 data_md # dispose_descr();
	 idx_md # dispose_descr();
	 pp_md # dispose_descr();
      )
      table_md;
    journal_md # dispose_descr();
    generation_md # dispose_descr();
    lock_md # dispose_descr();
    files_are_open <- false


end


class manager () : database_manager =
object
  method opendb dirname =
    new database_impl dirname 

  method dropdb dirname =
    let lock_filename =
      Filename.concat dirname "db.lock" in
    let lock_md =
      new managed_descr
	~filename:lock_filename
	~flags:[Unix.O_RDWR; Unix.O_CREAT]
	~reopen_flags:[Unix.O_RDWR]
	~perm:0o666
	~reopen_perm:0o666 in
    let generation_filename =
      Filename.concat dirname "db.generation" in
    let generation_md =
      new managed_descr
	~filename:generation_filename
	~flags:[Unix.O_RDWR; Unix.O_CREAT]
	~reopen_flags:[Unix.O_RDWR]
	~perm:0o666
	~reopen_perm:0o666 in
    let generation_rw = new buf_rd_wr generation_md in
    lock_md # lock_op `Exclusive journal_lock;
    lock_md # lock_op `Exclusive dir_lock;
    let gen = generation_rw # input_vint() in
    generation_rw # seek 0L;
    generation_rw # output_vint (gen +@ 1L);
    let files = Sys.readdir dirname in
    Array.iter
      (fun f ->
	 if f <> "db.lock" then
	   Sys.remove (Filename.concat dirname f)
      )
      files;
     Sys.remove (Filename.concat dirname "db.lock"); (* do this last *)
     Unix.rmdir dirname;
     lock_md # dispose_descr();
     generation_md # dispose_descr()

  method createdb dirname =
    ( try Unix.mkdir dirname 0o777 
      with Unix.Unix_error(Unix.EEXIST,_,_) -> ());
    let create_excl_file suffix =
      new managed_descr
	~filename:(Filename.concat dirname ("db" ^ suffix))
	~flags:[Unix.O_RDWR; Unix.O_CREAT; Unix.O_EXCL]
	~reopen_flags:[Unix.O_RDWR]
	~perm:0o666
	~reopen_perm:0o666 in
    let lock_md = create_excl_file ".lock" in
    let journal_md = create_excl_file ".journal" in
    let generation_md = create_excl_file ".generation" in
    with_exclusive_lock
      lock_md
      journal_lock
      (fun () ->
	 let journal = Jseqdb_journal.access_journal journal_md in
	 journal # reset();
      );
    let gen_rw = new buf_rd_wr generation_md in
    gen_rw # output_vint 0L;
    journal_md # dispose_descr();
    lock_md # dispose_descr();
    generation_md # dispose_descr();
end


let manager = new manager
