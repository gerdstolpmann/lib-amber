(* $Id$ *)

(** For simplicity, superblocks are now always 4096 bytes in size *)

open Jseqdb_types

val create : unit -> superblock
  (** Creates a new empty superblock object *)

val to_string : int64 -> string
val of_string : string -> int64
  (** For accessing PURPOSE etc. *)

module Constants : sig
  val magic : string
  val sbsize_name : string
    (** SBSIZE is the size of the superblock in bytes *)

  val format_name : string
    (** FORMAT says which format the file has: One of
        [kvseq_format], [hindex_format], or [journal_format].
     *)

  val purpose_name : string
    (** PURPOSE is a string of 8 bytes that gives a hint for what the
        file is used.
     *)

  val filesize_name : string
    (** FILESIZE is the logical length of the file. That means only the
        range from 0 to FILESIZE-1 is considered as the part of the file
        that contains valid data. The range FILESIZE to the real end
        is seen as allocated but free extension space.

        FILESIZE is used for all formats. As Hindex files have a fixed
        length, it does not make sense to allocate space beyond FILESIZE,
        however.
     *)

  val fileincr_name : string
    (** FILEINCR is the increment by which the file is extended when the
        file is full. Values of several megs prevent that the file gets
        fragmented on the disk too much
     *)

  val syncsize_name : string
    (** SYNCSIZE is the logical length of the file at the time of the
        last checkpoint. SYNCSIZE must not be larger than FILESIZE.
        The range SYNCSIZE to FILESIZE-1 is considered as possibly
        corrupted.

        SYNCSIZE only applies to Kvseq files.
     *)

  val synctime_name : string
    (** SYNCTIME is the timestamp (as seconds since the epoch) of the
        last checkpoint.

        SYNCTIME only applies to Kvseq files.
     *)

  val entries_name : string
    (** ENTRIES is the total number of entries in the file (deleted and
        active entries). ENTRIES is only meaningful for Kvseq and Hindex.
     *)

  val aentries_name : string
    (** AENTRIES is the number of active (non-deleted) entries in the file.
        AENTRIES is only meaningful for Kvseq and Hindex.
     *)

  val htsize_name : string
    (** HTSIZE is the size of the Hindex table as number of elements *)

  val gciter_name : string
    (** GCITER is the iteration number of the garbage collector. It is
        increased at every GC round
     *)

  val gctime_name : string
    (** GCTIME is the timestamp when the last GC was done (informational) *)

  val gcvers_name : string
    (** GCVERS is a checksum that is computed during a GC run and that
        is only computed from the contents of the file
     *)

  val jstate_name : string
    (** JSTATE is the journal state *)

  val pplast_name : string
    (** PPLAST is the data pointer contained in the last pp entry *)

  val ppnext_name : string
    (** PPNEXT is the next persistent pointer *)

  (** Values for [FORMAT]: *)

  val kvseq_format : int64
  val hindex_format : int64
  val journal_format : int64
end
