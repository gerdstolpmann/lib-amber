#use "topfind";;
#require "pcre,rpc,unix";;
#load "jseqdb.cma";;
open Jseqdb_buffer;;
open Jseqdb_types;; 
let md = rw_descr "test";;
let config = (object method fileincr = 40960L method purpose="foo" end);;
let gcp = { gciter=0L; gctime=0L; gcvers=0L };;
let m = Jseqdb_kvseq.manager();;
let j_md = rw_descr "jtest";;
let j = Jseqdb_journal.access_journal j_md;;

let table_config =
  ( object
      method data_config =
	( object
	    method fileincr = 65536L
	    method purpose = "testing"
	  end
	)
      method pp_config =
	( object
	    method fileincr = 65536L
	    method purpose = "testing"
	  end
	)
      method idx_config =
	( object
	    method htsize = 1024L
	    method purpose = "testing"
	  end
	)
    end
  );;
let db = Jseqdb_database.manager();;
let d = db # opendb "tst";;
let tab1 = d # table "tab1";;
d # start_transaction ~rw:true();;   
d # start_transaction ~rw:false();;   
d # commit_transaction();;
