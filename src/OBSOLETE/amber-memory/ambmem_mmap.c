/* $Id$ */

#define _GNU_SOURCE
/* for posix_fallocate */

#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>

#include "ambmem_mmap.h"
#include "ambmem_basics.h"

int ambmem_map_anon(struct mapping *m, void *start, size_t increment)
{
    void *a0;
    size_t pagesize;
	
    enter_function("ambmem_map_anon");

    /* mmap immediately one page to check the validity of [start] */
    pagesize = sysconf(_SC_PAGESIZE);
    a0 = mmap(start, pagesize, PROT_READ|PROT_WRITE,
	      MAP_SHARED | MAP_ANONYMOUS | MAP_FIXED,
	      -1, 0
	      );
    if (a0 == MAP_FAILED) return (-1);

    m->start = (char *) start;
    m->mmap_length = pagesize;
    m->length = 0;
    m->is_rw = 1;
    m->is_anonymous = 1;
    m->fd = (-1);
    m->increment = increment;
    return 0;
}


int ambmem_map_file(struct mapping *m, int fd, void *start, size_t increment,
		    int rw)
{
    void *a0;
    size_t pagesize;
    struct stat st;
    int code;
	
    enter_function("ambmem_map_file");
    pagesize = sysconf(_SC_PAGESIZE);
    /* Check: file size must be a multiple of pagesize */
    code = fstat(fd, &st);
    if (code == -1) return code;
    if (st.st_size % pagesize != 0) return (-2);

    a0 = mmap(start, st.st_size, PROT_READ|(rw ? PROT_WRITE : 0),
	      MAP_SHARED | MAP_FIXED,
	      fd, 0
	      );
    if (a0 == MAP_FAILED) return (-1);

    m->start = (char *) start;
    m->mmap_length = st.st_size;
    m->length = st.st_size;
    m->is_rw = rw;
    m->is_anonymous = 0;
    m->fd = fd;
    m->increment = increment;
    return 0;
}


int ambmem_map_extend(struct mapping *m, size_t new_length)
{
    void *a0;
    size_t pagesize;
    size_t needed_pages;
    size_t needed_bytes;
    size_t increment_pages;
    int code;

    enter_function("ambmem_map_extend");
    if (m->length > new_length) return (-2);
    if (m->length == new_length) return 0;

    if (m->mmap_length >= new_length) {
	/* no remapping required */
	m->length = new_length;
	return 0;
    }

    pagesize = sysconf(_SC_PAGESIZE);
#ifdef DEBUG
    fprintf(stderr, "new_length=%ld mmap_length=%ld\n", new_length, m->mmap_length);
#endif
    needed_pages = (new_length - m->mmap_length - 1) / pagesize + 1;
    increment_pages = (m->increment - 1) / pagesize + 1;

    if (needed_pages < increment_pages) needed_pages = increment_pages; 

    needed_bytes = needed_pages * pagesize;

    if (m->is_anonymous) {
	char *a1;
	a1 = (char *) m->start;
	/*
	  -- does not work (why?)
	a0 = mremap(a1, m->mmap_length,
		    m->mmap_length + needed_bytes,
		    0);
	*/
	a0 = mmap(a1 + m->mmap_length,
		  needed_bytes,
		  PROT_READ|PROT_WRITE,
		  MAP_SHARED | MAP_ANONYMOUS | MAP_FIXED,
		  -1, 0
		  );
	if (a0 == MAP_FAILED) return (-1);
	m->mmap_length += needed_bytes;
	m->length = new_length;
    }
    else {
	char *a1;

	if (!m->is_rw) return (-2);
	/* Enlarge the file: */
	code = ftruncate(m->fd, m->mmap_length + needed_bytes);
	if (code == -1) return -1;
	
	code = posix_fallocate(m->fd, m->mmap_length, needed_bytes);
	if (code != 0) {
	    errno = code;
	    return -1;
	}

	/* Map the new part: */
	a1 = (char *) m->start;
	a0 = mmap(a1 + m->mmap_length,
		  needed_bytes,
		  PROT_READ|PROT_WRITE,
		  MAP_SHARED | MAP_FIXED,
		  m->fd, m->mmap_length
		  );
	if (a0 == MAP_FAILED) return (-1);
	m->mmap_length += needed_bytes;
	m->length = new_length;
    }

    return 0;
}


int ambmem_unmap(struct mapping *m)
{
    int code;
    enter_function("ambmem_unmap");
    code = munmap(m->start, m->mmap_length);
    return code;
}
