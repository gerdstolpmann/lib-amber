/* $Id$ */

#include "ambmem_file.h"
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int ambmem_file_init(struct mapping *m, struct file_header **hp)
{
    struct file_header *h;
    unsigned long r;
    int code;

    enter_function("ambmem_file_init");
    if (m->length != 0) return (-2);

    code = ambmem_map_extend(m, FILE_HEADER_SIZE);
    if (code < 0) return code;

    h = (struct file_header *) m->start;
    strcpy(h->magic1, "AMBMEM0");
    h->start = (unsigned long) h;
    h->header_length = FILE_HEADER_SIZE;
    h->total_length = FILE_HEADER_SIZE;

    r = (FILE_HEADER_SIZE - sizeof(struct file_header)) / sizeof(value);
    h->byteregion[0] = (r << 10) | String_tag;
    memset(&(h->byteregion[1]), 
	   0, 
	   FILE_HEADER_SIZE - sizeof(struct file_header));
    
    *hp = h;
    return 0;
}


int ambmem_file_open(struct mapping *m, struct file_header **hp)
{
    struct file_header *h;
    unsigned long w;

    enter_function("ambmem_file_open");
    if (m->length < sizeof(struct file_header)) return (-2);

    h = (struct file_header *) m->start;
    if (memcmp(h->magic1, "AMBMEM0", 8) != 0) return (-2);
    if (m->start != (void *) h->start) return (-2);
    if (h->total_length > m->length) return (-2);
    if (h->header_length > h->total_length) return (-2);
    if (h->header_length < sizeof(struct file_header)+sizeof(value)) 
	return (-2);
    if ((h->byteregion[0] & 0xff) != String_tag) return (-2);
    w = (h->byteregion[0] >> 10);
    if (w > (h->header_length - sizeof(struct file_header))/sizeof(value))
	return (-2);

    *hp = h;
    return 0;
}


int ambmem_file_inspect(int fd, void **start)
{
    struct file_header h;
    char *b;
    off_t lseek_code;
    ssize_t read_code;
    size_t w;

    enter_function("ambmem_file_inspect");
    b = (char *) &h;

    lseek_code = lseek(fd, 0, SEEK_SET);
    if (lseek_code == (off_t) -1) return (-1);

    read_code = read(fd, b, sizeof(struct file_header));
    if (read_code == -1) return (-1);

    if (memcmp(h.magic1, "AMBMEM0", 8) != 0) return (-2);
    if (h.header_length > h.total_length) return (-2);
    if (h.header_length < sizeof(struct file_header)+sizeof(value)) 
	return (-2);
    if ((h.byteregion[0] & 0xff) != String_tag) return (-2);
    w = (h.byteregion[0] >> 10);
    if (w > (h.header_length - sizeof(struct file_header))/sizeof(value))
	return (-2);
    
    *start = (void*) h.start;
    return 0;
}

int ambmem_keytab_region(struct file_header *h, 
			 void **keytabstart)
{
    enter_function("ambmem_keytab_region");
    *keytabstart = &(h->byteregion[0]);
    return 0;
}


int ambmem_alloc(struct mapping *m, struct file_header *h, size_t n, 
		 int oflag, void **addr)
{
    char *a0;
    char *a1;
    int code;

    enter_function("ambmem_alloc");
    switch(sizeof(void *)) {
    case 4:
	if ((n & 3) != 0) return (-2);
	break;
    case 8:
	if ((n & 7) != 0) return (-2);
	break;
    default:
	if (n % sizeof(void *) != 0) return (-2);
	break;
    }
    a0 = m->start + h->total_length;
    a1 = a0 + n + sizeof(void *);
    
    if (a1 >= m->start + m->length) {
	code = ambmem_map_extend(m, a1 - m->start);
	if (code < 0) return code;
    };
    
    *((size_t *) a0) = n | (oflag ? 1 : 0);
    *addr = (void *) (a0 + sizeof(void *));
    h->total_length += n + sizeof(void *);

    return 0;
}


int ambmem_reset(struct mapping *m, struct file_header *h, 
		 struct mem_iterator *i)
{
    enter_function("ambmem_reset");
    if (h->total_length > h->header_length) {
	char *a0;
	size_t l;
	int is_ocaml;
	a0 = m->start + h->header_length;
	l = ((size_t *) a0)[0];
	is_ocaml = (l & 1) != 0;
	if (is_ocaml) l--;
	i->addr = (void *) (a0 + sizeof(void *));
	i->length = l;
	i->is_ocaml = is_ocaml;
    }
    else {
	i->addr = NULL;
	i->length = 0;
	i->is_ocaml = 0;
    }
    return 0;
}


int ambmem_next(struct mapping *m, struct file_header *h, 
		struct mem_iterator *i)
{
    char *a0;
    size_t l;
    int is_ocaml;

    enter_function("ambmem_next");
    a0 = (char *) i->addr;
    a0 += i->length;

    if (a0 >= m->start + h->total_length) {
	i->addr = NULL;
	i->length = 0;
	i->is_ocaml = 0;
    }
    else {
	l = ((size_t *) a0)[0];
	is_ocaml = (l & 1) != 0;
	if (is_ocaml) l--;
	i->addr = (void *) (a0 + sizeof(void *));
	i->length = l;
	i->is_ocaml = is_ocaml;
    }

    return 0;
}
