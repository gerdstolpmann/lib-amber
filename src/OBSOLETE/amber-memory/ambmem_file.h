/* $Id$ */

/* Amber memory file format */

#ifndef AMBMEM_FILE
#define AMBMEM_FILE

#include "ambmem_basics.h"
#include "ambmem_mmap.h"

#define FILE_HEADER_SIZE 4096

struct file_header {
    char  magic1[8];
    unsigned long  start;
    unsigned long  header_length;
    unsigned long  total_length;   /* Logical length. <= the length of the mapping */
    value byteregion[1];

    /* Followed by a byte region that looks like an O'Caml string
       (1 header, 1-4 trailing 0 bytes as padding at end).

       This string encodes a "(string * nativeint) list".
       The strings are the keys, and the nativeints the pointers to the
       memory blocks (absolute pointers). The size of the byte region
       is header_length - sizeof(file_header).

       Encoding of the key list:
        - First (unsigned) byte: number of entries
        - Followed by the entries without padding

       Encoding of an entry:
        - 0-terminated string
        - 4 or 8 bytes with the pointer to the memory block
          (4 bytes on 32 bit, 8 bytes on 64 bit)
    */
};

/* After the file header, there are the blocks of allocated memory.
   Every block is preceded by an ambmem header - one word. This word
   is the length of the block. If the block is for an O'Caml value,
   the LSB of the word is set to 1 (otherwise the block is taken as
   opaque data).
*/

extern int ambmem_file_init(struct mapping *m, struct file_header **hp);
/* Initializes the mapping [m], which must be empty and read/write. 
   The mapping is extended as necessary. The header [h] is also initialized.

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error. On success, the structure [h] is initialized.
*/

extern int ambmem_file_open(struct mapping *m, struct file_header **hp);
/* Checks that there is a valid file header at the beginning of [m].

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error. On success, the pointer [hp] is set to the address
   of the header.
*/


extern int ambmem_file_inspect(int fd, void **start);
/* Inspects the file given by descriptor fd. If the file is a mappable
   ambmem file, the start address is written into [start], and 0 is
   returned.

   Return (-1) on system error (errno), or (-2) if the file is not
   an ambmem file.
*/


extern int ambmem_keytab_region(struct file_header *h, 
				void **keytabstart);
/* Returns the memory region occupied by the keytab. On success,
   keytabstart is set t� the start address.

   The word following keytabstart is a valid O'Caml string.

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error.

   Note: The code for interpeting the keytab bytes is written in O'Caml.
*/

extern int ambmem_alloc(struct mapping *m, struct file_header *h, size_t n, 
			int oflag, void **addr);
/* Allocates a new block at the end of the file. [n] is the number of
   bytes (which must be divisible by the word size). [oflag] says whether
   the block will contain an O'Caml value.

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error. On success [addr] is set to the address of the user part
   of the block.
*/

struct mem_iterator {
    void    *addr;
    size_t   length;
    int      is_ocaml;
};

extern int ambmem_reset(struct mapping *m, struct file_header *h, 
			struct mem_iterator *i);
/* Initializes [i] so it points to the first memory block (if any).
   i->addr is the address of the user part of the memory block, and
   i->length the length of the user part. i->is_ocaml says whether the
   block contains an O'Caml value. i->addr is NULL if there is no
   first memory block.

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error. 
*/

extern int ambmem_next(struct mapping *m, struct file_header *h, 
		       struct mem_iterator *i);
/* Advances [i] so it points to the next memory block (if any).
    i->addr becomes NULL if there is no next memory block.

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error. 
*/

#endif
