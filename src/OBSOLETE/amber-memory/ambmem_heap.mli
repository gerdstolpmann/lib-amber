(* $Id$ *)

exception Library_error


type heap_config =
    { start : nativeint;
      increment : nativeint;
    }

type heap
  (** The type of ambmem-managed memory *)

type heap_flags =
    Heap_init
  | Heap_rdonly
  | Heap_rdwr

val create : heap_config -> heap
  (** Creates a new anonymous memory heap. *)

val map_file : Unix.file_descr -> heap_config -> heap_flags list -> heap
  (** Maps a file as heap. If read/write, the descriptor must remain open
      as long as the heap is extended.
   *)

val copy : ?copy_bigarrays:bool -> heap -> 'a -> 'a
  (** Copies a value into the heap. The returned value is a heap member.

      - [copy_bigarrays]: If true, bigarray contents are also copied to
        the heap

      Caveats: polymorphic comparison, and the polymorphic hash function
      do not work for copied values. E.g.
      - [ [| |] <> copy h [| |] ] - workaround: test [Array.length] instead
      - [ Hashtbl.hash "foo" <> Hashtbl.hash (copy h "foo") ] -
        workaround: use non-polymorphic hash function
   *)

val mem : 'a -> heap -> bool
  (** Returns whether the value is a heap member *)

val keytab_keys : heap -> string list
  (** Returns the named heap values (keytab) *)

val keytab_lookup : heap -> string -> 'a
  (** Looks up a key (or raises Not_found) *)

val keytab_add : heap -> string -> 'a -> unit
  (** Adds a value to the keytab *)

val keytab_del : heap -> string -> unit
  (** Deletes a value from the keytab *)

val fnv1_hash : string -> int
  (** computes the FNV-1 hash for a string argument (the two most significant
      bits, however, are lost when the hash value is converted to an int).

      The returned int is non-negative.
   *)
