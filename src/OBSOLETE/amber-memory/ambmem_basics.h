/* $Id$ */

#ifndef AMBMEM_BASICS
#define AMBMEM_BASICS

#include "caml/alloc.h"
#include "caml/bigarray.h"
#include "caml/callback.h"
#include "caml/custom.h"
#include "caml/fail.h"
#include "caml/memory.h"
#include "caml/misc.h"
#include "caml/mlvalues.h"
#include "caml/unixsupport.h"

#include <assert.h>

#define DEBUG

#ifdef DEBUG
#include <stdio.h>
#define enter_function(name) fprintf(stderr, "Entering %s\n", name)
#else
#define enter_function(name)
#endif

#endif
