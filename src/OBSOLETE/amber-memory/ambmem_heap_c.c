/* $Id$ */

#include "ambmem_basics.h"
#include "ambmem_mmap.h"
#include "ambmem_file.h"
#include "ambmem_htab.h"
#include <string.h>
#include <assert.h>

struct heap {
    struct mapping *m;
    struct file_header *h;
};

#define heapptr_val(v) ((struct heap *) Data_custom_val(v))
/* heap_val(v): returns the "struct heap *" pointing to the part of the
   O'Caml value v where struct heap is stored
*/


extern int caml_ba_element_size[];

/* Missing O'Caml defines. We finally need Is_in_heap_or_young. The 
   organization of O'Caml's page table changed in 3.11, so we have two
   versions.
 */
#if 0
#ifdef OCAML_GE_311
/**********************************************************************/
/* Version as of O'Caml 3.11                                          */
/**********************************************************************/
#define In_heap 1
#define In_young 2
#ifdef ARCH_SIXTYFOUR
int caml_page_table_lookup(void * addr);
#define Classify_addr(a) (caml_page_table_lookup((void *)(a)))
#else
#define Pagetable2_log 11
#define Pagetable2_size (1 << Pagetable2_log)
#define Pagetable1_log (Page_log + Pagetable2_log)
#define Pagetable1_size (1 << (32 - Pagetable1_log))
CAMLextern unsigned char * caml_page_table[Pagetable1_size];
#define Pagetable_index1(a) (((uintnat)(a)) >> Pagetable1_log)
#define Pagetable_index2(a) \
  ((((uintnat)(a)) >> Page_log) & (Pagetable2_size - 1))
#define Classify_addr(a) \
  caml_page_table[Pagetable_index1(a)][Pagetable_index2(a)]
#endif
#define Is_in_heap_or_young(a) (Classify_addr(a) & (In_heap | In_young))
#else
/**********************************************************************/
/* Version up to O'Caml 3.10                                          */
/**********************************************************************/
/* from minor_gc.h */
#define Is_young(val) \
  (Assert (Is_block (val)), \
   (addr)(val) < (addr)caml_young_end && (addr)(val) > (addr)caml_young_start)
/* from major_gc.h */
#ifdef __alpha
typedef int page_table_entry;
#else
typedef char page_table_entry;
#endif
CAMLextern char *caml_heap_start;
CAMLextern char *caml_heap_end;
CAMLextern page_table_entry *caml_page_table;
#define Page(p) ((uintnat) (p) >> Page_log)
#define Is_in_heap(p) \
  (Assert (Is_block ((value) (p))), \
   (addr)(p) >= (addr)caml_heap_start && (addr)(p) < (addr)caml_heap_end \
   && caml_page_table [Page (p)])
/* ours */
#define Is_in_heap_or_young(val) (Is_young(val) || Is_in_heap(val))
#endif
#endif

/**********************************************************************/
/* Copy algorithm                                                     */
/**********************************************************************/

/* We maintain a mapping of orignal addresses to copied addresses in a
   table t. When we reach a new O'Caml block, we check whether the
   address is already in t - if so, it is already copied. If it is not
   in t, we first create an uninitialized copy of the block, and put
   the addresses into t. Second, we scan the O'Caml block for included
   addresses to other O'Caml blocks. These addresses are copied by
   recursive invocation of the copy function. In a third step, the
   O'Caml blocked is again scanned. Now it can be assumed that the
   included addresses are already in t. We can look them up, and copy
   the block while mapping the addresses.

   ambmem_copy must only be invoked for heap-allocated values v.
*/

int ambmem_copy(struct mapping *m,
		struct file_header *h,
		struct htab *t,
		value orig,
		int enable_bigarrays)
{
    void *orig_addr;
    void *copy_addr;
    int code, orig_tag;
    char *header;
    char *copy_header;
    mlsize_t size_words, i;
    size_t size_bytes;
    value copy;

    enter_function("ambmem_copy");
    orig_addr = (void *) orig;
    code = ambmem_htab_lookup(t, orig_addr, &copy_addr);
    if (code < 0) return code;

    if (copy_addr == NULL) {
	orig_tag = Tag_val(orig);
	header = Hp_val (orig);
	if (orig_tag < No_scan_tag) {
	    switch (orig_tag) {
	    case Object_tag:
	    case Closure_tag:
	    case Lazy_tag:
	    case Forward_tag:
		return (-2);   /* unsupported */
	    };

	    size_words = Wosize_hp(header);
#ifdef DEBUG
	    fprintf(stderr, "size_words=%ld\n", size_words);
#endif
	    
	    /* Step 1: Create the copy block */
	    size_bytes = Bhsize_hp (header);
	    code = ambmem_alloc(m, h, size_bytes, 1, (void **) (&copy_header));
	    if (code < 0) return code;

	    *((value *) copy_header) = *((value *) header);
	    copy = Val_hp(copy_header);
	    copy_addr = (void *) copy;

	    code = ambmem_htab_add(t, orig_addr, copy_addr);
	    if (code < 0) return code;

#ifdef DEBUG
	    fprintf(stderr, "Orig header: %lx | Copy header: %lx\n",
		    *((unsigned long *) header),
		    *((unsigned long *) copy_header));
	    fprintf(stderr, "Add mapping: %lx -> %lx\n",
		    (unsigned long) orig_addr,
		    (unsigned long) copy_addr);
#endif

	    /* Step 2: Scan and recurse */

	    /* Note. We use the simple test Is_atom to check whether to
               traverse a value or whether to skip it. The GC uses a 
               different test: Is_in_heap_or_young. However, we don't want
               any pointers to non-O'Caml blocks anyway, so there is no need to
               handle such pointers differently (ok, the copy routine 
               may crash).

	       Is_atom expands to different code for the bytecode and the
	       native version of the runtime!
	    */

	    for (i=0; i < size_words; ++i) {
		value field = Field(orig, i);
		if (Is_block (field)) {
		    ambmem_copy(m, h, t, field, enable_bigarrays);
		}
	    }

	    /* Step 3: Scan and look up copied addresses */

	    for (i=0; i < size_words; ++i) {
		void *field_addr;
		void *field_copy_addr;
		value field_copy;
		value field;
		field = Field(orig, i);
		if (Is_block (field)) {
		    field_addr = (void *) field;
		    code = ambmem_htab_lookup(t, field_addr, &field_copy_addr);
		    if (field_copy_addr == NULL)
			return (-2);
		    field_copy = (value) field_copy_addr;
#ifdef DEBUG
		    fprintf(stderr, "For field #%d: map %lx -> %lx\n",
			    (int) i, 
			    (unsigned long) field_addr, 
			    (unsigned long) field_copy_addr);
#endif
		    Field(copy, i) = field_copy;
		}
		else
		    Field(copy, i) = field;
	    }

	}
	else {
	    int do_copy = 0;
	    int do_bigarray = 0;
	    /* Check for bigarrays and other custom blocks */
	    switch (orig_tag) {
	    case Abstract_tag:
		return(-2);
	    case String_tag:
		do_copy = 1; break;
	    case Double_tag:
		do_copy = 1; break;
	    case Double_array_tag:
		do_copy = 1; break;
	    case Custom_tag: 
		{
		    struct custom_operations *custom_ops;
		    char *id;
		    custom_ops = Custom_ops_val(orig);
		    id = custom_ops->identifier;
		    if (id[0] == '_') {
			switch (id[1]) {
			case 'b':
			    if (strcmp(id, "_bigarray") == 0) {
				if (enable_bigarrays) {
				    struct caml_ba_array *b;
				    int m;
				    b = Bigarray_val(orig);
				    m = b->flags & CAML_BA_MANAGED_MASK;
				    /* Only non-mapped O'Caml-managed arrays: */
				    if (m != CAML_BA_MANAGED)
					return (-2);
				    /* There must not be any proxies, i.e. this
				       bigarray must be neither a subarray, or
				       a superarray
				    */
				    if (b->proxy != NULL) 
					return (-2);
				    do_copy = 1;
				    do_bigarray = 1;
				}
				else
				    return (-2);
			    };
			    break;
			case 'i': /* int32 */
			case 'j': /* int64 */
			case 'n': /* nativeint */
			    if (id[2] == 0) {
				do_copy = 1;
				break;
			    }
			default:
			    return (-2);
			}
		    }
		    else
			return (-2);
		}
	    }

	    if (do_copy) {
		/* Simply copy the block as-is */
		size_bytes = Bhsize_hp (header);
		code = ambmem_alloc(m, h, size_bytes, 1, (void **) &copy_header);
		if (code < 0) return code;
		memcpy(copy_header, header, size_bytes);

		copy = Val_hp(copy_header);
		copy_addr = (void *) copy;

		code = ambmem_htab_add(t, orig_addr, copy_addr);
		if (code < 0) return code;

#ifdef DEBUG
	    fprintf(stderr, "Add mapping: %lx -> %lx\n",
		    (unsigned long) orig_addr,
		    (unsigned long) copy_addr);
#endif
	    }

	    if (do_bigarray) {
		/* postprocessing for copying bigarrays */
		struct caml_ba_array *b_orig, *b_copy;
		void * data_copy;
		size_t size = 1;
		b_orig = Bigarray_val(orig);
		b_copy = Bigarray_val(copy);
		for (i = 0; i < b_orig->num_dims; i++) {
		    size = size * b_orig->dim[i];
		};
		size = 
		    size * caml_ba_element_size[b_orig->flags & BIGARRAY_KIND_MASK];

		code = ambmem_alloc(m, h, size, 0, (void **) &data_copy);
		if (code < 0) return code;

		memcpy(data_copy, b_orig->data, size);
		b_copy->data = data_copy;

#ifdef DEBUG
		fprintf(stderr, "Bigarray copy: %lx -> %lx\n",
			(unsigned long) b_orig->data,
			(unsigned long) data_copy);
#endif
	    }
	}
    }

    return 0;
}

/**********************************************************************/
/* Stubs                                                              */
/**********************************************************************/

static void report_error(int code)
{
    /* raise the right exception */
    switch (code) {
    case -1:
	uerror("Ambmem_heap", Nothing);
	assert(0);
    case -2:
	caml_raise_constant(*caml_named_value("Ambmem_heap.Library_error"));
	assert(0);
    default:
	assert(0);
    }
}


CAMLprim value ambmem_copy_ml(value heap_val, 
			      value start_val, 
			      value copy_bigarrays_val)
{
    struct mapping *m;
    struct file_header *h;
    struct htab t;
    int copy_bigarrays;
    int code;
    void *start_addr;
    void *copy_addr;

    /* NO allocation of O'Caml values must happen here! */

    enter_function("ambmem_copy_ml");
    m = heapptr_val(heap_val)->m;
    h = heapptr_val(heap_val)->h;
    copy_bigarrays = Int_val(copy_bigarrays_val);

    if (!m->is_rw) report_error(-2);

    if (Is_block(start_val)) {
	code = ambmem_htab_init(&t, 1009);
	if (code < 0) report_error(code);
	
	code = ambmem_copy(m, h, &t, start_val, copy_bigarrays);
	if (code < 0) {
	    ambmem_htab_free(&t);
	    report_error(code);
	};
	
	start_addr = (void *) start_val;
	code = ambmem_htab_lookup(&t, start_addr, &copy_addr);
	if (code < 0) {
	    ambmem_htab_free(&t);
	    report_error(code);
	}
	if (copy_addr == NULL) {
	    ambmem_htab_free(&t);
	    report_error(-2);
	}

	ambmem_htab_free(&t);

	return (value) copy_addr;
    }
    else {
	return start_val;   /* nonsense */
    }
}

static struct custom_operations heap_ops = {
  "http://gerd-stolpmann.de/amber-memory/heap",
  custom_finalize_default,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

CAMLprim value ambmem_create_ml(value heapstart_val, value incr_val)
{
    struct heap p;
    void *heapstart;
    size_t incr;
    int code;
    value p_val;

    enter_function("ambmem_create_ml");
    heapstart = (void *) Nativeint_val(heapstart_val);
    incr = Nativeint_val(incr_val);

    p.m = stat_alloc(sizeof(struct mapping));

    code = ambmem_map_anon(p.m, heapstart, incr);
    if (code < 0) {
	stat_free(p.m);
	report_error(code);
    };

    code = ambmem_file_init(p.m, &(p.h));
    if (code < 0) {
	stat_free(p.m);
	report_error(code);
    };

    p_val = caml_alloc_custom(&heap_ops, sizeof(struct heap), 0, 1);
    memcpy(heapptr_val(p_val), &p, sizeof(struct heap));

    return p_val;
}

CAMLprim value ambmem_init_file_ml(value fd_val, value heapstart_val, 
				   value incr_val)
{
    int fd, code;
    void *heapstart;
    size_t incr;
    struct heap p;
    value p_val;

    enter_function("ambmem_init_file_ml");
    fd = Int_val(fd_val);
    heapstart = (void *) Nativeint_val(heapstart_val);
    incr = Nativeint_val(incr_val);

    code = ftruncate(fd, sysconf(_SC_PAGESIZE));
    if (code == -1) report_error(code);

    p.m = stat_alloc(sizeof(struct mapping));

    code = ambmem_map_file(p.m, fd, heapstart, incr, 1);
    if (code < 0) {
	stat_free(p.m);
	report_error(code);
    };

    p.m->length = 0;
    code = ambmem_file_init(p.m, &(p.h));
    if (code < 0) {
	stat_free(p.m);
	report_error(code);
    };

    p_val = caml_alloc_custom(&heap_ops, sizeof(struct heap), 0, 1);
    memcpy(heapptr_val(p_val), &p, sizeof(struct heap));

    return p_val;
}


CAMLprim value ambmem_open_file_ml(value fd_val, value rw_val, value incr_val)
{
    int fd, rw, code;
    void *heapstart;
    size_t incr;
    struct heap p;
    value p_val;

    enter_function("ambmem_open_file_ml");
    fd = Int_val(fd_val);
    rw = Bool_val(rw_val);
    incr = Nativeint_val(incr_val);

    code = ambmem_file_inspect(fd, &heapstart);
    if (code < 0) report_error(code);
    
    p.m = stat_alloc(sizeof(struct mapping));


    code = ambmem_map_file(p.m, fd, heapstart, incr, rw);
    if (code < 0) {
	stat_free(p.m);
	report_error(code);
    };

    code = ambmem_file_open(p.m, &(p.h));
    if (code < 0) {
	stat_free(p.m);
	report_error(code);
    };

    p_val = caml_alloc_custom(&heap_ops, sizeof(struct heap), 0, 1);
    memcpy(heapptr_val(p_val), &p, sizeof(struct heap));

    return p_val;
}



CAMLprim value ambmem_member_ml(value heap_val, value v)
{
    struct mapping *m;
    struct file_header *h;
    
    void *mem_start;
    void *mem_end;
    void *addr;

    enter_function("ambmem_member_ml");
    m = heapptr_val(heap_val)->m;
    h = heapptr_val(heap_val)->h;

    mem_start = (void *) h;
    mem_end = (void *) (h + h->total_length);
    addr = (void *) v;

    return Val_bool(Is_block(v) && addr >= mem_start && addr < mem_end);
}

CAMLprim value ambmem_keytab_string_ml(value heap_val)
{
    struct mapping *m;
    struct file_header *h;
    void *keytabstart;
    int code;

    enter_function("ambmem_keytab_string_ml");
    m = heapptr_val(heap_val)->m;
    h = heapptr_val(heap_val)->h;

    code = ambmem_keytab_region(h, &keytabstart);
    if (code < 0) report_error(code);
    
    return (value) (((value *) keytabstart) + 1);
}


CAMLprim value ambmem_address_ml(value val)
{
    value r;

    /* Works only! for vals that cannot move! */
    enter_function("ambmem_address_ml");
    r = caml_copy_nativeint(val);
    return r;
}


CAMLprim value ambmem_reference_ml(value val)
{
    long r;

    /* Works only! for vals that cannot move! */
    enter_function("ambmem_reference_ml");
    r = Nativeint_val(val);
    return r;
}


CAMLprim value ambmem_natint_of_string_ml(value s)
{
    long r;

    r = *((long *) String_val(s));
    return caml_copy_nativeint(r);
}


CAMLprim value ambmem_string_of_natint_ml(value val)
{
    long n;
    value s;

    n = Nativeint_val(val);
    s = caml_alloc_string(sizeof(long));
    *((long *) String_val(s)) = n;

    return s;
}


CAMLprim value ambmem_fnv1_hash_ml(value val)
{
    char *s;
    long l;
    unsigned long h;

    s = String_val(val);
    l = string_length(val);
    h = ambmem_htab_hash(s, l);
#ifdef ARCH_SIXTYFOUR
    h &= 0x3fffffffffffffff;
#else
    h &= 0x3fffffff;
#endif
    return Val_long( (long) h );
}
