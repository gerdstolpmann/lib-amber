(* $Id$ *)

exception Library_error


type heap_config =
    { start : nativeint;
      increment : nativeint;
    }

type heap
  (** The type of ambmem-managed memory *)

type heap_flags =
    Heap_init
  | Heap_rdonly
  | Heap_rdwr


external ambmem_create : nativeint -> nativeint -> heap
  = "ambmem_create_ml"
external ambmem_init_file : Unix.file_descr -> nativeint -> nativeint -> heap
  = "ambmem_init_file_ml"
external ambmem_open_file : Unix.file_descr -> bool -> nativeint -> heap
  = "ambmem_open_file_ml"
external ambmem_copy : heap -> 'a -> bool -> 'a
  = "ambmem_copy_ml"
external ambmem_member : heap -> 'a -> bool
  = "ambmem_member_ml"
external ambmem_keytab_string : heap -> string
  = "ambmem_keytab_string_ml"
external ambmem_address : 'a -> nativeint
  = "ambmem_address_ml"
external ambmem_reference : nativeint -> 'a
  = "ambmem_reference_ml"
external ambmem_natint_of_string : string -> nativeint
  = "ambmem_natint_of_string_ml"
external ambmem_string_of_natint : nativeint -> string
  = "ambmem_string_of_natint_ml"
external fnv1_hash : string -> int
  = "ambmem_fnv1_hash_ml"


let create config =
  let heap = ambmem_create config.start config.increment in
  let ktab = ambmem_keytab_string heap in
  ktab.[0] <- '\000';
  heap

let map_file fd config flags =
  let have_init = List.mem Heap_init flags in
  let have_rdonly = List.mem Heap_rdonly flags in
  let have_rdwr = List.mem Heap_rdwr flags in
  
  if (have_rdonly && have_rdwr) || (not have_rdonly && not have_rdwr) then
    invalid_arg "Ambmem_heap.map_file";

  if have_init && not have_rdwr then
    invalid_arg "Ambmem_heap.map_file";

  if have_init then (
    
    let heap = ambmem_init_file fd config.start config.increment in
    let ktab = ambmem_keytab_string heap in
    ktab.[0] <- '\000';
    heap
  )
  else (
    ambmem_open_file fd have_rdwr config.increment 
  )


let copy ?(copy_bigarrays=false) heap v =
  ambmem_copy heap v copy_bigarrays

let mem v heap =
  ambmem_member heap v

let keytab_get heap : (string * nativeint) list =
  let ktab_s = ambmem_keytab_string heap in
  let n = Char.code ktab_s.[0] in
  let k = ref 1 in
  let l = ref [] in
  let b = Sys.word_size / 8 in
  for i = 1 to n do
    let k0 = !k in
    while ktab_s.[ !k ] <> '\000' do incr k done;
    let key = String.sub ktab_s k0 (!k-k0) in
    incr k;
    let ptr_s = String.sub ktab_s !k b in
    k := !k + b;
    let ptr = ambmem_natint_of_string ptr_s in
    l := (key,ptr) :: !l
  done;
  List.rev !l

let keytab_set heap l =
  let ktab_s_orig = ambmem_keytab_string heap in
  let ktab_s = String.make (String.length ktab_s_orig) '\000' in
  try
    let n = List.length l in
    ktab_s.[0] <- Char.chr n;
    let k = ref 1 in
    let b = Sys.word_size / 8 in
    let l = ref l in
    for i = 1 to n do
      let (key,ptr) = List.hd !l in
      l := List.tl !l;
      String.blit key 0 ktab_s !k (String.length key);
      k := !k + String.length key;
      ktab_s.[ !k ] <-'\000';
      incr k;
      let ptr_s = ambmem_string_of_natint ptr in
      String.blit ptr_s 0 ktab_s !k b;
      k := !k + b;
    done;
    String.blit ktab_s 0 ktab_s_orig 0 (String.length ktab_s)
  with
    | _ ->
	failwith "Ambmem_heap: key table is full"

let keytab_keys heap =
  List.map fst (keytab_get heap)

let keytab_lookup heap key =
  let l = keytab_get heap in
  let ptr = List.assoc key l in
  ambmem_reference ptr

let keytab_add heap key v =
  let l = keytab_get heap in
  let l' = 
    (key, ambmem_address v) :: (List.filter (fun (k,_) -> k <> key) l) in
  keytab_set heap l'

let keytab_del heap key =
  let l = keytab_get heap in
  let l' = List.filter (fun (k,_) -> k <> key) l in
  keytab_set heap l'

let () =
  Callback.register_exception "Ambmem_heap.Library_error" Library_error
