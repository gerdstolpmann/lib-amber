/* $Id$ */

/* Amber memory mapping management */

#ifndef AMBMEM_MMAP_H
#define AMBMEM_MMAP_H

#include <stdlib.h>

struct mapping {
    char    *start;
    size_t   mmap_length;
    size_t   length;
    int      is_rw;
    int      is_anonymous;
    int      fd;
    size_t   increment;
};

extern int ambmem_map_file(struct mapping *m, int fd, void *start, 
			   size_t increment, int rw);
/* Maps the file [fd] into memory at the fixed address [start]. If [rw != 0]
   the file is mapped read/write, otherwise read-only.

   It is an error if the file length is not a multiple of the page size.

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error. On success, the structure [m] is initialized.
*/

extern int ambmem_map_anon(struct mapping *m, void *start,
			   size_t increment
			   );
/* Creates an anonymous shared mapping at the fixed address [start].
   The mapping is always read/write. Initially, the mapping is empty
   (zero length).

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error. On success, the structure [m] is initialized.
*/

extern int ambmem_map_extend(struct mapping *m, size_t new_length);
/* Extend the mapping [m] so it has at least the total [new_length]. 
   If [new_length] is not a multiple of the page size, the number
   of bytes are rounded up to the next page size multiple.

   It is an error to shrink the mapping.

   m->fd must still be open.

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error.
*/

extern int ambmem_unmap(struct mapping *m);
/* Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error.

   Dangerous!
*/

#endif
