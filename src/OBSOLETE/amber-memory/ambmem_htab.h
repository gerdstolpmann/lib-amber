/* $Id$ */

/* Amber memory hash table */

/* This table maps original addresses to relocated addresses */

#ifndef AMBMEM_HTAB
#define AMBMEM_HTAB

#include "ambmem_basics.h"

struct htab_cell {
    void *orig_addr;
    void *relo_addr;
    /* Both are NULL if the cell is unused. */
};

struct htab {
    struct htab_cell *table;
    unsigned long     table_size;
    unsigned long     table_used;
};

extern int ambmem_htab_init(struct htab *t, unsigned long n);
/* Initializes the htab t for n cells

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error. On success, the structure [t] is initialized.
 */

extern int ambmem_htab_add(struct htab *t, 
			   void *orig_addr, void *relo_addr);
/* Adds the mapping from orig_addr to relo_addr to t.

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error.
 */

extern int ambmem_htab_lookup(struct htab *t, 
			      void *orig_addr, void **relo_addr);
/* Looks up orig_addr in t, and returns the corresponding relo_addr
   (or NULL if not found).

   Return 0 on success, or (-1) on system error (errno), or (-2) on
   library error.
 */

extern void ambmem_htab_free(struct htab *t);
/* Frees the memory allocated by t */

extern unsigned long ambmem_htab_hash(char *s, long n);
/* Computes the hash value of the buffer s which is n bytes long. */


#endif

