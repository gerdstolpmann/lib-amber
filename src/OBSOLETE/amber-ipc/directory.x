/* $Id$ -*- c -*- */

#ifndef DIRECTORY_X
#define DIRECTORY_X

#include "common.x"

#ifdef INTERNAL_API
struct jseqdb_info {
    jseqdb_name jsdbi_name;   /* This struct is about this jseqdb */
    bool        jsdbi_del;    /* Delete flag. The jseqdb will go soon away */
    hyper       jsdbi_ts;     /* timestamp of last commit */
    jseqdb_vers jsdbi_vers;   /* version after last commit */
    hyper       jsdbi_grace;
    /* Until this timestamp the jseqdb must not be used. Such grace times
       are set at server startup, and after jseqdb creation/before deletion.
    */
    bool        jsdbi_sync;
    /* whether this jseqdb is being synchronized from another host */
};

typedef jseqdb_info jseqdb_info_list<>;

struct jseqdb_detail {
    jseqdb_info jsdbd_info;     /* the above data */
    hyper       jsdbd_ts<16>;   /* timestamps of last N commits */
    jseqdb_vers jsdbd_vers<16>; /* versions after last N commits */
};

typedef jseqdb_detail *jseqdb_detail_opt;

program Directory {
    version V1 {
	void null(void) = 0;

	jseqdb_info_list get_jseqdb_list(node_domain) = 1;
	/* Get a list of jseqdb's and the most recent version string.
           This is not an atomic operation, and the data for the jseqdb's
           are collected one after the other.

	   SYSTEM_ERR if the node_domain is wrong.
	 */

	jseqdb_detail_opt get_jseqdb_detail(node_domain, jseqdb_name) = 2;
	/* Get details about a certain jseqdb. Will return NULL if this
           name is unknown.

	   SYSTEM_ERR if the node_domain is wrong.
	 */

    } = 1;
} = 36000;
#endif

#endif

