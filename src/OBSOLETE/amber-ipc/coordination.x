/* $Id$ -*- c -*- */

/* Coordination protocol

   This is about finding and electing the coordinator for a certain
   jseqdb and all its replicas. The coordinator determines which of
   the replicas are up to date, and thus qualify as a source for read
   accesses.  Also, the coordinator manages writes to the replicas,
   and ensures that updates are applied to all replicas in the same
   order.

   The node working as coordinator for a certain jseqdb must itself
   have a replica of the jseqdb. It is not required, however, that this
   replica is up to date (i.e. the coordinator can exclude its own
   node from read or write accesses).

   The election of the coordinator bases on a "dignity value". The
   node with the highest dignity value wins. The dignity values
   depend on the node identity and the name of the jseqdb, but not on
   random data. Because of this, the outcome of the election is
   relatively deterministic, and the only non-predictible influence is
   which nodes are actually up and running.

   It is nevertheless required that all participating nodes share a
   common view, and agree upon the coordinator. A chain letter is used
   to ensure this. The program Coord_election is the protocol used for
   electing the coordinator using a chain letter, RPC's of this program
   are only called by candidates for coordination.

   The outer interface to the clients of the coordinator is the
   program Coordinator. The clients can query here which jseqdb are ok
   and which should be left out for data accesses. In order to decide
   this, the Coordinator uses information from the Directory program,
   and from the system jseqdb. The Directory returns for every jseqdb
   the list of recent versions (with MD5 sums and timestamps). By
   comparing the lists gathered from all replicas it can be found out
   which replicas missed an update. The system jseqdb contains
   configuration data for each jseqdb, such as on which node replicas
   exist, and whether the replicas are enabled or disabled. The
   coordinator is conservative in its decision, and only replicas
   qualify that meet all minimum criteria.

   Note that the system jseqdb is accessed without having the extra
   information from the system jseqdb.

*/

/**********************************************************************/
/* The election protocol                                              */
/**********************************************************************/

/* The nodes are arranged as a ring: When n[1],...,n[N] are the nodes,
   then 

   k is the successor of j if k=j+1 or (k=1 && n=N)

   This is achieved by ordering the nodes by IP addresses. During the
   election new nodes can be added to the ring, and nodes can be
   removed.  Thus, the successor is also a function of which nodes are
   up and running.

   The idea is now that there is a letter in the ring, and that this
   letter is passed on to the successor by each node, so that after
   short time all ring members have seen the letter, and have had the
   opportunity to modify it. There is some special functionality for
   ensuring the conditions

   (1) that there must not be two or more letters in the ring
   (2) that there must be at least one letter in the ring

   The protocol does not ensure that (1) and (2) are met all the time.
   However, it ensures that the time periods are short where (1) and
   (2) are violated.

   There is only one letter containing data for all jseqdb's. However,
   the election is done for each jseqdb separately. The idea is that
   several nodes should have the chance to take over the coordinator
   role when there are several jseqdb's (load balancing).

   Assumed all nodes are up and running, the algorithm works as
   follows.  When a node receives a letter, it checks all jseqdb's for
   which it has replicas. If the node is not already entered into the
   letter for these jseqdb's, the letter is modified, and the node is
   added to the lists for these jseqdb's. The modified letter is then
   passed on to the successor node. Also, the node removes itself when
   it no longer has a replica for a jseqdb. If no modification is
   required, the unmodified letter is passed on to the successor.
   The letter is handed to each memeber of the ring in turn, and
   after one round the iteration starts over with the first ring
   element, i.e. the letter passing is done infinitely.

   The node lists also contain the dignity values for each combination
   of jseqdb name and replica node. The node with the highest dignity
   value wins the election. A node can assume that it has won if it
   gets the letter in two consecutive rounds of letter passing, and
   the list of replicas for a jseqdb is the same in both rounds, and
   the dignity values are the same, and the node has the highest
   dignity value. Also, we require that the letter version must be the
   same (i.e. no creation or duplication in the meantime).

   In theory it is possible that the election never finishes, because
   nodes are constantly added or removed, or replicas are constantly
   created or destroyed. The problem is ignored, as it has little or
   no practical relevance.

   Letters can be spontaneously created by nodes, and they can be
   duplicated (on the assumption the original letter is dead, i.e.  no
   longer passed on). In order to distinguish between versions of the
   letter there is a version number of the letter which is a
   timestamp. The timestamp is only refreshed on creation and
   duplication, but not on a simple pass-on event.

   In order to ensure (1), every node announces publicly that it has
   got the letter when its pass_letter RPC is invoked by the
   predecessor node. The announcement is done by multicasting
   announce_letter to all nodes. All nodes listen to such
   announcements, and thus know which letters exist in the ring, and
   which versions these have, and which is the latest existing
   version. When a node gets an outdated letter it is immediately
   dropped.

   In order to ensure (2), there are two conditions for creation and
   duplication:

   - A letter is duplicated by a node when the pass_letter RPC
     times out after t1 seconds (i.e. no reply). The failing node
     is skipped, i.e. the duplicate is sent to the successor of
     the successor. t1 is a small multiple of t0 = the time after
     which the latter is passed on (1/freq).

   - A letter is created by a node when there is no announce_letter
     for t2 seconds. t2 must be bigger than t1 (better t2 > 2*t1), and
     for avoiding duplicates t2 should contain some randomness.

   After creation or duplication there is some likeliness that two
   nodes have sent out letters. One of these letters will be quickly
   dropped, but for some time two letters exist. For this reason,
   there is a flag cel_first_round which is true during the first
   round of letter passing, and which prevents the election.

   If only a single node fails, letter duplication will get into
   effect. Only in the more unlikely case of a multi-node failure
   letter creation is required. Of course, at system startup the
   letter also needs to be created.

   Practical values for t0

   e.g. t0=0.1s for the first rounds after system startup, and after
   nodes are added/removed. Otherwise t0=10s seems to be sufficient.


*/


#include "common.x"

#ifdef INTERNAL_API
struct cel_jseqdb {
    jseqdb_name cel_jseqdb_name;
    /* The name of the jseqdb this struct is about */

    node_name   cel_jseqdb_replicas<>;
    /* Replicas exist on these nodes. This array must only contain nodes
       that are up and running according to the node presence protocol.
     */

    hyper       cel_jseqdb_dignity0<>;
    /* This array has the same number of elements as cel_jseqdb_replicas.
       For each replica, the MSB part of the dignity value is given here.
    */

    hyper       cel_jseqdb_dignity1<>;
    /* This array has the same number of elements as cel_jseqdb_replicas.
       For each replica, the LSB part of the dignity value is given here.
    */

    hyper       cel_grace;
    /* This is the max of all jsdbi_grace timestamps (see directory protocol).
       When this time is reached, the election takes into effect. Before,
       the election is only preliminary.
    */

    node_name   *cel_jseqdb_coord;
    /* This is set to non-NULL when the election takes into effect. This
       is always done by the elected coordinator. Nodes must check whether
       the coordinator is still alive before they use this node name.

       It is set to NULL at startup. It is never set to NULL again.
    */
};


struct coord_election_letter {
    hyper cel_version0;
    /* The MSB part of the version number. This is the timestamp of
       the last creation or duplication of the letter. The timestamp
       is _not_ updated when the letter is just handed over to the
       next node.
    */

    hyper cel_version1;
    /* The LSB part of the version number, a random number. Used to
       enforce an ordering between letters with the same timestamp.
    */

    bool cel_first_round;
    /* True during the first round of letter passing. It is set to
       true by the creator or duplicator, and set to false by any node
       that sees this version the second time. In the first round it
       is more likely that there are several versions of the letter
       underway, and for stability there is no election in the first
       round.
    */

    cel_jseqdb cel_jseqdb<>;
    /* The array of jseqdb's and attached election data */
};


program Coord_election {
    version V1 {
	void null(void) = 0;

	void pass_letter(node_domain, coord_election_letter) = 1;
	/* Passes the letter to the successor. The successor must reply!
	 */
    }  = 1;
} = 37000;


program Coord_election_mcast {
    version V1 {

	void announce_letter(node_domain, hyper, hyper) = 1;
	/* Announces that the sender of this message has just got the letter
	   via pass_letter. The two hyper's are cel_version0 and cel_version1.

	   Listeners must ignore messages coming from a foreign domain.
	*/
    } = 1;
} = 37001;

#endif

/**********************************************************************/
/* The query protocol                                                 */
/**********************************************************************/

/*** return enum ***/

enum coord_enum {
    C_OK = 0,
    C_COORD_ERROR = 1
};

/*** return type of find_coord ***/

union r_find_coord switch (coord_enum d) {
 case C_OK:
     node_name n;
 case C_COORD_ERROR:
     coord_error e;
};


/*** return type of get_qualifying_replicas ***/

union r_get_qualifying_replicas switch (coord_enum d) {
 case C_OK:
     /* The array with the good replicas: */
     node_name replicas<>;
 case C_COORD_ERROR:
     coord_error e;
};



program Coordinator {
    version V1 {
	void null(void) = 0;

	r_find_coord 
	    find_coord(node_domain, jseqdb_name) = 1;
	/* Find the coordinator for this jseqdb. Returns COORD_ERROR when
           the coordinator cannot be determined. Otherwise OK with the
	   node name
	*/

	r_get_qualifying_replicas
	    get_qualifying_replicas(node_domain, jseqdb_name) = 2;
	/* Find the replicas that qualify for data access. This query
	   can be directed to all nodes, and is automatically
	   forwarded to the coordinator. Returns COORD_ERROR when the
	   coordinator cannot be determined. Otherwise OK with the
	   node names.

	   It is possible that an empty array of replicas is returned.
	   This is a critical error situation.
	*/
    } = 1;
} = 37002;
