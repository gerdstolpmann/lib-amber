/* $Id$ -*- c -*- */

/* Node presence protocol.

   On each node there is a node presence server. This server sends 
   continuously messages to a multicast address, and listens to such
   messages received from other nodes.

   These messages contain:
     - which nodes are present
     - a hint which services are running on the nodes
    
   As there is a length-limit for these messages, the service list is
   not transmitted directly. Only the version number of the list of
   services is included in the message. When this number changes, the
   list has changed, and listeners are advised to reload the list
   by doing a TCP-based RPC call.

   Servers running on the same node as the node presence server 
   communicate with it as follows. First, it is possible to get a
   list of available nodes and services in the whole cluster.
   Second, a server for a specific service tells the node presence
   server which service it runs so it can be announced in the 
   cluster.

   Terms:

   - NODE NAME: This is the hostname of the node, without any
     domain qualification.

   - DOMAIN: Each cluster is identified by a domain string. A node
     presence server can manage only one domain. It ignores
     messages about foreign domains. The domains it knows are configured
     in a local text file.
*/

#ifndef NODE_PRESENCE_X
#define NODE_PRESENCE_X

#include "common.x"


struct node_announcement {
    node_name node_ann_name;
    /* Usually the hostname. */

    node_domain node_ann_domain;
    /* The domain of the cluster this node wants to be part of. */

    int    node_ann_slist_version;
    /* The version number of the list of services. Can only increase */

    int    node_ann_frequency;
    /* Every how many seconds the announcement is sent out */

    int    node_ann_query_port;
    /* On this TCP port the Node_presence_query program is available */
};


typedef node_announcement node_announcements<>;


typedef node_announcement *node_announcement_opt;

struct node_service {
    node_name node_srv_node_name;
    /* The node name this service runs on */

    string node_srv_name<64>;
    /* The name of the service */

    string node_srv_inst<64>;
    /* The instance identifier - this is used to distinguish between
       several instances of the same service in the cluster. If not used,
       the instance identifier is usually set to the hostname.
    */

    string node_srv_proto<64>;
    /* The protocol name of this entry. For ONCRPC it is just "ONCRPC".
       For ICE it is just "ICE". For HTTP-based protocols it is
       "HTTP_" plus another identifier.
    */

    string node_srv_transport<16>;
    /* The transport protocol: "TCP", "UDP", "UNIXDOMAIN". */

    string node_srv_port<128>;
    /* The port name. Interpretation depends on transport:
       - For TCP and UDP it is the port as decimal number
       - For UNIXDOMAIN it is the path of the socket.
    */

    string node_srv_licinfo<128>;
    /* A string with license information. Mainly used to detect whether
       the same license is used several times in the cluster. If a
       service has several struct node_service, only the first struct
       contains this license string. (The other strings can be empty.)
    */
};


struct node_services {
    node_name node_slist_node_name;
    /* The node name this list refers to */

    int node_slist_version;
    /* The version number of the service list */

    node_service node_slist<>;
    /* The list of services. It is possible to get several records
       for the same service when there are several instances, or several
       protocols/ports.
    */
};


#ifdef INTERNAL_API
struct node_srv_registration {
    string node_reg_name<64>;
    /* The node service to register */

    node_service node_reg<>;
    /* The node service definition is a list of node_service entries for
       the same node_srv_name. This list replaces the previous list for
       this service. By passing an empty list, the service is unregistered.
    */

    hyper node_reg_timeout;
    /* If a positive number: At this time the service is automatically 
       deleted unless a follow-up registration updates the service entry.
       If null or negative no such deletion will happen.
    */

    int node_reg_pid;
    /* If a positive number: The service is automatically deleted when
       the process with this PID vanishes.
       If null or negative no such deletion will happen.
    */
};
#endif


#ifdef INTERNAL_API
program Node_presence_mcast {
    version V1 {
	/* This program is used in the multicast messages */

	void announce_presence(node_announcement) = 1;
	/* This (UDP) message is sent to the multicast group to announce
           the presence of this node. This message is not replied.

	   Listeners must ignore messages coming from a foreign domain.
	*/
    } = 1;
} = 35000;
#endif


program Node_presence_query {
    version V1 {
	/* This program can be reached from any node on the cluster */

	void null(void) = 0;

	node_announcement get_local_announcement(node_domain) = 2;
	/* Get the current local announcement */

	node_announcement_opt get_node_announcement(node_domain, node_name) = 3;
	/* Get the announcement sent out by this node. This can be NULL
           if no such announcement has recently been received.
	*/

	node_announcements get_all_announcements(node_domain) = 4;
	/* Get all recently received announcements (including the local
	   one).
	*/

	node_services get_local_services(node_domain) = 5;
	/* Get the list of services running on the local node. 
	   The returned node_slist_version is the same as in the
           announcement, and refers to the local list.
	 */

	node_services get_node_services(node_domain, node_name) = 6;
	/* Get the list of services running on the passed node. 
	   The returned node_slist_version is that of the 
           remote node. If the node is unknown node_slist is returned 
	   empty.
	 */
    } = 1;
} = 35001;


#ifdef INTERNAL_API
program Node_presence_reg {
    version V1 {
	/* This program is used by locally running servers to register
	   themselves
	*/

	void null(void) = 0;

	void register_service(node_domain, node_srv_registration) = 1;
	/* Register this service. */

    } = 1;
} = 35002;
#endif

#endif
