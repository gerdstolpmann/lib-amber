/* $Id$ -*- c -*- */

/* Transaction protocol

   Transactions have quite special ACID semantics. Atomicity is
   ensured by using a 3-phase commit protocol (Skeen &
   Stonebraker). There is only one consistency condition: All keys are
   unique. This condition is checked at commit time.

   Isolation is solved in a very special way. First, transactions
   do not see their own modifications. Second, transactions see
   changes of committed other transactions. In some sense, we 
   implement READ COMMITTED with the special interpretation that
   even the own transaction needs to be committed before its
   data updates can be seen by reads.

   The advantage of this is that file locks need only to be held for
   very short periods of time. Until COMMIT, all writes are simply
   buffered up by the coordinator, and transmitted to the other
   replica nodes. Only at COMMIT time, the jseqdbs need to be write-
   locked. Reads only require read locks for the duration of reading
   data. The short lock times improve concurrency.

   For doing read-modify-write cycles, the coordinator allows it to
   lock individual rows for the duration of the transaction. The lock
   prevents that any other transaction modifies the row. The other
   transaction gets a lock error in this case - it is not tried to
   wait until the lock is released again.

   Transactions only exist as long as the coordinator node is
   operational and keeps its coordinator role. It is possible that a
   different node takes over the coordinator role during a
   transaction. In this case, the transaction is aborted. For some
   time the whole system lacks then a coordinator, until all nodes
   agree upon a different coordinator. This state is not hidden
   from clients, which must be prepared to wait for some time until
   they can repeat the transaction.

   If the coordinator fails during a transaction, the transaction is
   automatically aborted after some timeout. If the third phase of the
   commit protocol is already entered, it is also possible that the
   transaction is committed, but the client cannot be notified about
   this. (The client will simply run into a timeout.) Applications have
   to work around this, and have to check themselves whether the 
   transaction is committed or not. Note that this case exists anyway,
   because it is also possible that the system breaks during the
   transmission of the reply code of a successful commit.

   Clients can connect to any node in order to request transactions.
   If the node happens not to be the coordinator, the requests are
   automatically forwarded to the coordinator. It is of course better
   when the client directly contacts the coordinator because this
   eliminates a possible point of failure.

 */

/**********************************************************************/
/* The transaction program                                            */
/**********************************************************************/

/* This program is called by clients to start and finish transactions. */

#include "common.x"

#ifdef INTERNAL_API
struct trans_id {
    /* A transaction ID can be treated by clients as an opaque value */
    opaque  trans_id_node[32];  /* name of the coordinator (space-padded) */
    hyper   trans_id_ts;        /* time when the coordinator started up */
    hyper   trans_id_nr;        /* cardinal number */
};
#else
typedef opaque trans_id[48];
#endif


enum trans_enum {
    T_OK = 0,
    T_COORD_ERROR = 1,
    T_PRIV_ERROR = 2,
    T_DISABLED_ERROR = 3,
    T_WRITE_DISABLED_ERROR = 4,
    T_ABORTED = 5,
    T_TRANSACTION_ERROR = 6,
    T_NO_SUCH_TRANSACTION = 7,
    T_CONTINUE_POLLING = 8,
    T_NO_SUCH_TABLE = 9,
    T_LOCK_TIMEOUT = 10,
    T_DEADLOCK = 11,
    T_PROTOCOL_ERROR = 12,
    T_NO_SUCH_KEY = 13,
    T_END_OF_TABLE = 14
};

#define unsupported_case void
/* used in unions for T_* cases that cannot occur */


/*** return type of open_data_channel ***/

struct data_channel {
    string dch_node<64>;  /* The IP addr of the node as dotted quad */
    int    dch_port;      /* The port */
};

union r_open_data_channel switch (trans_enum d) {
 case T_OK:
     /* The data channel can be accessed by connecting to this host
	and port, and by calling the Data_channel program.
     */
     data_channel ch;
 case T_COORD_ERROR:
     coord_error e;
 default:
     unsupported_case;
};


/*** return type of begin_transaction ***/

union r_begin_transaction switch (trans_enum d) {
 case T_OK:
     trans_id trans_id;
     /* The transaction ID is meaningful only while the coordinator
	remains operational. If the coordinator fails, or another
	node takes over coordination, the ID does no longer refer
	to anything. Other coordinators may use the same ID.
     */

 case T_COORD_ERROR:
     coord_error e;

 case T_PRIV_ERROR:
     string s<>;
     /* The user does not have the privilege for accessing this jseqdb.
	The string is a more detailed explanation.
      */

 case T_DISABLED_ERROR:
     string s<>;
     /* Access to this jseqdb is administratively disabled. The string
	explains the reason.
     */

 case T_WRITE_DISABLED_ERROR:
     string s<>;
     /* Write transactions are currently not possible, because some
        administration is being done. The string
	explains the reason.
     */
 default:
     unsupported_case;
};


/*** return type of commit_transaction ***/

enum replica_status_enum {
    /* -- Neutral: -- */
    REPLICA_IGNORED = 0,
    /* The replica has not been contacted, because other replicas have
       already indicated an error status.
    */

    /* -- Integrity errors (lead to abortion): -- */
    REPLICA_UNIQUE_KEY = 1,
    /* Unique key constraint has been violated */
    REPLICA_VALUE_TOO_LONG = 2,
    /* The max of the value length has been exceeded */
    REPLICA_KEY_NOT_FOUND = 3,
    /* A delete cannot be done because the key has not been found */
    REPLICA_ROWID_NOT_FOUND = 4,
    /* A delete cannot be done because the row ID has not been found */

    /* -- other consistency errors (lead to abortion): -- */
    REPLICA_INDEX_FULL = 10,
    /* Too many entries in the index (a reindex run may fix this) */
    REPLICA_TABLE_FULL = 11,
    /* Too many rows in the table (only non-deleted rows count) */
    REPLICA_OUT_OF_SPACE = 12,
    /* Not enough space on disk, or the configured max for the jseqdb is
       reached. (a compaction run may fix this)
    */
    REPLICA_INCONSISTENT = 19,
    /* another consistency problem */

    /* -- system errors that lead to abortion: -- */
    REPLICA_ABORTED = 20,
    /* The replica has aborted for an unknown reason */
    REPLICA_ABORTED_AFTER_TIMEOUT = 21,
    /* The replica cannot be contacted, and it is assumed that it will
       abort the transaction when restarting
    */

    /* -- conditions that lead to commit: -- */
    REPLICA_COMMITTED = 30,
    /* The replica has committed the data */
    REPLICA_COMMITTED_AFTER_TIMEOUT = 31,
    /* The replica cannot be contacted, and it is assumed that it will
       keep the transaction committed when restarting
    */

    /* -- hard errors -- */
    REPLICA_CRITICAL = 40
    /* A critical error occurred during the transaction, and it is unclear
       whether an abort or a commit is possible. The replica should be
       excluded from the list of available replicas.
    */
};

union replica_status switch (replica_status_enum d) {
 case REPLICA_IGNORED:
     void;
 default:
     struct {
	 string rs_msg<>;
	 /* An explanatory message */
	 int    rs_statement;
	 /* Which statement caused the error (for integrity and consistency
	    errors), or (-1) if not applicable
	 */
     } s;
};


struct commit_details {
    trans_id    cd_trans_id;     /* the transaction ID */
    hyper       cd_ts;           /* the timestamp of the last commit */
    jseqdb_vers cd_vers;         /* the version string of the jseqdb */
    
    node_name   cd_replicas<>;   /* the names of the replicas */
    replica_status cd_status<>;  /* the status of each replica */
};

enum transaction_error_enum {
    TE_LOST_COORDINATOR = 0,
    TE_OUT_OF_BUFFER_SPACE = 1,
    TE_READ_ONLY = 2,
    TE_SYSTEM_ERROR = 3
};

union transaction_error switch (transaction_error_enum d) {
    /* The RPC running into the error returns a transaction_error. Any
       following RPC will simply return NO_SUCH_TRANSACTION, because
       a transaction error implies that the transaction is aborted.
    */
 case TE_LOST_COORDINATOR:
     /* There is no coordinator anymore */
     void;
 case TE_OUT_OF_BUFFER_SPACE:
     /* The transaction cannot be buffered */
     string msg<>;
 case TE_READ_ONLY:
     /* It is tried to write to a read-only transaction */
     void;
 case TE_SYSTEM_ERROR:
     /* Another error */
     string msg<>;
};


union r_commit_transaction switch (trans_enum d) {
 case T_OK:
     /* The (write) transaction could be executed, and the minimum number
	of replicas have written the data to disk.
     */
     commit_details cd;
 case T_ABORTED:
     /* The transaction has been aborted, either because of inconsistency,
	or because there are not enough replicas left.
     */
     commit_details cd;
 case T_TRANSACTION_ERROR:
     /* A transaction error also implies abortion */
     transaction_error e;
 case T_NO_SUCH_TRANSACTION:
     /* The transaction does not exist */
     void;
 case T_CONTINUE_POLLING:
     /* The result is not yet available */
     void;
 default:
     unsupported_case;
};


/*** return type of drop_transaction ***/

union r_drop_transaction switch(trans_enum d) {
 case T_OK:
     void;
 case T_TRANSACTION_ERROR:
     /* This implies that the transaction has been dropped anyway */
     transaction_error e;
 case T_NO_SUCH_TRANSACTION:
     /* The transaction does not exist */
     void;
 default:
     unsupported_case;
};


/*** return type of lock_row ***/

union r_lock switch (trans_enum d) {
 case T_OK:
     void;
 case T_TRANSACTION_ERROR:
     transaction_error e;
 case T_NO_SUCH_TRANSACTION:
     /* The transaction does not exist */
     void;
 case T_NO_SUCH_TABLE:
     /* The table does not exist */
     void;
 case T_LOCK_TIMEOUT:
     /* The lock could not be acquired */
     void;
 case T_DEADLOCK:
     /* A deadlock was recognized */
     void;
 default:
     unsupported_case;
};

enum lock_op {
    LOCK_ROW = 0,
    UNLOCK_ROW = 1,
    LOCK_TABLE = 2,
    UNLOCK_TABLE = 3
};


program Transaction {
    version V1 {
	void null(void) = 0;

	r_open_data_channel
	    open_data_channel(node_domain, jseqdb_name) = 1;
	/* Open a data channel for exchanging data. The channel is bound
	   to the named jseqdb, but can be used for all transactions.
	   (Actually, there is no check that only this jseqdb is accessed.
	   Data for all transactions can be read and written over the
	   channel, provided that the transaction is known at the target
	   node.)
	*/

	r_begin_transaction
	    begin_transaction(node_domain, jseqdb_name, bool) = 2;
	/* Starts a new transaction. Note that it is allowed for a client
	   to start several transactions, even for the same jseqdb.
	   The bool says whether this is a read/write transaction (true),
	   or a read-only transaction.
	*/
	

	void
	    commit_transaction(node_domain, trans_id) = 3;
	/* Initiates the COMMIT of the transaction. The result is not
	   immediately available, and the caller should use 
	   commit_transaction_poll to get it.
	 */

	r_commit_transaction
	    commit_transaction_poll(node_domain, trans_id, float) = 4;
	 /* Get the result of the commit_transaction. The float is the
	    timeout; the server will respond with CONTINUE_POLLING
	    if the timeout is reached before the COMMIT is done.
	    In this case, the caller should simply repeat
	    the commit_transaction_poll call.
	 */

	r_drop_transaction
	    drop_transaction(node_domain, trans_id) = 5;
	/* Drops the transaction. 
	   This is also implicitly done when the TCP connection is closed.
	*/

	r_lock
	    lock(node_domain, trans_id, table_name, lock_op, key, float) = 6;
	/* Locks/unlocks the row/table for the time of the transaction.
	   This RPC performs the lock/unlock operation immediately.
	   The float arg is the timeout value.

	   The key arg is ignored for a table lock/unlock.

	   The float arg is ignored for unlocking.
	*/
    } = 1;
} = 38000;

/**********************************************************************/
/* The data channel program                                           */
/**********************************************************************/

/* The data channel program is called by clients to exchange data with
   the coordinator. It is available on special ports only. 
   If authentication is required, the data channel client needs to be
   authenticated first before any data can be exchanged.

   A TCP connection is bound to a certain jseqdb, but can be used
   for any transaction for this jseqdb.

   Write statements are not immediately executed. Because of this,
   one cannot get errors except transaction errors.
*/


/* -- return type of all write statements -- */

union r_write_stm switch(trans_enum d) {
 case T_OK:
     int stnr;    /* the statement number */
 case T_TRANSACTION_ERROR:
     transaction_error e;
 case T_NO_SUCH_TRANSACTION:
     /* The transaction does not exist */
     void;
 case T_NO_SUCH_TABLE:
     /* The table does not exist */
     void;
 case T_PROTOCOL_ERROR:
     /* Protocol violation */
     void;
 case T_LOCK_TIMEOUT:
     /* The lock could not be acquired */
     void;
 case T_DEADLOCK:
     /* A deadlock was recognized */
     void;
 default:
     unsupported_case;
};


/* -- return type of read: -- */

struct data_block {
    hyper     data_offset;
    hyper     data_total_length;
    bool      data_is_last_block;
    string    data<1048576>;    /* max 1M of data at a time */
};

union r_read switch(trans_enum d) {
 case T_OK:
     struct {
	 key        read_key;
	 row_id     read_row_id;
	 data_block read_data;
     } data;
 case T_TRANSACTION_ERROR:
     transaction_error e;
 case T_NO_SUCH_TRANSACTION:
     /* The transaction does not exist */
     void;
 case T_NO_SUCH_TABLE:
     /* The table does not exist */
     void;
 case T_NO_SUCH_KEY:
     /* the key does not exist */
     void;
 default:
     unsupported_case;
};


/* -- return type of iterate: -- */

union r_iterate switch(trans_enum d) {
 case T_OK:
     struct {
	 row_id     iter_row_id_first;
	 row_id     iter_row_id_last;
	 key        iter_keys<>;
	 data_block iter_data<>;
     } data;
 case T_TRANSACTION_ERROR:
     transaction_error e;
 case T_NO_SUCH_TRANSACTION:
     /* The transaction does not exist */
     void;
 case T_NO_SUCH_TABLE:
     /* The table does not exist */
     void;
 case T_END_OF_TABLE:
     /* No more row to iterate over */
     void;
 default:
     unsupported_case;
};


program Data_channel {
    version V1 {
	void null(void) = 0;

	/* Writing data: */

	r_write_stm
	    delete(trans_id, table_name, key) = 1;
	/* Delete the row with this key. If the key does not exist,
	   REPLICA_KEY_NOT_FOUND is returned at commit time.

	   If there is a shared lock on key, it is upgraded to an exclusive
	   lock before the deletion. If there is a table lock, it is also
	   upgraded before.
	*/

	r_write_stm
	    delete_ignore(trans_id, table_name, key) = 2;
	/* Delete the row with this key	if existing, and do nothing if
	   there is no such row.

	   If there is a shared lock on key, it is upgraded to an exclusive
	   lock before the deletion. If there is a table lock, it is also
	   upgraded before.
	*/

	r_write_stm
	    insert(trans_id, table_name, key, hyper) = 3;
	/* Insert a row with this key, and with a value of the passed
	   length. It is an error if the key already exists, and
	   REPLICA_UNIQUE_KEY is returned in this case at commit time.
	   The data must be transmitted by insert_data calls immediately
	   following this call (or else PROTOCOL_ERROR).

	   If there is a shared lock on key, it is upgraded to an exclusive
	   lock before the insertion. If there is a table lock, it is also
	   upgraded before.
	*/

	r_write_stm
	    insert_ignore(trans_id, table_name, key, hyper) = 4;
	/* Insert a row with this key, and with a value of the passed
	   length. If there is already a row with this key, nothing
	   is inserted, however.
	   The data must be transmitted by insert_data calls immediately
	   following this call (or else PROTOCOL_ERROR).

	   If there is a shared lock on key, it is upgraded to an exclusive
	   lock before the insertion. If there is a table lock, it is also
	   upgraded before.
	*/

	r_write_stm
	    insert_data(trans_id, data_block) = 5;
	/* Transmit data to insert for the preceding insert or
	   insert_ignore call. All fields of the data block must be
	   filled. If they are inconsistent with the previous data blocks,
	   or with the statement paramaters, PROTOCOL_ERROR is returned.
	*/



	/* Reading data */

	r_read
	    read(trans_id, table_name, key, hyper, hyper) = 6;
	/* read(transid, tabname, key, offset, max_length):
	   Read meta data of this row, and the passed range of data
	   (which can be requested as empty to only read meta data).
	   It is not an error when the requested range is bigger or
	   smaller than the available data range. 
	*/

	r_iterate
	    iterate (trans_id, table_name, row_id_opt, int, hyper) = 7;
	/* iterate(transid, tabname, rowid, n, max_length):
	   Iterate over the table. If a rowid is passed, the iteration
	   starts at the next row with a rowid bigger than the passed one.
	   Otherwise, the iteration starts at the beginning of the
	   table. In each iteration step up to n rows are returned.
	   For every row, the beginning of the data part is also
	   returned, but only up to max_length bytes. (The remaining
	   part should be retrieved by further read calls.)
	*/


	/* Locking */

	r_lock
	    lock(trans_id, table_name, lock_op, key) = 8;
	/* Locks/unlocks the row/table for the time of the transaction.
	   This RPC performs the lock/unlock operation immediately.

	   The key arg is ignored for a table lock/unlock.
	*/

	void
	    lock_timeout(float) = 9;
	/* Set the lock timeout. This is honoured by [lock], and when 
           shared locks are upgraded to exclusive locks (by insert/delete).
	*/


	/* Management - TODO */

        /* Compaction and reindexing can also go here */

	/* Also a function for reading jseqdb files block by block.
	   There should be a flag whether the jseqdb is read-locked
	   until the end of the transaction. The block-read function
	   should be conditional: Only if the checksum is newer the
	   data are transmitted.
	*/

        /* Read the size and other metadata of tables */

    } = 1;
} = 38001;

/**********************************************************************/
/* Transaction management                                             */
/**********************************************************************/

/* This program is purely internal. Every node provides it, and the 
   controller calls the procedures on remote nodes.
*/

#ifdef INTERNAL_API

union r_create_transaction switch(trans_enum d) {
 case T_OK:
     void;
 case T_TRANSACTION_ERROR:
     transaction_error e;
 default:
     unsupported_case;
};


program Trans_management {
    version V1 {
	void null(void) = 0;

	r_create_transaction 
	    create_transaction(node_domain, trans_id, jseqdb_name, bool) = 1;
	/* The coordinator calls this RPC to notify the node that this
	   transaction exists, and that data may be written. 
	   The bool says whether this is a r/w transaction.

           The transaction is bound to the passed jseqdb.

	   If there are existing transactions for this jseqdb with
	   older trans_id_ts from the same trans_id_node, the node can
	   delete them - it is ensured that they are aborted in the
	   meantime, but the notification was not possible.
	*/

	void 
	    delete_transaction(node_domain, trans_id) = 2;
	/* This transaction can be deleted. This is also the ABORT
	   in the commit protocol.
	 */

	void 
	    delete_transactions_older_than(node_domain, trans_id) = 3;
	/* All transactions from this trans_id_node can be deleted that
	   have an older trans_id, i.e. either a smaller trans_id_ts,
	   or (for same trans_id_ts), a smaller trans_id_nr. The
	   argument is the smallest active transaction the controller
	   currently manages.

	   This RPC is invoked from time to time by the controller.
	*/

	data_channel
	    open_data_channel(node_domain, bool) = 4;
	/* Opens a data channel that can be used to access all transactions.
	   The bool is the forward_flag: If true, all data ops are forwarded
	   to replica nodes.
	 */

	 void
	     commit_request(node_domain, trans_id) = 5;
	/* Request the COMMIT of this write transaction. This RPC only
	   initiates the COMMIT. The caller has then to invoke 
	   commit_request_poll to get the result.

	   A positive
	   reply is interpreted as agreement; an error is refusal.

           On error, the node can delete the transaction (ABORT). There
	   is no extra delete_transaction by the controller.

	   The node appends the write data in the buffer to the
	   jseqdb, i.e. it performs all writes now, but does not commit
	   the jseqdb yet. The coordinator does the same.

           A successful reply is interepreted as AGREED message, and
	   the coordinator can continue with commit_prepare.

	   The commit_details struct contains only data about this node.
	*/

	 r_commit_transaction
	     commit_request_poll(node_domain, trans_id, float) = 6;
	 /* Get the result of the commit_request. The float is the
	    timeout; the server will respond with CONTINUE_POLLING
	    if the timeout is reached before the COMMIT is done.
	    In this case, the caller should simply repeat
	    the commit_request_poll call.
	 */

	void 
	    commit_prepare(node_domain, trans_id) = 7;
	/* Prepare the COMMIT of this write transaction (second phase
	   of the commit protocol). The node cannot refuse the COMMIT
	   in this phase - it can only crash. A reply is interpreted as
	   ACK by the coordinator.

	   This message mainly notifies all nodes that all jseqdb
	   are commit-ready.

	   The nodes will now commit anyway if they do not get the COMMIT
	   in time (or a late ABORT). If the coordinator does not get
	   the ACK response of commit_preapre in time, it also commits.
	*/

	void 
	    commit(node_domain, trans_id) = 8;
	/* Perform the COMMIT of this write transaction (third phase
	   of the commit protocol). The node cannot refuse the COMMIT
	   in this phase - it can only crash. The reply is not interpreted
	   by the coordinator.

	   Nodes must COMMIT anyway if they do not get this message
	   in time after commit_prepare.
	*/
    } = 1;
} = 38002;


program Commit_engine {
    /* This program is locally used to execute phase 1 and phase 3 of the
       COMMIT in a separate thread/process.
    */

    version V1 {
	void null(void) = 0;

	r_commit_transaction
	     exec_commit_request(node_domain, trans_id) = 1;
	/* Performs phase 1. There is no timeout value - the RPC first
	   returns when the result is available.
	*/

	void 
	    exec_commit(node_domain, trans_id) = 2;
	/* Performs phase 3 */

    } = 1;
} = 38003;
#endif
