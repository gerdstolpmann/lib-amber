/* $Id$ -*- c -*- */

#ifndef COMMON_X
#define COMMON_X


typedef string jseqdb_name<64>;
/* The name of a jseqdb. A name is a dot-separated list of name components
   which match alphanum* (including '_')
*/

typedef opaque jseqdb_vers<16>;
/* A version string (16 bytes), actually an MD5 sum of all relevant version
   data.
 */

typedef string node_name<32>;
/* the hostname of the node, without any domain qualification */

typedef string node_domain<32>;
/* Each cluster is identified by a domain string. Usually only servers
   can talk with each other that are member of the same domain.

   RPC's return SYSTEM_ERR when they receive messages for a domain they
   cannot process. Multicast RPC messages for foreign domains are ignored.
 */

typedef string table_name<64>;
/* name of a jseqdb table */

typedef string key<255>;
/* key of a jseqdb row */

typedef hyper row_id;
/* row ID of a jseqdb row */

typedef row_id *row_id_opt;



enum coord_error_enum {
    COORD_JSEQDB_UNKNOWN = 0,
    COORD_UNAVAILABLE = 1
};

union coord_error switch(coord_error_enum d) {
 case COORD_JSEQDB_UNKNOWN:
     /* The jseqdb name is unknown */
     void;
 case COORD_UNAVAILABLE:
     /* There is right now no elected coordinator. The hyper is a timestamp
	when the election is likely to be finished, and it makes sense to
	repeat the query.
     */
     hyper delay_until;
};

#endif

