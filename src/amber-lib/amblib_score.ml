(* $Id$ *)

let log_2 = log 2.0

let ld x =
  log x /. log_2

let ld_of_int_tab =
  Array.init
    1000
    (fun n ->
       ld (0.1 *. float (1+n)))
    

let ld_of_int n =
  if n >= 0 && n < 1000 then
    ld_of_int_tab.(n)
  else
    ld (0.1 *. float (1+n))


let wdt freq =
  (3.33 +. ld_of_int freq) /. 3.48
