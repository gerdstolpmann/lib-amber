(* $Id$ *)

(* This module assumes that strings are UTF-8-encoded *)


type token =
    [ `Word of string * int
    ]


val normalize : string -> string
  (* Performs the following normalizations:
     - Uppercase to lowercase (however, the so-called conditional case folding
       is not implemented)
     - Removal of accents (for Roman scripts)
     - whitespace to space

     Input and result strings are encoded in UTF-8. The function is optimized
     for the case that most of the chars are in the ASCII subset (code <= 126).
   *)

val tokenize : string -> token list
  (* Split the UTF-8 string into a token list. The tokens are not yet
     normalized
   *)
