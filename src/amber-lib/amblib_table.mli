(* $Id$ *)

(** Lookup tables string -> any *)

(** This is a subset of [Hashtbl]. We only support adding to the table -
    no deletes, no replacements. A key may only be added once.

    These tables can be put into the ancient heap.
 *)

type 'a t

val create : int -> 'a t

val add : 'a t -> string -> 'a -> unit

val find : 'a t -> string -> 'a

val mem : 'a t -> string -> bool
