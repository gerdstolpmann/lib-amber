(* $Id$ *)

(** Attribute vectors: these are arrays of attribute elements

 *)

type att_type =
    [ `U1 | `U2 | `U4 | `U8 | `U16 | `S8 | `S16 | `S32 | `S64 | `F32 | `F64 ]
   (** Attribute types:
       - [`U1]: 1 bit
       - [`U2]: 2 bits (unsigned)
       - [`U4]: 4 bits (unsigned)
       - [`U8]: 8 bits (unsigned)
       - [`U16]: 16 bits (unsigned)
       - [`S8]: 8 bits (signed)
       - [`S16]: 16 bits (signed)
       - [`S32]: 32 bits (signed)
       - [`S64]: 64 bits (signed)
       - [`F32]: single-precision floating point
       - [`F64]: double-precision floating point
    *)

val string_of_typ : att_type -> string
  (** The string representation of the type is the name without [`], e.g.
      for [`S64] it is "S64".
   *)

val typ_of_string : string -> att_type
  (** Returns the type of a string, or raises [Invalid_argument] *)

module Attribute : sig
  type att_vector
    (** The array of attribute elements *)

  val size : att_vector -> int
    (** The numebr of elements *)

  val typ : att_vector -> att_type
    (** The element type *)

  val create : att_type -> att_vector
    (** Creates an array of size 0 with the given type of elements *)

  val create_for_size : att_type -> int ->  att_vector
    (** Creates a logically empty array with the given size of buffer space.
        The buffer space is specified in units of the given type of elements.
     *)

  val set_immutable_size : att_vector -> unit
    (** Set that the size of the vector cannot be changed *)

  val expand : att_vector -> int -> unit
    (** Expands the array to the requested size. New elements are initialized
        with 0
     *)

  val get_int64 : att_vector -> int -> int64
    (** Returns the value of the element at this position as [int64] number.
        For [`F32] and [`F64] this is the bit representation.
     *)

  val get_float : att_vector -> int -> float
    (**  Returns the value of the element at this position as float number.
         Only defined for [`F32] and [`F64].
     *)

  val set_int64 : att_vector -> int -> int64 -> unit
    (** Sets the value of the element at this position to the [int64] number.
        For [`F32] and [`F64] one can set the bit representation.
     *)

  val set_float : att_vector -> int -> float -> unit
    (** Sets the value of the element at this position to the float number.
        Only defined for [`F32] and [`F64].
     *)

  val dbg_print_int : att_vector -> string -> unit
    (** Prints the elements of the attribute as integers to stdout, prefixed
        by the passed label.
     *)

  val dbg_print_float : att_vector -> string -> unit
    (** Prints the elements of the attribute as floats to stdout, prefixed
        by the passed label.
     *)

  (** Low-level *)

  open Bigarray

  val memory : att_vector -> Netsys_mem.memory
    (** Return the attributes as char bigarray *)

  val rep_int8_unsigned : 
        att_vector -> (int,int8_unsigned_elt,c_layout) Bigarray.Array1.t
        (** Return the internal bigarray storing the vector for [`U1],
            [`U2], [`U4], and [`U8]
	 *)

  val rep_int16_unsigned :
        att_vector -> (int,int16_unsigned_elt,c_layout) Bigarray.Array1.t
        (** Return the internal bigarray storing the vector for [`U16] *)

  val rep_int8_signed :
        att_vector -> (int,int8_signed_elt,c_layout) Bigarray.Array1.t
        (** Return the internal bigarray storing the vector for [`S8] *)

  val rep_int16_signed :
        att_vector -> (int,int16_signed_elt,c_layout) Bigarray.Array1.t
        (** Return the internal bigarray storing the vector for [`S16] *)

  val rep_int32 :
        att_vector -> (int32,int32_elt,c_layout) Bigarray.Array1.t
        (** Return the internal bigarray storing the vector for [`S32] *)

  val rep_int64 :
        att_vector -> (int64,int64_elt,c_layout) Bigarray.Array1.t
        (** Return the internal bigarray storing the vector for [`S64] *)

  val rep_float32 :
        att_vector -> (float,float32_elt,c_layout) Bigarray.Array1.t
        (** Return the internal bigarray storing the vector for [`F32] *)

  val rep_float64 :
        att_vector -> (float,float64_elt,c_layout) Bigarray.Array1.t
        (** Return the internal bigarray storing the vector for [`F64] *)
end


module type SORTABLE_VECTOR = sig
  type t
  type elt

  val length : t -> int
  val get : t -> int -> elt
  val compare : elt -> elt -> int
  val swap : t -> int -> int -> unit
end


module Quicksort (V : SORTABLE_VECTOR) : sig
  val sort : V.t -> unit
end
