(* $Id$ *)

(** Access dump files *)

(** {1 Writing dump files} *)

type dump_writer

val create_dump_file : string -> dump_writer

val write_object : dump_writer -> Amblib_dump_fmt_aux.obj -> unit

val close_dump_writer : dump_writer -> unit

val pos_writer : dump_writer -> int64
val seek_writer : dump_writer -> int64 -> unit
val seek_end_writer : dump_writer -> unit

(** {1 Reading dump files} *)

type dump_reader

val open_dump_file : string -> dump_reader

val read_object : dump_reader -> Amblib_dump_fmt_aux.obj

val close_dump_reader : dump_reader -> unit

(** {1 Auxiliary functions} *)

val hyper : int64 -> int
  (** Converts int64 to int. Fails on overflow *)
