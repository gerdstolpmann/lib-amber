(* $Id$ *)

(** Formulas for computing the score *)


val wdt : int -> float
  (** computes w(d,t) of the argument 10 * f(d,t)  (given as int). 
      The argument must be >= 0.
      The result is positive, and
      - [wdt 0 ~ 0.0]
      - [wdt 1 ~ 1.0]
      - [wdt] grows logarithmically for increasing f(d,t)
   *)
