(* $Id$ *)

(** Compressed term vector slices *)

(** About the chosen FP representation: Weights are stored as half-precision
    FP numbers (http://en.wikipedia.org/wiki/Half_precision), and the
    allowed range is [0 <= x <= 65504]. Bigger numbers are stored as
    positive infinity.
 *)

type half_float
  (** The type of non-negative half-precise floats

      It is meaningful to compare [half_float]: Except for [nan], we
      have

      [ x < y ] iff [ float_of_half x < float_of_half y ]

      The value [nan] is the largest (larger  than [infinity]).
   *)

type docids = int array
type weights = half_float array
type positions = int array array

val encode_slice : docids -> weights -> positions  -> bytes
  (** Pre-condition:
      - [length(docids)] = [length(weights)]
      - [length(positions)] <= [length(docids)]
      - all docids must be positive
      - the docids must be sorted in ascending order
      - the weights must be non-negative, and in the allowed value range
      - the positions must be non-negative
      - the positions must be sorted in ascending order per document
      - a position can only occur once per document
   *)

val decode_slice : bytes -> int -> int -> docids * weights * int
  (** [let docids, weights, k_positions = decode_slice s k len]:
      Decodes the slice starting at [s.[k]]. Returns in [k_positions]
      where the position data starts.

      [len] is the max number of bytes to interpret.
   *)

val mem_slice : int -> bytes -> int -> int -> bool
  (** [mem_slice docid s k len]: Checks whether [docid] is in the
      slice defined by [s, k, len]
   *)

val decode_slice_positions : bytes -> int -> int -> int -> positions
  (** [let positions = decode_slice_positions s k len n]: Decodes
      the positions starting at [s.[k]]. [len] is the max number of bytes 
      to interpret. [n] is the number of documents (= [Array.length docids]).
   *)

(** The following is helpful for phrase queries. We can more quickly
   find positions if we run over s and count null bytes (i.e. end-of-pos-array
   marks). In pos_decoder we remember data for quicker accesses, e.g.
   store the byte offset for every 10-th position array.
 *)

type pos_decoder

val create_pos_decoder :  bytes -> int -> int -> int -> pos_decoder
  (** [let decoder = create_pos_decoder s k len n] *)

val get_positions : pos_decoder -> int -> int array
  (** [get_positions decoder j]: returns the position array for the
      j-th docuemnt
   *)


val float_of_half : half_float -> float
  (** Conversion *)

val half_of_float : float -> half_float
  (** Conversion *)

val bits_of_half_float : half_float -> int
  (*+ Return the 15 bits representing half floats *)

val half_float_of_bits : int -> half_float
  (** reverse *)


val bytes_of_vint : int -> int
  (** How many bytes the encoding as vint takes *)

val set_vint : bytes -> int -> int -> unit
  (** [set_vint s p n]: Stores n into [s] at byte position [p] as vint *)

val get_vint : bytes -> int -> int
  (** [get_vint s p]: returns the vint at byte position [p] of string [s] *)
