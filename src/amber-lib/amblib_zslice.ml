(* $Id$ *)

open Printf

type half_float = int

type docids = int array
type weights = half_float array
type positions = int array array


let bits_of_half_float x = x
let half_float_of_bits x = x land 0x7fff

let half_of_float v =
  if v < 0.0 then failwith "Amblib_zslice.half_of_float: negative number";
  let all_bits = Int64.bits_of_float v in
  let exp_bits = 
    (Int64.to_int(Int64.shift_right_logical all_bits 52)) land 0x7ff in
  let mantissa = 
    (Int64.to_int(Int64.shift_right_logical all_bits 42)) land 0x3ff in
  if exp_bits < 999 then
    (* v too small, map it to zero *)
    0
  else if exp_bits < 1009 then
    (* subnormal case *)
    (mantissa lor 0x400) lsr (1009 - exp_bits)
  else if exp_bits < 1039 then
    (* normal case *)
    mantissa lor ( (exp_bits - 1008) lsl 10 )
  else if exp_bits < 2047 then
    (* v too big, map it to infinity *)
    31 lsl 10
  else if Int64.logand all_bits 0xf_ffff_ffff_ffffL = 0L then
    (* infinity *)
    31 lsl 10
  else
    (* NaN *)
    (31 lsl 10) lor 1


let float_of_half v =
  let exp_bits = (v lsr 10) land 0x1f in
  let mantissa = v land 0x3ff in
  if exp_bits = 0 && mantissa=0 then
    0.0
  else if exp_bits = 0 then (
    (* subnormal case *)
    let m = ref mantissa in
    let x = ref 1009 in
    while !m < 0x400 do
      m := !m lsl 1;
      decr x
    done;
    m := !m land 0x3ff;
    let dbl_mant = Int64.shift_left (Int64.of_int !m) 42 in
    let dbl_exp = Int64.shift_left (Int64.of_int !x) 52 in
    Int64.float_of_bits (Int64.logor dbl_mant dbl_exp)
  )
  else if exp_bits < 31 then (
    (* normal case *)
    let dbl_mant = Int64.shift_left (Int64.of_int mantissa) 42 in
    let dbl_exp = Int64.shift_left (Int64.of_int (exp_bits + 1008)) 52 in
    Int64.float_of_bits (Int64.logor dbl_mant dbl_exp)
  )
  else if mantissa = 0 then
    infinity
  else 
    nan


let limit =
  if Sys.word_size = 32 then
    35
  else
    63

let rec compute_bytes_of_vint m n x =
  if m=limit || x < 1 lsl m then
    n
  else
    compute_bytes_of_vint (m+7) (n+1) x


let bytes_of_vint x =
  if x < 128 then
    1
  else if x < 16384 then
    2
  else if x < 2097152 then
    3
  else if x < 268435456 then
    4
  else
    compute_bytes_of_vint 35 5 x


let rec store_vint_loop s p x last =
  (* Stores the vint from s.[p] to s.[p-(by-1)], i.e. BACKWARD *)
  let r1 = x land 127 in
  let r2 = x lsr 7 in
  let bit7 = if last then 0 else 128 in
  Bytes.unsafe_set s p (Char.unsafe_chr (r1 lor bit7));
  if r2 <> 0 then
    store_vint_loop s (p-1) r2 false


let store_vint s p x =
  (* allow this to be inlined *)
  if x < 128 then
    Bytes.unsafe_set s p (Char.unsafe_chr x)
  else
    store_vint_loop s p x true


let set_vint s p x =
  store_vint s (p + bytes_of_vint x - 1) x


let rec retr_vint_loop s p x_prev =
  (* Even if the string contained a malformed vint, this loop would be 
     safe, because OCaml stores an implicit zero byte after the
     official end of the string
   *)
  let x = Char.code (Bytes.unsafe_get s !p) in
  incr p;
  if x < 128 then
    x lor x_prev
  else
    retr_vint_loop s p (((x land 0x7f) lor x_prev) lsl 7)
  

let retr_vint s p =
  let x = Char.code (Bytes.unsafe_get s !p) in
  incr p;
  if x < 128 then
    x
  else
    retr_vint_loop s p ((x land 0x7f) lsl 7)


let get_vint s p =
  retr_vint s (ref p)


let rec enlarge sl q =
  (* "let rec" to prevent inlining *)
  let (s,l) = sl in   
  let u = Bytes.create(max q (2*l)) in
  Bytes.blit s 0 u 0 l;
  (u, Bytes.length u)


let maybe_enlarge sl q =
  let (_,l) = sl in
  if q < l then
    sl
  else
    enlarge sl q


let encode_slice docids weights positions =
  let n = Array.length docids in
  let np = Array.length positions in
  assert(Array.length weights = n);
  assert(np <= n);

  let rec add_docids sl p k last_docid =
    if k >= n then
      add_weights sl p 0 32768
    else (
      let docid = Array.unsafe_get docids k in
      if docid <= 0 then 
        failwith "Amblib_zslice.encode_slice: non-positive docid";
      let x = docid - last_docid in
      let by = bytes_of_vint x in
      let sl = maybe_enlarge sl (p+by) in
      let (s,_) = sl in
      store_vint s (p+by-1) x;
      add_docids sl (p+by) (k+1) docid
    )

  and add_weights sl p k last_weight = 
    (* last_weight: the repr of the last two-byte FP *)
    if k >= n then
      add_positions sl p 0
    else (
      let weight = Array.unsafe_get weights k in
      if last_weight land 0xff80 = weight land 0xff80 then (
        let sl = maybe_enlarge sl (p+1) in
        let (s,_) = sl in
        Bytes.unsafe_set s p (Char.unsafe_chr ((weight land 127) lor 128));
        add_weights sl (p+1) (k+1) last_weight
      )
      else (
        let sl = maybe_enlarge sl (p+2) in
        let (s,_) = sl in
        Bytes.unsafe_set s p (Char.unsafe_chr (weight lsr 8));
        Bytes.unsafe_set s (p+1) (Char.unsafe_chr (weight land 0xff));
        add_weights sl (p+2) (k+1) weight
      )
    )

  and add_positions sl p k =
    if k >= np then
      let (s,_) = sl in
      Bytes.sub s 0 p
    else
      add_positions_for sl p k positions.(k) 0 (-1)

  and add_positions_for sl p k docpos j last_pos =
    if j >= Array.length docpos then (
      let sl = maybe_enlarge sl (p+1) in
      let (s,_) = sl in
      Bytes.unsafe_set s p '\000';
      add_positions sl (p+1) (k+1)
    )
    else (
      let pos = Array.unsafe_get docpos j in
      if pos < 0 then failwith "Amblib_zslice.encode_slice: negative position";
      let x = pos - last_pos in
      let by = bytes_of_vint x in
      let sl = maybe_enlarge sl (p+by) in
      let (s,_) = sl in
      store_vint s (p+by-1) x;
      add_positions_for sl (p+by) k docpos (j+1) pos
    )
  in

  let s = Bytes.create (6*n) in
  let sl = (s, Bytes.length s) in
  let by = bytes_of_vint n in
  let sl = maybe_enlarge sl by in
  store_vint (fst sl) (by-1) n;
  add_docids sl by 0 0
;;


let decode_slice s k len =
  let l = k+len in
  let p = ref k in
  let fail() = failwith "Amblib_zslice.decode_slice" in

  if !p >= l then fail();
  let n = retr_vint s p in
  let docids = Array.make n 0 in
  let weights = Array.make n 0 in
  
  let last_docid = ref 0 in
  for k = 0 to n-1 do
    if !p >= l then fail();
    let delta = retr_vint s p in
    let docid = !last_docid + delta in
    Array.unsafe_set docids k docid;
    last_docid := docid
  done;

  let last_weight = ref 0 in
  for k = 0 to n-1 do
    if !p >= l then fail();
    let x1 = Char.code(Bytes.unsafe_get s !p) in
    incr p;
    let weight =
      if x1 < 128 then (
        if !p >= l then fail();
        let x0 = Char.code(Bytes.unsafe_get s !p) in
        incr p;
        let w = (x1 lsl 8) lor x0 in
        last_weight := w land 0x7f80;
        w
      )
      else
        !last_weight lor (x1 land 0x7f) in
    Array.unsafe_set weights k weight;
  done;

  (docids, weights, !p)


exception Result of bool

let mem_slice test_docid s k len =
  let l = k+len in
  let p = ref k in
  let fail() = failwith "Amblib_zslice.mem_slice" in

  if !p >= l then fail();
  let n = retr_vint s p in
  let last_docid = ref 0 in
  try
    for k = 0 to n-1 do
      if !p >= l then raise (Result false);
      let delta = retr_vint s p in
      let docid = !last_docid + delta in
      if docid >= test_docid then
        raise(Result(docid = test_docid));
      last_docid := docid
    done;
    false
  with
    | Result b -> b


let decode_slice_positions s k len n =
  let l = k+len in
  let p = ref k in
  let fail() = failwith "Amblib_zslice.decode_slice_positions" in

  let rec pos_loop j acc =
    if !p < l && j < n then (
(* eprintf "dsp index=%d p=%d\n%!" j !p; *)
      docpos_loop j acc [] (-1)
    )
    else
      Array.of_list(List.rev acc)

  and docpos_loop j acc1 acc2 last_pos =
    if !p >= l then fail();
    let delta = retr_vint s p in
    if delta = 0 then
      let pos_array = Array.of_list(List.rev acc2) in
      pos_loop (j+1) (pos_array :: acc1)
    else
      let pos = last_pos + delta in
      docpos_loop j acc1 (pos :: acc2) pos
  in
  
  pos_loop 0 []


type pos_decoder =
  { pos_data : bytes;
    pos_n : int;       (* number of docs *)
    pos_end : int;     (* end index in pos_data *)
    pos_buffer : int array;
    mutable pos_buf_end : int
  }


let create_pos_decoder data k_start k_len n =
  let b = Array.make (max 1 n) 0 in
  b.(0) <- k_start;
  { pos_data = data;
    pos_n = n;
    pos_end = k_start + k_len;
    pos_buffer = b;
    pos_buf_end = 1;
  }

let find_zero_vint s k =
  (* We exploit that all OCaml strings are implicitly terminated by a null
     byte at s.[length]
   *)
  let l = Bytes.length s in
  let rec find k vint_start =
    let c = Bytes.unsafe_get s k in
    if c < '\128' then (
      if c = '\000' then (
        if k < l then (
          if vint_start then
            k
          else
            find (k+1) true
        )
        else
          raise Not_found
      )
      else
        find (k+1) true
    )
    else
      find (k+1) false in
  find k true


let advance pdec k =
  if k >= pdec.pos_end then
    raise Not_found
  else
    let k' = find_zero_vint pdec.pos_data k in
    if k' >= pdec.pos_end then
      raise Not_found;
    k' + 1
      

let find_start pdec j =
  let new_buf_end = max pdec.pos_buf_end (j+1) in
  for q = pdec.pos_buf_end to new_buf_end - 1 do
    let k0 = pdec.pos_buffer.(q-1) in
    let k1 = advance pdec k0 in
    pdec.pos_buffer.(q) <- k1
  done;
  pdec.pos_buf_end <- new_buf_end;
  let k = pdec.pos_buffer.(j) in
  assert(k > 0);
  k


let get_positions pdec j =
  let fail() = failwith "Amblib_zslice.get_positions" in
  try
    if j < 0 || j >= pdec.pos_n then
      raise Not_found;
    let p = ref(find_start pdec j) in
(* eprintf "gp index=%d p=%d\n%!" j !p; *)
    let s = pdec.pos_data in
    
    let rec docpos_loop acc last_pos =
      if !p >= pdec.pos_end then fail();
      let delta = retr_vint s p in
      if delta = 0 then
        Array.of_list(List.rev acc)
      else
        let pos = last_pos + delta in
        docpos_loop (pos :: acc) pos
    in
    
    docpos_loop [] (-1)
  with
    | Not_found -> [| |]




