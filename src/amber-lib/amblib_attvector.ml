(* $Id$ *)

open Printf

type att_type =
    [ `U1 | `U2 | `U4 | `U8 | `U16 | `S8 | `S16 | `S32 | `S64 | `F32 | `F64 ]

let att_initial_vector_length = 1024 (* * 1024 *)


let string_of_typ ty =
  match ty with
    | `U1   -> "U1"
    | `U2   -> "U2"
    | `U4   -> "U4"
    | `U8   -> "U8"
    | `U16  -> "U16"
    | `S8   -> "S8"
    | `S16  -> "S16"
    | `S32  -> "S32"
    | `S64  -> "S64"
    | `F32  -> "F32"
    | `F64  -> "F64"

let typ_of_string s =
  match s with
    | "U1" -> `U1
    | "U2" -> `U2
    | "U4" -> `U4
    | "U8" -> `U8
    | "U16" -> `U16
    | "S8"  -> `S8
    | "S16" -> `S16
    | "S32" -> `S32
    | "S64" -> `S64
    | "F32" -> `F32
    | "F64" -> `F64
    | _ -> invalid_arg "Amb_create_index.typ_of_string"


module Attribute = struct
  open Bigarray

  type att_rep =
      [ `U1 of (int,int8_unsigned_elt,c_layout) Array1.t
      | `U2 of (int,int8_unsigned_elt,c_layout) Array1.t
      | `U4 of (int,int8_unsigned_elt,c_layout) Array1.t
      | `U8 of (int,int8_unsigned_elt,c_layout) Array1.t
      | `U16 of (int,int16_unsigned_elt,c_layout) Array1.t
      | `S8 of (int,int8_signed_elt,c_layout) Array1.t
      | `S16 of (int,int16_signed_elt,c_layout) Array1.t
      | `S32 of (int32,int32_elt,c_layout) Array1.t
      | `S64 of (int64,int64_elt,c_layout) Array1.t
      | `F32 of (float,float32_elt,c_layout) Array1.t
      | `F64 of (float,float64_elt,c_layout) Array1.t
      ]

  type att_vector =
      { mutable att_size : int;
	mutable att_type : att_type;
	mutable att_rep : att_rep;
        mutable att_immutable : bool
      }

  let size att = att.att_size

  let typ att = att.att_type

    (* In the following:
       - length = length of the bigarray
       - size = user-visible length of the vector (may be a multiple of length)
     *)

  let size_of_length att_type len =
    match att_type with
      | `U1 -> len * 8
      | `U2 -> len * 4
      | `U4 -> len * 2
      | _   -> len

  let length_of_size att_type size =
    match att_type with
      | `U1 -> (size - 1) / 8 + 1
      | `U2 -> (size - 1) / 4 + 1
      | `U4 -> (size - 1) / 2 + 1
      | _   -> size

  let rep_int8_unsigned att =
    match att.att_rep with
      | `U1 ba | `U2 ba | `U4 ba | `U8 ba ->
	  Array1.sub ba 0 (length_of_size att.att_type att.att_size)
      | _ -> assert false

  let rep_int16_unsigned att =
    match att.att_rep with
      | `U16 ba ->
	  Array1.sub ba 0 att.att_size
      | _ -> assert false

  let rep_int8_signed att =
    match att.att_rep with
      | `S8 ba ->
	  Array1.sub ba 0 att.att_size
      | _ -> assert false

  let rep_int16_signed att =
    match att.att_rep with
      | `S16 ba ->
	  Array1.sub ba 0 att.att_size
      | _ -> assert false

  let rep_int32 att =
    match att.att_rep with
      | `S32 ba ->
	  Array1.sub ba 0 att.att_size
      | _ -> assert false

  let rep_int64 att =
    match att.att_rep with
      | `S64 ba ->
	  Array1.sub ba 0 att.att_size
      | _ -> assert false

  let rep_float32 att =
    match att.att_rep with
      | `F32 ba ->
	  Array1.sub ba 0 att.att_size
      | _ -> assert false

  let rep_float64 att =
    match att.att_rep with
      | `F64 ba ->
	  Array1.sub ba 0 att.att_size
      | _ -> assert false

  let memory att =
    match att.att_rep with
      | `U1 ba | `U2 ba | `U4 ba | `U8 ba ->
          Netsys_mem.memory_of_bigarray_1 (rep_int8_unsigned att)
      | `U16 ba ->
          Netsys_mem.memory_of_bigarray_1 (rep_int16_unsigned att)
      | `S8 ba ->
          Netsys_mem.memory_of_bigarray_1 (rep_int8_signed att)
      | `S16 ba ->
          Netsys_mem.memory_of_bigarray_1 (rep_int16_signed att)
      | `S32 ba ->
          Netsys_mem.memory_of_bigarray_1 (rep_int32 att)
      | `S64 ba ->
          Netsys_mem.memory_of_bigarray_1 (rep_int64 att)
      | `F32 ba ->
          Netsys_mem.memory_of_bigarray_1 (rep_float32 att)
      | `F64 ba ->
          Netsys_mem.memory_of_bigarray_1 (rep_float64 att)

  let create_fill kind length initvalue =
    let ba = Array1.create kind c_layout length in
    Array1.fill ba initvalue;
    ba

  let init_length (att_type:att_type) =
    match att_type with
      | `U1 -> att_initial_vector_length / 8
      | `U2 -> att_initial_vector_length / 4
      | `U4 -> att_initial_vector_length / 2
      | _   -> att_initial_vector_length

  let create_rep (att_type:att_type) length : att_rep =
    match att_type with
      | `U1 ->
	  `U1(create_fill int8_unsigned length 0)
      | `U2 ->
	  `U2(create_fill int8_unsigned length 0)
      | `U4 ->
	  `U4(create_fill int8_unsigned length 0)
      | `U8 ->
	  `U8(create_fill int8_unsigned length 0)
      | `U16 ->
	  `U16(create_fill int16_unsigned length 0)
      | `S8 ->
	  `S8(create_fill int8_signed length 0)
      | `S16 ->
	  `S16(create_fill int16_signed length 0)
      | `S32 ->
	  `S32(create_fill int32 length 0l)
      | `S64 ->
	  `S64(create_fill int64 length 0L)
      | `F32 ->
	  `F32(create_fill float32 length 0.0)
      | `F64 ->
	  `F64(create_fill float64 length 0.0)

  let length att =
    match att.att_rep with
      | `U1 r  -> Array1.dim r
      | `U2 r  -> Array1.dim r
      | `U4 r  -> Array1.dim r
      | `U8 r  -> Array1.dim r
      | `U16 r -> Array1.dim r
      | `S8 r  -> Array1.dim r
      | `S16 r -> Array1.dim r
      | `S32 r -> Array1.dim r
      | `S64 r -> Array1.dim r
      | `F32 r -> Array1.dim r
      | `F64 r -> Array1.dim r


  let create (att_type:att_type) =
    { att_size = 0;
      att_type = att_type;
      att_rep = create_rep att_type (init_length att_type);
      att_immutable = false;
    }

  let create_for_size (att_type:att_type) size =
    { att_size = 0;
      att_type = att_type;
      att_rep = create_rep att_type (length_of_size att_type size);
      att_immutable = false;
    }

  let set_immutable_size att =
    att.att_immutable <- true

  let expand att req_size =
    if att.att_immutable then
      failwith "Amblib_attvector.Attribute.expand: Cannot resize (immutable)";
    let len = length att in
    let cap = size_of_length att.att_type len in
    if req_size > cap then (
      let new_size = max req_size (cap + cap/4) in
      let new_len = length_of_size att.att_type new_size in
      let new_rep = create_rep att.att_type new_len in
      ( match att.att_rep, new_rep with
	  | `U1 r, `U1 nr  -> Array1.blit r (Array1.sub nr 0 len)
	  | `U2 r, `U2 nr  -> Array1.blit r (Array1.sub nr 0 len)
	  | `U4 r, `U4 nr  -> Array1.blit r (Array1.sub nr 0 len)
	  | `U8 r, `U8 nr  -> Array1.blit r (Array1.sub nr 0 len)
	  | `U16 r,`U16 nr -> Array1.blit r (Array1.sub nr 0 len)
	  | `S8 r, `S8 nr  -> Array1.blit r (Array1.sub nr 0 len)
	  | `S16 r,`S16 nr -> Array1.blit r (Array1.sub nr 0 len)
	  | `S32 r,`S32 nr -> Array1.blit r (Array1.sub nr 0 len)
	  | `S64 r,`S64 nr -> Array1.blit r (Array1.sub nr 0 len)
	  | `F32 r,`F32 nr -> Array1.blit r (Array1.sub nr 0 len)
	  | `F64 r,`F64 nr -> Array1.blit r (Array1.sub nr 0 len)
	  | _ -> assert false 
      );
      att.att_rep <- new_rep;
      att.att_size <- req_size;
    ) else (
      att.att_size <- max att.att_size req_size
    )

  let get_int64 att pos =
    if pos < 0 || pos >= att.att_size then
      invalid_arg "Amb_create_index.Attribute.get_int64";
    match att.att_rep with
      | `U1 r ->
	  let pos' = pos lsr 3 in
	  let bit  = pos land 7 in
	   Int64.of_int(((Array1.get r pos') lsr bit) land 1)
      | `U2 r ->
	  let pos' = pos lsr 2 in
	  let bit  = (pos land 3) lsl 1 in
	   Int64.of_int(((Array1.get r pos') lsr bit) land 3)
      | `U4 r ->
	  let pos' = pos lsr 1 in
	  let bit  = (pos land 1) lsl 2 in
	   Int64.of_int(((Array1.get r pos') lsr bit) land 15)
      | `U8 r ->
	  Int64.of_int(Array1.get r pos)
      | `U16 r ->
	  Int64.of_int(Array1.get r pos)
      | `S8 r ->
	  Int64.of_int(Array1.get r pos)
      | `S16 r ->
	  Int64.of_int(Array1.get r pos)
      | `S32 r ->
	  Int64.of_int32(Array1.get r pos)
      | `S64 r ->
	  Array1.get r pos
      | `F32 r ->
	  Int64.of_int32(Int32.bits_of_float(Array1.get r pos))
      | `F64 r ->
	  Int64.bits_of_float(Array1.get r pos)
	  
  let get_float att pos =
    if pos < 0 || pos >= att.att_size then
      invalid_arg "Amb_create_index.Attribute.get_float";
    match att.att_rep with
      | `F32 r ->
	  Array1.get r pos
      | `F64 r ->
	  Array1.get r pos
      | _ ->
	  invalid_arg "Amb_create_index.Attribute.get_float"

  let dbg_print_int att label =
    printf "attribute %s:" label;
    for k = 0 to att.att_size - 1 do
      printf " %Ld" (get_int64 att k)
    done;
    printf "\n%!"

  let dbg_print_float att label =
    printf "attribute %s:" label;
    for k = 0 to att.att_size - 1 do
      printf " %f" (get_float att k)
    done;
    printf "\n%!"
	  
  let set_int64 att pos v =
    if pos < 0 || pos >= att.att_size then
      invalid_arg "Amb_create_index.Attribute.set_int64";
    match att.att_rep with
      | `U1 r ->
	  let pos' = pos lsr 3 in
	  let bit  = pos land 7 in
	  let x = (Array1.get r pos') land (lnot (1 lsl bit)) in
	  let v1 = Int64.to_int(Int64.logand v 1L) in
	  Array1.set r pos' (x lor (v1 lsl bit))
      | `U2 r ->
	  let pos' = pos lsr 2 in
	  let bit  = (pos land 3) lsl 1 in
	  let x = (Array1.get r pos') land (lnot (3 lsl bit)) in
	  let v1 = Int64.to_int(Int64.logand v 3L) in
	  Array1.set r pos' (x lor (v1 lsl bit))
      | `U4 r ->
 	  let pos' = pos lsr 1 in
	  let bit  = (pos land 7) lsl 2 in
	  let x = (Array1.get r pos') land (lnot (7 lsl bit)) in
	  let v1 = Int64.to_int(Int64.logand v 15L) in
	  Array1.set r pos' (x lor (v1 lsl bit))
      | `U8 r ->
	  Array1.set r pos (Int64.to_int v)
      | `U16 r ->
	  Array1.set r pos (Int64.to_int v)
      | `S8 r ->
	  Array1.set r pos (Int64.to_int v)
      | `S16 r ->
	  Array1.set r pos (Int64.to_int v)
      | `S32 r ->
	  Array1.set r pos (Int64.to_int32 v)
      | `S64 r ->
	  Array1.set r pos v
      | `F32 r ->
	  Array1.set r pos (Int32.float_of_bits(Int64.to_int32 v))
      | `F64 r ->
	  Array1.set r pos (Int64.float_of_bits v)

  let set_float att pos v =
    if pos < 0 || pos >= att.att_size then
      invalid_arg "Amb_create_index.Attribute.set_float";
    match att.att_rep with
      | `F32 r ->
	  Array1.set r pos v
      | `F64 r ->
	  Array1.set r pos v
      | _ ->
	  invalid_arg "Amb_create_index.Attribute.set_float"

end


module type SORTABLE_VECTOR = sig
  type t
  type elt

  val length : t -> int
  val get : t -> int -> elt
  val compare : elt -> elt -> int
  val swap : t -> int -> int -> unit
end


module Quicksort (V : SORTABLE_VECTOR) = struct

  let rec sort_range (v:V.t) (l:int) (h:int) =
    (* Sort the elements from l..h in-place *)
    if l < h then (
      (* Get pivot element: *)
      let m = l in
      let p = V.get v m in
      (* Now partition: *)
      let k1 = ref (l+1) in
      let k2 = ref h in
      while !k1 < !k2 do
	if V.compare (V.get v !k1) p <= 0 then
	  incr k1
	else
	  if  V.compare (V.get v !k2) p >= 0 then
	    decr k2 
	  else
	    V.swap v !k1 !k2;
      done;
      (* Swap the pivot element in: *)
      if V.compare (V.get v !k1) p < 0 then (
	V.swap v !k1 m;
	decr k1
      )
      else (
	decr k1;
	V.swap v !k1 m;
      );
      sort_range v l !k1;
      sort_range v !k2 h;
    )

  let randomize (v:V.t) =
    let l = V.length v in
    for k = 0 to l - 1 do
      let j = Random.int l in
      V.swap v j k
    done


  let sort v =
    randomize v;
    sort_range v 0 (V.length v - 1)
end
