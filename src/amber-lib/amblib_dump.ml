(* $Id$ *)

type dump_writer = out_channel

let t_obj =
  Netxdr.validate_xdr_type Amblib_dump_fmt_aux.xdrt_obj

let create_dump_file name =
  open_out_bin name

let write_object ch obj =
  let xdr = Amblib_dump_fmt_aux._of_obj obj in
  let s = Netxdr.pack_xdr_value_as_string xdr t_obj [] in
  let l = String.length s in
  output_string ch (Netnumber.BE.uint4_as_string (Netnumber.uint4_of_int l));
  output_string ch s

let close_dump_writer = close_out

let pos_writer = LargeFile.pos_out
let seek_writer = LargeFile.seek_out

let seek_end_writer ch =
  let eof = LargeFile.out_channel_length ch in
  seek_writer ch eof


(** {1 Reading dump files} *)

type dump_reader = in_channel

let open_dump_file name =
  open_in_bin name

let read_object ch =
  let n = input_binary_int ch in
  let s = Bytes.create n in
  really_input ch s 0 n;
  let ms_factories = Hashtbl.create 5 in
  Hashtbl.add ms_factories "mstring" Netxdr_mstring.bytes_based_mstrings;
  let xdr = 
    Netxdr.unpack_xdr_value 
      ~xv_version:`Ocamlrpcgen 
      ~mstring_factories:ms_factories
      s t_obj [] in
  Amblib_dump_fmt_aux._to_obj xdr

let close_dump_reader = close_in



let min_intL = Int64.of_int min_int
let max_intL = Int64.of_int max_int
let hyper_fails() = failwith "Integer exceeds range"

let hyper x =
  if x < min_intL || x > max_intL then hyper_fails();
  Int64.to_int x
