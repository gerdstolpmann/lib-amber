/* $Id$ -*- c -*- */

/* Dump files can contain data for the base set, or for the update set */

enum set_kind {
  BASE = 0,
  UPDATE = 1
};


typedef string longstring<>;

typedef _managed string mstring<>;

struct toc {
    int         format;         /* 4711 for now */
    bool        byte_order;     /* false = little, true = big endian */
    set_kind    set;            /* whether base or update */
    hyper       all_docs_base;  /* how many docs in the base set, including 
                                   deleted docs. The base docs have IDs from 1
                                   to all_docs_base
                                */
    hyper       eff_docs_base;  /* how many non-deleted docs in the base set */
    hyper       all_docs;       /* how many docs in total (base+update). The
                                   update docs have IDs from all_docs_base+1 to
                                   all_docs
                                */
    hyper       eff_docs;       /* how many non-deleted docs in total */
    longstring  att_names<>;    /* names of attributes */
    longstring  ft_names<>;     /* names of features */
    hyper       ft_all_docs<>;  /* how many docs for this feature, including 
                                   deleted docs, for base+update */
    hyper       ft_eff_docs<>;  /* how many non-deleted docs, for base+update */
};

struct attribute {
    longstring att_name;
    longstring att_type;  /* U8, S32 etc. */
    hyper      att_size;
    hyper      att_offset;
    set_kind   att_set;
    /* If BASE, index 0 of the bigarray contains the value for dociD 0, and
       the bigarray has a size of all_docs_base+1. If UPDATE, index 0 
       contains the value for docID all_docs_base+1, and the bigarray has
       a size of all_docs-all_docs_base.

       Dump file for base can only contain attributes with att_set=BASE.

       Update dump files may contain both types of attributes, BASE and UPDATE.
     */
    mstring att_value; 
    /* the image of the bigarray in host byte order */
};

struct vector {
    longstring vec_key;
    int        vec_slices;   /* how many slices */
    hyper      vec_docs;     /* how many documents, excluding deleted docs, 
                                and including the update set */
};


/* This also assigns ftid as short identifier for the feature name */
struct feature {
    longstring ft_name;
    int        ft_ftid;
    hyper      ft_datalen;  /* the total sum of all following sl_data and
                               ssl_data strings, where each string is
                               preceded by a length field encoded as vint
                            */
    vector     ft_vectors<>;  /* Sorted by vec_key */
};

struct vectorslice {
    int        sl_ftid;       /* The feature identifier */
    int        sl_index;      /* The index of the vector in ft_vectors */
    int        sl_ord;        /* Ordinal number of the slice */
    hyper      sl_start;      /* how many docs are in previous slices
                                 (including deleted docs) */
    int        sl_count;      /* how many docs are in this slice
                                 (including deleted docs) */
    hyper      sl_min_docid;  /* minimum doc ID */
    hyper      sl_max_docid;  /* maximum doc ID */
    float      sl_rest_max_docw;  /* the max weight in this slice and all
                                     following slices */
    longstring sl_data;       /* the encoded data */
};


struct shortvector {
    int        ssl_ftid;       /* The feature identifier */
    int        ssl_index;      /* The index of the vector in ft_vectors */
    longstring ssl_data;
};


enum object_kind {
    TOC = 0,
    ATTRIBUTE = 1,
    FEATURE = 2,
    VECTORSLICE = 3,
    SHORTVECTOR = 4,
    ENDMARK = 5
};

union obj switch (object_kind kind) {
case TOC:
    toc toc;
case ATTRIBUTE:
    attribute att;
case FEATURE:
    feature ft;
case VECTORSLICE:
    vectorslice vec;
case SHORTVECTOR:
    shortvector vec;
case ENDMARK:
    void;
};

typedef unsigned int encap_obj_size;

struct encap_obj {
    encap_obj_size obj_size;    /* in bytes */
    obj            obj;
};
