(* $Id$ *)

(* Cheap implementation. We should optimize this.

   We use here the functor-style [Hashtbl] module to do it. As the table
   may live in the ancient heap, the polymorphic hash primitive 
   [Hashtbl.hash] does not work, however. The workaround is to store the
   hash values with the keys (so the hash primitive is called before the
   table is moved to the ancient heap).
 *)

module E = struct
  type t = string * int

  let (equal : t -> t -> bool) (s1,n1) (s2,n2) =
    (* The type annotation for [equal] is essential because this avoids
       that the generic equality is used. The generic equality, however,
       does not work for values of the ancient heap.
     *)
    n1 = n2 && s1 = s2

  let hash (_,n) =
    n
end


module H = Hashtbl.Make(E)


type 'a t = 'a H.t


let create n =
  H.create n

let add tab key_s value =
  let key_h = Hashtbl.hash key_s in
  if H.mem tab (key_s,key_h) then
    failwith "Amblib_table.add: key already used";
  H.add tab (key_s,key_h) value

let find tab key_s =
   let key_h = Hashtbl.hash key_s in
   H.find tab (key_s,key_h)

let mem tab key_s =
   let key_h = Hashtbl.hash key_s in
   H.mem tab (key_s,key_h)
